class AirPlasma extends GenericProjectile;

var int SpinRate;

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	local rotator Rot;

	Super.Tick( DeltaTime );
	
	Rot = rotator(Velocity);
	Rot.Roll = (Rotation.Roll + (SpinRate * DeltaTime)) & 0xFFFF;
	
	SetRotation( Rot );
}

DefaultProperties
{
SpinRate=200000
ProjFlightTemplate=ParticleSystem'WP_LinkGun.Effects.P_WP_Linkgun_Projectile'
ProjExplosionTemplate=ParticleSystem'WP_LinkGun.Effects.P_WP_Linkgun_Impact'
MaxEffectDistance=7000.0
Speed=2000
MaxSpeed=2000
AccelRate=1000.0
Damage=10
NetCullDistanceSquared=+144000000.0
DrawScale=0.25
ExplosionSound=SoundCue'WOT.Sounds.HitLevelDT_Cue'
MyDamageType=class'AirDart'
bCollideWorld=true
CheckRadius=26.0
}
