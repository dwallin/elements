//================================================================================
// AirShieldVisual.
//================================================================================

class AirShieldVisual extends ShieldParticleMesh;

defaultproperties
{
    TextureSet(0)=Texture2d'WOT.Icons.Air_A01'

    TextureSet(1)=Texture2d'WOT.Icons.Air_A02'

    TextureSet(2)=Texture2d'WOT.Icons.Air_A03'

    TextureSet(3)=Texture2d'WOT.Icons.Air_A04'

    TextureSet(4)=Texture2d'WOT.Icons.Air_A05'

    Texture=Texture2d'WOT.Icons.Air_A01'

    LightHue=0

    LightSaturation=255

}