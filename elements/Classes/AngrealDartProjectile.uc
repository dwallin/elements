//================================================================================
// AngrealDartProjectile.
//================================================================================

class AngrealDartProjectile extends GenericProjectile;

var FakeCorona CoronaLight;
var() Texture2d CoronaTexture;
var() Texture2d ExplosionTexture;
var() int spinRate;

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	local rotator Rot;

	Super.Tick( DeltaTime );
	
	Rot = rotator(Velocity);
	Rot.Roll = (Rotation.Roll + (SpinRate * DeltaTime)) & 0xFFFF;
	
	SetRotation( Rot );

	//if( CoronaLight == None )
	//{
		//CoronaLight = Spawn( class'FakeCorona' );
		//CoronaLight.Texture = CoronaTexture;
	//}

	//CoronaLight.SetLocation( Location );

}

//------------------------------------------------------------------------------
//simulated function PostBeginPlay()
//{
//	Super.PostBeginPlay();
//	if( CoronaLight == None )
//	{
//		CoronaLight = Spawn( class'FakeCorona', Self,, Location );
//		//CoronaLight.Texture = CoronaTexture;
//		CoronaLight.SetBase( Self );
//	}
//}

//------------------------------------------------------------------------------
//simulated function Detach( Actor Other )
//{
	//if( Other == CoronaLight )
	//{
	//	CoronaLight.SetLocation( Location );
	//	CoronaLight.SetBase( Self );
	//}
//}

//------------------------------------------------------------------------------
//simulated function Destroyed()
//{
//	if( CoronaLight != None )
//	{
//		CoronaLight.Destroy();
//	}

//	Super.Destroyed();
//}

//------------------------------------------------------------------------------
simulated function Explode( vector HitLocation, vector HitNormal )
{
	local DartExplosion Expl;

	Expl = Spawn( class'DartExplosion',,, HitLocation );
	//Expl.Texture = ExplosionTexture;
	//Expl.LightHue = LightHue;

	Super.Explode( HitLocation, HitNormal );
}


defaultproperties
{
spinRate=200000
ProjFlightTemplate=ParticleSystem'WP_LinkGun.Effects.P_WP_Linkgun_Projectile'
ProjExplosionTemplate=ParticleSystem'WP_LinkGun.Effects.P_WP_Linkgun_Impact'
HitPawnSound=SoundCue'Wot.Sounds.HitPawnDT_Cue'
HitWaterSound=SoundCue'Wot.Sounds.HitWaterDT_Cue'
HitWaterSoundPitch=1.20
DamageType=class'AxFxx'
//HitWaterClass=Class'ParticleSystems.WaterSplash'
speed=2000.00
Damage=10.00
SpawnSound=SoundCue'WOT.sounds.LaunchDT_Cue'
ImpactSound=SoundCue'Wot.sounds.HitLevelDT_cue'
DrawScale=1.00
bCollideWorld=true
bCollideActors=true
AmbientSound=SoundCue'Wot.sounds.LoopDT_Cue'
}