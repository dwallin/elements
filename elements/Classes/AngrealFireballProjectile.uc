//------------------------------------------------------------------------------
// AngrealFireballProjectile.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 5 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class AngrealFireballProjectile extends GenericProjectile;

var FireballSprayer Head;
var ParticleSprayer  /*Head,*/ Cone, Smoke;

var() float SprayerDelay;
var() float RockDuration;
var() rotator RockRotationRate;

var() SoundCue BurstSound;		// Sound played when we burst into flames.  :)
var bool bBurstSoundPlayed;

var vector LastFBLocation;

//------------------------------------------------------------------------------
simulated function PreBeginPlay()
{
	local rotator Rot;

	Super.PreBeginPlay();

	RockDuration += WorldInfo.TimeSeconds;
	SprayerDelay += WorldInfo.TimeSeconds;

	//LightType = LT_None;

	Rot = Rotation;
	Rot.Roll = 0xFFFF * FRand();
	SetRotation( Rot );

}

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	local rotator VeloDir;

	VeloDir = rotator(Velocity);
	
	// Update rock.
	//if( WorldInfo.TimeSeconds > RockDuration )
	//{
		//DrawType = DT_None;
	//}
	//else
	//{
		SetRotation( Rotation + RockRotationRate * DeltaTime );
		
		//`log(Location);
		//SetLocation(Location);
		//ServerSetLocation(Location);
		
		
	//}

	// Create our particle systems once we know what direction we are heading.
	if( WorldInfo.TimeSeconds > SprayerDelay && Velocity != vect(0,0,0) )
	{
		//LightType = default.LightType;

		if( !bBurstSoundPlayed )
		{
			PlaySound( BurstSound );
			bBurstSoundPlayed = true;
		}

		//SetLocation( Location );
		//Head.ShiftParticles( Location - LastFBLocation );
		//LastFBLocation = Location;
		//if( Rotation != VeloDir )
		//{
		//	SetRotation( VeloDir );
		//}


		//if( Head == None )
		//{
		//	Head = Spawn( class'FireballSprayer',,, Location, VeloDir );
		//	//Head.FollowActor = Self;
		//	Head.Disable('Tick');

		//	LastFBLocation = Location;
		//}

		//if( Cone == None )
		//{
		//	Cone = Spawn( class'FireballFlame',,, Location, VeloDir );
		//	//Cone.FollowActor = Self;
		//	Cone.Disable('Tick');
		//}

		//if( Smoke == None )
		//{
		//	Smoke = Spawn( class'FireballSmoke',,, Location, VeloDir );
		//	//Smoke.FollowActor = Self;
		//	Smoke.Disable('Tick');
		//}
	}
	if(self != none)
		SetLocation(Location);
	// Fix positions.
	//if( Head != None )
	//{
	//	Head.SetLocation( Location );
	//	//Head.ShiftParticles( Location - LastFBLocation );
	//	LastFBLocation = Location;
	//	if( Head.Rotation != VeloDir )
	//	{
	//		Head.SetRotation( VeloDir );
	//	}
	//}

	//if( Cone != None ) 
	//{
	//	Cone.SetLocation( Location );
	//	if( Cone.Rotation != VeloDir )
	//	{
	//		Cone.SetRotation( VeloDir );
	//	}
	//}

	//if( Smoke != None )
	//{
	//	Smoke.SetLocation( Location );
	//	if( Smoke.Rotation != VeloDir )
	//	{
	//		Smoke.SetRotation( VeloDir );
	//	}
	//}

	Super.Tick( DeltaTime );
}

//------------------------------------------------------------------------------
simulated function Explode( vector HitLocation, vector HitNormal )
{
	//local FireballFire Fire;
	//local Decal BurnMark;

	//Spawn( class'FireballExplode3',,, HitLocation );
	//Spawn( class'BlastBits',,, HitLocation, rotator(HitNormal) );

	//if( HitActor != None && !HitActor.IsA('Pawn') )
	//{
	//	SpawnChunks( class'BurningChunk', HitLocation, HitNormal );

	//	if( HitActor.IsA('WorldInfo') )
	//	{
	//		//BurnMark = Spawn( class'BurnDecal',,, HitLocation );
	//		BurnMark = Spawn( class'ScorchMark',,, HitLocation );
	//		BurnMark.SetDrawScale (2.0);
	//		BurnMark.Align( HitNormal );
	//	}
	//	else
	//	{
	//		Fire = Spawn( class'FireballFire',,, HitLocation, rotator(vect(0,0,1)) );
	//		Fire.SetFollowActor( HitActor );
	//	}
	//}
	
	Super.Explode( HitLocation, HitNormal );
}

//------------------------------------------------------------------------------
simulated function Destroyed()
{
	//if( Head != None )
	//{
	//	Head.LifeSpan = 5.0;
	//	//Head.bOn = False;
	//}

	//if( Cone != None )
	//{
	//	Cone.LifeSpan = 5.0;
	//	Cone.bOn = False;
	//}

	//if( Smoke != None )
	//{
	//	Smoke.LifeSpan = 5.0;
	//	Smoke.bOn = False;
	//}

	Super.Destroyed();
}

//------------------------------------------------------------------------------
simulated function HitWater()
{
	Super.HitWater();
	Destroy();
}
defaultproperties
{
    SprayerDelay=0.30

    RockDuration=0.50

    RockRotationRate=(Pitch=0,Yaw=0,Roll=65000)

    BurstSound=SoundCue'WOT.Sounds.LaunchFB_Cue'

    HitPawnSound=SoundCue'WOT.Sounds.HitPawnFB_Cue'

    HitWaterSound=SoundCue'WOT.Sounds.HitWaterFB_Cue'

    ImpactSoundPitch=0.50

    HitWaterSoundPitch=1.20

    DamageType=xxFxx

    DamageRadius=220.00

    HitWaterClass=Class'FireballFizzle'

    HitWaterOffset=(X=0.00,Y=0.00,Z=56.00)

    speed=1000.00

    Damage=40.00

    MomentumTransfer=3500.00

    SpawnSound=SoundCue'WOT.Sounds.Launch2FB_Cue'

    ImpactSound=SoundCue'WOT.Sounds.HitLevelFB_Cue'

    Mesh=SkeletalMesh'WOT.Mesh.LavaRock'

    DrawScale=0.90

    ScaleGlow=5.00

    AmbientGlow=60

    SoundRadius=160

    SoundVolume=255

    SoundPitch=96

    AmbientSound=SoundCue'WOT.Sounds.LoopFB'

    CollisionRadius=6.00

    CollisionHeight=12.00

    LightType=1

    LightEffect=13

    LightBrightness=192

    LightHue=20

    LightSaturation=32

    LightRadius=8
DecalWidth=128.0
DecalHeight=128.0
ExplosionLightClass=class'FireballExplosionLight'
AmbientSound=SoundCue'WOT.Sounds.Launch2FB_Cue'
ExplosionSound=SoundCue'WOT.Sounds.HitLevelFB_Cue'
HitPawnSound=SoundCue'WOT.Sounds.HitPawnFB_Cue'
ProjFlightTemplate=ParticleSystem'WOT.Particles.FireballMain'
ProjExplosionTemplate=ParticleSystem'WOT.Particles.P_WP_RocketLauncher_RocketExplosion'
ExplosionDecal=MaterialInstanceTimeVarying'WOT.Decals.MITV_WP_RocketLauncher_Impact_Decal01'
MaxEffectDistance=7000.0
RockRotationRate=(Pitch=0,Yaw=0,Roll=65000)
bCollideWorld=true
bCollideActors=true
CheckRadius=42.0
bWaitForEffects=true
bCheckProjectileLight=true
ProjectileLightClass=class'UTGame.UTRocketLight'
Lifespan=0.00
}