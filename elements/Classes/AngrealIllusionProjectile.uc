//------------------------------------------------------------------------------
// AngrealIllusionProjectile.uc
// $Author: Mfox $
// $Date: 1/09/00 4:04p $
// $Revision: 8 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class AngrealIllusionProjectile extends AngrealProjectile;

var byte Team;

var() float FadePercent;							// Percentage of lifespan to use to fade out.
													// Set to 0.0 to preserve bandwidth.
var() bool bAnimate;								// Whether or not to animate the mesh
//var class<AnimationTableWOT>  AnimationTableClass;	// Used for animating the mesh.

var() float ShowSelfResolution;		// How often we call MyShowSelf().
var float NextShowSelfTime;

var Leech Timer;

var Actor ReplicatedOwner;

replication
{
	if( Role==ROLE_Authority )
		Team,
		ReplicatedOwner;
}

//------------------------------------------------------------------------------
simulated function PostBeginPlay()
{
	Super.PostBeginPlay();
	FClamp( FadePercent, 0.0, 1.0 );	// Just to be safe.

	// Send SeePlayer notifications.
	MyShowSelf();

	ReplicatedOwner = Owner;
}

//------------------------------------------------------------------------------
function SetSourceAngreal( AngrealInventory Source )
{
	Super.SetSourceAngreal( Source );

	// IconInfo.
	if( SourceAngreal != None && ePawn(SourceAngreal.Owner) != None )
	{
		RemoveIcon();
		Timer = Spawn( class'IllusionTimerLeech' );
		Timer.InitializeWithProjectile( Self );
		Timer.LifeSpan = LifeSpan;
		Timer.AttachTo( ePawn(SourceAngreal.Owner) );
	}
}

//------------------------------------------------------------------------------
simulated function MyShowSelf()
{
	local Projectile Proj;
	local vector Vect2D;

	// Send SeePlayer notifications.

	foreach AllActors(class'Projectile', Proj)
	{
		//if( WotProjectile(Proj).bNotifySeePlayer )
		//{
			// Check visibility.
			Vect2D = Location - Proj.Location;
			Vect2D.Z = 0.0;
			//if
			//(	VSize(Vect2D)						<= WotProjectile(Proj).SeePlayerRadius 
			//&&	Abs( Location.Z - Proj.Location.Z )	<= WotProjectile(Proj).SeePlayerHeight
			//)
			//{
			//	WotProjectile(Proj).SeePlayer( Self );
			//}
		//}
	}

	NextShowSelfTime = WorldInfo.TimeSeconds + ShowSelfResolution;
}

//------------------------------------------------------------------------------
//simulated function bool SetAnimationTableClass( Pawn Image )
//{
//	AnimationTableClass = None;
//	if( ePawn(Image) != None )
//	{	
//		AnimationTableClass = WOTPlayer(Image).AnimationTableClass;
//	}
//	//else if( WOTPawn(Image) != None )
//	//{
//	//	AnimationTableClass = WOTPawn(Image).AnimationTableClass;
//	//}
//	else
//	{
//		`warn( "Invalid illusion source!" );
//	}

//	return (AnimationTableClass != None);
//}

//------------------------------------------------------------------------------
simulated function AcquireImage( Pawn Image )
{
	local int i;
	local AppearEffect AE;

	// Add others as needed.
	
	if( Image != None )
	{
		//Mesh			= Image.Mesh;
		//DrawType		= ePawn(Image).DrawType;
		//Style			= ePawn(Image).Style;
		SetDrawScale( Image.DrawScale );
		//Team			= ePlayerReplicationInfo(Image.PlayerReplicationInfo).Team;

		//Texture			= Image.Texture;
		//Skin			= Image.Skin;
		//Sprite			= Image.Sprite;

		//for( i = 0; i < ArrayCount(MultiSkins); i++ )
		//{
		//	MultiSkins[i] = Image.MultiSkins[i];
		//}

		SetCollisionSize( Image.GetCollisionRadius(), Image.GetCollisionHeight() );

		SetHidden( False );

		// Visual
		AE = Spawn( class'AppearEffect' );
		if( ePawn(Owner) != None )
		{
			AE.SetColors( /*ePawn(Owner).PlayerColor*/ );
		}
		else
		{
			// Defaults to Blue.
			AE.SetColors();
		}
		AE.SetAppearActor( Self );

		//if( bAnimate && SetAnimationTableClass( Image ) )
		//{
		//	GotoState( 'WaitingIdle' );
		//}
	}
}

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	// Fade out the last FadePercent of our lifespan.
	if( FadePercent > 0.0 )
	{
		if( LifeSpan <= default.LifeSpan * FadePercent )
		{
			//Style = BLEND_Translucent;
			//ScaleGlow = LifeSpan / (default.LifeSpan * FadePercent);
		}
	}

	// Send see player notifications.
	if( WorldInfo.TimeSeconds > NextShowSelfTime )
	{
		MyShowSelf();
	}
}

//------------------------------------------------------------------------------
simulated function Destroyed()
{
	RemoveIcon();
	Super.Destroyed();
}

//------------------------------------------------------------------------------
function RemoveIcon()
{
	if( Timer != None && !Timer.bDeleteMe )
	{
		Timer.UnAttach();
		Timer.Destroy();
		Timer = None;
	}
}

//------------------------------------------------------------------------------
// Don't explode.
//------------------------------------------------------------------------------
simulated function Explode( vector HitLocation, vector HitNormal );


//------------------------------------------------------------------------------
state WaitingIdle
{
Begin:
LoopAnimation:
	//AnimationTableClass.static.TweenLoopSlotAnim( Self, 'Wait' );
	//FinishAnim();
	Goto( 'LoopAnimation' );
}


defaultproperties
{
    FadePercent=0.10

    bAnimate=True

    bNetTemporary=False

    Physics=0

    RemoteRole=1

    LifeSpan=20.00

    bCollideWorld=False

    bProjTarget=True

}

