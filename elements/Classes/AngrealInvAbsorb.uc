//================================================================================
// AngrealInvAbsorb.
//================================================================================

class AngrealInvAbsorb extends ReflectorInstaller;

defaultproperties
{
Begin Object Class=CylinderComponent Name=CollisionAbsorb
	CollisionRadius=+0021.000000
	CollisionHeight=+0042.000000
	CollideActors=true
End Object
	CollisionComponent=CollisionAbsorb
	Components.Add(CollisionAbsorb)
    Duration=8.00
    ReflectorClasses=Class'elements.AbsorbReflector'
    DurationType=1
    bElementFire=True
    bElementWater=True
    bElementAir=True
    bElementEarth=True
    bElementSpirit=True
    bRare=True
    bDefensive=True
    bCombat=True
    MaxInitialCharges=2
    MaxCharges=5
    ActivateSoundName="WOT.Sounds.ActivateABS_Cue"
    MinChargeGroupInterval=7.00
    Title="Absorb"
    Description="Absorb weaves a shield around you for a short time.  If a weave from another ter'angreal strikes it, the artifact that generated that weave is snatched from the owner's grasp and placed in your inventory."
    Quote="@How did you...?@ Elayne said wonderingly. @The flows just... vanished.@"
    StatusIconFrame=Texture2d'WOT.Icons.M_Absorb'
    InventoryGroup=50
    PickupMessage="You got the Absorb ter'angreal"
    StatusIcon=Texture2d'WOT.Icons.I_Absorb'
}