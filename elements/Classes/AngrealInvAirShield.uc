//================================================================================
// AngrealInvAirShield.
//================================================================================

class AngrealInvAirShield extends ReflectorInstaller;

defaultproperties
{
    Duration=20.00

    ReflectorClasses=Class'IgnoreAirElementReflector'

    DurationType=2

    bElementAir=True

    bRare=True

    bDefensive=True

    bCombat=True

    MaxInitialCharges=3

    MaxCharges=10

    ActivateSoundName="WOT.Sounds.ActivateAS_Cue"

    Title="Air Shield"

    Description="Air Shield forms a protective barrier that prevents all air-based weaves or environmental hazards from affecting you."

    Quote="The air around him suddenly became choking soot, clogging his nostrils, shutting off breath, but he made it fresh air again, a cool mist."

    StatusIconFrame=Texture2d'WOT.Icons.M_ElShieldWhite'

    InventoryGroup=64

    PickupMessage="You got the Air Shield ter'angreal"

    //PickupViewMesh=Mesh'AngrealElementalShield'

    PickupViewScale=0.30

    StatusIcon=Texture2d'WOT.Icons.I_ElShieldWhite'

    Style=2

    //Skin=Texture'Skins.ElShieldWHITE'

    //Mesh=Mesh'AngrealElementalShield'

    DrawScale=0.30

}