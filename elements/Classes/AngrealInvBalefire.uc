class AngrealInvBalefire extends AngrealInventory;

var() float PrimeTime;
var float CastTime;
var() float Kickback;
var() SoundCue StartFireSound;
var() SoundCue EndFireSound;
var() SoundCue HitSound;
var BalefireCorona GatherEffect;
var() class<BalefireCorona> Corona;
var ePawn PawnOwner;
var() float VisualOffset;
var() float StreakLength;
var class<UTDamageType>	ComboDamageType;
var ParticleSystem ComboTemplate;
var float ComboRadius;
var int   ComboDamage;
var() SoundCue ComboExplosionSound;
var class<UTReplicatedEmitter> ComboExplosionEffect;
var bool bCasting;
var bool bComboed;
var float Scalar;
var ParticleSystem BeamTemplate;
var ParticleSystem HoleTemplate;
var() float SegmentLength;
var BalefireDecal BDecal;
var() class<BalefireDecal> BaleDecal;
var BeamEmitter Beam;
var() class<BeamEmitter> BeamEffect;
var HoleEmitter Hole;
var() class<HoleEmitter> Holes;
var bool bBeam;
var bool bUseAimingHelp;

replication
{
	if ( Role == ROLE_Authority )
		GatherEffect, Beam, BeamEffect, BDecal, bBeam;
}

reliable client function ClientSpawnCorona()
{
    ServerSpawnCorona();
}
reliable server function ServerSpawnCorona()
{
	GatherEffect = Spawn(Corona, Instigator,, GetStartLoc(), Instigator.GetViewRotation());
	GatherEffect.SetFollowPawn(Instigator);
}

simulated function StartFire(byte FireModeNum)
{
	if (bCasting)
		return;
	super.StartFire(0);
	
	if (Role == ROLE_Authority)
	{
		GatherEffect = Spawn(Corona, Instigator,, GetStartLoc(), ePawn(Owner).GetViewRotation());
		GatherEffect.SetFollowPawn(Instigator);
	}
	else
	{
		ClientSpawnCorona();       
	}
	ServerStartFire(0);
}

reliable server function ServerStartFire(byte FireModeNum)
{
	//super.StartFire(0);
	//super.ServerStartFire(0);
	CastTime = WorldInfo.TimeSeconds;
	bCasting = True;
}

simulated function StopFire(byte FireModeNum)
{
	local Pawn HitActor;
    local Vector HitLocation, HitNormal;
    local Vector Start;
    local Vector End;
    local Vector VisualStart;
	
    if( !bCasting ) return;
		
    Scalar=0.00;
	bBeam = false;
	if(Instigator.Health <= 0 && WorldInfo.TimeSeconds < CastTime + (PrimeTime / 2))
	{
		CastTime = WorldInfo.TimeSeconds - (PrimeTime / 2);
	}
	if( WorldInfo.TimeSeconds < CastTime + (PrimeTime / 2) || Instigator.IsInPain() || Instigator.bHidden)
	{
		GotoState('FinishCharging');
		return;
	}
	bCasting=false;

	if( GatherEffect != None )
	{
		GatherEffect.Destroy();
		GatherEffect = None;
	}
	
	Scalar = FMin( (WorldInfo.TimeSeconds - CastTime) / PrimeTime, 1.0 );
	super.StopFire(0);
    super.ServerStopFire(0);
			
    Start = GetStartLoc();
    End = GetEndLoc();
    VisualStart = Start + (vect(50.00,0.00,-30.00) >> Instigator.GetViewRotation());
	Beam = Spawn(BeamEffect, Instigator);
    Beam.SetBalePawn(Instigator, VisualStart, End);
    Hole = Spawn(Holes, Instigator);
    Hole.PlaceDecals(VisualStart,Normal(End - VisualStart),StreakLength);
    Hole.PlaceDecals(End,Normal(VisualStart - End),StreakLength);
    bBeam = true;
            
    Instigator.PlaySound(DeactivateSound,false,false,true,Instigator.Location);
	Instigator.AddVelocity( Kickback * Normal(vector(Instigator.GetViewRotation())) * -1.0 * Scalar,
		Instigator.GetTargetLocation(), class'BalefireJump');

    if (bBeam)
    {
        foreach DynamicActors(Class'Pawn', HitActor)
	    {
		    if ((HitActor != Instigator) 
			//&& !(HitActor.IsA('Fireball'))
			//&& !(HitActor.IsA('UTCTFFlag'))
			//&& !(HitActor.IsA('UTEmit_ShockCombo'))
			//&& !(HitActor.IsA('Fireball'))
			//&& !(HitActor.IsA('Projectile'))
			//&& !(HitActor.IsA('BalefireCorona'))
			//&& !(HitActor.IsA('BeamEmitter'))
			//&& !(HitActor.IsA('BlackHoles'))
			//&& !(HitActor.IsA('BaleHole'))
			//&& !(HitActor.IsA('AngrealInvBalefire'))
			//&& !(HitActor.IsA('AngrealInvDart'))
			//&& !(HitActor.IsA('AngrealInvShift'))
			//&& !(HitActor.IsA('AngrealInvHeal'))
			//&& !(HitActor.IsA('InterpActor'))
			//&& !(HitActor.IsA('PickupFactory'))
			//&& !(HitActor.IsA('DroppedPickup'))
			//&& !(HitActor.IsA('Pickup'))
			//&& !(HitActor.IsA('LiftCenter'))
			//&& !(HitActor.IsA('WorldInfo'))
			&& DidIndeedHit(HitActor,HitLocation,HitNormal,GetEndLoc(),GetStartLoc())) 
		    {
			    AffectActor(HitActor.Class,HitActor,HitLocation,HitNormal,Scalar);
		    }
	    }
    }
	UseCharge();
}

reliable server function ServerStopFire(byte FireModeNum)
{
	
}

static function bool VectorAproxEqual( vector FirstVector, vector SecondVector )
{
	return ( ( FirstVector.x ~= SecondVector.x ) &&
			( FirstVector.y ~= SecondVector.y ) &&
			( FirstVector.z ~= SecondVector.z ) );
}

simulated function PlaceDecals( vector Start, vector Direction, float Limit )
{
    local Actor HitActor;
	local vector HitLocation, HitNormal;
	local float TraceInterval;
	local vector End;
    local HoleEmitter D;

	TraceInterval = 100;

	if( Limit < TraceInterval )
	{
		TraceInterval = Limit;
	}

	End = Start + (TraceInterval * Direction);
	HitActor = Trace( HitLocation, HitNormal, End, Start, false );

	if( HitActor != None )
	{
           D = Spawn(class'HoleEmitter',Instigator);
           D.SetHole(self, Instigator, HitLocation, HitNormal);
           D.Align(HitNormal);
        if( D == None )
		{
		}
		else if( HitLocation == vect(0,0,0) || HitActor.bBlockActors)
		{
			D.Destroy();
		}
		else
		{
			D.Align(HitNormal);
		}

		HitLocation += 100 * Direction;		// get off the wall.
		Limit -= VSize(HitLocation - Start);
	}
	else
	{
		HitLocation = End;
		Limit -= TraceInterval;
	}

	if( Limit > 0.0 )
	{
		PlaceDecals( HitLocation, Direction, Limit );
	}    
}

function DropFrom( vector StartLocation, vector StartVelocity )
{
	if( bCasting ) Failed();
	Super.DropFrom( StartLocation, StartVelocity );
}

simulated state FinishCharging
{
	simulated function StartFire (byte FireModeNum)
	{
		GotoState('');
	}
	begin:
	Sleep(CastTime + (PrimeTime / 2) - WorldInfo.TimeSeconds);
	StopFire(0);
	GotoState('');
}

simulated function Reset()
{
	if( GatherEffect != None )
	{
		GatherEffect.Destroy();
		GatherEffect = None;
	}

	CastTime = 0.0;
	bCasting = false;
    bBeam = false;
}

//------------------------------------------------------------------------------
simulated function Failed()
{
	if( GatherEffect != None )
	{
		GatherEffect.FollowPawn = None;
		GatherEffect.Destroy();
		GatherEffect = None;
	}
}
//------------------------------------------------------------------------------
simulated function vector GetStartLoc()
{
	local vector Loc;
	Loc = Instigator.Location + vect(0.f,0.f,-30.f);
	Loc.Z += Instigator.BaseEyeHeight;
	return Loc;
}

//------------------------------------------------------------------------------
simulated function vector GetEndLoc()
{
	local vector Loc;
	Loc = GetStartLoc() + ((vect(1.f,0.f,0.f) * StreakLength) >> Instigator.GetViewRotation());
	return Loc;
}

simulated function bool DidIndeedHit( Actor HitActor, out vector HitLocation, out vector HitNormal, vector End, vector Start )
{
	local vector RelativePosition;
	local rotator Rot;
	local float CollisionHeight, CollisionRadius;

	HitNormal = Normal(End - Start);
	Rot = rotator(HitNormal);

	RelativePosition = (HitActor.Location - Start) << Rot;

	HitLocation.X = RelativePosition.X;
	HitLocation.Y = 0.0;
	HitLocation.Z = 0.0;
	HitActor.GetBoundingCylinder(CollisionRadius, CollisionHeight);
	if
	(	RelativePosition.X < -VisualOffset
	||	Abs(RelativePosition.Z) > CollisionHeight
	||	Abs(RelativePosition.Y) > CollisionRadius
	)
	{
		HitLocation = vect(0,0,0);
		HitNormal = vect(0,0,0);
		return false;
	}

	// Transform back to world coords.
	HitLocation = Start + (HitLocation >> Rot);
	return true;
}

function AffectActor(class<Actor> Victims, Actor TestVictim, vector HitLocation, vector HitNormal, float Scalar )
{
	if (TestVictim.IsA('UTVehicle'))
	{
        UTVehicle(TestVictim).OuterExplosionShakeRadius=300.00;
		UTVehicle(TestVictim).TimeTilSecondaryVehicleExplosion=0.50;
		UTVehicle(TestVictim).RBPenetrationDestroy();
		UTVehicle(TestVictim).Spawn(ComboExplosionEffect,Instigator,,HitLocation);
	}
	else if (TestVictim.IsA('UTPawn'))
	{
        UTPawn(TestVictim).PlayTeleportEffect(false, false);
		UTPawn(TestVictim).TakeDamage(ComboDamage, Instigator.Controller, HitLocation, HitNormal, class'Balefired');
		UTPawn(TestVictim).KilledBy(Instigator);
		UTPawn(TestVictim).Spawn(ComboExplosionEffect,Instigator,,HitLocation);
	}
	else if (TestVictim != None )
	{
        TestVictim.PlayTeleportEffect(false, false);
		TestVictim.TakeDamage(ComboDamage, Instigator.Controller, HitLocation, HitNormal, class'Balefired');
		TestVictim.KilledBy(Instigator);
		TestVictim.Spawn(ComboExplosionEffect,Instigator,,HitLocation);
		TestVictim.Destroy();
	}
	
	if( HitSound != None )
	{
		PlaySound( HitSound,,,true );
	}
}

simulated function Actor GetHitActor (out Vector HitLocation, out Vector HitNormal, Vector Start, Rotator Rot)
{
  local Actor HitActor;
  local Vector End;

  End = Start + (vect(1.f,0.f,0.f) * 3000 >> Rot);
  HitActor = Trace(HitLocation,HitNormal,End,Start,True);
  if ( HitActor == None )
  {
    HitActor = GetHitActor(HitLocation,HitNormal,End,Rot);
  }
  return HitActor;
}

function float GetMinRange ()
{
  return 80.0;
}

DefaultProperties
{
Begin Object Class=CylinderComponent Name=BalefireCylinder
	CollisionRadius=+0021.000000
	CollisionHeight=+0042.000000
	BlockNonZeroExtent=true
	BlockZeroExtent=true
	BlockActors=true
	CollideActors=true
End Object
	CollisionComponent=BalefireCylinder
	Components.Add(BalefireCylinder)
SegmentLength=18.00
WeaponFireTypes(0)=EWFT_Projectile
ComboDamage=100000
InstantHitDamage(0)=100000
BeamTemplate=ParticleSystem'WOT.Particles.P_Bale_Beam'
HoleTemplate=ParticleSystem'WOT.Particles.P_Bale_Beam_Impact'
ComboExplosionEffect=class'UTGame.UTEmit_ShockCombo'
ComboTemplate=ParticleSystem'WOT.Particles.P_Bale_Explo'
Kickback=1111.11
ShouldFireOnRelease(0)=1
InventoryGroup=51
DurationType=0
VisualOffset=50.0
PrimeTime=2.00
DrawScale3d=(3.0,3.0,3.0)
StreakLength=8000.00
StartFireSound=SoundCue'WOT.Sounds.ActivateBF_Cue'
ComboExplosionSound=SoundCue'WOT.Sounds.HitBF_Cue'
Corona=class'BalefireCorona'
BeamEffect=class'BeamEmitter'
Holes=class'HoleEmitter'
bNetDirty=true
bNetTemporary=false
bReplicateInstigator=true
bReplicateMovement=true
bGameRelevant=true
AimingHelpRadius[0]=0.00
AimingHelpRadius[1]=0.00
bUseAimingHelp=false
StatusIcon=Texture2d'WOT.Icons.I_Balefire'
HitSound=SoundCue'WOT.Sounds.HitBF_Cue'
BalefireableTypes(0)=Pawn
BalefireableTypes(1)=WOTPawn
BalefireableTypes(2)=Decoration
BalefireableTypes(3)=LegionProjectile
BalefireableTypes(4)=WallSlab
BalefireableTypes(5)=PortcullisMover
BalefireableTypes(6)=MashadarTrailer
NetUpdateFrequency=30
bElementFire=True
bElementSpirit=True
bRare=True
bOffensive=True
bCombat=True
MinInitialCharges=3
MaxInitialCharges=5
MaxCharges=10
Priority=10.00
ActivateSound=SoundCue'WOT.Sounds.ActivateBF_Cue'
DeactivateSound=SoundCue'WOT.Sounds.LaunchBF_Cue'
ActivateSoundName="WOT.Sounds.ActivateBF_Cue"
DeActivateSoundName="WOT.Sounds.LaunchBF_Cue"
MaxChargesInGroup=4
MinChargeGroupInterval=5.00
MissOdds=1.00
Title="Balefire"
Description="Balefire launches a stream of light which travels through everything, even through walls.  Anything the light touches is loosened from the timeline.  Most objects and victims disappear as if they never existed, although cuendillar seals"
Quote="Something leaped from his hands; he was not sure what it was. A bar of white light, solid as steel. Liquid fire."
StatusIconFrame=Texture2d'WOT.Icons.M_Balefire'
PickupMessage="You got the Balefire ter'angreal"
PickupViewScale=0.70
DrawScale=0.70
}