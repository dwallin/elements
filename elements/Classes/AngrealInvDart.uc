class AngrealInvDart extends ProjectileLauncher;

/** Holds the Actor currently being hit by the beam */
//var Actor	Victim;

/** whether link gun should auto-recharge */
//var bool	bAutoCharge;

/** recharge rate in ammo per second */
//var float RechargeRate;

//var int Damage;

//var GenericProjectile GProj;

//function SuperGiveTo (Pawn Other)
//{
  //Super.SuperGiveTo(Other);
  //if (ePawn(Other) != None )
  //{
  //  SetColor(ePawn(Other).PlayerColor);
  //} else {
  //  if ( Other.Mesh.Name == 'AesSedai' )
  //  {
  //    SetColor('Blue');
  //  } else {
  //    if ( Other.Mesh.Name == 'Forsaken' )
  //    {
  //      SetColor('Red');
  //    } else {
  //      if ( Other.Mesh.Name == 'Whitecloak' )
  //      {
  //        SetColor('Gold');
  //      } else {
  //        if ( Other.Mesh.Name == 'Hound' )
  //        {
  //          SetColor('Purple');
  //        } else {
  //          SetColor('Green');
  //        }
  //      }
  //    }
  //  }
  //}
//}

//function SetColor (name Color)
//{
//  switch (Color)
//  {
//    case 'PC_Red':
//    case 'Red':
//    ProjectileClassName = "Angreal.RedDart";
//    break;
//    case 'PC_Gold':
//    case 'Yellow':
//    case 'Gold':
//    ProjectileClassName = "Angreal.YellowDart";
//    break;
//    case 'PC_Green':
//    case 'Green':
//    ProjectileClassName = "Angreal.GreenDart";
//    break;
//    case 'PC_Purple':
//    case 'Purple':
//    ProjectileClassName = "Angreal.PurpleDart";
//    break;
//    case 'PC_Blue':
//    case 'Blue':
//    default:
//    ProjectileClassName = "Angreal.BlueDart";
//    break;
//  }
//}

defaultproperties
{
Begin Object Class=CylinderComponent Name=CollisionDart
	CollisionRadius=+0021.000000
	CollisionHeight=+0042.000000
	CollideActors=true
End Object
	CollisionComponent=CollisionDart
	Components.Add(CollisionDart)
    bAutoFire=True
	bFastRepeater=true
    ProjectileClassName="elements.BlueDart"

    bElementFire=True

    bElementAir=True

    bCommon=True

    bOffensive=True

    bCombat=True

    RoundsPerMinute=320.00

    MinInitialCharges=20

    MaxInitialCharges=70

    MaxCharges=100

    MaxChargesInGroup=20

    MinChargesInGroup=5

    MaxChargeUsedInterval=0.00

    Title="Dart"

    Description="The Dart ter�angreal focuses the One Power into a weak burst of energy.  Although a single charge may not necessarily inflict much damage, the artifact can spray multiple Darts at a victim in a very short time."

    Quote="His sudden roar made Egwene jump. Clapping a hand to his left buttock, he hobbled in a pained circle."

    StatusIconFrame=Texture2d'WOT.Icons.M_Dartwh'

    PickupMessage="You got the Dart ter'angreal"

    //PickupViewMesh=Mesh'AngrealInventoryDart'

    PickupViewScale=0.60

    StatusIcon=Texture2d'WOT.Icons.I_Dartwh'

    //Style=2

    //Skin=Texture2d'WOT.Icons.DartAesSedai'

    //Mesh=Mesh'AngrealInventoryDart'

    DrawScale=0.60
	WeaponFireTypes(0)=EWFT_Projectile

}
