//================================================================================
// AngrealInvDecay.
//================================================================================

class AngrealInvDecay extends ProjectileLauncher;

defaultproperties
{
	Begin Object Class=CylinderComponent Name=CollisionDecay
	CollisionRadius=+0021.000000
	CollisionHeight=+0042.000000
	CollideActors=true
End Object
	CollisionComponent=CollisionDecay
	Components.Add(CollisionDecay)

    ProjectileClassName="elements.AngrealDecayProjectile"

    DurationType=1

    bElementWater=True

    bElementSpirit=True

    bRare=True

    bOffensive=True

    bCombat=True

    RoundsPerMinute=30.00

    MinInitialCharges=3

    MaxInitialCharges=5

    MaxCharges=10

    Priority=3.00

    FailMessage="requires a target"

    bTargetsFriendlies=False

    MaxChargeUsedInterval=2.00

    MinChargeGroupInterval=4.00

    Title="Decay"

    Description="For a short while, Decay slowly drains away both the target's health and the charges of all held artifacts."

    Quote="They would not die right away; they might even live to make it beyond the city walls.  Long enough for the dead to be far off, not here to frighten the next Myrddraal that came."

    StatusIconFrame=Texture2d'WOT.Icons.M_Decay'

    InventoryGroup=57

    PickupMessage="You got the Decay ter'angreal"

    StatusIcon=Texture2d'WOT.Icons.I_Decay'

    DrawScale=0.75
	WeaponFireTypes(0)=EWFT_Projectile

}