//================================================================================
// AngrealInvEarthShield.
//================================================================================

class AngrealInvEarthShield extends ReflectorInstaller;



defaultproperties
{
	WeaponFireTypes(0)=EWFT_Custom
    Duration=20.00
    ReflectorClasses=Class'IgnoreEarthElementReflector'

    DurationType=2

    bElementEarth=True

    bRare=True

    bDefensive=True

    bCombat=True

    MaxInitialCharges=3

    MaxCharges=10

    ActivateSoundName="WOT.Sounds.ActivateES_Cue"

    Title="Earth Shield"

    Description="Earth Shield forms a protective barrier that prevents all earth-based weaves or environmental hazards from affecting you."

    Quote="He did not seem to feel the thrashing of the ground that had him now at one angle, now at another.  His balance never shifted, no matter how he was tossed."

    StatusIconFrame=Texture2d'WOT.Icons.M_ElShieldGreen'

    InventoryGroup=64

    PickupMessage="You got the Earth Shield ter'angreal"

    //PickupViewMesh=Mesh'AngrealElementalShield'

    PickupViewScale=0.30

    StatusIcon=Texture2d'WOT.Icons.I_ElShieldGreen'

    Style=2

    //Skin=Texture'Skins.ElShieldGREEN'

    //Mesh=Mesh'AngrealElementalShield'

    DrawScale=0.30
}