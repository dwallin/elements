//================================================================================
// AngrealInvExpWard.
//================================================================================

class AngrealInvExpWard extends ProjectileLauncher;

defaultproperties
{
    ProjectileClassName="elements.AngrealExpWardProjectile"

    bElementFire=True

    bElementEarth=True

    bCommon=True

    bTraps=True

    RoundsPerMinute=240.00

    MinInitialCharges=5

    MaxInitialCharges=10

    MaxCharges=25

    MaxChargesInGroup=5

    MinChargesInGroup=3

    MaxChargeUsedInterval=0.25

    MinChargeGroupInterval=10.00

    Title="Explosive Ward"

    Description="Activating the ter'angreal affixes a Ward upon any surface.  Walking near to the Ward causes it to unravel in an explosion of Earth.  If nothing triggers it, the weave automatically explodes after a while."

    Quote="With a roar the ground in front of him erupted in a narrow fountain of dirt and rocks higher than his head."

    StatusIconFrame=Texture2d'WOT.Icons.M_LandMine'

    InventoryGroup=58

    PickupMessage="You got the Explosive Ward ter'angreal"

    //PickupViewMesh=Mesh'AngrealLandMinePickup'

    PickupViewScale=0.50

    StatusIcon=Texture2d'WOT.Icons.I_LandMine'

    //Mesh=Mesh'AngrealLandMinePickup'

    DrawScale=0.50

}