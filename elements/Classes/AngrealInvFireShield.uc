//================================================================================
// AngrealInvFireShield.
//================================================================================

class AngrealInvFireShield extends ReflectorInstaller;

defaultproperties
{
//	Begin Object Class=CylinderComponent Name=CollisionFireShield
//	CollisionRadius=+0042.000000
//	CollisionHeight=+0024.000000
//	BlockNonZeroExtent=true
//	BlockZeroExtent=true
//	BlockActors=true
//	CollideActors=true
//End Object
//	CollisionComponent=CollisionFireShield
//	Components.Add(CollisionFireShield)
	WeaponFireTypes(0)=EWFT_Custom
    Duration=20.00
    ReflectorClasses=Class'IgnoreFireElementReflector'

    DurationType=2

    bElementFire=True

    bRare=True

    bDefensive=True

    bCombat=True

    MaxInitialCharges=3

    MaxCharges=10

    ActivateSoundName="WOT.Sounds.ActivateFS_Cue"

    MaxChargesInGroup=2

    Title="Fire Shield"

    Description="Fire Shield forms a protective barrier that prevents all fire-based weaves or environmental hazards from affecting you."

    Quote="She filled the corridor around his with fire from wall to wall, floor to ceiling fire so hot the stone itself smoked.  Rahvin screamed in the middle of the flame and staggered away from her.  A heartbeat, less, and he stood, inside the flame but surrounded by clear air.  Every scrap of saidar she could channel was going into that inferno, but he held it at bay."

    StatusIconFrame=Texture2d'WOT.Icons.M_ElShieldRed'

    InventoryGroup=64

    PickupMessage="You got the Fire Shield ter'angreal"

    //PickupViewMesh=Mesh'AngrealElementalShield'

    PickupViewScale=0.30

    StatusIcon=Texture2d'WOT.Icons.I_ElShieldRed'

    Style=2

    //Skin=Texture'Skins.ElShieldRED'

    //Mesh=Mesh'AngrealElementalShield'

    DrawScale=0.30
	bCollideActors=true

}