class AngrealInvFireball extends ProjectileLauncher;



DefaultProperties
{

ProjectileClassName="elements.AngrealFireballProjectile"
bElementFire=True
bCommon=True
bOffensive=True
bCombat=True
RoundsPerMinute=90.00
MinInitialCharges=10
MaxInitialCharges=20
MaxCharges=35
Priority=9.00
MaxChargesInGroup=6
MinChargesInGroup=3
MaxChargeUsedInterval=0.67
Title="Fireball"
Description="The Fireball ter'angreal launches a concentrated weave of fire. It explodes upon impact with anything except water, which causes it to fizzle."
Quote="A head sized fireball flashed down the street toward her. She leaped back just before it exploded against the corner where her own head had been, showering her with stone chips."
InventoryGroup=51
PickupMessage="You got the Fireball ter'angreal"
DrawScale=1.30
StatusIconFrame=Texture2d'WOT.Icons.M_Fireball'
StatusIcon=Texture2d'WOT.Icons.I_Fireball'
}
