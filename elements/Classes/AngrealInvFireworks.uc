//================================================================================
// AngrealInvFireworks.
//================================================================================

class AngrealInvFireworks extends ProjectileLauncher;

defaultproperties
{
Begin Object Class=CylinderComponent Name=CollisionFireWorks
	CollisionRadius=+0021.000000
	CollisionHeight=+0042.000000
	CollideActors=true
End Object
	CollisionComponent=CollisionFireWorks
	Components.Add(CollisionFireWorks)
    ProjectileClassName="elements.AngrealFireworksProjectile"

    bElementFire=True

    bCommon=True

    bOffensive=True

    bCombat=True

    MinInitialCharges=10

    MaxInitialCharges=20

    Title="Fireball"

    Description="None"

    Quote="None"

    StatusIconFrame=Texture2d'WOT.Icons.M_Fireball'

    InventoryGroup=51

    PickupMessage="You got the Fireworks Ter'angreal"

    //PickupViewMesh=Mesh'AngrealFireballPickup'

    StatusIcon=Texture2d'WOT.Icons.I_Fireball'

    //Mesh=Mesh'AngrealFireballPickup'

    DrawScale=1.30

}