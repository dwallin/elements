class AngrealInvHeal extends AngrealInventory;

//#exec MESH    IMPORT     MESH=AngrealHealPickup ANIVFILE=MODELS\AngrealHeal_a.3D DATAFILE=MODELS\AngrealHeal_d.3D X=0 Y=0 Z=0 MLOD=0
//#exec MESH    ORIGIN     MESH=AngrealHealPickup X=0 Y=0 Z=0 YAW=64 ROLL=-64
//#exec MESH    SEQUENCE   MESH=AngrealHealPickup SEQ=All  STARTFRAME=0  NUMFRAMES=1
//#exec TEXTURE IMPORT     NAME=AngrealHealPickupTex FILE=MODELS\AngrealHeal.PCX GROUP="Skins"
//#exec MESHMAP NEW        MESHMAP=AngrealHealPickup MESH=AngrealHealPickup
//#exec MESHMAP SCALE      MESHMAP=AngrealHealPickup X=0.03 Y=0.03 Z=0.06
//#exec MESHMAP SETTEXTURE MESHMAP=AngrealHealPickup NUM=7 TEXTURE=AngrealHealPickupTex

//#exec TEXTURE IMPORT FILE=Icons\I_Healing.pcx         GROUP=Icons MIPS=Off
//#exec TEXTURE IMPORT FILE=Icons\M_Healing.pcx         GROUP=Icons MIPS=Off

//#exec AUDIO IMPORT FILE=Sounds\Heal\ActivateHL.wav		GROUP=Heal

var() int HealingAmount;		// how much healing to you want to occur?

//=============================================================================
simulated function StartFire(byte FireModeNum)
{
	ServerStartFire(0);
}

reliable server function ServerStartFire(byte FireModeNum)
{
	local IncreaseHealthEffect Healer;

	// Don't use if we're already at a max.
	if(Pawn(Owner).Health >= 100 )
	{
		Failed();
		return;
	}
	
	//Healer = Spawn( class'IncreaseHealthEffect' );
	Healer = IncreaseHealthEffect( class'Invokable'.static.GetInstance( Self, class'IncreaseHealthEffect' ) );
	Healer.Initialize( HealingAmount );
	Healer.SetVictim( Pawn(Owner) );
	Healer.SetSourceAngreal( Self );
	if( Pawn(Owner) != None )
	{
		ePawn(Owner).ProcessEffect( Healer );
		Super.StartFire(0);
		super.ServerStartFire(0);
		UseCharge();
	}
	//else if( WOTPawn(Owner) != None )
	//{
	//	WOTPawn(Owner).ProcessEffect( Healer );
	//	Super.Cast();
	//	UseCharge();
	//}
}

//------------------------------------------------------------------------------
function float GetPriority()
{
	// Priority should be 0 unless Health is at least HealingAmount below default?
	if( Pawn(Owner) != None && Pawn(Owner).Health < (Pawn(Owner).default.Health-HealingAmount) )	
	{
		return Priority;
	}
	else																				
	{
		return 0.0;
	}
}

DefaultProperties
{
//Begin Object Name=PickupMesh
//	SkeletalMesh=SkeletalMesh'WOT.Mesh.angrealhealpickup'
//	Scale=.22
//End Object
WeaponFireTypes(0)=EWFT_Custom
bGameRelevant=true
bNetTemporary=false
bNetDirty=true

HealingAmount=10

bElementWater=True

bElementAir=True

bElementSpirit=True

bRare=True

bDefensive=True

MinInitialCharges=3

MaxInitialCharges=7

MaxCharges=10

Priority=100.00
StatusIconFrame=Texture2d'WOT.Icons.M_Healing'
StatusIcon=Texture2d'WOT.Icons.I_Healing'

ActivateSoundName="WOT.Sounds.ActivateHL_Cue"

MaxChargesInGroup=4

MinChargesInGroup=3

MinChargeGroupInterval=6.00

Title="Heal"

Description="With each activation, the Heal ter'angreal raises your health slightly."

Quote="A chill rippled through him, not the blasting cold of full Healing, but a chill that pushed weariness out as it passed."

InventoryGroup=61

PickupMessage="You got the Heal ter'angreal"
}