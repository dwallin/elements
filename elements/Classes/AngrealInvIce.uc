//================================================================================
// AngrealInvIce.
//================================================================================

class AngrealInvIce extends ProjectileLauncher;

defaultproperties
{
Begin Object Class=CylinderComponent Name=CollisionIce
	CollisionRadius=+0021.000000
	CollisionHeight=+0042.000000
	CollideActors=true
End Object
	CollisionComponent=CollisionIce
	Components.Add(CollisionIce)
    ProjectileClassName="elements.AngrealIceProjectile"

    DurationType=1

    bElementWater=True

    bUncommon=True

    bDefensive=True

    bCombat=True

    MinInitialCharges=3

    MaxInitialCharges=5

    MaxCharges=10

    FailMessage="requires a target"

    bTargetsFriendlies=False

    MinChargeGroupInterval=4.00

    Title="Freeze"

    Description="The ter'angreal wraps its target with a frozen weave of water, making movement impossible during the short time that the ice is melting."

    Quote="Men and Myrddraal stiffened where they stood. White frost grew thick on them, frost that smoked. The Myrddraal's upraised arm broke off with a loud crack. When it hit the floortiles, arm and sword shattered."

    StatusIconFrame=Texture2d'WOT.Icons.M_Freeze'

    PickupMessage="You got the Freeze ter'angreal"

    //PickupViewMesh=Mesh'Freeze'

    PickupViewScale=0.30

    StatusIcon=Texture2d'WOT.Icons.I_Freeze'

    //Mesh=Mesh'Freeze'

    DrawScale=0.30
	WeaponFireTypes(0)=EWFT_Projectile

}