//================================================================================
// AngrealInvRemoveCurse.
//================================================================================

class AngrealInvRemoveCurse extends LeechAttacher;

defaultproperties
{
    LeechClasses=Class'RemoveCurseLeech'

    bElementFire=True

    bElementWater=True

    bElementAir=True

    bElementEarth=True

    bElementSpirit=True

    bRare=True

    bDefensive=True

    bCombat=True

    MinInitialCharges=3

    MaxInitialCharges=5

    MaxCharges=10

    Priority=4.00

    MinChargeGroupInterval=3.00

    Title="Unravel"

    Description="Unravel instantly destroys any weave currently affecting you or located within a small area around you.  All effects, woven traps, or projectiles within this radius simply disappear."

    Quote="Something severed his flows; they snapped back so hard that he grunted."

    StatusIconFrame=Texture2d'WOT.Icons.M_RemoveCurse'

    InventoryGroup=59

    PickupMessage="You got the Unravel ter'angreal"

    //PickupViewMesh=Mesh'AngrealRemoveCursePickup'

    PickupViewScale=0.80

    StatusIcon=Texture2d'WOT.Icons.I_RemoveCurse'

    //Mesh=Mesh'AngrealRemoveCursePickup'

    DrawScale=0.80
	WeaponFireTypes(0)=EWFT_Custom

}