class AngrealInvShift extends AngrealInventory;

//------------------------------------------------------------------------------
// AngrealInvShift.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 6 $
//
// Description:	Teleports the caster about five feet in a random direction.  
//				Any tracking spells locked on to the caster lose their target.
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------

//#exec MESH    IMPORT     MESH=AngrealShiftPickup ANIVFILE=MODELS\AngrealShift_a.3D DATAFILE=MODELS\AngrealShift_d.3D X=0 Y=0 Z=0 MLOD=0
//#exec MESH    ORIGIN     MESH=AngrealShiftPickup X=0 Y=0 Z=0 YAW=64 ROLL=-64
//#exec MESH    SEQUENCE   MESH=AngrealShiftPickup SEQ=All  STARTFRAME=0  NUMFRAMES=1
//#exec TEXTURE IMPORT     NAME=AngrealShiftPickupTex FILE=MODELS\AngrealShift.PCX GROUP="Skins"
//#exec MESHMAP NEW        MESHMAP=AngrealShiftPickup MESH=AngrealShiftPickup
//#exec MESHMAP SCALE      MESHMAP=AngrealShiftPickup X=0.03 Y=0.03 Z=0.06
//#exec MESHMAP SETTEXTURE MESHMAP=AngrealShiftPickup NUM=2 TEXTURE=AngrealShiftPickupTex

//#exec TEXTURE IMPORT FILE=Icons\I_Shift.pcx           GROUP=Icons MIPS=Off
//#exec TEXTURE IMPORT FILE=Icons\M_Shift.pcx           GROUP=Icons MIPS=Off

//#exec AUDIO IMPORT FILE=Sounds\Shift\ActivateSF.wav			GROUP=Shift

//=============================================================================
simulated function StartFire(byte FireModeNum)
{
	ServerStartFire(0);
}

reliable server function ServerStartFire(byte FireModeNum)
{
	local ShiftEffect SEffect;
	Super.StartFire(0);
	Super.ServerStartFire(0);
	SEffect = Spawn( class'ShiftEffect' );
	SEffect.SetVictim( Pawn(Owner) );
	SEffect.SetSourceAngreal( Self );
	if( ePawn(Owner) != None )
	{
		ePawn(Owner).ProcessEffect( SEffect );
		
	}
	//else if( WOTPawn(Owner) != None )
	//{
	//	WOTPawn(Owner).ProcessEffect( SEffect );
	//}
	
	if( SEffect.LastShiftSucceeded() )
	{
		
		UseCharge();
	}
	else if (!Instigator.SetLocation(Instigator.Location + ((vect(1,0,0) * 160) >> Instigator.GetViewRotation())))
	{
		Failed();
	}
}


defaultproperties
{
//Begin Object Name=PickupMesh
//	SkeletalMesh=SkeletalMesh'WOT.Mesh.angrealshiftpickup'
//	Scale=.22
//End Object
bGameRelevant=true
WeaponFireTypes(0)=EWFT_Custom
bReplicateInstigator=true
bReplicateMovement=true
bNetDirty=true
bNetTemporary=false
ActivateSoundName="WOT.Sounds.ActivateSF_Cue"
StatusIconFrame=Texture2d'WOT.Icons.M_Shift'
StatusIcon=Texture2d'WOT.Icons.I_Shift'
bElementAir=True

    bElementSpirit=True

    bUncommon=True

    bDefensive=True

    bCombat=True

    MinInitialCharges=3

    MaxInitialCharges=5

    MaxCharges=10

    Priority=6.00
    MaxChargeUsedInterval=1.00

    MinChargeGroupInterval=3.00

    Title="Shift"

    Description="Shift instantly moves you a few paces ahead of your current location, through all obstacles, as long as the destination is clear.  Any weaves currently locked on to you lose their target."

    Quote="You did not need to know a place at all to Travel if you only intended to go a very short distance"

    InventoryGroup=54

    PickupMessage="You got the Shift ter'angreal"

    PickupViewScale=0.80

    DrawScale=0.80

}
