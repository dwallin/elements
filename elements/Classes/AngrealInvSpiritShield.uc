//================================================================================
// AngrealInvSpiritShield.
//================================================================================

class AngrealInvSpiritShield extends ReflectorInstaller;

defaultproperties
{
	WeaponFireTypes(0)=EWFT_Custom
    Duration=20.00

    ReflectorClasses=Class'IgnoreSpiritElementReflector'

    DurationType=2

    bElementSpirit=True

    bRare=True

    bDefensive=True

    bCombat=True

    MaxInitialCharges=3

    MaxCharges=10

    ActivateSoundName="WOT.Sounds.ActivateSS_Cue"

    Title="Spirit Shield"

    Description="Spirit Shield forms a protective barrier that prevents all spirit-based weaves or environmental hazards from affecting you."

    Quote="With all her strength Nynaeve wove a shield of Spirit and hurled it between the other woman and saidar. Tried to hurl it between; it was like chopping at a tree with a paper hatchet."

    StatusIconFrame=Texture2d'WOT.Icons.M_ElShieldGold'

    InventoryGroup=64

    PickupMessage="You got the Spirit Shield ter'angreal"

    //PickupViewMesh=Mesh'AngrealElementalShield'

    PickupViewScale=0.30

    StatusIcon=Texture2d'WOT.Icons.I_ElShieldGold'

    Style=2

    //Skin=Texture'Skins.ElShieldGOLD'

    //Mesh=Mesh'AngrealElementalShield'

    DrawScale=0.30

}