//================================================================================
// AngrealInvTaint.
//================================================================================

class AngrealInvTaint extends ProjectileLauncher;

defaultproperties
{
    ProjectileClassName="elements.AngrealTaintProjectile"

    bElementFire=True

    bElementWater=True

    bElementAir=True

    bElementEarth=True

    bElementSpirit=True

    bRare=True

    bOffensive=True

    bCombat=True

    MaxInitialCharges=2

    MaxCharges=10

    FailMessage="requires a target"

    bTargetsFriendlies=False

    MinChargeGroupInterval=8.00

    Title="Taint"

    Description="All ter'angreal held by the target are permanently tainted. If the tainted artifacts are used, they cause damage to the user; more powerful artifacts inflict more damage."

    Quote="@It is flawed,@ she replied curtly, @lacking the buffer that makes other sa'angreal safe to use. And it apparently magnifies the taint, inducing wildness of the mind.@"

    StatusIconFrame=Texture2d'WOT.Icons.M_Taint'

    InventoryGroup=63

    PickupMessage="You got the Taint ter'angreal"

    //PickupViewMesh=Mesh'AngrealTaintPickup'

    StatusIcon=Texture2d'Wot.Icons.I_Taint'

    //Mesh=Mesh'AngrealTaintPickup'

}