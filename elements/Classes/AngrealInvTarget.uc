//================================================================================
// AngrealInvTarget.
//================================================================================

class AngrealInvTarget extends ReflectorInstaller;

defaultproperties
{
    Duration=15.00

    ReflectorClasses=Class'elements.AutoTargetReflector'

    DurationType=1

    bElementAir=True

    bElementSpirit=True

    bRare=True

    bOffensive=True

    bCombat=True

    MaxInitialCharges=2

    MaxCharges=5

    ActivateSoundName="WOT.Sounds.ActivateTA_Cue"

    MinChargeGroupInterval=5.00

    Title="Find Target"

    Description="For a short time, all seeking weaves automatically target the nearest victim, regardless of line of sight."

    Quote="With the Power you had to see something to affect it, or know exactly where it was in relation to you down to a hair.  Perhaps it was different here."

    StatusIconFrame=Texture2d'WOT.Icons.M_Targeting'

    InventoryGroup=66

    PickupMessage="You got the Find Target ter'angreal"

    PickupViewScale=0.80

    StatusIcon=Texture2d'WOT.Icons.I_Targeting'

    DrawScale=0.80
	WeaponFireTypes(0)=EWFT_Custom
}