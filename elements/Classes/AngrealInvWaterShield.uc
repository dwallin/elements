//================================================================================
// AngrealInvWaterShield.
//================================================================================

class AngrealInvWaterShield extends ReflectorInstaller;

defaultproperties
{
    Duration=20.00

    ReflectorClasses=Class'IgnoreWaterElementReflector'

    DurationType=2

    bElementWater=True

    bRare=True

    bDefensive=True

    bCombat=True

    MaxInitialCharges=3

    MaxCharges=10

    ActivateSoundName="WOT.Sounds.ActivateWS_Cue"

    Title="Water Shield"

    Description="Water Shield forms a protective barrier that prevents all water-based weaves or environmental hazards from affecting you."

    Quote="The water roiled, throwing him about violently. No breath left. He tried to think of air, or the water being air. Suddenly, it was."

    StatusIconFrame=Texture2d'WOT.Icons.M_ElShieldBlue'

    InventoryGroup=64

    PickupMessage="You got the Water Shield ter'angreal"

    //PickupViewMesh=Mesh'AngrealElementalShield'

    PickupViewScale=0.30

    StatusIcon=Texture2d'WOT.Icons.I_ElShieldBlue'

    Style=2

    //Skin=Texture'Skins.ElShieldBLUE'

    //Mesh=Mesh'AngrealElementalShield'

    DrawScale=0.30
	WeaponFireTypes(0)=EWFT_Custom
}