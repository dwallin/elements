//================================================================================
// AngrealSeekerGlobe.
//================================================================================

class AngrealSeekerGlobe extends Effects;

simulated function Tick (float DeltaTime)
{
  Super.Tick(DeltaTime);
  if ( (Owner != None) && (Base == None) )
  {
    SetLocation(Owner.Location);
    SetBase(Owner);
  }
}

defaultproperties
{
    bCanTeleport=True

    RemoteRole=1

    DrawType=1

    Style=3

    Texture=Texture2d'WOT.Icons.SG_A00'

    DrawScale=0.75

}