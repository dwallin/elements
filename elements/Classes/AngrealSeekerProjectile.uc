//------------------------------------------------------------------------------
// AngrealSeekerProjectile.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 2 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class AngrealSeekerProjectile extends SeekingProjectile;

//#exec OBJ LOAD FILE=Textures\Seeker.utx PACKAGE=Angreal.Seeker

//#exec AUDIO IMPORT FILE=Sounds\Seeker\HitSK.wav			GROUP=AngrealSeekerProjectile
//#exec AUDIO IMPORT FILE=Sounds\Seeker\LaunchSK.wav		GROUP=AngrealSeekerProjectile
//#exec AUDIO IMPORT FILE=Sounds\Seeker\LoopSK.wav		GROUP=AngrealSeekerProjectile

var AngrealSeekerGlobe Globe;
var SeekerSmoke Smoke;

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	Super.Tick( DeltaTime );

	//if( Smoke == None && !bDeleteMe )
	//{
	//	Smoke = Spawn( class'SeekerSmoke' );
	//}

	//if( Smoke != None )
	//{
	//	Smoke.SetLocation( Location );
	//	Smoke.SetRotation( rotator(Velocity) );
	//}
	if (self != none)
	{
		SetLocation( Location );
		SetRotation( rotator(Velocity) );
	}
}

//------------------------------------------------------------------------------
simulated function PreBeginPlay()
{
	Super.PreBeginPlay();

	//if( Globe == None )
	//{
	//	Globe = Spawn( class'AngrealSeekerGlobe', Self,, Location );
	//	Globe.SetBase( Self );
	//}
}

//------------------------------------------------------------------------------
//simulated function Detach( Actor Other )
//{
//	if( Other == Globe )
//	{
//		Globe.SetLocation( Location );
//		Globe.SetBase( Self );
//	}
//}

//------------------------------------------------------------------------------
//simulated function Explode( vector HitLocation, vector HitNormal )
//{
// //	Spawn( class'GreenExplode',,, HitLocation );

//	//if( Smoke != None )
//	//{
//	//	Smoke.bOn = false;
//	//	Smoke.LifeSpan = 2.0;
//	//	Smoke = None;
//	//}

//	Super.Explode( HitLocation, HitNormal );
//}

//------------------------------------------------------------------------------
// Engine notification for when this seeker has been destroyed.
//------------------------------------------------------------------------------
simulated function Destroyed()
{
	//if( Globe != None )
	//{
	//	Globe.Destroy();
	//}
	//if( Smoke != None )
	//{
	//	Smoke.bOn = false;
	//	Smoke.LifeSpan = 2.0;
	//	Smoke = None;
	//}

	Super.Destroyed();
}
defaultproperties
{
    DamageRadius=240.00

    speed=150.00

    Damage=60.00

    MomentumTransfer=2000

    SpawnSound=SoundCue'WOT.Sounds.LaunchSK_Cue'

    ImpactSound=SoundCue'WOT.Sounds.HitSK_Cue'

    DrawType=1

    Style=3

    //Texture=Texture2d'WOT.Icons.SLightning01'
		bUsesDestination=true
    SoundRadius=160

    SoundVolume=255

    AmbientSound=SoundCue'WOT.Sounds.LoopSK_Cue'

    CollisionRadius=6.00

    CollisionHeight=12.00

    LightType=1

    LightBrightness=64

    LightHue=96

    LightSaturation=128
	ProjFlightTemplate=ParticleSystem'WOT.Particles.P_Bale_Ball'
	ProjectileLightClass=class'UTGame.UTShockBallLight'

}