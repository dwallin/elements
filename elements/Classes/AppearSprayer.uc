//------------------------------------------------------------------------------
// AppearSprayer.uc
// $Author: Mpoesch $
// $Date: 8/24/99 9:20p $
// $Revision: 1 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class AppearSprayer extends ParticleSprayer;

//------------------------------------------------------------------------------
simulated function PreBeginPlay()
{
	LifeSpan = 0.000000;	// Hardcoded due to struct bug where actual LifeSpan gets overwritten with data from struct.
	Super.PreBeginPlay();
}

defaultproperties
{
    Spread=5.00

    Volume=25.00

    NumTemplates=1

    Templates=(LifeSpan=2.00,Weight=1.00,MaxInitialVelocity=20.00,MinInitialVelocity=0.00,MaxDrawScale=0.50,MinDrawScale=0.25,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=1,MaxGrowRate=0.30,MinGrowRate=-0.30,FadePhase=1,MaxFadeRate=-0.50,MinFadeRate=-0.70)

    bOn=True

    VolumeScalePct=0.00

    bLOSClip=True

    bStatic=False

    VisibilityRadius=0.00

    VisibilityHeight=0.00

}