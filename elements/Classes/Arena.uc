class Arena extends giMPArena;

DefaultProperties
{
bGivePhysicsGun=false
Acronym="Arena"
HUDType=class'MainHUD'
name='Default__Arena'
TickGroup=TG_PreAsyncWork
GameSpeed=1.1
PlayerControlClass=class'elements.ePlayerController'
AccessControlClass=class'Engine.AccessControl'
BroadcastHandlerClass=class'Engine.BroadcastHandler'
DeathMessageClass=class'UTGame.UTDeathMessage'
GameMessageClass=class'GameMessage'
GameReplicationInfoClass=class'UTGame.UTGameReplicationInfo'
AutoTestManagerClass=class'Engine.AutoTestManager'
FearCostFalloff=+0.95
CurrentID=1
PlayerReplicationInfoClass=Class'elements.ePlayerReplicationInfo'
MaxSpectatorsAllowed=32
// Defaults for if your game has only one skill leaderboard
LeaderboardId=0xFFFE0000
ArbitratedLeaderboardId=0xFFFF0000
PopulationManagerClass=class'UTGame.UTPopulationManager'
StreamingPauseIcon=Material'EngineResources.M_StreamingPause'
ConsolePlayerControllerClass=class'UTGame.UTConsolePlayerController'
BotClass=class'UTBot'
VictoryMessageClass=class'UTGame.UTVictoryMessage'
// Voice is only transmitted when the player is actively pressing a key
bRequiresPushToTalk=true
bExportMenuData=true
SpawnProtectionTime=+2.0
MidgameScorePanelTag=DMPanel
SpreeStatEvents.Add(SPREE_KILLINGSPREE)
SpreeStatEvents.Add(SPREE_RAMPAGE)
SpreeStatEvents.Add(SPREE_DOMINATING)
SpreeStatEvents.Add(SPREE_UNSTOPPABLE)
SpreeStatEvents.Add(SPREE_GODLIKE)
SpreeStatEvents.Add(SPREE_MASSACRE)
OnlineStatsWriteClass=Class'UTStatsWriteDM'
// Default set of options to publish to the online service
OnlineGameSettingsClass=class'UTGame.UTGameSettingsDM'
}