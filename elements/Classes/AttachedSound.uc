//================================================================================
// AttachedSound.
//================================================================================

class AttachedSound extends Leech;

var Vector OwnerLocation;

replication
{
  if ( (Role == ROLE_Authority) && (Owner != None) /*&&  !Owner.bNetRelevant*/ )
    OwnerLocation;
}

simulated function Tick (float DeltaTime)
{
  if ( Owner != None )
  {
    OwnerLocation = Owner.Location;
  }
  SetLocation(OwnerLocation);
  Super.Tick(DeltaTime);
}

defaultproperties
{
    RemoteRole=1

    DrawType=0

    SoundRadius=64

    SoundVolume=192

}