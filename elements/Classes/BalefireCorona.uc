class BalefireCorona extends UTProjectile;

var Pawn FollowPawn;
var rotator FollowPawnViewRotation;
var vector LastLocation;
var() float FollowFactor;
var() float FadeVolumeRate;
var() vector RelativeOffset;

replication
{
	if ( Role == Role_Authority )
		FollowPawn, FollowPawnViewRotation;
}

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	LastLocation = Location;
}

simulated function SetFollowPawn(Pawn FollowPawn)
{
	self.FollowPawn = FollowPawn;
}

simulated function Tick( float DeltaTime )
{
	local vector DesiredLocation;

	// Update position relative to FollowPawn.
	if( FollowPawn == None || FollowPawn.Health <= 0 || FollowPawn.IsInPain() /*|| FollowPawn.Weapon == none*/)
	{
		Destroy();
		return;
	}
	if( Role == ROLE_Authority || (ePawn(FollowPawn) != None && ePawn(FollowPawn).Controller != None) )
	{
		FollowPawnViewRotation = FollowPawn.GetViewRotation();
	}
	DesiredLocation = FollowPawn.Location;
	DesiredLocation.z += FollowPawn.BaseEyeHeight;
	DesiredLocation += RelativeOffset >> FollowPawnViewRotation;
	SetLocation(DesiredLocation);
	LastLocation = DesiredLocation;
}	

defaultproperties
{
ProjFlightTemplate=ParticleSystem'WOT.Particles.P_Bale_Ball'
ProjectileLightClass=class'UTGame.UTShockBallLight'
AmbientSound=SoundCue'WOT.Sounds.LoopBF_Cue'
FollowFactor=1.00
FadeVolumeRate=0.50
RelativeOffset=(X=50.00,Y=0.00,Z=-30.00)
bNetTemporary=false
Physics=5
RemoteRole=1
RotationRate=(Pitch=100000,Yaw=80000,Roll=60000)
DrawScale=1.0
MaxEffectDistance=8000.0
bBlockActors=false
bCollideWorld=false
bProjTarget=false
bCollideComplex=false
bOnlyOwnerSee=false
bOnlyRelevantToOwner=false
bReplicateMovement=true
bUpdateSimulatedPosition=true
bCheckProjectileLight=true
lifespan=0.00
}