class BalefireDecal extends UTProjectile;

var bool bSpawnedHole;
var() float FrameTime;
var float FrameTimer;
var() float FadeTime;
var bool bLastFrame;

replication
{
	if ( Role == ROLE_Authority )
		bSpawnedHole, FrameTime, FrameTimer, FadeTime, bLastFrame;
}

//------------------------------------------------------------------------------
simulated function PostBeginPlay()
{
	Super.PostBeginPlay();
	FrameTimer = FrameTime;
}

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
    local BaleHole BHole;
	Super.Tick( DeltaTime );

	if( !bSpawnedHole )
	{
       bSpawnedHole = true;
       BHole = Spawn(class'BaleHole',,,Location,,,true);
       BHole.Align(Vector(Rotation));
	}

	if( !bLastFrame )
	{
		FrameTimer -= DeltaTime;
		if( FrameTimer <= 0.0 )
		{
			FrameTimer = FrameTime;		
			bLastFrame = true;
			LifeSpan = FadeTime;
		}
	}
}

DefaultProperties
{
FrameTime=0.10
FadeTime=5.00
RemoteRole=1
DrawScale=0.50
}
