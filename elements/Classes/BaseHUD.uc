//=============================================================================
// BaseHUD.uc
// $Author: Mfox $
// $Date: 1/09/00 4:04p $
// $Revision: 32 $
//=============================================================================
class BaseHUD extends UTHUDBase;	  

//#exec FONT    IMPORT FILE=Fonts\BaseHud\F_Charge.pcx			GROUP=UI
//#exec FONT    IMPORT FILE=Fonts\BaseHud\F_Key.pcx				GROUP=UI
//#exec FONT    IMPORT FILE=Fonts\BaseHud\F_KeySelected.pcx		GROUP=UI
//#exec FONT    IMPORT FILE=Fonts\BaseHud\F_Charge_S.pcx			GROUP=UI
//#exec FONT    IMPORT FILE=Fonts\BaseHud\F_Key_S.pcx				GROUP=UI
//#exec FONT    IMPORT FILE=Fonts\BaseHud\F_KeySelected_S.pcx		GROUP=UI
//#exec TEXTURE IMPORT FILE=Graphics\Warn.pcx						GROUP=UI MIPS=Off
//#exec TEXTURE IMPORT FILE=Graphics\I_Notch.pcx					GROUP=UI MIPS=Off Flags=2
//#exec TEXTURE IMPORT FILE=Graphics\D_Notch.pcx					GROUP=UI MIPS=Off Flags=2

////#if 1 //NEW
////#exec AUDIO	IMPORT FILE=Sounds\Notification\HandNotify.wav	GROUP=Interface
//#exec AUDIO		IMPORT FILE=Sounds\Notification\LeftNotify.wav	GROUP=Interface
//#exec AUDIO		IMPORT FILE=Sounds\Notification\RightNotify.wav	GROUP=Interface
//#exec TEXTURE	IMPORT FILE=Icons\Turtle.pcx					GROUP=UI		MIPS=Off Flags=2
//#exec TEXTURE	IMPORT FILE=Icons\TurtleDead.pcx				GROUP=UI		MIPS=Off Flags=2
//#endif

const BaseSizeX 				= 640.0;
const BaseSizeY 				= 480.0;
const IconWidth 	= 32;
const IconHeight 	= 64;
const NotchWidth 	= 8;
const IconSpacing	= 8;
const HealthHeight 	= 64;
const HealthOffsetX	= 24;
const HealthOffsetY = 54;
const HandMessageOffsetY	  	= 96;

// syncronized with InterfaceLevel from uiConsole
const NormalInterface         	= 0;
const MinimizedInterface     	= 1;
const UltraMinimizedInterface 	= 2;
var bool bDrawIdentifyInfo;

var ePlayerController ePlayerOwner;
var UTGameReplicationInfo UTGRI;
var UTPlayerReplicationInfo UTPRI;
var UTWeapon Weapon;
var UTWeapon LastSelectedWeapon;
var() Color GoldColor;
var() Color SilverColor;
//#if 1 //NEW
struct MessageData
{
	var() string Message;
	var() float LifeSpan;
};

struct GenericMessageData
{
	var() string Message;
	var() int X;
	var() int Y;
	var() bool bCenter;
	var() byte Intensity;
	var() Font F;
	var() float LifeSpan;
};

var int InterfaceLevel; // shadow variable from uiConsole (for improved performance)

var MessageData HandMessage;
var MessageData LeftMessages[10];
var MessageData RightMessages[10];
var MessageData CenterMessages[2];
var MessageData SubtitleMessage;
var GenericMessageData GenericMessages[16];

// In seconds.
var() float HandMessageDuration;
var() float LeftMessageDuration;
var() float RightMessageDuration;
var() float CenterMessageDuration;
var() float SubtitleMessageDuration;
var() float GenericMessageDuration;

// 0.0 to 1.0
var() float HandMessageFadePercent;
var() float LeftMessageFadePercent;
var() float RightMessageFadePercent;
var() float CenterMessageFadePercent;
var() float SubtitleMessageFadePercent;
var() float GenericMessageFadePercent;

// Text intensities.
var() byte HandMessageIntensity;
var() byte LeftMessageIntensity;
var() byte RightMessageIntensity;
var() byte CenterMessageIntensity;
var() byte SubtitleMessageIntensity;
var() byte GenericMessageIntensity;

// Audio notifications.
var() SoundCue HandMessageSound;
var() SoundCue CenterMessageSound;
var() SoundCue GenericMessageSound;

var() string LeftMessageName;
var() string RightMessageName;
var() SoundCue LeftMessageSound;
var() SoundCue RightMessageSound;

var float LastTimeSeconds;

//var() config bool bTurtleEnabled;	// Display turtle if framerate drops below threshold.
//var() float TurtleThresholdSP;		// Time bewteen two consecutive frames
//var() float TurtleThresholdMP;		// after which performance is considered too slow.
//var() float DeadTurtleThresholdSP;	// Unplayable.
//var() float DeadTurtleThresholdMP;

//var() float TurtleFadeTime;		// How long it takes the turtle to fade out.

// Icon flash support.
var float FlashTextureTime;
var float FlashTextureTimer;
var Texture2d FlashTexture;

var() bool bHideHealth;
var() bool bHideStatusIcons;
//var bool bHideKeys;
var() bool bHideMessages;

// Player ID support.
var float IdentifyFadeTime;
var string IdentifyTarget;
var() bool  bPlayerIDEnabled;
var() float PlayerIDTraceLen;
var() float PlayerIDYOffsetRatio;
//#endif

//=============================================================================

var bool  bDrawHand;
var bool  AllowMinimizedInterface;
var() bool  WarningEnabled;
var() int   ScaledSizeX;
var() int   ScaledSizeY;
var int   HandOffsetY;

var() float    TimeOut;
var float      TriggerTime;
var float      TriggerWarningTime;
var() float FactorX;
var() float FactorY;

var LegendCanvas LegendCanvas;

replication
{
    // Functions the server replicates to the client player only
	if( Role==ROLE_Authority && bNetOwner )
		SelectItem,
		UpdateTime,
		bDrawHand,
		TriggerTime;
}

//=============================================================================

simulated function PreBeginPlay()
{
	Super.PreBeginPlay();
	PlayerOwner = ePlayerController(Owner);
	PlayerIDTraceLen			= FClamp( PlayerIDTraceLen, 0.0, 65535.0 );	
	PlayerIDYOffsetRatio		= FClamp( PlayerIDYOffsetRatio, 0.0, 1.0 );
	
	if( SizeX == 0 || SizeX >= BaseSizeX ) 
	{
		ScaledSizeX = SizeX;
	} 
	else 
	{
		ScaledSizeX = BaseSizeX;
	}

	if( SizeY == 0 || SizeY >= BaseSizeY ) 
	{
		ScaledSizeY = SizeY;
	} 
	else 
	{
		ScaledSizeY = BaseSizeY;
	}
	//`log(SizeX@SizeY);
	SetScale();
}

//=============================================================================
// functions called externally
//-----------------------------------------------------------------------------
simulated function SetWarning()
{
	TriggerWarningTime = WorldInfo.TimeSeconds;
}

// Call this to make a little red light warning light appear to notify you 
// that something bad is happening with your code.  
// Pass in a valid object so we can perform a foreach search.  
// (Self will do in most cases.)
static function ClientWarn( Actor Helper )
{
	local BaseHUD IterHUD;

	foreach Helper.AllActors( class'BaseHUD', IterHUD )
	{
		IterHUD.WarningEnabled = true;	// NOTE[aleiby]: Why have a configurable variable if we're just going to override it anyway?
		IterHUD.SetWarning();
	}
}

//=============================================================================

simulated function SelectItem()
{
	bDrawHand  = true;
	TriggerTime = WorldInfo.TimeSeconds;
}

//=============================================================================

simulated function ChangeHud( int d )
{
}

function DoSetScale( out float OutFactorX, out float OutFactorY, optional bool bScaleUp )
{
	OutFactorX = 1.0;
	if( (SizeX >= 1) && (SizeX < BaseSizeX || bScaleUp) ) 
	{
		OutFactorX = SizeX / BaseSizeX;
	}

	OutFactorY = 1.0;
	if( (SizeY >= 1) && (SizeY < BaseSizeY || bScaleUp) ) 
	{
		OutFactorY = SizeY / BaseSizeY;
	}
}

//=============================================================================

function SetScale()
{
	DoSetScale( FactorX, FactorY, false );
}

//=============================================================================

simulated function DrawMinimizedHands( )
{
    local int		i; //, j, k;
	local int		x, y;
	local int		SelectedIndex;
	local string	CountStr;
	local HandSet	CurrentHandSet;
    local HandInfo	Hand;
	local class<Inventory> ItemClass;
	local Inventory Inv;
	local class<WOTInventory> InvClass;

	//C.Style = ERenderStyle.STY_NORMAL;

	CurrentHandSet = ePawn(Owner).CurrentHandSet;
	if( CurrentHandSet != None ) 
	{
		// draw the central active Angreal
		x = ( ScaledSizeX - IconWidth ) / 2;
		y = ScaledSizeY - IconHeight;

		Hand = CurrentHandSet.GetSelectedHand();
		if( Hand != None )
		{
			ItemClass = Hand.GetSelectedClassName();
			Inv = Hand.GetSelectedItem();

			// hack to select forward if current item is empty
			//if( ItemClass == None && !Hand.IsEmpty() )
			//{
			//	SelectedIndex = Hand.Selected;
			//	Hand.SelectFirst();
			//	ItemClass = Hand.GetSelectedClassName();
			//	Inv = ePawn(Owner).FindInventoryName( ItemClass );
			//	//Inv = ePawn(Owner).FindInventoryType( ItemClass, true );
			//	Hand.Selected = SelectedIndex;
			//}					   

			if( ItemClass != None )
			{
				// draw item icon
				if( Inv != None )
				{
					Canvas.SetDrawColor (255,255,255);
					Canvas.SetPos(x + (IconHeight) * i, y);
					AngrealInventory(Inv).DrawStatusIconAt( Canvas, AngrealInventory(Inv).StatusIcon, 0.5 );
				}
				else
				{
					Canvas.SetDrawColor (255,255,255);
					Canvas.SetPos(x + (IconHeight) * i, y);
					Canvas.DrawTile( WOTInventory(Inv).default.StatusIcon, 64,64, 0,0, 128, 128,,, BLEND_Opaque);
				}
				
				// draw item charge count
				if( AngrealInventory(Inv) != None ) 
				{
					if( AngrealInventory(Inv).ChargeCost == 0 ) 
					{
						CountStr = ":"; // The infinity is stored after the 9, where the : symbol lives.
					}
					else 
					{
						CountStr = string( AngrealInventory( Inv ).CurCharges );
					}
				} 
				else if( WOTInventory(Inv) != None )
				{
					CountStr = string( WOTInventory( Inv ).Count );
				}
				else
				{
					CountStr = "0";
				}

				// charges # should be centered under tile as value changes
				Canvas.DrawColor=GoldColor;
				Canvas.SetPos(x + IconHeight * i + IconHeight/2 + 2, y + 3 * IconHeight / 4 + 3);
				Canvas.DrawText( CountStr );
			}
		}

		if( InterfaceLevel == MinimizedInterface || bDrawHand ) 
		{
			y = ScaledSizeY - IconHeight / 2;

			// slide the item off the screen
			if( InterfaceLevel == UltraMinimizedInterface ) 
			{
				y += ( IconHeight / 2 ) * ( WorldInfo.TimeSeconds - TriggerTime ) / TimeOut;
			}

			// Now draw smaller hands
			for( i = 0; i < CurrentHandSet.GetArrayCount(); ++i )
			{
				if( i < CurrentHandSet.GetArrayCount() / 2 ) 
				{
					x = ( ScaledSizeX - IconWidth ) / 2 + ( i - CurrentHandSet.GetArrayCount() / 2 ) * IconWidth / 2;
				}
				else
				{
					x = ( ScaledSizeX + IconWidth ) / 2 + ( i - CurrentHandSet.GetArrayCount() / 2 ) * IconWidth / 2;
				}

				Hand = CurrentHandSet.GetHand( i );
				if( Hand != None && Hand.GetSelectedClassName() != None ) 
				{
					Inv = Hand.GetSelectedItem();
					if( WOTInventory(Inv) != None )
					{
						Canvas.SetDrawColor (255,255,255);
						Canvas.SetPos(x + (IconHeight) * i, y);
						Canvas.DrawTile( WOTInventory(Inv).default.StatusIconFrame, 64,64, 0,0, 128, 128,,, BLEND_Opaque);
					}
					else
					{
						InvClass = class<WOTInventory>( class'ePawn'.static.LoadClassFromName( Hand.GetSelectedClassName().name ) );
						Canvas.SetDrawColor (255,255,255);
						Canvas.SetPos(x + (IconHeight) * i, y);
						Canvas.DrawTile( InvClass.default.StatusIconFrame, 64,64, 0,0, 128, 128,,, BLEND_Opaque);
					}
				}
			}
		}
	}
}

//=============================================================================
simulated function DrawNormalHands( )
{
    local int		i, j, k;
	local int		x, y;
	local int		SelectedIndex;
	local int		ItemCount;
    local HandSet	CurrentHandSet;
    local HandInfo	Hand;
	local class<Inventory>	ItemName;
	local Inventory Inv;
	local class<AngrealInventory> InvClass;
	local Texture2d T;
	local string CountStr;
	local font HandNumFont;

	if (ePawn(ePlayerController(PlayerOwner).Pawn) == none) {
		return;
	}
		CurrentHandSet = ePawn(ePlayerController(PlayerOwner).Pawn).CurrentHandSet;
		if( CurrentHandSet != None ) 
		{
			x = ( Canvas.SizeX - CurrentHandSet.GetArrayCount() * IconHeight ) / 2;
			y = Canvas.SizeY - IconHeight;

			for( i = 0; i < CurrentHandSet.GetArrayCount(); ++i )
			{
				Hand = CurrentHandSet.GetHand( i );

				if( Hand != None )
				{
					ItemName = Hand.GetSelectedClassName();
					Inv = Hand.GetSelectedItem();

					// hack to select forward if current item is empty
					if( ItemName == none && !Hand.IsEmpty() )
					{
						SelectedIndex = Hand.Selected;
						Hand.SelectFirst();
						ItemName = Hand.GetSelectedClassName();
						Inv = ePawn(Owner).FindInventoryName( ItemName );
						//Inv = Hand.GetSelectedItem();
						Hand.Selected = SelectedIndex;
					}

					if( Inv != none )
					{
						//draw item icon
						if( ItemName != None)
						{   
							Canvas.SetDrawColor (255,255,255);
							Canvas.SetPos(x + (IconHeight) * i, y);
							//Canvas.DrawTile( WOTInventory(Inv).default.StatusIcon, 64,64, 0,0, 128, 128,,, BLEND_Opaque);
							AngrealInventory(Inv).DrawStatusIconAt(Canvas, AngrealInventory(Inv).StatusIcon);
						}
						else
						{
							Canvas.SetDrawColor (255,255,255);
							Canvas.SetPos((x + IconHeight * i),y);
							Canvas.DrawTile(WOTInventory(Inv).StatusIcon, IconHeight,IconHeight, 0,0, 128, 128,,false,BLEND_Opaque);
						}
						

						// draw selected highlight and hand numbers
						if( Hand == CurrentHandSet.GetSelectedHand() && Hand.GetSelectedClassName() != none )
						{
							Canvas.SetDrawColor (255,255,255);
							Canvas.SetPos((x + IconHeight * i), (y));
							Canvas.DrawTile(Texture2d'WOT.Icons.S', IconHeight, IconHeight, 0,0, 128,128,,false,BLEND_Additive);
							Canvas.Font=Font'UI_Fonts.Fonts.UI_Fonts_Positec14';
							Canvas.DrawColor=GoldColor;
							Canvas.SetPos(x + IconHeight * i + 1, y + IconHeight / 2 - 12);
							Canvas.DrawText(( i + 1 ) % 10);
						}
						else
						{
							Canvas.SetDrawColor (255,255,255);
							Canvas.SetPos((x + IconHeight * i), (y));
							Canvas.DrawTile(Texture2d'WOT.Icons.I_0', IconHeight, IconHeight, 0,0, 64,64,,false,BLEND_Additive);
							Canvas.Font=Font'UI_Fonts.Fonts.UI_Fonts_Positec14';
							Canvas.DrawColor=SilverColor;
							Canvas.SetPos(x + IconHeight * i + 1, y + IconHeight / 2 - 12);
							Canvas.DrawText(( i + 1 ) % 10);
						}
						

						// draw item charge count
						if( AngrealInventory(Inv) != None )
						{
							if( AngrealInventory(Inv).ChargeCost == 0 ) 
							{
								CountStr = ":";
							}
							else 
							{
								CountStr =  string(AngrealInventory(Inv).CurCharges);
							}
						} 
						else if( WOTInventory(Inv) != None ) 
						{
							CountStr = string( WOTInventory( Inv ).Count );
						}
						else
						{
							CountStr = "0";
						}
						// charges # should be centered under tile as value changes
						Canvas.Font = Font'WOT.Fonts.UI_Fonts_Positec8';
						Canvas.DrawColor=GoldColor;
						Canvas.SetPos(x + IconHeight * i + IconHeight/2 + 2, y + 3 * IconHeight / 4 + 3);
						Canvas.DrawText( CountStr );

						// draw tick marks
						ItemCount = 0;
						SelectedIndex = 0;
						for( j = 0; j < Hand.GetArrayCount(); j++ ) 
						{
							if( Hand.GetClassName( j ) != none ) 
							{
								if( j == Hand.Selected )
								{
									SelectedIndex = ItemCount;
								}
								ItemCount++;
							}
						}
						for( j = 0; j < ItemCount; j++ ) 
						{
							if( j == SelectedIndex )
							{
								T = Texture2d'WOT.Icons.I_Notch_0';
							}
							else
							{
								T = Texture2d'WOT.Icons.D_Notch_0';
							}
							Canvas.SetDrawColor(255,255,255);
							Canvas.SetPos(x + IconHeight * ( 2 * i + 1 ) / 2 + NotchWidth * ( 2 * j - ItemCount ) / 2, y);
							Canvas.DrawTile(T, notchwidth,notchwidth, 0,0, notchwidth, notchwidth,,, BLEND_Additive);
						}
					}
				}
			}
		}
}

//=============================================================================

simulated function DrawCurrentHand( )
{
	local int i;
	local int j;
	local int X;
	local int Y;
	local HandInfo Hand;
	local int ItemCount;
	local class<Inventory> ItemName;
	local Inventory Inv;
	local class<WOTInventory> ItemClass;
	local Texture2d FrameIcon;

	if (ePawn(PlayerOwner.Pawn) == none) {
		return;
	}
		if( ePawn(PlayerOwner.Pawn).CurrentHandSet != None ) 
		{
			Hand = ePawn(PlayerOwner.Pawn).CurrentHandSet.GetSelectedHand();
			if( Hand != None ) 
			{
				ItemCount = Hand.GetItemCount();
				
				// compute the position of the first item based on the interface style and number of items
				if( InterfaceLevel != NormalInterface && AllowMinimizedInterface )
				{
					X = ScaledSizeX / 2 - ItemCount * IconWidth / 4;
					Y = ScaledSizeY - 3 * IconHeight / 2;
				}
				else
				{
					X = ( Canvas.SizeX - ePawn(PlayerOwner.Pawn).CurrentHandSet.GetArrayCount() * IconHeight ) / 2;
					X += ( 2 * ePawn(PlayerOwner.Pawn).CurrentHandSet.Selected + 1 ) * IconHeight / 2; 
					Y = ((Canvas.SizeY - IconHeight) - IconHeight / 2);

					if( X - ItemCount * IconHeight / 4 < 0 ) 
					{
						X = 0;
					}
					else if( X + ItemCount * IconHeight / 4 > Canvas.SizeX) 
					{
						X = Canvas.SizeX - ItemCount * IconHeight / 2;
					}
					else
					{
						X -= ItemCount * IconHeight / 4;
					}
				}
				// draw all items in the hand (scaled to 50% normal size)
				j = 0;
				for( i = 0; i < Hand.GetArrayCount(); i++ ) 
				{
					ItemName = Hand.GetClassName( i );
					if( ItemName != none )
					{
						// draw the item
						Inv = Hand.GetItem( i );

						if( Inv != None )
						{
							FrameIcon = WOTInventory(Inv).StatusIconFrame;
						}
						else
						{
							Inv = PlayerOwner.Pawn.InvManager.FindInventoryType(ItemName);
							if (Inv != none)
								FrameIcon = WOTInventory(Inv).StatusIconFrame;
						}
						if (FrameIcon != none)
						{
							Canvas.SetDrawColor (255,255,255);
							Canvas.SetPos(X + j * IconHeight / 2, Y);
							Canvas.DrawTile(FrameIcon, 32, 32, 0,0,128,128,,false, BLEND_Opaque);
						}
					
						// draw the selection highlight
						if(Inv != none)
						{
							if( i == Hand.Selected) 
							{
								Canvas.SetDrawColor (255,255,255);
								Canvas.SetPos(X + j * IconHeight / 2, Y);
								Canvas.DrawTile(Texture2d'WOT.Icons.M_0', 32,32,0,0,64, 64,,false, BLEND_Additive);
							}
						}
						j++;
					}
				}
			}
		}
	}

//=============================================================================

simulated function DrawAddedItems( )
{
	local int i;
	local int X;
	local int Y;
	local HandSet CurrentHandSet;
	local HandInfo Hand;
	local Inventory Inv;

	if (ePawn(PlayerOwner.Pawn) == none) {
		return;
	}
		CurrentHandSet = ePawn(PlayerOwner.Pawn).CurrentHandSet;
		if( CurrentHandSet != None )
		{
			for( i = 0; i < CurrentHandSet.GetArrayCount(); i++ ) 
			{
				Hand = ePawn(PlayerOwner.Pawn).CurrentHandSet.GetHand( i );
				if( Hand != None && Hand.ItemAdded != none )
				{
					if( WorldInfo.TimeSeconds - Hand.ItemAddedTime > TimeOut || WorldInfo.TimeSeconds < Hand.ItemAddedTime )
					{
						Hand.ItemAdded = none;
					} 
					else
					{
						X = ( Canvas.SizeX - CurrentHandSet.GetArrayCount() * IconHeight ) / 2 - 16;
						X += ( 4 * i + 1 ) * IconHeight / 4;
						Y = ((Canvas.SizeY - IconHeight) - IconHeight);
						//Y = HandOffsetY - IconHeight / 2;

						Y += ( IconHeight / 2 ) * ( WorldInfo.TimeSeconds - Hand.ItemAddedTime ) / TimeOut;
						Inv = Hand.GetSelectedItem();
						Canvas.SetDrawColor (255,255,255);
						Canvas.SetPos(X, Y);
						Canvas.DrawTile(WOTInventory(Inv).default.StatusIconFrame,  64,64, 0,0,128,128,,false,BLEND_Masked);
					}
				}
			}
		}
	}

//=============================================================================

simulated function DrawStatusIcons( )
{
}

//=============================================================================

//simulated function DrawKeys( )
//{
//}

//=============================================================================

simulated function DrawHealth( )
{
}

//=============================================================================

simulated function DrawWarning( )
{
	if (ePawn(PlayerOwner.Pawn) != none) {
		if( WorldInfo.TimeSeconds - TriggerWarningTime < TimeOut )
		{
			Canvas.SetPos(0,0);
			Canvas.DrawTile(Texture2d'WOT.Icons.Warn', ( Canvas.SizeX - 16 ) / 2 , 0, 0, 0, ( Canvas.SizeX - 16 ) / 2 , 0);
		}
	}
}

//=============================================================================

simulated function UpdateTime()
{
	if( WorldInfo.TimeSeconds - TriggerTime > TimeOut )
	{
		bDrawHand = false;
	}
}

//=============================================================================

simulated function DrawHands()
{
	if( InterfaceLevel == NormalInterface || !AllowMinimizedInterface ) 
	{
		DrawNormalHands();
	}
	else
	{
		DrawMinimizedHands();
	}
}


//=============================================================================
simulated function DrawProgressMessage(  )
{
	local int i;
	local float YOffset, XL, YL;

	if (ePawn(PlayerOwner.Pawn) != none) {
		Canvas.DrawColor.R = 255;
		Canvas.DrawColor.G = 255;
		Canvas.DrawColor.B = 255;
		Canvas.bCenter = true;

		Canvas.Font = font'EngineFonts.SmallFont';
		Canvas.StrLen( "TEST", XL, YL );
		YOffset = 0;
		//for( i = 0; i < ArrayCount(class'ePawn'.default.ProgressMessage); i++ )
		//{
		//	Canvas.SetPos( 0, 0.25 * Canvas.ClipY + YOffset );
		//	Canvas.DrawColor = PlayerOwner.Pawn.ProgressColor[i];
		//	Canvas.DrawText( PlayerOwner.Pawn.ProgressMessage[i], false );
		//	YOffset += YL + 1;
		//}
		Canvas.bCenter = false;
		Canvas.DrawColor.R = 255;
		Canvas.DrawColor.G = 255;
		Canvas.DrawColor.B = 255;
	}
}

simulated function PostRender(  )
{
	local float DeltaTime;
	local int StartX, StartY;
	
	Super.PostRender();

	RenderDelta = WorldInfo.TimeSeconds - LastHUDRenderTime;
	LastHUDRenderTime = WorldInfo.TimeSeconds;
	UTGRI = UTGameReplicationInfo(WorldInfo.GRI);
	UTPRI = UTPlayerReplicationInfo(Owner);

	if( ePawn(PlayerOwner.Pawn) != None && ePawn(PlayerOwner.Pawn).CurrentHandSet != None )
	{
		ePawn(PlayerOwner.Pawn).CurrentHandSet.Update();
	}

	//crosshair
	StartX = 0.5 * Canvas.ClipX - 8;
	StartY = 0.5 * Canvas.ClipY - 8;
	Canvas.SetDrawColor (255,255,255);
	Canvas.SetPos(StartX,StartY);
	Canvas.DrawTile(Texture2D'WOT.Crosshair.Crosshair14_0', 16, 16, 0, 0, 16, 16,, true, BLEND_Masked);
	
		if( !bHideHealth )
		{
			DrawHealth( );
		}

		if( !bHideStatusIcons )
		{
			DrawStatusIcons( );
		}

		//if( !bHideKeys )
		//{
		//	DrawKeys( Canvas );
		//}

		// draw hands for main interface and inventory interface (through overridden functions)
		if( bDrawHand )
		{
			DrawCurrentHand( );
		}
		else if( InterfaceLevel == NormalInterface )
		{
			DrawAddedItems( );
		}
		DrawHands( );

		// Get DeltaTime.
		DeltaTime = WorldInfo.TimeSeconds - LastTimeSeconds;
		LastTimeSeconds = WorldInfo.TimeSeconds;

		//if( ePawn(Owner).ProgressTimeOut > WorldInfo.TimeSeconds )
		//{
			//DrawProgressMessage( );
		//}

		if( !bHideMessages )
		{
			DrawMessages( DeltaTime );
		}
		
		// Player ID
		if( bPlayerIDEnabled && (WorldInfo.NetMode != NM_Standalone || WorldInfo.Game.IsA( 'giCombatBase' ) ) )
		{
			UpdatePlayerID( DeltaTime );
		}

		if( FlashTextureTimer > 0.0 )
		{
			DrawFlashTexture( DeltaTime );
		}

		// draw debug warning
		if( WarningEnabled ) 
		{
			DrawWarning( );
		}
		UpdateTime();
}

//=============================================================================
//#if 1 //NEW
//------------------------------------------------------------------------------
// + Displays a message above the hands.
// + Only one message will be displayed at a time.  New messages will simply
//   overwrite the current one.
// + The message will be displayed for HandMessageDuration seconds, and fade out
//   over the last HandMessageFadePercent of its duration.
// + Plays HandMessageSound.
//------------------------------------------------------------------------------

simulated function AddHandMessage( string Message, optional float Duration )
{
	// Play sound.
	if( HandMessageSound != None && PlayerOwner.Pawn != None )
	{
		ePawn(PlayerOwner.Pawn).PlaySound( HandMessageSound );
	}

	HandMessage.Message = Message;
	if( Duration > 0.0 )
	{
		HandMessage.LifeSpan = Duration;
	}
	else
	{
		HandMessage.LifeSpan = HandMessageDuration;
	}
}

//------------------------------------------------------------------------------
// Adds a message to the left side of the screen.
// Left justified.
// Below the health/key icons.
// Only stores up to 6 messages.
// Messages last for LeftMessageDuration seconds.
// Messages fade out over LeftMessageFadePercent of its duration.
// Plays LeftMessageSound.
//------------------------------------------------------------------------------
simulated function AddLeftMessage( string Message, optional float Duration )
{
	local int i;

	// Play sound.
	if( LeftMessageSound == None && LeftMessageName != "" )
	{
		LeftMessageSound = SoundCue( DynamicLoadObject( LeftMessageName, class'SoundCue' ) );
	}
	if( LeftMessageSound != None && ePawn(PlayerOwner.Pawn) != None )
	{
		ePawn(PlayerOwner.Pawn).PlaySound( LeftMessageSound );
	}

	// Slide existing messages down...
	for( i = ArrayCount(LeftMessages) - 1; i > 0; i -= 1 )
	{
		LeftMessages[i].Message = LeftMessages[i-1].Message;
		LeftMessages[i].LifeSpan = LeftMessages[i-1].LifeSpan;
	}

	// Insert new message on top.
	LeftMessages[0].Message = Message;
	if( Duration > 0.0 )
	{
		LeftMessages[0].LifeSpan = Duration;
	}
	else
	{
		LeftMessages[0].LifeSpan = LeftMessageDuration;
	}

	//PlayerOwner.Pawn.Player.Console.Message( None, Message, 'Message' );
}

//------------------------------------------------------------------------------
// Adds a message to the right side of the screen.
// Right justified.
// Below the health/key icons.
// Only stores up to 6 messages.
// Messages last for RightMessageDuration seconds.
// Messages fade out over RightMessageFadePercent of its duration.
// Plays RightMessageSound.
//------------------------------------------------------------------------------
simulated function AddRightMessage( string Message, optional float Duration )
{
	local int i;

	// Play sound.
	if( RightMessageSound == None && RightMessageName != "" )
	{
		RightMessageSound = SoundCue( DynamicLoadObject( RightMessageName, class'SoundCue' ) );
	}
	if( RightMessageSound != None && ePawn(PlayerOwner.Pawn) != None )
	{
		ePawn(PlayerOwner.Pawn).PlaySound( RightMessageSound );
	}

	// Slide existing messages down...
	for( i = ArrayCount(RightMessages) - 1; i > 0; i -= 1 )
	{
		RightMessages[i].Message = RightMessages[i-1].Message;
		RightMessages[i].LifeSpan = RightMessages[i-1].LifeSpan;
	}

	// Insert new message on top.
	RightMessages[0].Message = Message;
	if( Duration > 0.0 )
	{
		RightMessages[0].LifeSpan = Duration;
	}
	else
	{
		RightMessages[0].LifeSpan = RightMessageDuration;
	}

	//PlayerOwner.Pawn.Player.Console.Message( None, Message, 'Message' );
}

//------------------------------------------------------------------------------
// Adds a message to the Center side of the screen.
// Center justified.
// Below the health/key icons.
// Only stores up to 6 messages.
// Messages last for CenterMessageDuration seconds.
// Messages fade out over CenterMessageFadePercent of its duration.
// Plays CenterMessageSound.
//------------------------------------------------------------------------------
simulated function AddCenterMessage( string Message, optional float Duration, optional bool bEcho )
{
	local int i;

	// Play sound.
	if( CenterMessageSound != None && PlayerOwner.Pawn != None )
	{
		ePawn(PlayerOwner.Pawn).PlaySound( CenterMessageSound );
	}

	// Slide existing messages down...
	for( i = ArrayCount(CenterMessages) - 1; i > 0; i -= 1 )
	{
		CenterMessages[i].Message = CenterMessages[i-1].Message;
		CenterMessages[i].LifeSpan = CenterMessages[i-1].LifeSpan;
	}

	// Insert new message on top.
	CenterMessages[0].Message = Message;
	if( Duration > 0.0 )
	{
		CenterMessages[0].LifeSpan = Duration;
	}
	else
	{
		CenterMessages[0].LifeSpan = CenterMessageDuration;
	}

	//if( bEcho )
	//{
	//	PlayerOwner.Pawn.Player.Console.Message( None, Message, 'Message' );
	//}
}

//------------------------------------------------------------------------------
// Adds a subtitle message to the Center of the screen.
// Center justified, near the top.
// Only stores up to 1 messages.
// Messages last for CenterMessageDuration seconds.
// Messages fade out over CenterMessageFadePercent of its duration.
//------------------------------------------------------------------------------
simulated function AddSubtitleMessage( string Message, optional float Duration, optional bool bEcho )
{
	// Insert new message on top.
	SubtitleMessage.Message = Message;
	if( Duration > 0.0 )
	{
		SubtitleMessage.LifeSpan = Duration;
	}
	else
	{
		SubtitleMessage.LifeSpan = CenterMessageDuration;
	}

	//if( bEcho )
	//{
	//	PlayerOwner.Pawn.Player.Console.Message( None, Message, 'Message' );
	//}
}

//------------------------------------------------------------------------------
// Adds a generic message to the screen.
// Center justified.
// At the specified location.
// Only stores up to 16 messages.
// Messages last for GenericMessageDuration seconds.
// Messages fade out over GenericMessageFadePercent of its duration.
// Plays GenericMessageSound.
//------------------------------------------------------------------------------
simulated function AddGenericMessage( string Message, int X, int Y, bool bCenter, byte Intensity, Font F, optional float Duration )
{
	local int i;

	// Play sound.
	if( GenericMessageSound != None && PlayerOwner.Pawn != None )
	{
		ePawn(PlayerOwner.Pawn).PlaySound( GenericMessageSound );
	}

	// Slide existing messages down...
	for( i = ArrayCount(GenericMessages) - 1; i > 0; i -= 1 )
	{
		GenericMessages[i].Message = GenericMessages[i-1].Message;
		GenericMessages[i].X = GenericMessages[i-1].X;
		GenericMessages[i].Y = GenericMessages[i-1].Y;
		GenericMessages[i].bCenter = GenericMessages[i-1].bCenter;
		GenericMessages[i].Intensity = GenericMessages[i-1].Intensity;
		GenericMessages[i].F = GenericMessages[i-1].F;
		GenericMessages[i].LifeSpan = GenericMessages[i-1].LifeSpan;
	}

	// Insert new message on top.
	GenericMessages[0].Message = Message;
	GenericMessages[0].X = X;
	GenericMessages[0].Y = Y;
	GenericMessages[0].bCenter = bCenter;
	GenericMessages[0].Intensity = Intensity;
	
	if( F != None )
	{
		GenericMessages[0].F = F;
	}
	else
	{
		GenericMessages[0].F = Font'EngineFonts.SmallFont';
	}
		
	if( Duration > 0.0 )
	{
		GenericMessages[0].LifeSpan = Duration;
	}
	else
	{
		GenericMessages[0].LifeSpan = GenericMessageDuration;
	}

	//PlayerOwner.Pawn.Player.Console.Message( None, Message, 'Message' );
}

//=============================================================================

simulated function DrawMessages( float DeltaTime )
{
	local int i, j;
	local float TextX, TextY;
	local float Text1X, Text1Y;
	local int X, Y;
	local float FadeTime;
	local float Scale;
	local float OldOrgX, OldClipX;

	// Update message lifespans.
	if( HandMessage.LifeSpan > 0.0 )
	{
		HandMessage.LifeSpan = FMax( HandMessage.LifeSpan - DeltaTime, 0.0 );
	}
	for( i = 0; i < ArrayCount(LeftMessages); i++ )
	{
		if( LeftMessages[i].LifeSpan > 0.0 )
		{
			LeftMessages[i].LifeSpan = FMax( LeftMessages[i].LifeSpan - DeltaTime, 0.0 );
		}
	}
	for( i = 0; i < ArrayCount(RightMessages); i++ )
	{
		if( RightMessages[i].LifeSpan > 0.0 )
		{
			RightMessages[i].LifeSpan = FMax( RightMessages[i].LifeSpan - DeltaTime, 0.0 );
		}
	}
	for( i = 0; i < ArrayCount(CenterMessages); i++ )
	{
		if( CenterMessages[i].LifeSpan > 0.0 )
		{
			CenterMessages[i].LifeSpan = FMax( CenterMessages[i].LifeSpan - DeltaTime, 0.0 );
		}
	}
	if( SubtitleMessage.LifeSpan > 0.0 )
	{
		SubtitleMessage.LifeSpan = FMax( SubtitleMessage.LifeSpan - DeltaTime, 0.0 );
	}
	for( i = 0; i < ArrayCount(GenericMessages); i++ )
	{
		if( GenericMessages[i].LifeSpan > 0.0 )
		{
			GenericMessages[i].LifeSpan = FMax( GenericMessages[i].LifeSpan - DeltaTime, 0.0 );
		}
	}

	//Canvas.Style = ERenderStyle.STY_Translucent;
	Canvas.Font = Font'EngineFonts.SmallFont';
	
	// Draw HandMessage
	if( HandMessage.LifeSpan > 0.0 && HandMessageIntensity > 0 )
	{
		Canvas.DrawColor.R = HandMessageIntensity;
		Canvas.DrawColor.G = HandMessageIntensity;
		Canvas.DrawColor.B = HandMessageIntensity;
		FadeTime = HandMessageDuration * HandMessageFadePercent;
		if( HandMessage.LifeSpan < FadeTime )
		{
			Scale = HandMessage.LifeSpan / FadeTime;
			Canvas.DrawColor.R = Canvas.DrawColor.R * Scale;
			Canvas.DrawColor.G = Canvas.DrawColor.G * Scale;
			Canvas.DrawColor.B = Canvas.DrawColor.B * Scale;
		}
		Canvas.SetPos((Canvas.SizeX - TextX) / 2, Canvas.SizeY - (ScaleValY(HandMessageOffsetY) + TextY));
		Canvas.TextSize( HandMessage.Message, TextX, TextY );
		Canvas.DrawText( HandMessage.Message);
		//LegendCanvas(Canvas).DrawTextAt( (Canvas.SizeX - TextX) / 2, Canvas.SizeY - (LegendCanvas(Canvas).ScaleValY(HandMessageOffsetY) + TextY), HandMessage.Message );
	}

	// Draw LeftMessages
	if( LeftMessageIntensity > 0 )
	{
		X = 0;
		Y = ScaleValY(80);
		//`log(Y);
		FadeTime = LeftMessageDuration * LeftMessageFadePercent;
		for( i = 0; i < ArrayCount(LeftMessages); i++ )
		{
			if( LeftMessages[i].LifeSpan > 0.0 )
			{
				Canvas.DrawColor.R = LeftMessageIntensity;
				Canvas.DrawColor.G = LeftMessageIntensity;
				Canvas.DrawColor.B = LeftMessageIntensity;
				if( LeftMessages[i].LifeSpan < FadeTime )
				{
					Scale = LeftMessages[i].LifeSpan / FadeTime;
					Canvas.DrawColor.R = Canvas.DrawColor.R * Scale;
    				Canvas.DrawColor.G = Canvas.DrawColor.G * Scale;
					Canvas.DrawColor.B = Canvas.DrawColor.B * Scale;
				}
				Canvas.TextSize( LeftMessages[i].Message, TextX, TextY );
				Canvas.SetPos(X, Y);
				Canvas.DrawText(LeftMessages[i].Message); 
				Y += TextY;
			}
		}
	}

	// Draw RightMessages
	if( RightMessageIntensity > 0 )
	{
		Y = ScaleValY(80);
		FadeTime = RightMessageDuration * RightMessageFadePercent;
		for( i = 0; i < ArrayCount(RightMessages); i++ )
		{
			if( RightMessages[i].LifeSpan > 0.0 )
			{
				Canvas.DrawColor.R = RightMessageIntensity;
				Canvas.DrawColor.G = RightMessageIntensity;
				Canvas.DrawColor.B = RightMessageIntensity;
				if( RightMessages[i].LifeSpan < FadeTime )
				{
					Scale = RightMessages[i].LifeSpan / FadeTime;
					Canvas.DrawColor.R = Canvas.DrawColor.R * Scale;
					Canvas.DrawColor.G = Canvas.DrawColor.G * Scale;
					Canvas.DrawColor.B = Canvas.DrawColor.B * Scale;
				}
				Canvas.TextSize( RightMessages[i].Message, TextX, TextY );
				X = Canvas.SizeX - TextX - 1;
				Canvas.SetPos(X,Y);
				Canvas.DrawText(RightMessages[i].Message,,TextX, TextY);
				//LegendCanvas(Canvas).DrawTextAt( X, Y, RightMessages[i].Message );
				Y += TextY;
			}
		}
	}

	// Draw CenterMessages
	if( CenterMessageIntensity > 0 )
	{
		Canvas.TextSize( " ", TextX, TextY );
		Y = TextY;
		FadeTime = CenterMessageDuration * CenterMessageFadePercent;
		for( i = 0; i < ArrayCount(CenterMessages); i++ )
		{
			if( CenterMessages[i].LifeSpan > 0.0 )
			{
				Canvas.DrawColor.R = CenterMessageIntensity;
				Canvas.DrawColor.G = CenterMessageIntensity;
				Canvas.DrawColor.B = CenterMessageIntensity;
				if( CenterMessages[i].LifeSpan < FadeTime )
				{
					Scale = CenterMessages[i].LifeSpan / FadeTime;
					Canvas.DrawColor.R = Canvas.DrawColor.R * Scale;
					Canvas.DrawColor.G = Canvas.DrawColor.G * Scale;
					Canvas.DrawColor.B = Canvas.DrawColor.B * Scale;
				}
				Canvas.TextSize( CenterMessages[i].Message, TextX, TextY );
				// draw centered text
				Canvas.SetPos((Canvas.SizeX - TextX) / 2,Y);
				Canvas.DrawText(CenterMessages[i].Message,,TextX, TextY);
				//LegendCanvas(Canvas).DrawTextAt( (Canvas.SizeX - TextX) / 2, Y, CenterMessages[i].Message );
				Y += TextY;
			}
		}
	}

	// Draw SubtitleMessage
	if( SubtitleMessageIntensity > 0 )
	{
		Y = ScaleValY(24);
		FadeTime = SubtitleMessageDuration * SubtitleMessageFadePercent;
   		if( SubtitleMessage.LifeSpan > 0.0 )
   		{
   			Canvas.DrawColor.R = SubtitleMessageIntensity;
   			Canvas.DrawColor.G = SubtitleMessageIntensity;
   			Canvas.DrawColor.B = SubtitleMessageIntensity;
   			if( SubtitleMessage.LifeSpan < FadeTime )
   			{
   				Scale = SubtitleMessage.LifeSpan / FadeTime;
   				Canvas.DrawColor.R = Canvas.DrawColor.R * Scale;
   				Canvas.DrawColor.G = Canvas.DrawColor.G * Scale;
   				Canvas.DrawColor.B = Canvas.DrawColor.B * Scale;
   			}

   			OldOrgX	= Canvas.OrgX;
   			OldClipX = Canvas.ClipX;

			// show in center 8/10ths of top of screen?
   			Canvas.OrgX = Canvas.SizeX/10;
   			Canvas.ClipX	= 8*Canvas.SizeX/10;

   			Canvas.SetPos( 0, 0 );
			Canvas.StrLen( SubtitleMessage.Message, TextX, TextY );
			Canvas.StrLen( "X", Text1X, Text1Y );

			// center if only 1 line
			Canvas.bCenter = ( TextY <= Text1Y );

   			Canvas.DrawText( SubtitleMessage.Message, false );

   			Canvas.OrgX = OldOrgX;
   			Canvas.ClipX	= OldClipX;
			Canvas.bCenter = false;
		}
	}

	// Draw GenericMessages
	if( GenericMessageIntensity > 0 )
	{
		FadeTime = GenericMessageDuration * GenericMessageFadePercent;
		for( i = 0; i < ArrayCount(GenericMessages); i++ )
		{
			Canvas.bCenter = GenericMessages[i].bCenter;
			
			if( GenericMessages[i].LifeSpan > 0.0 )
			{
				if( GenericMessages[i].Intensity > 0 )
				{
					Canvas.DrawColor.R = GenericMessages[i].Intensity;
					Canvas.DrawColor.G = GenericMessages[i].Intensity;
					Canvas.DrawColor.B = GenericMessages[i].Intensity;
				}
				else	
				{
					Canvas.DrawColor.R = GenericMessageIntensity;
					Canvas.DrawColor.G = GenericMessageIntensity;
					Canvas.DrawColor.B = GenericMessageIntensity;
				}
				
				if( GenericMessages[i].LifeSpan < FadeTime )
				{
					Scale = GenericMessages[i].LifeSpan / FadeTime;
					Canvas.DrawColor.R = Canvas.DrawColor.R * Scale;
					Canvas.DrawColor.G = Canvas.DrawColor.G * Scale;
					Canvas.DrawColor.B = Canvas.DrawColor.B * Scale;
				}
				
				Canvas.SetPos( ScaleValX(GenericMessages[i].X), ScaleValY(GenericMessages[i].Y) );
				Canvas.Font = GenericMessages[i].F;
				Canvas.DrawText( GenericMessages[i].Message );
			}
		}
		Canvas.bCenter = false;
	}

	Canvas.DrawColor.R = 255;
	Canvas.DrawColor.G = 255;
	Canvas.DrawColor.B = 255;

	Canvas.SetPos( 0, 0 );			
	Canvas.Font = Font'EngineFonts.SmallFont';
	//Canvas.Style = ERenderStyle.STY_Normal;
}

//=============================================================================

function float ScaleValX( float Val )
{
	return Val*FactorX;
}

//=============================================================================

function float ScaleValY( float Val )
{
	return Val*FactorY;
}

//=============================================================================

simulated function SetFlashTexture( Texture2d T, optional float Duration )
{
	if( Duration > 0.0 )
		FlashTextureTime = Duration;
	else
		FlashTextureTime = 1.0;

	FlashTexture = T;
	FlashTextureTimer = FlashTextureTime;
}

//=============================================================================

simulated function DrawFlashTexture( float DeltaTime )
{
	local byte Intensity;
	
	if( FlashTexture != None )
	{
		//Canvas.Style = ERenderStyle.STY_Translucent;
		Intensity = 255 * FMin( (FlashTextureTimer / FlashTextureTime), 1.0 );
		Canvas.DrawColor.R = Intensity;
		Canvas.DrawColor.G = Intensity;
		Canvas.DrawColor.B = Intensity;

		//Canvas.DrawIconAt( FlashTexture, ScaledSizeX - 64, 64 );
		Canvas.DrawTile(FlashTexture, ScaledSizeX - 64, 64, 0,0,ScaledSizeX - 64, 64,,,BLEND_Translucent);
		Canvas.DrawColor.R = 255;
		Canvas.DrawColor.G = 255;
		Canvas.DrawColor.B = 255;
		//Canvas.Style = ERenderStyle.STY_Normal;
	}

	FlashTextureTimer -= DeltaTime;
}

//=============================================================================

//simulated function UpdateTurtle( float DeltaTime )
//{
	//local float TurtleThreshold;
	//local float DeadTurtleThreshold;

	//// Get thresholds.
	//if( WorldInfo.Netmode == NM_Standalone )
	//{
	//	TurtleThreshold = TurtleThresholdSP;
	//	DeadTurtleThreshold = DeadTurtleThresholdSP;
	//}
	//else
	//{
	//	TurtleThreshold = TurtleThresholdMP;
	//	DeadTurtleThreshold = DeadTurtleThresholdMP;
	//}

	// Check frame difference.
	//if( DeltaTime > DeadTurtleThreshold )
	//{
	//	SetFlashTexture( Texture2d'WOT.Icons.TurtleDead', TurtleFadeTime );
	//}
	//else if( DeltaTime > TurtleThreshold )
	//{
	//	SetFlashTexture( Texture2d'WOT.Icons.Turtle', TurtleFadeTime );
	//}
//}

//=============================================================================

//exec function GetRidOfTurtle()
//{
//	bTurtleEnabled = False;
//}

////=============================================================================

//exec function BringBackTurtle()
//{
//	bTurtleEnabled = True;
//}

////=============================================================================

//exec function SetTurtleFadeTime( float Time )
//{
//	TurtleFadeTime = Time;
//}
//#endif


// Player ID functions
simulated function UpdatePlayerID( float DeltaTime )
{
	if (ePawn(PlayerOwner.Pawn) == none) {
		return;
	}
	IdentifyFadeTime = FMax(0.0, IdentifyFadeTime - DeltaTime * 85);

	if ( TraceIdentify(  ) )
	{
		Canvas.Font = Font'EngineFonts.SmallFont';
		Canvas.DrawColor = GoldColor;
		Canvas.SetPos( 0.0, Canvas.SizeY*PlayerIDYOffsetRatio );
		Canvas.DrawColor.R = 1 * (IdentifyFadeTime * 0.5 );
		Canvas.DrawColor.G = 1 * (IdentifyFadeTime * 0.5 );
		Canvas.DrawColor.B = 1 * (IdentifyFadeTime * 0.5 );
		Canvas.bCenter = true;
		Canvas.DrawText( IdentifyTarget, false );
		Canvas.bCenter = false;
		Canvas.DrawColor = GoldColor;

	}
}

simulated function bool TraceIdentify()
{
	local actor Other;
	local vector HitLocation, HitNormal, StartTrace;
	local ePawn HitPawn;
	
	if (ePawn(PlayerOwner.Pawn) == none) {
		return false;
	}

	StartTrace    = ePawn(PlayerOwner.Pawn).Location;
	StartTrace.Z += ePawn(PlayerOwner.Pawn).BaseEyeHeight;

	Other = class'LegendActorComponent'.static.TraceRecursive( Owner, HitLocation, HitNormal, StartTrace, true,, Vector(ePawn(PlayerOwner.Pawn).GetViewRotation()), PlayerIDTraceLen );

	if( Other.IsA( 'AngrealIllusionProjectile' ) )
	{
		HitPawn = ePawn( AngrealIllusionProjectile(Other).ReplicatedOwner );
	}
	else
	{
		HitPawn = ePawn( Other );
	}

	if ( HitPawn != None && HitPawn.IsPlayerOwned() && !HitPawn.bHidden )
	{
		if( ePlayerReplicationInfo(HitPawn.PlayerReplicationInfo).DisguiseName != "" )
		{
			IdentifyTarget = ePlayerReplicationInfo(HitPawn.PlayerReplicationInfo).DisguiseName;
		}
		else
		{
			IdentifyTarget = HitPawn.PlayerReplicationInfo.PlayerName;
		}

		IdentifyFadeTime = 350.0;
	}

	return ( (IdentifyFadeTime != 0.0) && (IdentifyTarget != "") );
}
defaultproperties
{
	bHideHealth=false
	bHideStatusIcons=false
	bHideMessages=false
	WarningEnabled=true
	FactorX=1.00

    Factory=1.00
    HandMessageDuration=1.50

    LeftMessageDuration=10.00

    RightMessageDuration=10.00

    CenterMessageDuration=5.00

    SubtitleMessageDuration=5.00

    GenericMessageDuration=5.00

    HandMessageFadePercent=0.50

    LeftMessageFadePercent=0.20

    RightMessageFadePercent=0.20

    CenterMessageFadePercent=0.25

    SubtitleMessageFadePercent=0.25

    GenericMessageFadePercent=0.25

    HandMessageIntensity=255

    LeftMessageIntensity=255

    RightMessageIntensity=255

    CenterMessageIntensity=255

    SubtitleMessageIntensity=255

    GenericMessageIntensity=255

    LeftMessageName="WOT.Interface.LeftNotify"

    RightMessageName="WOT.Interface.RightNotify"

    bPlayerIDEnabled=True

    PlayerIDTraceLen=32767.00

    PlayerIDYOffsetRatio=0.67

    ScaledSizeX=640.0
    ScaledSizeY=480.0

    TimeOut=1.50

	GoldColor=(R=255,G=183,B=11,A=255)
	SilverColor=(R=125,G=125,B=125,A=255)
}