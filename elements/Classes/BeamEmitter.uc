class BeamEmitter extends UTReplicatedEmitter;

var class<UDKExplosionLight> ExplosionLightClass;
var() float StreakLength;
var Pawn BalePawn;
var float InitialLifeSpan;
var DynamicLightEnvironmentComponent BeamLightEnvironment;
var Vector Start;
var Vector End;

replication
{
	if ( Role == ROLE_Authority )
		StreakLength, BeamLightEnvironment, BalePawn, ExplosionLightClass, InitialLifeSpan, Start, End;
}

simulated function PreBeginPlay ()
{
  Super.PreBeginPlay();
  InitialLifeSpan = LifeSpan;
}

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
}

function SetBalePawn(Pawn BalePawns, Vector Starts, Vector Ends)
{
	self.BalePawn = BalePawns;
    self.BalePawn.Weapon = BalePawns.Weapon;
    self.Start = Starts;
    self.End = Ends;
}

simulated function Tick (float DeltaTime)
{
    if( Role == ROLE_Authority || (BalePawn != None && BalePawn.Controller != None) )
	{
          SetDrawScale((LifeSpan / InitialLifeSpan));
	}
	if( BalePawn == None || BalePawn.Health <= 0 || BalePawn.IsInPain() || BalePawn.Weapon == none || !(AngrealInvBalefire(AngrealInventory(self.BalePawn.Weapon)).bCasting))
	{
        SetLocation(Start);
        SetVectorParameter('ShockBeamEnd', End);
	}
    else
    {
        return;
    }
}

defaultproperties
{
Begin Object Class=DynamicLightEnvironmentComponent Name=BeamLightEnvironmentComp
 	bCastShadows=True
	AmbientGlow=(R=1.0,G=0.0,B=1.0,A=0.5)
	LightDistance=5.0
	bIsCharacterLightEnvironment=true
	bEnabled=true
End Object
Components.Add(BeamLightEnvironmentComp)
BeamLightEnvironment=BeamLightEnvironmentComp
bCanBeDamaged=true
bTicked=true
bUpdateSimulatedPosition=true
bCollideComplex=false
bDestroyInPainVolume=true
bDestroyOnSystemFinish=true
bForceNetUpdate=true
bLockLocation=true
bPostRenderIfNotVisible=true
bPostUpdateTickGroup=true
StreakLength=8000.00
EmitterTemplate=ParticleSystem'WOT.Particles.P_Bale_Beam'
ExplosionLightClass=class'BaleImpactLight'
LifeSpan=1.0
DrawScale=0.50
TickGroup=TG_PreAsyncWork
bNetDirty=true
bGameRelevant=true
bAlwaysRelevant=true
bAlwaysTick=true
bReplicateInstigator=true
bReplicateRigidBodyLocation=true
}