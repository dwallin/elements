//================================================================================
// BlackSmoke01.
//================================================================================

class BlackSmoke01 extends ParticleSprayer;

simulated function PreBeginPlay ()
{
  LifeSpan = 0.0;
  Super.PreBeginPlay();
}

defaultproperties
{
    Spread=100.00

    Volume=12.00

    Gravity=(X=0.00,Y=0.00,Z=40.00)

    NumTemplates=3

    Templates(0)=(LifeSpan=5.00,Weight=5.00,MaxInitialVelocity=30.00,MinInitialVelocity=10.00,MaxDrawScale=1.00,MinDrawScale=0.50,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=1,MaxGrowRate=-0.20,MinGrowRate=-0.30,FadePhase=0,MaxFadeRate=0.00,MinFadeRate=0.00)

    Templates(1)=(LifeSpan=5.00,Weight=3.00,MaxInitialVelocity=50.00,MinInitialVelocity=5.00,MaxDrawScale=1.00,MinDrawScale=1.00,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=1,MaxGrowRate=-0.20,MinGrowRate=-0.50,FadePhase=0,MaxFadeRate=0.00,MinFadeRate=0.00)

    Templates(2)=(LifeSpan=5.00,Weight=1.00,MaxInitialVelocity=0.00,MinInitialVelocity=0.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=2,MaxGrowRate=0.70,MinGrowRate=0.20,FadePhase=0,MaxFadeRate=0.00,MinFadeRate=0.00)

    Templates(3)=(LifeSpan=5.00,Weight=1000000.00,MaxInitialVelocity=0.00,MinInitialVelocity=0.00,MaxDrawScale=1.00,MinDrawScale=1.00,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=1,MaxGrowRate=-0.20,MinGrowRate=-0.40,FadePhase=0,MaxFadeRate=0.00,MinFadeRate=0.00)

    Particles(0)=Texture2d'WOT.Blk32_005'

    Particles(1)=Texture2d'WOT.Blk32_008'

    Particles(2)=Texture2d'WOT.Blk32_011'

    bOn=True

    bStatic=False

    bSelected=True

    Style=4

    VisibilityRadius=500.00

    VisibilityHeight=500.00

}