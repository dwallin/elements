//================================================================================
// BlastBits.
//================================================================================

class BlastBits extends ParticleSprayer;

simulated function PreBeginPlay ()
{
  LifeSpan = 2.0;
  SetTimer(0.2,False);
  Super.PreBeginPlay();
}

simulated function Timer ()
{
  bOn = False;
}

defaultproperties
{
    Spread=125.00

    Volume=120.00

    NumTemplates=2

    Templates(0)=(LifeSpan=0.50,Weight=2.00,MaxInitialVelocity=600.00,MinInitialVelocity=560.00,MaxDrawScale=0.70,MinDrawScale=0.40,MaxScaleGlow=1.00,MinScaleGlow=0.00,GrowPhase=1,MaxGrowRate=-0.40,MinGrowRate=-1.00,FadePhase=2,MaxFadeRate=0.00,MinFadeRate=0.00)

    Templates(1)=(LifeSpan=0.50,Weight=0.50,MaxInitialVelocity=550.00,MinInitialVelocity=0.00,MaxDrawScale=0.75,MinDrawScale=0.20,MaxScaleGlow=1.00,MinScaleGlow=0.00,GrowPhase=1,MaxGrowRate=-1.50,MinGrowRate=-1.25,FadePhase=2,MaxFadeRate=0.00,MinFadeRate=0.00)

    Particles(0)=Texture2d'WOT.AniBitA_0'

    Particles(1)=Texture2d'WOT.AniBitB_0'

    bOn=True

    VolumeScalePct=0.00

    bStatic=False

}