//------------------------------------------------------------------------------
// BloodDecal.uc
// $Author: Aleiby $
// $Date: 9/13/99 11:42p $
// $Revision: 2 $
//
// Description:
//------------------------------------------------------------------------------
// How to use this class:
//------------------------------------------------------------------------------
// How this class works:
//------------------------------------------------------------------------------
class BloodDecal extends Decal;

var Texture2d BloodTextures[15];

//------------------------------------------------------------------------------
simulated function PostBeginPlay()
{
	local int i;

	Super.PostBeginPlay();

	Skin = None;
	while( Skin == None && i < 100 )
	{
		Skin = BloodTextures[ Rand( ArrayCount(BloodTextures) ) ];
		i++;	// to prevent an infinite loop if there are no BloodTextures.
	}

	if( Skin == None )
	{
		`warn( "Couldn't find a valid BloodTexture to use as a Skin." );
		Destroy();
	}
}
defaultproperties
{
    BloodTextures(0)=Texture2d'WOT.Blood05'

    BloodTextures(1)=Texture2d'WOT.Blood06'

    BloodTextures(2)=Texture2d'WOT.Blood07'

    BloodTextures(3)=Texture2d'WOT.Blood08'

    BloodTextures(4)=Texture2d'WOT.Blood09'

    BloodTextures(5)=Texture2d'WOT.Blood10'

    BloodTextures(6)=Texture2d'WOT.Blood11'

    BloodTextures(7)=Texture2d'WOT.Blood12'

    BloodTextures(8)=Texture2d'WOT.Blood13'

    BloodTextures(9)=Texture2d'WOT.Blood15'

    BloodTextures(10)=Texture2d'WOT.Blood16'

    BloodTextures(11)=Texture2d'WOT.Blood17'

    BloodTextures(12)=Texture2d'WOT.Blood18'

    BloodTextures(13)=Texture2d'WOT.Blood19'

    BloodTextures(14)=Texture2d'WOT.Blood04'

    DetailLevel=2

    GoreLevel=2

    DrawScale=0.70

}