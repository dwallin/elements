//================================================================================
// BlueDart.
//================================================================================

class BlueDart extends AngrealDartProjectile;

defaultproperties
{
    CoronaTexture=Texture2d'WOT.Icons.Corona_blue'

    ExplosionTexture=Texture2d'WOT.Icons.Expl_blue'

    Skin=Texture2d'WOT.Icons.Comet_blue'
	ProjFlightTemplate=ParticleSystem'WP_LinkGun.Effects.P_WP_Linkgun_Projectile'
	ProjExplosionTemplate=ParticleSystem'WP_LinkGun.Effects.P_WP_Linkgun_Impact'
	DrawScale=0.25
}