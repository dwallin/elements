//================================================================================
// BurnDecal.
//================================================================================

class BurnDecal extends BloodDecal;

defaultproperties
{
    BloodTextures(0)=Texture2d'WOT.Icons.scorch1'

    BloodTextures(1)=Texture2d'WOT.Icons.scorch3'

    BloodTextures(2)=Texture2d'WOT.Icons.scorch4'

    BloodTextures(3)=Texture2d'WOT.Icons.scorch2'

    BloodTextures(4)=Texture2d'WOT.Icons.scorch1'

    BloodTextures(5)=Texture2d'WOT.Icons.scorch3'

    BloodTextures(6)=Texture2d'WOT.Icons.scorch4'

    BloodTextures(7)=Texture2d'WOT.Icons.scorch2'

    BloodTextures(8)=Texture2d'WOT.Icons.scorch1'

    BloodTextures(9)=Texture2d'WOT.Icons.scorch3'

    BloodTextures(10)=Texture2d'WOT.Icons.scorch4'

    BloodTextures(11)=Texture2d'WOT.Icons.scorch2'

    BloodTextures(12)=Texture2d'WOT.Icons.scorch1'

    BloodTextures(13)=Texture2d'WOT.Icons.scorch3'

    BloodTextures(14)=Texture2d'WOT.Icons.scorch4'

    GoreLevel=0

}