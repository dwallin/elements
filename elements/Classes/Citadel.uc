class Citadel extends UTCTFGame;

DefaultProperties
{
DesiredPlayerCount=16
DefaultPawnClass=class'ePawn'
bDelayedStart=false;
PlayerControllerClass=class'ePlayerController'
DefaultInventory(0)=class'AngrealInvDart'
DefaultInventory(1)=class'AngrealInvFireball'
DefaultInventory(2)=class'AngrealInvBalefire'
DefaultInventory(3)=class'AngrealInvShift'
DefaultInventory(4)=class'AngrealInvHeal'
DefaultInventory(5)=class'AngrealInvReflect'
//DefaultInventory(6)=class'IceBolt'
//DefaultInventory(7)=class'UTWeap_LinkGun'
//DefaultInventory(8)=class'UTWeap_RocketLauncher_Content'
//DefaultInventory(9)=class'UTWeap_ShockRifle'
MaxPlayersAllowed=64
}
