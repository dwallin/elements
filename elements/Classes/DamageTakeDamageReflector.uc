//================================================================================
// DamageTakeDamageReflector.
//================================================================================

class DamageTakeDamageReflector extends Reflector;

event TakeDamage(int DamageAmount, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
  local bool bDoDamage;

  bDoDamage = True;
  if (  !Owner.IsA('Pawn') )
  {
    bDoDamage = False;
  } else {
    if ( Pawn(Owner).Health <= 0 )
    {
      bDoDamage = False;
    } else {
      if ( (Owner.Tag == 'balefired') && (Pawn(Owner).Health - DamageAmount <= 0) )
      {
        bDoDamage = False;
      }
    }
  }
  if ( bDoDamage )
  {
    if ( (ePawn(Owner) != None) && (ePawn(Owner).ReducedDamageType != 'All') && (ePawn(Owner).AssetsHelper != None) )
    {
      ePawn(Owner).AssetsHelper.HandleDamage(DamageAmount,HitLocation,'DamageType');
    } else {
      //if ( (WOTPawn(Owner) != None) && (WOTPawn(Owner).AssetsHelper != None) )
      //{
      //  WOTPawn(Owner).AssetsHelper.HandleDamage(Damage,HitLocation,DamageType);
      //}
    }
  }
  Super.TakeDamage(DamageAmount,EventInstigator,HitLocation,Momentum,DamageType);
}

defaultproperties
{
    bRemovable=False

    bDisplayIcon=False

}