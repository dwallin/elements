//================================================================================
// DecayExplosion.
//================================================================================

class DecayExplosion extends Explosion;

var Vector OwnerLocation;

replication
{
  if ( (Role == ROLE_Authority) && (Owner != None)/* &&  !Owner.bNetRelevant*/ )
    OwnerLocation;
}

simulated function Tick (float DeltaTime)
{
  if ( Owner != None )
  {
    OwnerLocation = Owner.Location;
  }
  SetLocation(OwnerLocation);
  Super.Tick(DeltaTime);
}

defaultproperties
{
    ExplosionAnim(0)=Texture2d'WOT.Icons.DecayExplosion00'

    ExplosionAnim(1)=Texture2d'WOT.Icons.DecayExplosion01'

    ExplosionAnim(2)=Texture2d'WOT.Icons.DecayExplosion02'

    ExplosionAnim(3)=Texture2d'WOT.Icons.DecayExplosion03'

    ExplosionAnim(4)=Texture2d'WOT.Icons.DecayExplosion04'

    ExplosionAnim(5)=Texture2d'WOT.Icons.DecayExplosion05'

    ExplosionAnim(6)=Texture2d'WOT.Icons.DecayExplosion06'

    ExplosionAnim(7)=Texture2d'WOT.Icons.DecayExplosion07'

    ExplosionAnim(8)=Texture2d'WOT.Icons.DecayExplosion08'

    ExplosionAnim(9)=Texture2d'WOT.Icons.DecayExplosion09'

    LifeSpan=1.00

    DrawScale=0.80

    SoundPitch=32

    LightEffect=13

    LightBrightness=192

    LightHue=112

    LightSaturation=64

    LightRadius=4

}