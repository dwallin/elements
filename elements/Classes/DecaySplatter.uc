//================================================================================
// DecaySplatter.
//================================================================================

class DecaySplatter extends BloodDecal;

var() Texture2d DropTextures[2];
var() float DecalDrawScale;

simulated function PostBeginPlay ()
{
  Super.PostBeginPlay();
  //Texture = DropTextures[Rand(2)];
}

simulated function Align (Vector Normal, optional int NumAttempts)
{
  //DrawType = 2;
  SetDrawScale( DecalDrawScale );
  Super.Align(Normal,NumAttempts);
}

defaultproperties
{
    DropTextures(0)=Texture2d'WOT.decaydrop'

    DropTextures(1)=Texture2d'WOT.decaydrop1'

    DecalDrawScale=0.60

    BloodTextures(0)=Texture2d'WOT.decaysplat'

    BloodTextures(1)=Texture2d'WOT.decaysplat'

    BloodTextures(2)=Texture2d'WOT.decaysplat'

    BloodTextures(3)=Texture2d'WOT.decaysplat'

    BloodTextures(4)=Texture2d'WOT.decaysplat'

    BloodTextures(5)=Texture2d'WOT.decaysplat'

    BloodTextures(6)=Texture2d'WOT.decaysplat'

    BloodTextures(7)=Texture2d'WOT.decaysplat'

    BloodTextures(8)=Texture2d'WOT.decaysplat'

    BloodTextures(9)=Texture2d'WOT.decaysplat'

    BloodTextures(10)=Texture2d'WOT.decaysplat'

    BloodTextures(11)=Texture2d'WOT.decaysplat'

    BloodTextures(12)=Texture2d'WOT.decaysplat'

    BloodTextures(13)=Texture2d'WOT.decaysplat'

    BloodTextures(14)=Texture2d'WOT.decaysplat'

    bHidden=False

    DrawType=1

    DrawScale=0.30

}