//------------------------------------------------------------------------------
// DefaultCastReflector
// $Author: Mfox $
// $Date: 1/05/00 2:37p $
// $Revision: 9 $
//
// Description:	Handles calls to UseAngreal and CeaseUsingAngreal in the
//				default manner for Pawns.
//------------------------------------------------------------------------------
// How to use this class:
//
// + Install in a ePawn at start-up using the Install() function.
//------------------------------------------------------------------------------
class DefaultCastReflector extends Reflector;

var float NextFireTime;	// When we are allowed to fire next.
var AngrealInventory LastCastAngrealInventory;

var bool bLastCastSuccess;

/////////////////////////
// Overriden Functions //
/////////////////////////

//-----------------------------------------------------------------------------
// Turn the currently selected angreal on / "apply" the currently selected item.
//-----------------------------------------------------------------------------
function UseAngreal()
{
	if( AngrealInventory(ePawn(Owner).SelectedItem) != None && WorldInfo.TimeSeconds > NextFireTime )
	{
		// Make sure that we aren't currently firing something.
		CeaseUsingAngreal();

		LastCastAngrealInventory = AngrealInventory(ePawn(Owner).SelectedItem);	

		if( LastCastAngrealInventory != None )
		{
			bLastCastSuccess = true;	// Invalidate.

			LastCastAngrealInventory.StartFire(0);
			ePawn(Owner).PlayFiring();

			// Only restrict usage if successfully fired.
			if( bLastCastSuccess )
			{
				NextFireTime = WorldInfo.TimeSeconds;	// Don't allow firing again this tick.
				if( LastCastAngrealInventory.bRestrictsUsage )
				{
					NextFireTime += (60.0 / LastCastAngrealInventory.RoundsPerMinute);
				}
			}
		}
	}
	else if( ePawn(Owner) != None /*&& Seal(ePawn(Owner).SelectedItem) != None*/ )
	{
		ePawn(Owner).Drop();
	}

	// Pass control off to next reflector.
	Super.UseAngreal();
}

//-----------------------------------------------------------------------------
function NotifyCastFailed( AngrealInventory FailedArtifact )
{
	bLastCastSuccess = false;

	// Pass control off to next reflector.
	Super.NotifyCastFailed( FailedArtifact );
}

//-----------------------------------------------------------------------------
// Turn the currently selected angreal off.
//-----------------------------------------------------------------------------
function CeaseUsingAngreal()
{
	if( LastCastAngrealInventory != None )
	{
		LastCastAngrealInventory.StopFire(0);
	}

	// Pass control off to next reflector.
	Super.CeaseUsingAngreal();
}

defaultproperties
{
    bLastCastSuccess=True

    bRemovable=False

    bDisplayIcon=False

}