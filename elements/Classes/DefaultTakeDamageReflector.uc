//------------------------------------------------------------------------------
// DefaultTakeDamageReflector.uc
// $Author: Mfox $
// $Date: 1/05/00 2:37p $
// $Revision: 2 $
//
// Description:	Handles TakeDamage Reflected calls for ePawns or WOTPawns.
//------------------------------------------------------------------------------
// How to use this class:
//
// + Install in a ePawn or WOTPawn at start-up using the Install() function.
//------------------------------------------------------------------------------
class DefaultTakeDamageReflector extends Reflector;

/////////////////////////
// Overriden Functions //
/////////////////////////

//------------------------------------------------------------------------------
// Default implementation.  Just use the superclass' implementation.
//------------------------------------------------------------------------------
function TakeDamage(int DamageAmount, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	// Call superclass' version.
	if( ePawn(Owner) != None )
	{
		ePawn(Owner).SuperTakeDamage( DamageAmount, EventInstigator, HitLocation, Momentum, DamageType );
	}
	//else if( WOTPawn(Owner) != None )
	//{
	//	WOTPawn(Owner).SuperTakeDamage( DamageAmount, EventInstigator, HitLocation, Momentum, DamageType );
	//}

	// Reflect funciton call to next reflector in line.
	Super.TakeDamage( DamageAmount, EventInstigator, HitLocation, Momentum, DamageType );
}
defaultproperties
{
    bRemovable=False

    bDisplayIcon=False

}