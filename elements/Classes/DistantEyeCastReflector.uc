//================================================================================
// DistantEyeCastReflector.
//================================================================================

class DistantEyeCastReflector extends Reflector;

var AngrealDistantEyeProjectile Eye;

function UseAngreal ()
{
  Eye.StartFire(0);
}

function CeaseUsingAngreal ()
{
  Eye.StopFire(0);
}

reliable server function float GetInitialDuration ()
{
  return Eye.Default.Health;
}

reliable server function float GetDuration ()
{
  return Eye.Health;
}

defaultproperties
{
    Priority=128

    bRemovable=False

}