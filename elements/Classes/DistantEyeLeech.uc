//================================================================================
// DistantEyeLeech.
//================================================================================

class DistantEyeLeech extends Leech;

var() Vector GlowFog;
var() float GlowScale;
var float InitFOV;
var() float FOV;

reliable server function AttachTo (Pawn NewHost)
{
  local ePawn P;

  P = ePawn(NewHost);
  if ( P != None )
  {
    InitFOV = P.FovAngle;
    //P.SetFOVAngle(FOV);
  }
  Super.AttachTo(NewHost);
  if ( Owner == NewHost )
  {
    //P.ClientAdjustGlow(GlowScale,GlowFog);
  } else {
    if ( P != None )
    {
      //P.SetFOVAngle(InitFOV);
    }
  }
}

reliable server function Unattach ()
{
  local ePawn P;

  P = ePawn(Owner);
  //P.ClientAdjustGlow( -GlowScale, -GlowFog);
  Super.Unattach();
  //P.SetFOVAngle(InitFOV);
}

defaultproperties
{
    GlowFog=(X=281.25,Y=421.88,Z=140.63)

    FOV=120.00

}