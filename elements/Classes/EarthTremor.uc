//------------------------------------------------------------------------------
// EarthTremor.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 4 $
//
// Description:	This class simply orchestrates the placement of EarthTremorRocks.
//              You will never actually see this class, only its results.
//------------------------------------------------------------------------------
// How to use this class:
//
// + Spawn.
// + SetSourceAngreal
// + Call Go();
//------------------------------------------------------------------------------
class EarthTremor extends AngrealProjectile;

// Distance between the center and the first ripple.
var() float RippleDistance;

// How high do we spawn these so that we can go up steps, etc.
var() float SpawnHeight;

var() float FirstRingDelay;
var() float SecondRingDelay;
var() float ThirdRingDelay;
var() float FourthRingDelay;

var float StartTime;

var ETSoundProxy ETS;

//------------------------------------------------------------------------------
// Engine notifications.
//------------------------------------------------------------------------------
simulated function Touch( Actor Other, PrimitiveComponent OtherComp, vector HitLocation, vector HitNormal );
simulated function HitWall(vector HitNormal, actor Wall, PrimitiveComponent WallComp);

//------------------------------------------------------------------------------
simulated function PreBeginPlay()
{
	local EarthTremorRock IterRock;
	
	foreach VisibleCollidingActors( class'EarthTremorRock', IterRock, RippleDistance * 4.0 )
	{
		IterRock.Destroy();
	}

	Super.PreBeginPlay();
}

//////////////////////
// Helper functions //
//////////////////////

//------------------------------------------------------------------------------
simulated function SpawnRing( float Scale, float StartAngle, float AngleInterval, float RingRadius )
{
	local vector Loc, X, Y, Z, XAxis, YAxis, ZAxis;
	local rotator Rot;
	local float Angle;
	local EarthTremorRock Rock;
	local vector Ignored;
	
	GetAxes( Rotation, X, Y, Z );
	
	for( Angle = StartAngle; Angle < 360.0; Angle += AngleInterval )
	{
		XAxis = class'WOTUtil'.static.DSin( Angle ) * Y + class'WOTUtil'.static.DCos( Angle ) * X;

		Loc = Location + XAxis*RingRadius;
		
		// Make sure we aren't going to run into geometry.
		if( Trace( Ignored, Ignored, Loc, Location, false ) == None )
		{
			ZAxis = Z;
			YAxis = ZAxis cross XAxis;
			Rot = OrthoRotation( XAxis, YAxis, ZAxis );
			
			Rock = Spawn( class'EarthTremorRock',,, Loc, Rot );
			if( Rock != None )
			{
				Rock.SetSourceAngreal( SourceAngreal );
				Rock.Instigator = Instigator;
				Rock.Lifespan = Default.Lifespan;
				Rock.SetDrawScale(Rock.DrawScale * Scale);
				if( ETS != None )
				{
					ETS.AddRock( Rock );
				}
				Rock.ETS = ETS;
				Rock.Go();
			}
		}
	}
}

//////////////////
// State Timers //
//////////////////

simulated function Go()
{
	GotoState( 'FirstRing' );
}

//------------------------------------------------------------------------------
simulated state FirstRing
{
	simulated function BeginState(name FirstRing)
	{
		local EarthTremorRock Rock;
		local vector X, Y, Z;

		StartTime = WorldInfo.TimeSeconds;
		
		GetAxes( Rotation, X, Y, Z );
		Rock = Spawn( class'ETInner',,, Location, Rotation );
		if( Rock != None )
		{
			Rock.SetSourceAngreal( SourceAngreal );
			Rock.Lifespan = Default.Lifespan;
			ETS = Spawn( class'ETSoundProxy',,, Location );
			if( ETS != None )
			{
				ETS.SetLifeSpan( Default.Lifespan );
				ETS.AddRock( Rock );
			}
			Rock.ETS = ETS;
			Rock.Go();
		}
	}

	simulated function Tick( float DeltaTime )
	{
		Super.Tick( DeltaTime );

		if( (WorldInfo.TimeSeconds - StartTime) >= FirstRingDelay )
		{
			SpawnRing( 1.0, 0.0, 72.0, RippleDistance );
			GotoState( 'SecondRing' );
		}
	}
}

//------------------------------------------------------------------------------
simulated state SecondRing
{
	simulated function BeginState(name SecondRing)
	{
		StartTime = WorldInfo.TimeSeconds;
	}

	simulated function Tick( float DeltaTime )
	{
		Super.Tick( DeltaTime );

		if( (WorldInfo.TimeSeconds - StartTime) >= SecondRingDelay )
		{
			SpawnRing( 0.85, 36.0, 72.0, RippleDistance * 2.0 );
			GotoState( 'ThirdRing' );
		}
	}
}

//------------------------------------------------------------------------------
simulated state ThirdRing
{
	simulated function BeginState(name ThirdRing)
	{
		StartTime = WorldInfo.TimeSeconds;
	}

	simulated function Tick( float DeltaTime )
	{
		Super.Tick( DeltaTime );

		if( (WorldInfo.TimeSeconds - StartTime) >= ThirdRingDelay )
		{
			SpawnRing( 0.70, 0.0, 36.0, RippleDistance * 3.0 );
			GotoState( 'FourthRing' );
		}
	}
}

//------------------------------------------------------------------------------
simulated state FourthRing
{
	simulated function BeginState(name FourthRing)
	{
		StartTime = WorldInfo.TimeSeconds;
	}

	simulated function Tick( float DeltaTime )
	{
		Super.Tick( DeltaTime );

		if( (WorldInfo.TimeSeconds - StartTime) >= FourthRingDelay )
		{
			SpawnRing( 0.65, 18.0, 36.0, RippleDistance * 4.0 );
			Destroy();
		}
	}
}
defaultproperties
{
    RippleDistance=70.00

    SpawnHeight=50.00

    FirstRingDelay=0.20

    SecondRingDelay=0.20

    ThirdRingDelay=0.20

    FourthRingDelay=0.20

    bHidden=false

    RemoteRole=1

    LifeSpan=15.00

    DrawType=0

}