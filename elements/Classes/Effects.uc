//=============================================================================
// Effects, the base class of all gratuitous special effects.
//=============================================================================
class Effects extends LegendActorComponent;

var() soundcue 	EffectSound1;
var() soundcue 	EffectSound2;
var() bool bOnlyTriggerable;
var() float CollisionRadius, CollisionHeight;

defaultproperties
{
    bNetTemporary=True

    DrawType=0

    bGameRelevant=True

    CollisionRadius=0.00

    CollisionHeight=0.00

}