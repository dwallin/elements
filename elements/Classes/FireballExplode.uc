//================================================================================
// FireballExplode.
//================================================================================

class FireballExplode extends Explosion;

defaultproperties
{
    ExplosionAnim(0)=Texture2d'WOT.Icons.FBExp100'

    ExplosionAnim(1)=Texture2d'WOT.Icons.FBExp101'

    ExplosionAnim(2)=Texture2d'WOT.Icons.FBExp102'

    ExplosionAnim(3)=Texture2d'WOT.Icons.FBExp103'

    ExplosionAnim(4)=Texture2d'WOT.Icons.FBExp104'

    ExplosionAnim(5)=Texture2d'WOT.Icons.FBExp105'

    ExplosionAnim(6)=Texture2d'WOT.Icons.FBExp106'

    ExplosionAnim(7)=Texture2d'WOT.Icons.FBExp107'

    ExplosionAnim(8)=Texture2d'WOT.Icons.FBExp108'

    ExplosionAnim(9)=Texture2d'WOT.Icons.FBExp109'

    ExplosionAnim(10)=Texture2d'WOT.Icons.FBExp110'

    ExplosionAnim(11)=Texture2d'WOT.Icons.FBExp111'

    ExplosionAnim(12)=Texture2d'WOT.Icons.FBExp112'

    ExplosionAnim(13)=Texture2d'WOT.Icons.FBExp113'

    ExplosionAnim(14)=Texture2d'WOT.Icons.FBExp114'

    ExplosionAnim(15)=Texture2d'WOT.Icons.FBExp115'

    ExplosionAnim(16)=Texture2d'WOT.Icons.FBExp116'

    ExplosionAnim(17)=Texture2d'WOT.Icons.FBExp117'

    LifeSpan=1.00

    DrawScale=2.50

    SoundPitch=32

    LightEffect=13

    LightRadius=12

}