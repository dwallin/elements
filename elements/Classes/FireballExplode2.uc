//================================================================================
// FireballExplode2.
//================================================================================

class FireballExplode2 extends Explosion;

defaultproperties
{
    ExplosionAnim(0)=Texture2d'WOT.Icons.FBExp200'

    ExplosionAnim(1)=Texture2d'WOT.Icons.FBExp201'

    ExplosionAnim(2)=Texture2d'WOT.Icons.FBExp202'

    ExplosionAnim(3)=Texture2d'WOT.Icons.FBExp203'

    ExplosionAnim(4)=Texture2d'WOT.Icons.FBExp204'

    ExplosionAnim(5)=Texture2d'WOT.Icons.FBExp205'

    ExplosionAnim(6)=Texture2d'WOT.Icons.FBExp206'

    ExplosionAnim(7)=Texture2d'WOT.Icons.FBExp207'

    ExplosionAnim(8)=Texture2d'WOT.Icons.FBExp208'

    ExplosionAnim(9)=Texture2d'WOT.Icons.FBExp209'

    ExplosionAnim(10)=Texture2d'WOT.Icons.FBExp210'

    ExplosionAnim(11)=Texture2d'WOT.Icons.FBExp211'

    ExplosionAnim(12)=Texture2d'WOT.Icons.FBExp212'

    ExplosionAnim(13)=Texture2d'WOT.Icons.FBExp213'

    ExplosionAnim(14)=Texture2d'WOT.Icons.FBExp214'

    ExplosionAnim(15)=Texture2d'WOT.Icons.FBExp215'

    ExplosionAnim(16)=Texture2d'WOT.Icons.FBExp216'

    ExplosionAnim(17)=Texture2d'WOT.Icons.FBExp217'

    LifeSpan=1.00

    DrawScale=2.50

    SoundPitch=32

    LightEffect=13

    LightRadius=12

}