//================================================================================
// FireballExplode3.
//================================================================================

class FireballExplode3 extends Explosion;

defaultproperties
{
    ExplosionAnim(0)=Texture2d'WOT.Icons.FBExp400'

    ExplosionAnim(1)=Texture2d'WOT.Icons.FBExp401'

    ExplosionAnim(2)=Texture2d'WOT.Icons.FBExp402'

    ExplosionAnim(3)=Texture2d'WOT.Icons.FBExp403'

    ExplosionAnim(4)=Texture2d'WOT.Icons.FBExp404'

    ExplosionAnim(5)=Texture2d'WOT.Icons.FBExp405'

    ExplosionAnim(6)=Texture2d'WOT.Icons.FBExp406'

    ExplosionAnim(7)=Texture2d'WOT.Icons.FBExp407'

    ExplosionAnim(8)=Texture2d'WOT.Icons.FBExp408'

    ExplosionAnim(9)=Texture2d'WOT.Icons.FBExp409'

    ExplosionAnim(10)=Texture2d'WOT.Icons.FBExp410'

    ExplosionAnim(11)=Texture2d'WOT.Icons.FBExp411'

    ExplosionAnim(12)=Texture2d'WOT.Icons.FBExp412'

    ExplosionAnim(13)=Texture2d'WOT.Icons.FBExp413'

    ExplosionAnim(14)=Texture2d'WOT.Icons.FBExp414'

    ExplosionAnim(15)=Texture2d'WOT.Icons.FBExp415'

    ExplosionAnim(16)=Texture2d'WOT.Icons.FBExp416'

    LifeSpan=1.00

    DrawScale=2.50

    SoundPitch=32

    LightEffect=13

    LightRadius=12

}