//================================================================================
// FireballFire.
//================================================================================

class FireballFire extends Flame01;

var() float Duration;

simulated function PreBeginPlay ()
{
  Super.PreBeginPlay();
  LifeSpan = Duration;
}

simulated function Tick (float DeltaTime)
{
  Volume = Default.Volume * LifeSpan / Duration;
  bOn = (FollowActor != None) &&  !FollowActor.bDeleteMe;
  Super.Tick(DeltaTime);
}

defaultproperties
{
    Duration=7.00

}