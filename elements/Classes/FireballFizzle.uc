//================================================================================
// FireballFizzle.
//================================================================================

class FireballFizzle extends Explosion;

defaultproperties
{
    ExplosionAnim(0)=Texture2d'WOT.Icons.FBFiz100'

    ExplosionAnim(1)=Texture2d'WOT.Icons.FBFiz101'

    ExplosionAnim(2)=Texture2d'WOT.Icons.FBFiz102'

    ExplosionAnim(3)=Texture2d'WOT.Icons.FBFiz103'

    ExplosionAnim(4)=Texture2d'WOT.Icons.FBFiz104'

    ExplosionAnim(5)=Texture2d'WOT.Icons.FBFiz105'

    ExplosionAnim(6)=Texture2d'WOT.Icons.FBFiz106'

    ExplosionAnim(7)=Texture2d'WOT.Icons.FBFiz107'

    ExplosionAnim(8)=Texture2d'WOT.Icons.FBFiz108'

    ExplosionAnim(9)=Texture2d'WOT.Icons.FBFiz109'

    ExplosionAnim(10)=Texture2d'WOT.Icons.FBFiz110'

    ExplosionAnim(11)=Texture2d'WOT.Icons.FBFiz111'

    ExplosionAnim(12)=Texture2d'WOT.Icons.FBFiz112'

    ExplosionAnim(13)=Texture2d'WOT.Icons.FBFiz113'

    ExplosionAnim(14)=Texture2d'WOT.Icons.FBFiz114'

    ExplosionAnim(15)=Texture2d'WOT.Icons.FBFiz115'

    ExplosionAnim(16)=Texture2d'WOT.Icons.FBFiz116'

    ExplosionAnim(17)=Texture2d'WOT.Icons.FBFiz117'

    ExplosionAnim(18)=Texture2d'WOT.Icons.FBFiz118'

    LifeSpan=1.00

    DrawScale=4.00

    SoundPitch=32

    LightEffect=13

    LightBrightness=128

    LightSaturation=128

    LightRadius=8

}