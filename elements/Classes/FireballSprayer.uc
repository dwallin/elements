class FireballSprayer extends UTProjectile;

//var float SprayerDelay;
//var float RockDuration;
//var rotator RockRotationRate;
//var SoundCue HitPawnSound;
//var bool bHitActor;

///**
// * Set the initial velocity and cook time
// */
//simulated function PostBeginPlay()
//{
//	Super.PostBeginPlay();
//	SetTimer(2.5+FRand()*0.5,false);                  //Grenade begins unarmed
//	RandSpin(100000);
//}

//function Init(vector Direction)
//{
//	SetRotation(Rotator(Direction));

//	Velocity = Speed * Direction;
//	TossZ = TossZ + (FRand() * TossZ / 2.0) - (TossZ / 4.0);
//	Velocity.Z += TossZ;
//	Acceleration = AccelRate * Normal(Velocity);
//}

/////**
//// * Explode
//// */
//simulated function Timer()
//{
//	Explode(Location, vect(0,0,1));
//}


////------------------------------------------------------------------------------
//simulated function Tick( float DeltaTime )
//{
//	Super.Tick( DeltaTime );

//	SetRotation( Rotation + RockRotationRate * DeltaTime );

//	SetLocation(Location + (vect(10,0,0) >> Instigator.GetViewRotation()));
//}

////------------------------------------------------------------------------------
//simulated function bool CanSplash()
//{
//	local PhysicsVolume NewZone;
//	NewZone = self.PhysicsVolume;
//	if (NewZone.bWaterVolume)
//		self.PlaySound(SoundCue'WOT.Sounds.DripSound_Cue');
//	return false;
//}

DefaultProperties
{
//DamageRadius=220
//Speed=1000
//MaxSpeed=1000
//Damage=40
//DecalWidth=128.0
//DecalHeight=128.0
//MomentumTransfer=50000.00
ExplosionLightClass=class'FireballExplosionLight'
AmbientSound=SoundCue'WOT.Sounds.Launch2FB_Cue'
ExplosionSound=SoundCue'WOT.Sounds.HitLevelFB_Cue'
HitPawnSound=SoundCue'WOT.Sounds.HitPawnFB_Cue'
ProjFlightTemplate=ParticleSystem'WOT.Particles.FireballMain'
ProjExplosionTemplate=ParticleSystem'WOT.Particles.P_WP_RocketLauncher_RocketExplosion'
ExplosionDecal=MaterialInstanceTimeVarying'WOT.Decals.MITV_WP_RocketLauncher_Impact_Decal01'
MaxEffectDistance=7000.0
RockRotationRate=(Pitch=0,Yaw=0,Roll=65000)
bCollideWorld=true
bCollideActors=true
CheckRadius=42.0
bWaitForEffects=true
bCheckProjectileLight=true
ProjectileLightClass=class'UTGame.UTRocketLight'
bNetTemporary=false
Lifespan=0.00
}