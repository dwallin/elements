//================================================================================
// Firework02.
//================================================================================

class Firework02 extends ParticleSprayer;

var() float RotRollRate;
var float RotRoll;

simulated function PreBeginPlay ()
{
  LifeSpan = 0.0;
  Super.PreBeginPlay();
}

function Tick (float DeltaTime)
{
  local Rotator Rot;

  RotRoll += RotRollRate * DeltaTime;
  Rot = Rotation;
  //Rot.Roll = InternalTime;
}

defaultproperties
{
    RotRollRate=50000.00

    Spread=90.00

    Volume=30.00

    NumTemplates=2

    Templates(0)=(LifeSpan=1.50,Weight=8.00,MaxInitialVelocity=-50.00,MinInitialVelocity=-50.00,MaxDrawScale=1.00,MinDrawScale=0.70,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=1,MaxGrowRate=-0.30,MinGrowRate=-0.60,FadePhase=1,MaxFadeRate=-0.30,MinFadeRate=-0.70)

    Templates(1)=(LifeSpan=1.50,Weight=1.00,MaxInitialVelocity=-15.00,MinInitialVelocity=-15.00,MaxDrawScale=0.20,MinDrawScale=0.15,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=1,MaxGrowRate=-0.10,MinGrowRate=-0.15,FadePhase=0,MaxFadeRate=0.00,MinFadeRate=0.00)

    Particles(0)=Texture2d'WOT.Sparks01'

    Particles(1)=Texture2d'WOT.Prtcl18'

    bOn=True

    MinVolume=8.00

    bInterpolate=True

    bGrouped=True

    bRotationGrouped=True

    bDisableTick=False

    bStatic=False

    bDynamicLight=True

    bMustFace=False

    VisibilityRadius=8000.00

    VisibilityHeight=8000.00

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=180

    LightRadius=10

}