//================================================================================
// Firework04.
//================================================================================

class Firework04 extends ParticleSprayer;

simulated function PreBeginPlay ()
{
  LifeSpan = 0.0;
  Super.PreBeginPlay();
}

defaultproperties
{
    Volume=25.00

    Gravity=(X=0.00,Y=0.00,Z=15.00)

    NumTemplates=3

    Templates(0)=(LifeSpan=1.50,Weight=8.00,MaxInitialVelocity=15.00,MinInitialVelocity=15.00,MaxDrawScale=1.00,MinDrawScale=0.70,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=1,MaxGrowRate=-0.30,MinGrowRate=-0.60,FadePhase=1,MaxFadeRate=-0.30,MinFadeRate=-0.70)

    Templates(1)=(LifeSpan=3.00,Weight=20.00,MaxInitialVelocity=15.00,MinInitialVelocity=0.00,MaxDrawScale=0.80,MinDrawScale=0.50,MaxScaleGlow=0.30,MinScaleGlow=0.15,GrowPhase=1,MaxGrowRate=-0.20,MinGrowRate=-0.30,FadePhase=2,MaxFadeRate=0.20,MinFadeRate=0.10)

    Templates(2)=(LifeSpan=1.00,Weight=1.00,MaxInitialVelocity=50.00,MinInitialVelocity=50.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=3,MaxGrowRate=3.00,MinGrowRate=1.00,FadePhase=5,MaxFadeRate=-0.30,MinFadeRate=-0.50)

    Particles(0)=Texture2d'WOT.AWhiteCorona'

    Particles(1)=Texture2d'WOT.PF11'

    Particles(2)=Texture2d'WOT.CyanCorona'

    bOn=True

    MinVolume=8.00

    bInterpolate=True

    bStatic=False

    bDynamicLight=True

    Rotation=(Pitch=112072,Yaw=-208,Roll=0)

    bMustFace=False

    VisibilityRadius=8000.00

    VisibilityHeight=8000.00

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=100

    LightSaturation=60

    LightRadius=10

    bFixedRotationDir=True

    RotationRate=(Pitch=0,Yaw=50000,Roll=0)

}