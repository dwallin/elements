//------------------------------------------------------------------------------
// Firework05.uc
// $Author: Aleiby $
// $Date: 8/26/99 8:24p $
// $Revision: 2 $
//
// Description:	Yellow Sparkly Exp Type
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class Firework05 extends ParticleSprayer;

var() float LightDuration;

//------------------------------------------------------------------------------
simulated function PreBeginPlay()
{
	LifeSpan = 3.000000;	// Hardcoded due to struct bug where actual LifeSpan gets overwritten with data from struct.
	Super.PreBeginPlay();
}

//------------------------------------------------------------------------------
simulated function SetInitialState()
{
	Super.SetInitialState();
	//Trigger( Self, None );
}

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	// Super.Tick( DeltaTime );  -- don't call super.

	//LightDuration -= DeltaTime;
	//LightBrightness = byte( FMax( (LightDuration / default.LightDuration) * float(default.LightBrightness), 0.0 ) );
}
defaultproperties
{
    LightDuration=3.00

    Spread=220.00

    Volume=200.00

    Gravity=(X=0.00,Y=0.00,Z=-40.00)

    NumTemplates=3

    Templates(0)=(LifeSpan=1.50,Weight=8.00,MaxInitialVelocity=15.00,MinInitialVelocity=15.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=10,MaxGrowRate=0.50,MinGrowRate=0.25,FadePhase=1,MaxFadeRate=-0.30,MinFadeRate=-0.70)

    Templates(1)=(LifeSpan=2.50,Weight=20.00,MaxInitialVelocity=45.00,MinInitialVelocity=30.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=0.30,MinScaleGlow=0.15,GrowPhase=15,MaxGrowRate=0.80,MinGrowRate=0.40,FadePhase=2,MaxFadeRate=0.40,MinFadeRate=0.10)

    Templates(2)=(LifeSpan=1.00,Weight=1.00,MaxInitialVelocity=50.00,MinInitialVelocity=50.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=12,MaxGrowRate=3.00,MinGrowRate=1.00,FadePhase=5,MaxFadeRate=-0.30,MinFadeRate=-0.50)

    Particles(0)=Texture2d'WOT.Sparks12'

    Particles(1)=Texture2d'WOT.Sparks13'

    Particles(2)=Texture2d'WOT.Sparks15'

    TimerDuration=0.40

    bInitiallyOn=False

    bOn=True

    MinVolume=8.00

    bInterpolate=True

    bDisableTick=False

    bStatic=False

    InitialState=TriggerTimed

    Rotation=(Pitch=147504,Yaw=-208,Roll=0)

    bMustFace=False

    VisibilityRadius=0.00

    VisibilityHeight=0.00

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=40

    LightSaturation=80

    LightRadius=15

    bFixedRotationDir=True

    RotationRate=(Pitch=0,Yaw=50000,Roll=0)

}