//================================================================================
// Firework06.
//================================================================================

class Firework06 extends ParticleSprayer;

simulated function PreBeginPlay ()
{
  LifeSpan = 0.0;
  Super.PreBeginPlay();
}

defaultproperties
{
    Volume=50.00

    Gravity=(X=0.00,Y=0.00,Z=-30.00)

    NumTemplates=6

    Templates(0)=(LifeSpan=2.00,Weight=5.00,MaxInitialVelocity=30.00,MinInitialVelocity=5.00,MaxDrawScale=0.20,MinDrawScale=0.10,MaxScaleGlow=0.00,MinScaleGlow=0.00,GrowPhase=1,MaxGrowRate=-0.10,MinGrowRate=-0.30,FadePhase=10,MaxFadeRate=10.00,MinFadeRate=5.00)

    Templates(1)=(LifeSpan=1.50,Weight=10.00,MaxInitialVelocity=0.00,MinInitialVelocity=0.00,MaxDrawScale=0.30,MinDrawScale=0.15,MaxScaleGlow=0.80,MinScaleGlow=0.60,GrowPhase=1,MaxGrowRate=-0.10,MinGrowRate=-0.30,FadePhase=1,MaxFadeRate=-0.30,MinFadeRate=-0.70)

    Templates(2)=(LifeSpan=2.50,Weight=1.00,MaxInitialVelocity=40.00,MinInitialVelocity=25.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=0.00,MinScaleGlow=0.00,GrowPhase=2,MaxGrowRate=0.50,MinGrowRate=0.30,FadePhase=2,MaxFadeRate=1.00,MinFadeRate=0.30)

    Templates(3)=(LifeSpan=1.00,Weight=10.00,MaxInitialVelocity=0.00,MinInitialVelocity=0.00,MaxDrawScale=0.50,MinDrawScale=0.25,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=0,MaxGrowRate=0.00,MinGrowRate=0.00,FadePhase=1,MaxFadeRate=-1.00,MinFadeRate=-1.10)

    Templates(4)=(LifeSpan=3.00,Weight=10.00,MaxInitialVelocity=40.00,MinInitialVelocity=-30.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=0.00,MinScaleGlow=0.00,GrowPhase=1,MaxGrowRate=0.20,MinGrowRate=0.10,FadePhase=2,MaxFadeRate=0.40,MinFadeRate=0.10)

    Templates(5)=(LifeSpan=2.00,Weight=8.00,MaxInitialVelocity=6.00,MinInitialVelocity=3.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=50,MaxGrowRate=2.00,MinGrowRate=1.50,FadePhase=0,MaxFadeRate=0.00,MinFadeRate=0.00)

    Particles(0)=Texture2d'WOT.YellowCorona'

    Particles(1)=Texture2d'WOT.Fire_Torch_001'

    Particles(2)=Texture2d'WOT.Fire_Torch_003'

    Particles(3)=Texture2d'WOT.YellowCorona'

    Particles(4)=Texture2d'WOT.YellowCorona'

    Particles(5)=Texture2d'WOT.YellowCorona'

    bOn=True

    MinVolume=8.00

    bInterpolate=True

    bStatic=False

    bDynamicLight=True

    Rotation=(Pitch=48956,Yaw=0,Roll=0)

    bMustFace=False

    VisibilityRadius=8000.00

    VisibilityHeight=8000.00

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=40

    LightSaturation=80

    LightRadius=10

}