//------------------------------------------------------------------------------
// Firework07.uc
// $Author: Aleiby $
// $Date: 8/26/99 8:24p $
// $Revision: 2 $
//
// Description:	Purple Swirly Exp Type
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class Firework07 extends ParticleSprayer;

var() float LightDuration;

var int Stage;
var float Stage0Time, Stage1Time, Stage2Time, Stage3Time;

//------------------------------------------------------------------------------
simulated function PreBeginPlay()
{
	LifeSpan = 4.250000;	// Hardcoded due to struct bug where actual LifeSpan gets overwritten with data from struct.
	Super.PreBeginPlay();

	Stage0Time = WorldInfo.TimeSeconds + 0.0;
	Stage1Time = WorldInfo.TimeSeconds + 1.0;
	Stage2Time = WorldInfo.TimeSeconds + 2.0;
	Stage3Time = WorldInfo.TimeSeconds + 3.0;
}

//------------------------------------------------------------------------------
simulated function SetInitialState()
{
	Super.SetInitialState();
	//Trigger( Self, None );
}

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	// Super.Tick( DeltaTime );  -- don't call super.

	if( WorldInfo.TimeSeconds > Stage1Time ) Stage = 1;
	if( WorldInfo.TimeSeconds > Stage2Time ) Stage = 2;
	if( WorldInfo.TimeSeconds > Stage3Time ) Stage = 3;

	// Fade light in.  Rotate.
	if( Stage == 0 )
	{
		//LightBrightness = default.LightBrightness * (WorldInfo.TimeSeconds - Stage0Time);
		RotationRate.Yaw = default.RotationRate.Yaw;
	}

	// Fade light out.  No rotate.
	else if( Stage == 1 )
	{
		//LightBrightness = default.LightBrightness * (1.0 - (WorldInfo.TimeSeconds - Stage1Time));
		RotationRate.Yaw = 0;
	}

	// Fade light in.  Rotate.
	else if( Stage == 2 )
	{
		//LightBrightness = default.LightBrightness * (WorldInfo.TimeSeconds - Stage2Time);
		RotationRate.Yaw = default.RotationRate.Yaw;
	}

	// Fade light out.  Rotate.
	else if( Stage == 3 )
	{
		//LightBrightness = FMax( default.LightBrightness * (1.0 - (WorldInfo.TimeSeconds - Stage3Time)), 0.0 );
		RotationRate.Yaw = default.RotationRate.Yaw;
	}
}
defaultproperties
{
    LightDuration=4.25

    Spread=150.00

    Volume=200.00

    Gravity=(X=0.00,Y=0.00,Z=-25.00)

    NumTemplates=2

    Templates(0)=(LifeSpan=4.00,Weight=5.00,MaxInitialVelocity=250.00,MinInitialVelocity=120.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=0.00,MinScaleGlow=0.00,GrowPhase=4,MaxGrowRate=1.50,MinGrowRate=0.60,FadePhase=4,MaxFadeRate=0.90,MinFadeRate=0.30)

    Templates(1)=(LifeSpan=1.50,Weight=1.00,MaxInitialVelocity=90.00,MinInitialVelocity=15.00,MaxDrawScale=0.10,MinDrawScale=0.01,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=2,MaxGrowRate=-0.10,MinGrowRate=-0.15,FadePhase=2,MaxFadeRate=0.00,MinFadeRate=0.00)

    Particles(0)=Texture2d'WOTSparks01'

    Particles(1)=Texture2d'WOT.Prtcl18'

    TimerDuration=0.25

    bInitiallyOn=False

    bOn=True

    MinVolume=8.00

    bInterpolate=True

    bRotationGrouped=True

    bDisableTick=False

    bStatic=False

    bDynamicLight=True

    Physics=5

    InitialState=TriggerTimed

    Rotation=(Pitch=82092,Yaw=0,Roll=0)

    bMustFace=False

    VisibilityRadius=0.00

    VisibilityHeight=0.00

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=180

    LightRadius=20

    bFixedRotationDir=True

    RotationRate=(Pitch=0,Yaw=50000,Roll=0)

}