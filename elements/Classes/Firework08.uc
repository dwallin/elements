//------------------------------------------------------------------------------
// Firework08.uc
// $Author: Aleiby $
// $Date: 8/26/99 8:24p $
// $Revision: 2 $
//
// Description:	Green/Blue Exp Type
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class Firework08 extends ParticleSprayer;

var() float LightDuration;

//------------------------------------------------------------------------------
simulated function PreBeginPlay()
{
	LifeSpan = 4.250000;	// Hardcoded due to struct bug where actual LifeSpan gets overwritten with data from struct.
	Super.PreBeginPlay();
}

//------------------------------------------------------------------------------
simulated function SetInitialState()
{
	Super.SetInitialState();
	//Trigger( Self, None );
}

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	// Super.Tick( DeltaTime );  -- don't call super.

	LightDuration -= DeltaTime;
	//LightBrightness = byte( FMax( (LightDuration / default.LightDuration) * float(default.LightBrightness), 0.0 ) );
}
defaultproperties
{
    LightDuration=4.25

    Volume=200.00

    Gravity=(X=0.00,Y=0.00,Z=-100.00)

    NumTemplates=3

    Templates(0)=(LifeSpan=4.00,Weight=1.00,MaxInitialVelocity=150.00,MinInitialVelocity=-20.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=0.00,MinScaleGlow=0.00,GrowPhase=1,MaxGrowRate=0.30,MinGrowRate=0.15,FadePhase=40,MaxFadeRate=10.00,MinFadeRate=8.00)

    Templates(1)=(LifeSpan=3.00,Weight=1.00,MaxInitialVelocity=150.00,MinInitialVelocity=50.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=0.30,MinScaleGlow=0.15,GrowPhase=4,MaxGrowRate=1.00,MinGrowRate=0.20,FadePhase=2,MaxFadeRate=0.20,MinFadeRate=0.10)

    Templates(2)=(LifeSpan=2.50,Weight=2.00,MaxInitialVelocity=500.00,MinInitialVelocity=200.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=0.00,MinScaleGlow=0.00,GrowPhase=7,MaxGrowRate=3.00,MinGrowRate=1.00,FadePhase=2,MaxFadeRate=2.00,MinFadeRate=0.80)

    Particles(0)=Texture2d'WOT.AWhiteCorona'

    Particles(1)=Texture2d'WOT.PF11'

    Particles(2)=Texture2d'WOT.CyanCorona'

    TimerDuration=0.25

    bInitiallyOn=False

    bOn=True

    MinVolume=8.00

    bInterpolate=True

    bDisableTick=False

    bStatic=False

    bDynamicLight=True

    InitialState=TriggerTimed

    Rotation=(Pitch=145072,Yaw=-208,Roll=0)

    bMustFace=False

    VisibilityRadius=0.00

    VisibilityHeight=0.00

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=128

    LightSaturation=30

    LightRadius=20

    bFixedRotationDir=True

    RotationRate=(Pitch=0,Yaw=50000,Roll=0)

}