//------------------------------------------------------------------------------
// Firework09.uc
// $Author: Aleiby $
// $Date: 8/26/99 8:24p $
// $Revision: 2 $
//
// Description:	Red Exp Type
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class Firework09 extends ParticleSprayer;

var() float LightDuration;

//------------------------------------------------------------------------------
simulated function PreBeginPlay()
{
	LifeSpan = 3.250000;	// Hardcoded due to struct bug where actual LifeSpan gets overwritten with data from struct.
	Super.PreBeginPlay();

}

//------------------------------------------------------------------------------
simulated function SetInitialState()
{
	Super.SetInitialState();
	//Trigger( Self, None );
}

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	// Super.Tick( DeltaTime );  -- don't call super.

	LightDuration -= DeltaTime;
	//LightBrightness = byte( FMax( (LightDuration / default.LightDuration) * float(default.LightBrightness), 0.0 ) );
}
defaultproperties
{
    LightDuration=3.25

    Spread=230.00

    Volume=200.00

    NumTemplates=3

    Templates(0)=(LifeSpan=1.50,Weight=8.00,MaxInitialVelocity=150.00,MinInitialVelocity=50.00,MaxDrawScale=1.00,MinDrawScale=0.70,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=1,MaxGrowRate=-0.30,MinGrowRate=-0.60,FadePhase=1,MaxFadeRate=-0.30,MinFadeRate=-0.70)

    Templates(1)=(LifeSpan=3.00,Weight=5.00,MaxInitialVelocity=100.00,MinInitialVelocity=100.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=16,MaxGrowRate=1.00,MinGrowRate=0.80,FadePhase=2,MaxFadeRate=0.20,MinFadeRate=0.10)

    Templates(2)=(LifeSpan=1.00,Weight=1.00,MaxInitialVelocity=-100.00,MinInitialVelocity=-150.00,MaxDrawScale=0.00,MinDrawScale=0.00,MaxScaleGlow=1.00,MinScaleGlow=1.00,GrowPhase=3,MaxGrowRate=3.00,MinGrowRate=1.00,FadePhase=5,MaxFadeRate=-0.30,MinFadeRate=-0.50)

    Particles(0)=Texture2d'WOT.Prtcl23'

    Particles(1)=Texture2d'WOT.AWhiteCorona'

    Particles(2)=Texture2d'WOT.Prtcl23'

    TimerDuration=0.25

    bInitiallyOn=False

    bOn=True

    MinVolume=8.00

    bInterpolate=True

    bRotationGrouped=True

    bDisableTick=False

    bStatic=False

    bDynamicLight=True

    Physics=5

    InitialState=TriggerTimed

    Rotation=(Pitch=114752,Yaw=0,Roll=0)

    bMustFace=False

    VisibilityRadius=0.00

    VisibilityHeight=0.00

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=18

    LightRadius=25

    bFixedRotationDir=True

    RotationRate=(Pitch=0,Yaw=16384,Roll=0)

}