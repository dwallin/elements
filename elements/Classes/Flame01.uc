//================================================================================
// Flame01.
//================================================================================

class Flame01 extends ParticleSprayer;

simulated function PreBeginPlay ()
{
  LifeSpan = 0.0;
  Super.PreBeginPlay();
}

defaultproperties
{
    Volume=25.00

    Gravity=(X=0.00,Y=0.00,Z=100.00)

    NumTemplates=2

    Templates(0)=(LifeSpan=3.00,Weight=1.00,MaxInitialVelocity=25.00,MinInitialVelocity=15.00,MaxDrawScale=0.50,MinDrawScale=0.30,MaxScaleGlow=0.00,MinScaleGlow=0.00,GrowPhase=1,MaxGrowRate=-0.75,MinGrowRate=-0.50,FadePhase=2,MaxFadeRate=0.75,MinFadeRate=0.50)

    Templates(1)=(LifeSpan=1.00,Weight=2.00,MaxInitialVelocity=25.00,MinInitialVelocity=15.00,MaxDrawScale=0.50,MinDrawScale=0.25,MaxScaleGlow=0.00,MinScaleGlow=0.00,GrowPhase=1,MaxGrowRate=-0.75,MinGrowRate=-0.50,FadePhase=2,MaxFadeRate=0.75,MinFadeRate=0.50)

    Particles(0)=Texture2d'Fire.FlameBase02'

    Particles(1)=Texture2d'Fire.Flame07'

    bOn=True

    bStatic=False

    VisibilityRadius=500.00

    VisibilityHeight=500.00

}