////------------------------------------------------------------------------------
//// ForkRTSReflector.uc
//// $Author: Mfox $
//// $Date: 1/05/00 2:27p $
//// $Revision: 9 $
////
//// Description:	
////------------------------------------------------------------------------------
//// How to use this class:
////
////------------------------------------------------------------------------------
//class ForkRTSReflector extends ReturnToSenderReflector;

////#exec AUDIO IMPORT FILE=Sounds\Fork\ForkFK.wav		GROUP=Fork

//var Projectile IgnoredProj;

//////////////////
//// Interfaces //
//////////////////

////------------------------------------------------------------------------------
//simulated function OnReflectedTouch( Projectile HitProjectile )
//{
//	local Projectile ForkedCopy;

//	if( HitProjectile == IgnoredProj )
//	{
//		return;
//	}

//	if( IsAffectable( HitProjectile ) )
//	{
//		// NOTE[aleiby]: Use Ryan's SpawnTemplated code.
//		ForkedCopy = Spawn( HitProjectile.Class, HitProjectile.Owner, HitProjectile.Tag, HitProjectile.Location, HitProjectile.Rotation );
//		GenericProjectile(ForkedCopy).SetSourceAngreal( GenericProjectile(HitProjectile).SourceAngreal );
//		ForkedCopy.Instigator = HitProjectile.Instigator;

//		super.OnReflectedTouch( HitProjectile );

//		IgnoredProj = ForkedCopy;
//		ForkedCopy.Touch( Instigator, none, Location, Location );
//		IgnoredProj = None;
//	}
//}

////------------------------------------------------------------------------------
//function SpawnImpactEffect( vector AimLoc )
//{
//	local ReflectSkinEffect Effect;

//	Effect = Spawn( class'elements.ReflectSkinEffect', Owner,, Owner.Location, Owner.Rotation); 
//	//Effect.Mesh = LegendActorComponent(Owner).Mesh;
//	Effect.SetDrawScale( Owner.DrawScale );
//	if( ePawn(Owner) != None )
//	{
//		Effect.SetColor( /*ePawn(Owner).PlayerColor,*/ true );
//	}
//	else
//	{
//		Effect.SetColor( /*'Green',*/ true );
//	}
//}

////------------------------------------------------------------------------------
//reliable server function NotifyHitByAngrealProjectile( AngrealProjectile HitProjectile )
//{
	
//	if( HitProjectile == NewProjectile )
//	{
//		GenericProjectile(NewProjectile).bAbortProcessTouch = true;
//		return;		// We're not done with it yet.
//	}

//	bHandled = false;

//	// Essentially, reflect all GenericProjectiles unless it is suppose to ignore our owner.
//	// Only reflect SeekingProjectiles that are actually after us.

//	if( HitProjectile != None && GenericProjectile(HitProjectile).IgnoredPawn != Owner )
//	{
//		// If it is a seeking projectile that is after us, reflect it.
//		if( SeekingProjectile(HitProjectile) != None && SeekingProjectile(HitProjectile).bSeeking )
//		{
//			if( SeekingProjectile(HitProjectile).bSeeking && SeekingProjectile(HitProjectile).Destination == Owner )
//			{
//				OnReflectedTouch( GenericProjectile(HitProjectile) );
//			}
//		}
//		else
//		{
//			OnReflectedTouch( HitProjectile );
//		}
//	}

//	if( !bHandled )
//	{
//		NotifyHitByAngrealProjectile( HitProjectile );
//	}
	
//	if( NewProjectile != None )
//	{
//		GenericProjectile(NewProjectile).bAbortProcessTouch = false;	// NOTE[aleiby]: Is this needed?  Is it bad or benign?
//		NewProjectile = None;
//	}
//}

////------------------------------------------------------------------------------
//simulated function bool StopsProjectile(Projectile P)
//{
//	if(P == NewProjectile)
//	{
//		return true;		// We're not done with it yet.
//	}

//	bHandled = false;

//	if(P != None)
//	{
//		NotifyHitByAngrealProjectile(AngrealProjectile(P));
//	}

//	if( !bHandled )
//	{
//		OnReflectedTouch(P);
//	}
	
//	if(NewProjectile != None)
//	{
//		NewProjectile = None;
//	}
//	return bHandled;
//}
//------------------------------------------------------------------------------
// ForkRTSReflector.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 9 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class ForkRTSReflector extends ReturnToSenderReflector;

//#exec AUDIO IMPORT FILE=Sounds\Fork\ForkFK.wav		GROUP=Fork

var GenericProjectile IgnoredProj;

////////////////
// Interfaces //
////////////////

//------------------------------------------------------------------------------
simulated function OnReflectedTouch( GenericProjectile HitProjectile )
{
	local GenericProjectile ForkedCopy;

	if( HitProjectile == IgnoredProj )
	{
		return;
	}

	if( IsAffectable( HitProjectile ) )
	{
		// NOTE[aleiby]: Use Ryan's SpawnTemplated code.
		ForkedCopy = Spawn( HitProjectile.Class, HitProjectile.Owner, HitProjectile.Tag, HitProjectile.Location, HitProjectile.Rotation );
		ForkedCopy.SetSourceAngreal( HitProjectile.SourceAngreal );
		ForkedCopy.Instigator = HitProjectile.Instigator;
		
		Super.OnReflectedTouch( HitProjectile );

		IgnoredProj = ForkedCopy;
		ForkedCopy.Touch( Owner, Owner.CollisionComponent, HitProjectile.Location, Normal(HitProjectile.Location) );
		IgnoredProj = None;
	}
}

//------------------------------------------------------------------------------
function SpawnImpactEffect( vector AimLoc )
{
	local ReflectSkinEffect Effect;

	Effect = Spawn( class'ReflectSkinEffect', Owner,, Owner.Location, Owner.Rotation); 
	Effect.Mesh = LegendActorComponent(Owner).Mesh;
	Effect.SetDrawScale( Owner.DrawScale );
	if( ePawn(Owner) != None )
	{
		Effect.SetColor(/* ePawn(Owner).PlayerColor, true*/ );
	}
	else
	{
		Effect.SetColor( /*'Green', */true );
	}
}

defaultproperties
{
    SoundReflectName="WOT.Sounds.ForkFK_Cue"

    Priority=150

}