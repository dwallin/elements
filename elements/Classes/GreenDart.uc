//================================================================================
// GreenDart.
//================================================================================

class GreenDart extends AngrealDartProjectile;

defaultproperties
{
    CoronaTexture=Texture2d'WOT.Icons.Corona_green'

    ExplosionTexture=Texture2d'WOT.Icons.Expl_green'

    Skin=Texture2d'WOT.Icons.Comet_green'

    LightHue=110

}