//================================================================================
// GreenExplode.
//================================================================================

class GreenExplode extends Explosion;

defaultproperties
{
    ExplosionAnim(0)=Texture2d'WOT.Icons.FBExp500'

    ExplosionAnim(1)=Texture2d'WOT.Icons.FBExp501'

    ExplosionAnim(2)=Texture2d'WOT.Icons.FBExp502'

    ExplosionAnim(3)=Texture2d'WOT.Icons.FBExp503'

    ExplosionAnim(4)=Texture2d'WOT.Icons.FBExp504'

    ExplosionAnim(5)=Texture2d'WOT.Icons.FBExp505'

    ExplosionAnim(6)=Texture2d'WOT.Icons.FBExp506'

    ExplosionAnim(7)=Texture2d'WOT.Icons.FBExp507'

    ExplosionAnim(8)=Texture2d'WOT.Icons.FBExp508'

    ExplosionAnim(9)=Texture2d'WOT.Icons.FBExp509'

    ExplosionAnim(10)=Texture2d'WOT.Icons.FBExp510'

    ExplosionAnim(11)=Texture2d'WOT.Icons.FBExp511'

    ExplosionAnim(12)=Texture2d'WOT.Icons.FBExp512'

    ExplosionAnim(13)=Texture2d'WOT.Icons.FBExp513'

    ExplosionAnim(14)=Texture2d'WOT.Icons.FBExp514'

    ExplosionAnim(15)=Texture2d'WOT.Icons.FBExp515'

    ExplosionAnim(16)=Texture2d'WOT.Icons.FBExp516'

    LifeSpan=1.00

    DrawScale=2.50

    SoundPitch=32

    LightEffect=13

    LightHue=46

    LightSaturation=60

    LightRadius=12

}