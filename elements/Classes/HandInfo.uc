//------------------------------------------------------------------------------
// HandInfo.uc
// $Author: Mfox $
// $Date: 1/05/00 2:38p $
// $Revision: 7 $
//------------------------------------------------------------------------------
class HandInfo extends Info;

var class<Inventory> ClassName[10];	// the plan for the hand
var byte		bHaveThis[10];	// flag this for latent search
var Inventory	Item[10];		// the items in the hand -- set in Update()

var int			Selected;		// which item in the list is selected

var class<Inventory> ItemAdded;		// recently added item (used in BaseHUD)
var float		ItemAddedTime;

simulated function PreBeginPlay() {
	super.PreBeginPlay();
}

//------------------------------------------------------------------------------
simulated function Destroyed()
{
	Empty();
}

//------------------------------------------------------------------------------
simulated function Empty()
{
	local int i;

	for( i = 0; i < ArrayCount(ClassName); i++ ) 
	{
		Item[i] = None;
		bHaveThis[i] = 0;
	}
	Selected = 0;
}

//------------------------------------------------------------------------------
simulated function Update()
{
	local int i;

	for( i = 0; i < ArrayCount(ClassName); i++ )
	{
		if( bHaveThis[i] != 0 && Item[i] == None )
		{
			Item[i] = Instigator.FindInventoryType( ClassName[i], true );
		}
	}
}

//------------------------------------------------------------------------------
simulated function int GetArrayCount()
{
	return ArrayCount(ClassName);
}

//------------------------------------------------------------------------------
simulated function int GetItemCount()
{
	local int i, ItemCount;

	for( i = 0; i < ArrayCount(ClassName); i++ )
	{
		if( bHaveThis[i] != 0 )
		{
			ItemCount++;
		}
	}

	return ItemCount;
}

//------------------------------------------------------------------------------
simulated function Inventory GetItem( int Index )
{
	assert( Index >= 0 && Index < ArrayCount(ClassName) );
	return( Item[Index] );
}

//------------------------------------------------------------------------------
simulated function Inventory GetSelectedItem()
{
	return GetItem( Selected );
}

simulated function int GetSelectedItemCharges()
{
	return AngrealInventory(GetItem(Selected)).CurCharges;
}

//------------------------------------------------------------------------------
simulated function class<Inventory> GetClassName( int Index )
{
	
	if( bHaveThis[Index] != 0 )
	{
		return ClassName[Index];
	}
	return none;
}

//------------------------------------------------------------------------------
simulated function class<Inventory> GetSelectedClassName()
{
	return GetClassName( Selected );
}

//------------------------------------------------------------------------------
simulated function bool IsEmpty()
{
	local int i;

	for( i = 0; i < ArrayCount(ClassName); i++ ) 
	{
		if( bHaveThis[i] != 0 )
		{
			return false;
		}
	}

	return true;
}

//------------------------------------------------------------------------------
simulated function bool IsIn( class<Inventory> ItemName )
{
	local int i;

	for( i = 0; i < ArrayCount(ClassName); i++ )
	{
		if( GetClassName( i ) == ItemName )
		{
			return true;
		}
	}

	return false;
}

//------------------------------------------------------------------------------
simulated function AddClassName( class<Inventory> NewClassName, int Slot )
{
	local int i;
	
	assert( Slot >= 0 && Slot < ArrayCount(ClassName) );

	// verify that the class isn't already present
	for( i = 0; i < ArrayCount(ClassName); i++ )
	{
		assert( ClassName[i] != NewClassName );
	}

	ClassName[Slot] = NewClassName;
}

//------------------------------------------------------------------------------
simulated function AddItem( class<Inventory> ItemName, optional bool bSelect )
{
	local int i;
	local bool bPreviouslyEmpty;

	bPreviouslyEmpty = IsEmpty();

	for( i = 0; i < ArrayCount(ClassName); i++ )
	{
		if( ClassName[i] == ItemName )
		{
			bHaveThis[i] = 1;

			// if this failes on the client, Update() will refresh Item[i] as soon as the object has replicated
			Item[i] = ePawn(Owner).FindInventoryType( ItemName );

			ItemAdded = ItemName;
			ItemAddedTime = WorldInfo.TimeSeconds;
			
			if( bPreviouslyEmpty || bSelect )
			{
				Select( i );
			}
			break;
		}
	}
}

//------------------------------------------------------------------------------
simulated function RemoveItem( class<Inventory> ItemName )
{
	local int i;

	for( i = 0; i < ArrayCount(ClassName); i++ )
	{
		if( ClassName[i] == ItemName )
		{
			Item[i] = None;
			bHaveThis[i] = 0;
			break;
		}
	}
}

//------------------------------------------------------------------------------
simulated function SelectItem( class<Inventory> ItemName )
{
	local int i;

	for( i = 0; i < ArrayCount(ClassName); i++ )
	{
		if( ClassName[i] == ItemName ) 
		{
			Select( i );
			break;
		}
	}
}

//------------------------------------------------------------------------------
simulated function SelectFirst()
{
	local int i;
	
	if( !IsEmpty() )
	{
		for( i = 0; i < ArrayCount(ClassName) && GetClassName( i ) == none; i++ );
	}
	
	Select( i );
}

//------------------------------------------------------------------------------
simulated function SelectNext()
{
	local int i;

	i = Selected;
	do
	{
		i = ( i + 1 ) % ArrayCount(ClassName);
		//`log("SelectNext i = "@i);
	}until( i == Selected || GetClassName( i ) != none );
	
	Select( i );
}

//------------------------------------------------------------------------------
simulated function SelectPrevious()
{
	local int i;

	i = Selected;
	do
	{
		i--;
		if( i < 0 )
		{
			//`log(ArrayCount(ClassName)-1);
			i = ArrayCount(ClassName) - 1;
		}
	} until( i == Selected || GetClassName( i ) != none );

	Select( i );
}

//------------------------------------------------------------------------------
simulated function Select( int Index )
{
	assert( Index >= 0 && Index < ArrayCount(ClassName) );
	Selected = Index;
}

// end of HandInfo.uc
defaultproperties
{
    RemoteRole=0

    bAlwaysRelevant=True

}