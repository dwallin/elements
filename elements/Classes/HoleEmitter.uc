class HoleEmitter extends UTReplicatedEmitter;

var class<UDKExplosionLight> ExplosionLightClass;
var bool bSpawnedHole;
var() float StreakLength;
var Pawn HolePawn;
var float InitialLifeSpan;
var() DynamicLightEnvironmentComponent HoleLightEnvironment;
var() float MinLifeSpan, MaxLifeSpan;
var() float MinimumSize;	// Smallest size of decal.
var() int   MaxNumAttempts;	// Default number of times to try before failing to place.
var() float RetryScale;		// Must be a positive number less than one.
var   float TraceDist;		// How far are we allowed to be off the ground?
var   float EdgeRadius;		// Set this to the collision radius.
var() float SeperationDist;	// How far off the ground we are placed.
var() name ManagerTag;
var AngrealInvBalefire Bale;
var Vector VStart;
var Vector VEnd;
var() class<BlackHoles> Holes;
var BlackHoles Hole;

replication
{
	if ( Role == ROLE_Authority )
		MinLifeSpan, MaxLifeSpan, MaxNumAttempts, RetryScale, TraceDist, EdgeRadius, SeperationDist, HoleLightEnvironment, HolePawn, VStart, VEnd;
}

simulated function PreBeginPlay ()
{
  Super.PreBeginPlay();
  LifeSpan = 5.0;
}

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
}

function SetHole(AngrealInvBalefire Bales, Pawn HolePawns, Vector Starts, Vector Ends)
{
	self.HolePawn = HolePawns;
    self.HolePawn.Weapon = HolePawns.Weapon;
    self.VStart = Starts;
    self.VEnd = Ends;
    self.Bale = Bales;
}

simulated function Tick (float DeltaTime)
{
    super.Tick(DeltaTime);
    if (!bSpawnedHole)
    {
        bSpawnedHole=true;
        Hole = Spawn(class'BlackHoles',,,Location,,,true);
        Hole.Align(Vector(Rotation));
    }
}

simulated function PlaceDecals( vector Start, vector Direction, float Limit )
{
    local Actor HitActor;
	local vector HitLocation, HitNormal;
	local float TraceInterval;
	local vector End;

    TraceInterval = 100;

	if( Limit < TraceInterval )
	{
		TraceInterval = Limit;
	}

	End = Start + (TraceInterval * Direction);
	HitActor = Trace( HitLocation, HitNormal, End, Start, false );

	if( HitActor != None )
	{   
        Hole = Spawn(Holes,,,HitLocation,,,true);
        Hole.Align(HitNormal);
   
        HitLocation += 100 * Direction;		// get off the wall.
		Limit -= VSize(HitLocation - Start);
	}
	else
	{
		HitLocation = End;
		Limit -= TraceInterval;
	}

	if( Limit > 0.0 )
	{
		PlaceDecals( HitLocation, Direction, Limit );
	}    
}


//------------------------------------------------------------------------------
simulated function Align( vector Normal, optional int NumAttempts )
{
	local vector X, Y, Z;
	local vector XPos, YPos, ZPos;
	local float Width;
	local vector Start[4], Ignored, HitLocation;
	local Actor HitActor;
	local int i;
	local rotator Rot;

	    // Default to a virtually infinite number of tries.
	    if( NumAttempts == 0 )
	    {
		    NumAttempts = MaxNumAttempts;
	    }

	    // Make sure we are flush and have the correct normal.
	    if( Trace( HitLocation, Normal, Location - (Normal * 16.f), Location, false ) != None )
	    {
		    SetLocation( HitLocation + SeperationDist * Normal );

		    // Adjust rotation.
		    Rot = rotator(Normal);
		    Rot.Roll = FRand() * 0x10000;	// 0 to 360 degrees.
		    SetRotation( Rot );
		    GetAxes( Rotation, X, Y, Z );
		    XPos = TraceDist * X;

		    // Try to fit.
		    while( NumAttempts > 0 )
		    {
			    Width = default.EdgeRadius * DrawScale;
			    if( Width < MinimumSize )
			    {
				    break;
			    }

			    ZPos = Width * Z;
			    YPos = Width * Y;

			    // Trace the four corners.
			    Start[0] = Location + ZPos + YPos;
			    Start[1] = Location + ZPos - YPos;
			    Start[2] = Location - ZPos + YPos;
			    Start[3] = Location - ZPos - YPos;

			    for( i = 0; i < ArrayCount(Start); i++ )
			    {
				    HitActor = Trace( Ignored, Ignored, Start[i] - XPos, Start[i], false );
				    if( HitActor == None || HitActor.IsA('BlockAll') )
				    {
					    // Can't touch the ground, or is touching a mover.
					    break;
				    }
			    }

			    // All four points hit ground... we are good to go.
			    if( i == ArrayCount(Start) )
			    {
				    SetPhysics( PHYS_None );
				    SetHidden(false);
				    return;
			    }

			    // Try again.
			    NumAttempts -= 1;
			    SetDrawScale(DrawScale * RetryScale);
		    }
        }
	    // If we get here, it means we didn't fit, and don't deserve to live.
	    Destroy();
}

defaultproperties
{
Begin Object Class=DynamicLightEnvironmentComponent Name=HoleLightEnvironmentComp
 	bCastShadows=True
	AmbientGlow=(R=1.0,G=0.0,B=1.0,A=0.5)
	LightDistance=5.0
	bIsCharacterLightEnvironment=true
	bEnabled=true
End Object
Components.Add(HoleLightEnvironmentComp)
HoleLightEnvironment = HoleLightEnvironmentComp
bTicked=true
bUpdateSimulatedPosition=true
bCollideComplex=true
bDestroyOnSystemFinish=true
bForceNetUpdate=true
bLockLocation=true
bPostRenderIfNotVisible=true
bPostUpdateTickGroup=true
StreakLength=8000.00
Holes=class'BlackHoles'
EmitterTemplate=ParticleSystem'WOT.Particles.P_Bale_Beam_Impact'
ExplosionLightClass=class'BaleImpactLight'
DrawScale=1.0
TickGroup=TG_PreAsyncWork
bNetDirty=true
bGameRelevant=true
bAlwaysRelevant=true
bAlwaysTick=true
bReplicateInstigator=true
bReplicateRigidBodyLocation=true
MinLifeSpan=180.00
MaxLifeSpan=300.00
MinimumSize=5.00
MaxNumAttempts=10
RetryScale=0.85
TraceDist=5.00
EdgeRadius=30.00
SeperationDist=1.00
bCollideWorld=true
bHidden=true
}