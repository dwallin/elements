//================================================================================
// IgnoreAirElementReflector.
//================================================================================

class IgnoreAirElementReflector extends IgnoreElementReflector;

function ProcessEffect (Invokable i)
{
  if ( (i.SourceAngreal != None) && i.SourceAngreal.bElementAir )
  {
    IgnoreEffect(i);
  } else {
    Super.ProcessEffect(i);
  }
}

function bool InvIsIgnored (AngrealInventory Inv)
{
  return Inv.bElementAir;
}

defaultproperties
{
    ImpactType=Class'AirShieldVisual'

    DeflectSound=SoundCUe'WOT.Sounds.DeflectAS_Cue'

    TriggerEvent="ElementalAirTriggered"

    IgnoredDamageType="Air"

}