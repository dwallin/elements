//================================================================================
// IgnoreEarthElementReflector.
//================================================================================

class IgnoreEarthElementReflector extends IgnoreElementReflector;

function ProcessEffect (Invokable i)
{
  if ( (i.SourceAngreal != None) && i.SourceAngreal.bElementEarth )
  {
    IgnoreEffect(i);
  } else {
    Super.ProcessEffect(i);
  }
}

function bool InvIsIgnored (AngrealInventory Inv)
{
  return Inv.bElementEarth;
}

defaultproperties
{
    ImpactType=Class'EarthShieldVisual'

    DeflectSound=SoundCue'WOT.Sounds.DeflectES_Cue'

    TriggerEvent="ElementalEarthTriggered"

    IgnoredDamageType="Earth"

}