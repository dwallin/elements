//================================================================================
// IgnoreFireElementReflector.
//================================================================================

class IgnoreFireElementReflector extends IgnoreElementReflector;

function ProcessEffect (Invokable i)
{
  if ( (i.SourceAngreal != None) && i.SourceAngreal.bElementFire )
  {
    ServerIgnoreEffect(i);
  } else {
    Super.ProcessEffect(i);
  }
}

reliable server function ServerIgnoreEffect(Invokable I)
{
	IgnoreEffect(I);
}

function bool InvIsIgnored (AngrealInventory Inv)
{
  return Inv.bElementFire;
}

defaultproperties
{
    ImpactType=Class'FireShieldVisual'

    DeflectSound=SoundCue'WOT.Sounds.DeflectFS_Cue'

    TriggerEvent="ElementalFireTriggered"

    IgnoredDamageType="Fire"

}