//================================================================================
// IgnoreSpiritElementReflector.
//================================================================================

class IgnoreSpiritElementReflector extends IgnoreElementReflector;

function ProcessEffect (Invokable i)
{
  if ( (i.SourceAngreal != None) && i.SourceAngreal.bElementSpirit )
  {
    IgnoreEffect(i);
  } else {
    Super.ProcessEffect(i);
  }
}

function bool InvIsIgnored (AngrealInventory Inv)
{
  return Inv.bElementSpirit;
}

defaultproperties
{
    ImpactType=Class'SpiritShieldVisual'

    DeflectSound=SoundCue'WOT.Sounds.DeflectSS_Cue'

    TriggerEvent="ElementalSpiritTriggered"

    IgnoredDamageType="Spirit"

}