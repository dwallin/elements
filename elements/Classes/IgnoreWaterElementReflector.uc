//================================================================================
// IgnoreWaterElementReflector.
//================================================================================

class IgnoreWaterElementReflector extends IgnoreElementReflector;

var Vector InitialZoneVelocity[64];
var ZoneInfo Zones[64];

//------------------------------------------------------------------------------
function Install( Pawn NewHost )
{
	local ZoneInfo Zone;
	//local int i;

	Super.Install( NewHost );

	//foreach AllActors( class'ZoneInfo', Zone )
	//{
	//	if( i < ArrayCount(Zones) )
	//	{
	//		Zones[i] = Zone;
	//		InitialZoneVelocity[i] = Zone.ZoneVelocity;
	//		Zone.ZoneVelocity = vect(0,0,0);
	//		i++;
	//	}
	//	else
	//	{
	//		`warn( "Zone array exceeded." );
	//		break;
	//	}
	//}
}

//------------------------------------------------------------------------------
function UnInstall()
{
	//local int i;

	Super.UnInstall();

	//for( i = 0; i < ArrayCount(Zones); i++ )
	//{
	//	if( Zones[i] != None )
	//	{
	//		Zones[i].ZoneVelocity = InitialZoneVelocity[i];
	//	}
	//}
}

/////////////////////////
// Overriden Functions //
/////////////////////////

//------------------------------------------------------------------------------
// Only pass this effect on to latter reflectors if it is its source angreal
// is NOT composed of ElementWater.
//------------------------------------------------------------------------------
function ProcessEffect( Invokable I )
{
	if( I.SourceAngreal != None && I.SourceAngreal.bElementWater )
	{
		IgnoreEffect( I );
	}
	else
	{
		Super.ProcessEffect( I );
	}
}

function TakeDamage(int DamageAmount, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	`log(Class'AngrealInventory'.static.DamageTypeContains(DamageType,IgnoredDamageType));
  if ( Class'AngrealInventory'.static.DamageTypeContains(DamageType,IgnoredDamageType) || (DamageType == class'Drowned') )
  {
    SpawnImpactEffect(HitLocation);
  } else {
    Super(Reflector).TakeDamage(DamageAmount,EventInstigator,HitLocation,Momentum,DamageType);
  }
}

function bool InvIsIgnored (AngrealInventory Inv)
{
  return Inv.bElementWater;
}

defaultproperties
{
    ImpactType=Class'WaterShieldVisual'

    DeflectSound=SoundCue'WOT.Sounds.DeflectWS_Cue'

    TriggerEvent="ElementalWaterTriggered"

    IgnoredDamageType="Water"

}