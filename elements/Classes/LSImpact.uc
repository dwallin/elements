//------------------------------------------------------------------------------
// LSImpact.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 5 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class LSImpact extends Effects;

//#exec MESH IMPORT MESH=LSimpact ANIVFILE=MODELS\LSimpact_a.3d DATAFILE=MODELS\LSimpact_d.3d X=0 Y=0 Z=0 MLOD=0
//#exec MESH ORIGIN MESH=LSimpact X=0 Y=0 Z=0 YAW=64 PITCH=0 ROLL=0

//#exec MESH SEQUENCE MESH=LSimpact SEQ=All      STARTFRAME=0 NUMFRAMES=1

//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A01.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A02.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A03.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A04.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A05.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A06.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A07.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A08.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A09.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A10.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A11.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A12.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A13.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A14.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A15.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A16.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A17.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A18.PCX GROUP=Skins FLAGS=2
//#exec TEXTURE IMPORT FILE=MODELS\Limpact_A19.PCX GROUP=Skins FLAGS=2

//#exec MESHMAP NEW   MESHMAP=LSimpact MESH=LSimpact
//#exec MESHMAP SCALE MESHMAP=LSimpact X=0.1 Y=0.1 Z=0.2

//#exec MESHMAP SETTEXTURE MESHMAP=LSimpact NUM=0 TEXTURE=Limpact_A01

var() 	float	FrameInterval;
var StaticMeshComponent ImpactMesh;
var()	Material	SkinAnim[19];
var	int	SkinAnimIndex;
var MaterialInstanceConstant MatInst;

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	InitMaterialInstance();
}

function InitMaterialInstance()
{
   MatInst = new(None) Class'MaterialInstanceConstant';
   MatInst.SetParent(ImpactMesh.GetMaterial(0));
}

function UpdateMaterialInstance()
{
   ImpactMesh.SetMaterial(0, Skin);
}
//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	FrameInterval -= DeltaTime;
	while( FrameInterval <= 0.0 ) 
	{
		FrameInterval += default.FrameInterval;
		Skin = SkinAnim[SkinAnimIndex++];
		UpdateMaterialInstance();

		if( SkinAnimIndex >= ArrayCount(SkinAnim) )
		{
			SkinAnimIndex = 0;
		}
	}
	
}
defaultproperties
{
	Begin Object class=StaticMeshComponent Name=LSImpactMesh
		StaticMesh=StaticMesh'WOT.Meshes.LSImpact'
		Materials[0]=Material'WOT.Icons.Limpact_A01_Mat'
		LightEnvironment=MyLightEnvironment
		AmbientGlow=200
		LightType=1
	    LightEffect=13
	    LightBrightness=190
	    LightHue=204
	    LightSaturation=204
	    LightRadius=8
	End Object
	CollisionComponent=LSImpactMesh
	Components.Add(LSImpactMesh)
	ImpactMesh=LSImpactMesh
    FrameInterval=0.05

    SkinAnim(0)=Material'WOT.Icons.Limpact_A01_Mat'

    SkinAnim(1)=Material'WOT.Icons.Limpact_A02_Mat'

    SkinAnim(2)=Material'WOT.Icons.Limpact_A03_Mat'

    SkinAnim(3)=Material'WOT.Icons.Limpact_A04_Mat'

    SkinAnim(4)=Material'WOT.Icons.Limpact_A05_Mat'

    SkinAnim(5)=Material'WOT.Icons.Limpact_A06_Mat'

    SkinAnim(6)=Material'WOT.Icons.Limpact_A07_Mat'

    SkinAnim(7)=Material'WOT.Icons.Limpact_A08_Mat'

    SkinAnim(8)=Material'WOT.Icons.Limpact_A09_Mat'

    SkinAnim(9)=Material'WOT.Icons.Limpact_A10_Mat'

    SkinAnim(10)=Material'WOT.Icons.Limpact_A11_Mat'

    SkinAnim(11)=Material'WOT.Icons.Limpact_A12_Mat'

    SkinAnim(12)=Material'WOT.Icons.Limpact_A13_Mat'

    SkinAnim(13)=Material'WOT.Icons.Limpact_A14_Mat'

    SkinAnim(14)=Material'WOT.Icons.Limpact_A15_Mat'

    SkinAnim(15)=Material'WOT.Icons.Limpact_A16_Mat'

    SkinAnim(16)=Material'WOT.Icons.Limpact_A17_Mat'

    SkinAnim(17)=Material'WOT.Icons.Limpact_A18_Mat'

    SkinAnim(18)=Material'WOT.Icons.Limpact_A19_Mat'

    RemoteRole=1

    DrawType=2

    Style=3

    Skin=Material'WOT.Icons.Limpact_A01_Mat'

    Mesh=SkeletalMesh'WOT.Mesh.LSImpact'

    DrawScale=4.00

    AmbientGlow=200

    bUnlit=True

    LightType=1

    LightEffect=13

    LightBrightness=190

    LightHue=204

    LightSaturation=204

    LightRadius=8

}