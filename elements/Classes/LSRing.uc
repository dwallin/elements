//================================================================================
// LSring.
//================================================================================

class LSring extends Effects;

var Material Materialss[10];
var StaticMeshComponent RingMesh;
var MaterialInstanceConstant MatInst;

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	InitMaterialInstance();
}

function InitMaterialInstance()
{
   MatInst = new(None) Class'MaterialInstanceConstant';
   MatInst.SetParent(RingMesh.GetMaterial(0));
}

simulated function Tick (float DeltaTime)
{
  Super.Tick(DeltaTime);
  UpdateMaterialInstance();
}

function UpdateMaterialInstance()
{
   RingMesh.SetMaterial(0, Materialss[Rand(10)]);
}
defaultproperties
{
	Begin Object class=StaticMeshComponent Name=LSRingMesh
		StaticMesh=StaticMesh'WOT.Meshes.LSRing'
		LightEnvironment=MyLightEnvironment
	End Object
	CollisionComponent=LSRingMesh
	Components.Add(LSRingMesh)
	RingMesh=LSRingMesh

    RemoteRole=1

    DrawType=2
	Materialss[0]=Material'WOT.Icons.LB1_Mat'
	Materialss[1]=Material'WOT.Icons.LB2_Mat'
	Materialss[2]=Material'WOT.Icons.LB3_Mat'
	Materialss[3]=Material'WOT.Icons.LB4_Mat'
	Materialss[4]=Material'WOT.Icons.LB5_Mat'
	Materialss[5]=Material'WOT.Icons.LB6_Mat'
	Materialss[6]=Material'WOT.Icons.LB7_Mat'
	Materialss[7]=Material'WOT.Icons.LB8_Mat'
	Materialss[8]=Material'WOT.Icons.LB9_Mat'
	Materialss[9]=Material'WOT.Icons.LB10_Mat'
    Style=3
    //Skin=Texture2d'WOT.Icons.LB10'

    //Mesh=SkeletalMesh'WOT.Mesh.LSring'

    DrawScale=0.5

    bUnlit=True

}