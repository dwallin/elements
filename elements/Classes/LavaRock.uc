//================================================================================
// LavaRock.
//================================================================================

class LavaRock extends BurningChunk;

defaultproperties
{
    MinSpeed=400.00

    MaxSpeed=600.00

    SprayerTypes=Class'elements.BlackSmoke01'

    DrawType=2

    Style=1

    //Mesh=Mesh'LavaRock'

    DrawScale=0.70

}