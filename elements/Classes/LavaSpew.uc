//================================================================================
// LavaSpew.
//================================================================================

class LavaSpew extends ParticleSprayer;

var() float Duration;
var() float ActualLifeSpan;

simulated function PreBeginPlay ()
{
  LifeSpan = ActualLifeSpan;
  Super.PreBeginPlay();
  SetTimer(Duration,False);
}

simulated function Timer ()
{
  bOn = False;
}

function Tick (float DeltaTime);

defaultproperties
{
    Duration=0.25

    ActualLifeSpan=3.50

    Volume=100.00

    Gravity=(X=0.00,Y=0.00,Z=-100.00)

    NumTemplates=3

    Templates(0)=(LifeSpan=5.00,Weight=4.00,MaxInitialVelocity=150.00,MinInitialVelocity=30.00,MaxDrawScale=0.60,MinDrawScale=1.00,MaxScaleGlow=1.00,MinScaleGlow=0.80,GrowPhase=1,MaxGrowRate=-0.10,MinGrowRate=-0.50,FadePhase=1,MaxFadeRate=-0.20,MinFadeRate=-0.40)

    Templates(1)=(LifeSpan=4.00,Weight=3.00,MaxInitialVelocity=140.00,MinInitialVelocity=10.00,MaxDrawScale=0.40,MinDrawScale=0.10,MaxScaleGlow=1.00,MinScaleGlow=0.70,GrowPhase=1,MaxGrowRate=-0.05,MinGrowRate=-0.30,FadePhase=1,MaxFadeRate=0.00,MinFadeRate=-0.40)

    Templates(2)=(LifeSpan=4.00,Weight=1.00,MaxInitialVelocity=400.00,MinInitialVelocity=80.00,MaxDrawScale=0.60,MinDrawScale=0.30,MaxScaleGlow=1.00,MinScaleGlow=0.70,GrowPhase=1,MaxGrowRate=-0.20,MinGrowRate=-0.50,FadePhase=1,MaxFadeRate=-0.30,MinFadeRate=-0.70)

    Particles(0)=Texture2d'WOT.Prtcl05'

    Particles(1)=Texture2d'WOT.Prtcl04'

    Particles(2)=Texture2d'WOT.Prtcl08'

    bStatic=False

    VisibilityRadius=500.00

    VisibilityHeight=500.00

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=18

    LightSaturation=36

    LightRadius=8

}