class LegendActorComponent extends Actor;

enum ESoundSlot {
SLOT_None,
SLOT_Misc,
SLOT_Pain,
SLOT_Interact,
SLOT_Ambient,
SLOT_Talk,
SLOT_Interface,
};

//-----------------------------------------------------------------------------
// Display properties.

// Drawing effect.
var(Display) enum EDrawType
{
	DT_None,
	DT_Sprite,
	DT_Mesh,
	DT_Brush,
	DT_RopeSprite,
	DT_VerticalSprite,
	DT_Terraform,
	DT_SpriteAnimOnce,
} DrawType;

// Other display properties.
var(Display) texture2d    Sprite;			 // Sprite texture if DrawType=DT_Sprite.
//#if 1 //NEW
var(Display) float      SpriteProjForward; // Move sprite projection planes forward this amount.
var(Display) bool       bMustFace;       // Set to false if you want this actor to be drawn whether you are facing it or not.
//#endif
var(Display) texture2d    Texture;		 // Misc texture.
var(Display) Material    Skin;            // Special skin or enviro map texture.
var(Display) SkeletalMesh       Mesh;            // Mesh if DrawType=DT_Mesh.
var(Display) float      ScaleGlow;		 // Multiplies lighting.
var(Display) byte       AmbientGlow;     // Ambient brightness, or 255=pulsing.
var(Display) byte       Fatness;         // Fatness (mesh distortion).

// Display.
var(Display)  bool      bUnlit;          // Lights don't affect actor.
var(Display)  bool      bNoSmooth;       // Don't smooth actor's texture.
var(Display)  bool      bParticles;      // Mesh is a particle system.
var(Display)  bool      bRandomFrame;    // Particles use a random texture from among the default texture and the multiskins textures
var(Display)  bool      bMeshEnviroMap;  // Environment-map the mesh.
var(Display)  bool      bMeshCurvy;      // Curvy mesh.
var(Display)  float     VisibilityRadius;// Actor is drawn if viewer is within its visibility
var(Display)  float     VisibilityHeight;// cylinder.  Zero=infinite visibility.

// Not yet implemented.
var(Display) bool       bShadowCast;     // Casts shadows.
var(Display) bool bOwnerNoSee;

// Multiple skin support.
var(Display) Material MultiSkins[8];

//-----------------------------------------------------------------------------
// Sound.

// Ambient sound.
var(Sound) byte         SoundRadius;	 // Radius of ambient sound.
var(Sound) byte         SoundVolume;	 // Volume of amient sound.
var(Sound) byte         SoundPitch;	     // Sound pitch shift, 64.0=none.
var(Sound) soundCue     AmbientSound;    // Ambient sound effect.

// Regular sounds.
var(Sound) float TransientSoundVolume;
var(Sound) float TransientSoundRadius;

var(Collision) bool       bBlockPlayers;    // Blocks other player actors.


//-----------------------------------------------------------------------------
// Lighting.

// Light modulation.
var(Lighting) enum ELightType
{
	LT_None,
	LT_Steady,
	LT_Pulse,
	LT_Blink,
	LT_Flicker,
	LT_Strobe,
	LT_BackdropLight,
	LT_SubtlePulse,
	LT_TexturePaletteOnce,
	LT_TexturePaletteLoop
} LightType;

// Spatial light effect to use.
var(Lighting) enum ELightEffect
{
	LE_None,
	LE_TorchWaver,
	LE_FireWaver,
	LE_WateryShimmer,
	LE_Searchlight,
	LE_SlowWave,
	LE_FastWave,
	LE_CloudCast,
	LE_StaticSpot,
	LE_Shock,
	LE_Disco,
	LE_Warp,
	LE_Spotlight,
	LE_NonIncidence,
	LE_Shell,
	LE_OmniBumpMap,
	LE_Interference,
	LE_Cylinder,
	LE_Rotor,
	LE_Unused
} LightEffect;

// Lighting info.
var(LightColor) byte
	LightBrightness,
	LightHue,
	LightSaturation;

// Light properties.
var(Lighting) byte
	LightRadius,
	LightPeriod,
	LightPhase,
	LightCone,
	VolumeBrightness,
	VolumeRadius,
	VolumeFog;

// Lighting.
var(Lighting) bool	     bSpecialLit;	 // Only affects special-lit surfaces.
var(Lighting) bool	     bActorShadows;  // Light casts actor shadows.
var(Lighting) bool	     bCorona;        // Light uses Skin as a corona.
var(Lighting) bool	     bLensFlare;     // Whether to use zone lens flare.

var EBlendMode Style;
var() float Mass;
var() float Buoyancy;

var() float CollisionRadius;

var() float CollisionHeight;

var bool bFixedRotationDir;

var name AttachTag;

var() bool bDirectional;
var() bool bNetOptional;

simulated function PreBeginPlay()
{
	Super.PreBeginPlay();
}

function Expired()
{
	Destroy();
}

function Destructed()
{
	super.Destroyed();
}

//static util functions
//=============================================================================
// Randomly modifies the given float by +/- given %.
//
// e.g. PerturbFloatPercent( 100.0, 20.0) will return a value in 80.0..120.0
//=============================================================================

static function float PerturbFloatPercent( float Num, float PerturbPercent )
{
	local float Perturb;

	Perturb = 2.0*PerturbPercent / 100.0;

	return Num + Num * ( ( Perturb * FRand() - Perturb / 2.0 ) );
}


//=============================================================================
// Randomly modifies the given int by +/- given #.
//
// e.g. PerturbInt( 100, 20) will return a value in 80..120
//=============================================================================

static function int PerturbInt( int Num, int PerturbPlusMinus )
{
	return Num + Rand( 2*PerturbPlusMinus+1 ) - PerturbPlusMinus;
}

//=============================================================================
// Recursively traces until we hit something (BSP or Actor).
//------------------------------------------------------------------------------
// Instance:		We need an instance Actor to call trace from.	(Pass in any Actor - Self is usually a good choice.)
// HitLocation:     Location of hit BSP/Actor.
// HitNormal:		Hit normal.
// StartLoc:		Location to start tracing from.
// bTraceActors:	Should we stop tracing if we hit an Actor?		(Default - false.)
// TraceInverval:	How far to trace per iteration.					(Default - 500 units.)
// TraceDirection:	Direction to trace in.							(Default - straight down.)
// TraceLimit:		How far to trace before giving up.				(Default - infinite.)
// Extent:			extents (optional)
// 
//------------------------------------------------------------------------------
// Returns:			Hit BSP/Actor or None if reached limit.
//------------------------------------------------------------------------------
event Trigger (Actor Other, Pawn EventInstigator);
event UnTrigger (Actor Other, Pawn EventInstigator);
static function Actor TraceRecursive
(	Actor			Instance,
	out vector		HitLocation,
	out vector		HitNormal,
	vector			StartLoc,
	optional bool	bTraceActors,
	optional float	TraceInterval,
	optional vector	TraceDirection,
	optional float	TraceLimit,
	optional vector Extent

)
{
	local Actor HitActor;
	local vector EndLoc;

	if( TraceInterval ~= 0.0 )
	{
		TraceInterval = 500.0;
	}

	if( TraceLimit != 0 && TraceInterval > TraceLimit )
	{
		TraceInterval = TraceLimit;
	}

	if( TraceDirection == vect(0,0,0) )
	{
		TraceDirection = vect(0,0,-1);
	}
	else
	{
		TraceDirection = Normal(TraceDirection);
	}

	EndLoc		= StartLoc + TraceDirection * TraceInterval;
	HitActor	= Instance.Trace( HitLocation, HitNormal, EndLoc, StartLoc, bTraceActors, Extent );

	if( TraceLimit > 0.0 )
	{
		TraceLimit -= TraceInterval;
		if( TraceLimit <= 0.0 )
		{
			return HitActor;	// Stop whether we've found anything or not.
		}
	}

	if( HitActor == None )
	{
		// we didn't hit anything -- continue tracing from where we stopped.
		HitActor = TraceRecursive( Instance, HitLocation, HitNormal, EndLoc, bTraceActors, TraceInterval, TraceDirection, TraceLimit, Extent );
	}

	return HitActor;
}

//=============================================================================
// Same as TraceRecursiveBlocking but only considers blocking actors. Kept this
// as a separate function to not slow down TraceRecursive. In any case, this
// could go away if some way to control whether hit actors must be blocking 
// could be added to the Trace function.

static function Actor TraceRecursiveBlocking
(	Actor			Instance,
	out vector		HitLocation,
	out vector		HitNormal,
	vector			StartLoc,
	optional bool	bTraceActors,
	optional float	TraceInterval,
	optional vector	TraceDirection,
	optional float	TraceLimit,
	optional vector Extent
)
{
	local Actor HitActor;
	local vector EndLoc;

	if( TraceInterval ~= 0.0 )
	{
		TraceInterval = 500.0;
	}

	if( TraceLimit != 0 && TraceInterval > TraceLimit )
	{
		TraceInterval = TraceLimit;
	}

	if( TraceDirection == vect(0,0,0) )
	{
		TraceDirection = vect(0,0,-1);
	}
	else
	{
		TraceDirection = Normal(TraceDirection);
	}

	EndLoc		= StartLoc + TraceDirection * TraceInterval;
	HitActor	= Instance.Trace( HitLocation, HitNormal, EndLoc, StartLoc, bTraceActors, Extent );
	if( HitActor != None && !HitActor.IsA( 'LevelInfo' ) && !HitActor.bBlockActors )
	{
		HitActor = None;
	}

	if( TraceLimit > 0.0 )
	{
		TraceLimit -= TraceInterval;
		if( TraceLimit <= 0.0 )
		{
			return HitActor;	// Stop whether we've found anything or not.
		}
	}

	if( HitActor == None )
	{
		// we didn't hit anything -- continue tracing from where we stopped.
		HitActor = TraceRecursiveBlocking( Instance, HitLocation, HitNormal, EndLoc, bTraceActors, TraceInterval, TraceDirection, TraceLimit, Extent );
	}

	return HitActor;
}

//=============================================================================
// Traces from ePawn's eyes 64K units along his ViewRotation and returns
// what was hit, and the HitLocation and HitNormal.
//=============================================================================

static function Actor GetHitActorInfo( ePawn P, out vector HitLocation, out vector HitNormal, bool bSafe )
{					 
	local vector StartTrace;
	local Actor HitActor;
	local Actor A;

	StartTrace		= P.Location; 
	StartTrace.Z   += P.BaseEyeHeight;

	// let us hit any visible actor which might not have the correct collision settings
	if( !bSafe )
	{
		foreach P.AllActors( Class'Actor', A )
		{
			if( !A.bHidden )
			{
				// this spams the old collision settings
  				A.SetCollision( true, true, true );
			}
		}		
	}

	HitActor = TraceRecursive( P, HitLocation, HitNormal, StartTrace, true, 0.0, vector(P.GetViewRotation()) );

	// let us hit any visible actor which might not have the correct collision settings
	if( !bSafe )
	{
		foreach P.AllActors( Class'Actor', A )
		{
			if( !A.bHidden )
			{
				A.SetCollision( A.default.bCollideActors, A.default.bBlockActors/*, A.default.bBlockPlayers*/ );
			}
		}		
	}

	return HitActor;		
}


//=============================================================================

static function Actor GetHitActor( ePawn P, bool bSafe )
{					 
	local vector HitLocation, HitNormal;

	return GetHitActorInfo( P, HitLocation, HitNormal, bSafe );
}

//=============================================================================
// Helper function for FindBestTarget.
// Returns the cosine of the angle between v1 and v2.
//=============================================================================

static function float FindCosAngle( vector v1, vector v2 )
{
    local float CosAngle;
    CosAngle = ( v1 dot v2 ) / ( VSize( v1 ) * VSize( v2 ) );
    return CosAngle;
}



//=============================================================================
// Returns the sin of the given angle.
// Angle is in degrees.
//=============================================================================

static function float DSin( float Angle )
{
	return Sin( Angle * PI / 180.0 );
}



//=============================================================================
// Returns the cos of the given angle.
// Angle is in degrees.
//=============================================================================

static function float DCos( float Angle )
{
	return Cos( Angle * PI / 180.0 );
}




//=============================================================================
// Returns true if the given vectors are aproximately equal.
// Fix MWP: This should probably be added to the engine.
//=============================================================================

static function bool VectorAproxEqual( vector FirstVector, vector SecondVector )
{
	return ( ( FirstVector.x ~= SecondVector.x ) &&
			( FirstVector.y ~= SecondVector.y ) &&
			( FirstVector.z ~= SecondVector.z ) );
}



//=============================================================================

static function SwapInt( out int Num1, out int Num2 )
{
	local int temp;

	temp = Num1;
	Num1 = Num2;
	Num2 = temp;
}



//=============================================================================

static function SwapFloat( out float Num1, out float Num2 )
{
	local float temp;

	temp = Num1;
	Num1 = Num2;
	Num2 = temp;
}



//=============================================================================
// Linearly scales odds between min/max odds given a distance and corresponding
// min/max distances. Odds can increase (MinOdds <= MaxOdds or decrease (MinOdds 
// > MaxOdds) with distance.
//=============================================================================
// Distance:		Distance to use.
// MinDistance:		Minimum distance.
// MaxDistance:		Maximum distance.
// MinOdds:			Odds to use at min distance.					(Default 0.0)
// MaxOdds:			Odds to use at max distance.					(Default 1.0)
//=============================================================================
// Returns:			Odds to use.
//=============================================================================

static function float RandDistance( float Distance, float MinDistance, float MaxDistance, optional float MinOdds, optional float MaxOdds )
{
	local float DistanceOdds;
	local float LinearScaleFactor;
	local float DeltaDistance;

	if( MaxOdds < MinOdds )
	{
		class'LegendActorComponent'.static.SwapFloat( MinOdds, MaxOdds );
		DeltaDistance = ( MaxDistance - Distance );
	}
	else
	{
		DeltaDistance = ( Distance - MinDistance );
	}

	// odds increase with distance	
	if( Distance >= MaxDistance )
	{
		DistanceOdds = MaxOdds;
	}
	else if( Distance <= MinDistance )
	{
		DistanceOdds = MinOdds;
	}
	else
	{
    	LinearScaleFactor	= ( MaxOdds - MinOdds ) / ( MaxDistance - MinDistance );
		DistanceOdds		= DeltaDistance*LinearScaleFactor + MinOdds;
	}

	return DistanceOdds;
}

//=============================================================================
// PawnCanSeeActor:
//
// Returns true if Actor is not entirely occluded by geometry from the given
// pawn's point of view. If bTraceActors is true, other actors will block the
// pawn's view of the TraceToActor.
//
// Determines whether a Pawn can see an actor, i.e. whether any line from the
// Pawn's eyes (BaseEyeHeight) to the Actor exists which doesn't hit any 
// geometry or another actor (if bTraceActors is true) and is within the 
// Pawn's FOV relative to his ViewRotation.
//
//=============================================================================
// TraceFromPawn:	Pawn to trace from
// TraceToActor:	Actor to trace to
// FOVCos:			(optional) cos of FOV of Pawn (1/2 of the total FOV), 
//					defaults to Pawn's PeripheralVision.
//
//=============================================================================
// Notes:
// 
// There's got to be a better way to do this.  Tim has to do this when he 
// draws the actors on the screen.  Is there any way we can piggy-back off
// of that code?
//
// CanSee is fubar: reports true even if facing 180 degrees away, but close,
// could be related to the fact that CanSee also takes noises into account...
// LineOfSightTo is fubar: doesn't take actor's into account?
//
// Will return true if the pawn can see the bottom, center or top of the given
// actor's collision cylinder. Currently only traces to the horizontal center 
// of the actor, doesn't currently check left and right sides.
//
// TBD: do left-left and right-right checks as well to handle case where 
// actors can only see each other's left/right side.
//
// Seems this can fail for small (fast?) actors e.g. tracing from a Pawn to a 
// dart projectile which was maybe 200 units in front consistently fails to
// hit the dart.


static function bool PawnCanSeeActor( Pawn TraceFromPawn, Actor TraceToActor, optional float FOVCos, optional bool bTraceActors )
{
	local bool bCanSee;

	local vector StartPoint;
	local vector EndPoints[3];

	local int i;

	local Actor HitActor;
	local vector HitLocation, HitNormal;
	local float CollisionRads, CollisionHeit;
	//
	// Is the Actor even in the Pawns FOV?
	//

	if( !class'LegendActorComponent'.static.ActorIsInPawnFOV( TraceFromPawn, 
											  TraceToActor,
											  FOVCos ) )
	{
	  	return false;
	}

	//
	// Can we trace a line from the Pawn's eyes to the actor without hitting geometry?
	//
	
	StartPoint = TraceFromPawn.Location + TraceFromPawn.BaseEyeHeight*vect(0,0,1);
	EndPoints[0] = TraceToActor.Location;	// pawn eyes->actor center
	TraceToActor.GetBoundingCylinder(CollisionRads, CollisionHeit);
	EndPoints[1] = TraceToActor.Location + CollisionHeit*vect(0,0,1);	// pawn eyes->actor top (head)
	EndPoints[2] = TraceToActor.Location - CollisionHeit*vect(0,0,1);	// pawn eyes->actor bottom (feet)

	for( i = 0; i < ArrayCount(EndPoints); i++ )
	{
		HitActor = TraceRecursive
			(	TraceFromPawn,
				HitLocation,
				HitNormal,
				StartPoint,
				bTraceActors,,
				Normal(EndPoints[i] - StartPoint),		// direction.
				VSize(EndPoints[i] - StartPoint)		// limit.
			);

		// if bTraceActors is true, trace should hit TraceToActor if pawn can see TraceToActor
		// if bTraceActors is false, trace should hit nothing if pawn can see TraceToActor
		bCanSee = ( HitActor == None || HitActor == TraceToActor );

    	if( bCanSee )	
			break;		// No need to go on.
	}

	return bCanSee;
}



//=============================================================================
// ActorCanSeeActor:
//
// Returns true if the first Actor has an uninterrupted LOS to the second actor.
//
//=============================================================================
// TraceFromActor:	Actor to trace from
// TraceToActor:	Actor to trace to
//=============================================================================

static function bool ActorCanSeeActor( Actor TraceFromActor, Actor TraceToActor )
{
	local vector TraceHitLocation, TraceHitNormal;

	local bool bRetVal;
	
	// can we trace a line from the Pawn's eyes to the actor?
	bRetVal = (
		// pawn eyes->actor center
		TraceFromActor.Trace( 
			TraceHitLocation, 
			TraceHitNormal,		
			TraceToActor.Location, 
			TraceFromActor.Location, 
			true ) == TraceToActor );
			
	return bRetVal;
}



//=============================================================================
// ActorVisibleToAnyPawn:
//
// Returns true if the given actor is visible to any pawn in the game which 
// matches the given class (e.g. all ePawns, WOTPawns, Trollocs...).
//
// Takes into account pawns' FOVs.
//
//=============================================================================
// A:			Target Actor
// pClass:		Class to iterate.
// FOVCos:		(optional) cos of FOV of Pawn (1/2 of the total FOV), 
//				defaults to Pawn's PeripheralVision.
//
//=============================================================================

static function bool ActorVisibleToAnyPawn( Actor A, class<Pawn> pClass, optional float FOVCos )
{
	local Actor Other;

	// iterate all players
	foreach A.AllActors( pClass, Other )
	{
		if( class'LegendActorComponent'.static.PawnCanSeeActor( Pawn(Other), A, FOVCos, true ) )
		{
			return true;
		}
	}
	
	// no player can see actor
	return false;
}



//=============================================================================
// Returns true if we can reach our destination without hitting any geometry.
//=============================================================================

//static function bool IsLocationDirectlyReachable( Actor ReachingActor, vector Destination )
//{
//	local vector TraceHitLocation, TraceHitNormal;
//	local Actor TraceHitActor;
	
//	if( ReachingActor.IsA( 'Pawn' ) )
//	{
//		ePawn( ReachingActor ).PointReachable( Destination );
//	}
//	else
//	{
//		TraceHitActor = ReachingActor.Trace( TraceHitLocation, TraceHitNormal,
//				Destination, ReachingActor.Location, false /*Don't trace Actors*/ );
//	}

//	return ( TraceHitActor == None );
//}

//static function bool IsActorDirectlyReachable( Actor ReachingActor, Actor Destination )
//{
//	local vector TraceHitLocation, TraceHitNormal;
//	local Actor TraceHitActor;
	
//	if( ReachingActor.IsA( 'Pawn' ) )
//	{
//		ePawn( ReachingActor ).ActorReachable( Destination );
//	}
//	else
//	{
//		TraceHitActor = ReachingActor.Trace( TraceHitLocation, TraceHitNormal,
//				Destination.Location, ReachingActor.Location, false /*Don't trace Actors*/ );
//	}

//	return ( TraceHitActor == None );
//}

//-----------------------------------------------------------------------------
// Allows you to check if an object is in your view.
//-----------------------------------------------------------------------------
// ViewVec:	A vector facing "forward"						(relative to your location.)
// DirVec:	A vector pointing to the object in question.	(relative to your location.)
// FOVCos:	Cosine of how many degrees the target must be within to be seen.
//-----------------------------------------------------------------------------
// REQUIRE: FOVCos > 0
// NOTE: While normalization is not required for ViewVec or DirVec, it helps
// if both vectors are about the same size.
//-----------------------------------------------------------------------------
static function bool IsInViewCos( vector ViewVec, vector DirVec, float FOVCos )
{
	local float CosAngle;		//cosine of angle from object's LOS to WP
    
    CosAngle = FindCosAngle( ViewVec, DirVec );

	//The first test makes sure the target is within the firer's front 180o view.
	//The second test might look backwards, but it isn't.  Since cos(0) == 1,
	//as the angle gets smaller, CosAngle *increases*, so an angle less than
	//the max will have a larger cosine value.
	
	return (0 <= CosAngle && FOVCos < CosAngle);
}

//-----------------------------------------------------------------------------
// Allows you to check if an object is in your view.
//-----------------------------------------------------------------------------
// ViewVec:	A vector facing "forward"						(relative to your location.)
// DirVec:	A vector pointing to the object in question.	(relative to your location.)
// FOV:		How many degrees the target must be within to be seen.
//-----------------------------------------------------------------------------
// REQUIRE: FOV < 90
// NOTE: While normalization is not required for ViewVec or DirVec, it helps
// if both vectors are about the same size.
//-----------------------------------------------------------------------------
static function bool IsInView( vector ViewVec, vector DirVec, float FOV )
{
	return class'LegendActorComponent'.static.IsInViewCos( ViewVec, DirVec, cos( ( 2 * Pi ) / ( 360 / FOV ) ) );
}



//=============================================================================
// ActorIsInPawnFOV
// Returns true if A is in P's FOV.
// If FOVCos is not given (~= 0.0), uses the Pawn's PeripheralVision setting.
//=============================================================================

static event /*function*/ bool ActorIsInPawnFOV( Pawn P, Actor A, optional float FOVCos )
{
	local rotator InViewRotation;

	if( FOVCos ~= 0.0 )
	{
		FOVCos = P.PeripheralVision;
	}

	if( ePawn(P) != None )
	{
		InViewRotation = P.GetViewRotation(); // use ViewRotation for players only
	}
	else
	{
		InViewRotation = P.Rotation;	 // ViewRotation not really used for NPCs
	}

	return (class'LegendActorComponent'.static.IsInViewCos( Vector(InViewRotation), 
										   A.Location - P.Location, 
										   FOVCos ));
}

//------------------------------------------------------------------------------
// Returns vector perpendicular to given vector, ignoring z component.

static function vector PerpendicularXY( vector V )
{
	local vector VOut;

	VOut.x = -V.y;
	VOut.y =  V.x;

	return VOut;
}

//------------------------------------------------------------------------------
// Sets given rotation parameter (yaw, pitch, roll) to 0 or 32768 depending on
// which value is closer to the original value.

static function ZeroRotParam( out int Val )
{
	Val = Val & 0xFFFF;

	if( Val < 16384 || Val >= 49152 )
	{
		Val = 0;
	}
	else
	{
		Val = 32768;
	}
}

//------------------------------------------------------------------------------
// Sets given rotation parameter (yaw, pitch, roll) to given value or given value
// plus 32768 depending on which is closer to the original value.

static function SetLandedRotParam( out int Val, int DesiredVal )
{
	// normalize angle
	Val = Val & 0xFFFF;

	if( abs(Val - DesiredVal) < 16384 || abs(Val - DesiredVal) >= 49152 )
	{
		Val = DesiredVal;
	}
	else
	{
		Val = DesiredVal + 32768;
	}

	// normalize angle
	Val = Val & 0xFFFF;
}


DefaultProperties
{
}
