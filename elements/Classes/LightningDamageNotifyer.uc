//------------------------------------------------------------------------------
// LightningDamageNotifyer.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 3 $
//
// Description:	Notifies our SourceAngreal when our Owner takes damage.
//------------------------------------------------------------------------------
// How to use this class:
//------------------------------------------------------------------------------
class LightningDamageNotifyer extends Reflector;

/////////////////////////
// Overriden Functions //
/////////////////////////

//------------------------------------------------------------------------------
event TakeDamage(int DamageAmount, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	AngrealInvLightning(SourceAngreal).NotifyDamagedBy( EventInstigator.Pawn );

	// Reflect funciton call to next reflector in line.
	Super.TakeDamage( DamageAmount, EventInstigator, HitLocation, Momentum, DamageType );
}
defaultproperties
{
    Priority=64

    bRemovable=False

    bDisplayIcon=False

}