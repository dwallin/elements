//------------------------------------------------------------------------------
// LightningLeech.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 3 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class LightningLeech extends Leech;

const MAX_WAYPOINTS = 15;
// Damage vars.
var DecreaseHealthLeech DHL;
var float DamagePerSecond;

// Linked list support.  (maintained by AngrealInvLightning)
var LightningLeech NextLightningLeech;

// Notifiers.
var NotifyInWaterReflector WaterNotifier;

// Our persistant streak object.
var LightningStreak02 PathStreak;
var LightningStreak02 chain[64];

// Type of streak to use.
var() class<Streak> StreakType;

// Impact visuals.
var LSImpact ImpactEffect;
var() float ImpactRollRate;
var float ImpactRoll;

// Type of sparks to use.
var() class<ParticleSprayer> SparkType;
var ParticleSprayer Sparks;

// Endpoints of streak.
var vector SourceLocation;
var vector TargetLocation;
var Actor SourceActor;


/** path points to travel to */
var vector WayPoints[MAX_WAYPOINTS];
/** total number of valid points in WayPoints list */
var repnotify int NumPoints;
/** current position in WayPoints list */
var int Position;

replication
{
	// Fix ARL: remove extra checks when Tim fixes the replication code.

	// Send SourceActor to the client when relevant.
	if( Role==ROLE_Authority && (SourceActor==None /*|| SourceActor.bNetRelevant*/) ) // Remove when Tim fixes Actor variable replication code.
		SourceActor;

	// Send the location updates to the client if Instigator is not relevant.
	if( Role==ROLE_Authority && SourceActor!=None /*&& !SourceActor.bNetRelevant*/ )
		SourceLocation;

	// Send the location updates to the client if Owner is not relevant.
	if( Role==ROLE_Authority && Owner!=None /*&& !Owner.bNetRelevant*/ )
		TargetLocation;
}

//------------------------------------------------------------------------------
simulated function PreBeginPlay()
{
	local int i;
	Super.PreBeginPlay();

	// Create visuals.
	ImpactEffect = Spawn( class'LSImpact' );
	PathStreak = Spawn( class'LightningStreak02', Instigator );
	Sparks = Spawn( SparkType );
	for(i=0;i<ArrayCount(chain);i++)
	{
		chain[i] = Spawn( class'LightningStreak02', Instigator );
	}
}

//------------------------------------------------------------------------------
reliable server function AttachTo( Pawn NewHost )
{
	Super.AttachTo( NewHost );

	if( Owner != None && Owner == NewHost )
	{
		// Let our owner know we were successfully attached to someone.
		if( AngrealInvLightning(SourceAngreal) != None )
		{
			AngrealInvLightning(SourceAngreal).NotifyAttached( Self );
		}

		// We don't need no stinkin' LeechAttacherEffect because we is a Leech.
		DHL = Spawn( class'DecreaseHealthLeech' );
		DHL.InitializeWithLeech( Self );
		DHL.AffectResolution = 1.0 / DamagePerSecond;
		DHL.AttachTo( Pawn(Owner) );
		
		InstallNotifiers();
	}
}

//------------------------------------------------------------------------------
reliable server function UnAttach()
{
	if( AngrealInvLightning(SourceAngreal) != None && Pawn(Owner) != None )
	{
		AngrealInvLightning(SourceAngreal).RemoveVictim( Pawn(Owner), true );
	}
	UnInstallNotifiers();
	Super.UnAttach();
}

//------------------------------------------------------------------------------
simulated function AffectHost( optional int Iterations )
{
	if( Pawn(Owner) == None || Pawn(Owner).Health <= 0 )
	{
		UnAttach();
		Destroy();
	}
}

//------------------------------------------------------------------------------
function SetSourceAngreal( AngrealInventory Source )
{
	Super.SetSourceAngreal( Source );
	SourceActor = Instigator;
}

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	local int i, segments;
	local vector Dir;
	local rotator ImpactRot;
	local int Start;
	local PlayerController P;
	local Actor HitActor;
	local Vector HitLocation,HitNormal;
	local float chain_distance, segment_distance;

	// Validate Owner.
	if( Owner == None )
	{
		UnAttach();
		Destroy();
		return;
	}

	// Update damage rate.
	if( Role == ROLE_Authority && DHL != None )
	{
		DHL.AffectResolution = 1.0 / DamagePerSecond;
	}

	// Server: Send new locations to clients.
	// Client: Store location in case our Instigator or Owner
	//         becomes un-relevant.
	if( SourceActor != None )
	{
		SourceLocation = SourceActor.Location;
	}
	if( Owner != None )
	{
		TargetLocation = Owner.Location;
	}
	
	// Server send Instigator (SourceActor) to clients.
	if( Role == ROLE_Authority )
	{
		SourceActor = Instigator;
	}

	// Calculate a normal vector that points from the 
	// victim to the castor.
	Dir = Normal(SourceLocation - TargetLocation);
	Dir.z = 0.0;
	//if( PathStreak != None )
	//{
		//PathStreak.SetBalePawn(Instigator, (Instigator.Location + (Dir * vect(0.00,0.00, 20.00) >> Instigator.GetViewRotation())), (Owner.Location));
		//PathStreak.ScaleGlow = ScaleGlow;
	//}
	// Connect castor and victim with lightning streak.
	if (Role == ROLE_Authority)
	{
		P = PlayerController(Instigator.Owner);
		if (P == None || P.Pawn == None)
		{
			Destroy();
		}
		else
		{
			P.FindPathTo(TargetLocation + (Dir * P.Pawn.GetCollisionRadius()));
			WayPoints[0] = SourceLocation + P.Pawn.GetCollisionHeight() * vect(0,1,0) + 200.0 * vector(P.Rotation);
			HitActor = Trace(HitLocation, HitNormal, WayPoints[0], Location, false);
			if (HitActor != None)
			{
				WayPoints[0] = HitLocation;

			}
			NumPoints++;

			if (P.RouteCache[0] != None && P.RouteCache.length > 1 && P.ActorReachable(P.RouteCache[1]))
			{
				Start = 1;
				chain[0].SetBalePawn(Instigator, (Instigator.Location + (Dir * vect(0.00,0.00, 20.00) >> Instigator.GetViewRotation())), (Owner.Location));
				for (i = Start; NumPoints < MAX_WAYPOINTS && i < P.RouteCache.length && P.RouteCache[i] != None; i++)
				{
					if (i < P.RouteCache.Length)
					{
						WayPoints[NumPoints++] = P.RouteCache[i].Location + P.Pawn.GetCollisionHeight() * Vect(0,1,0);
						chain[i].SetBalePawn(Instigator, (chain[i-1].Location + (Dir * vect(0.00,0.00, 20.00) >> Instigator.GetViewRotation())), (P.RouteCache[i].Location));
					}
					else
					{
						chain[i].SetBalePawn(Instigator, (chain[i-1].Location + (Dir * vect(0.00,0.00, 20.00) >> Instigator.GetViewRotation())), (Owner.Location));
					}
				}
			}
			else
			{
				chain[0].SetBalePawn(Instigator, (Instigator.Location + (Dir * vect(0.00,0.00, 20.00) >> Instigator.GetViewRotation())), (Owner.Location));
			}
		}
	}

	// Update Sparks location.
	if( Sparks != None )
	{
		Sparks.SetLocation( PathStreak.Location );
		Sparks.SetRotation( rotator(Dir) );
	}

	// Realign effects.
	if( ImpactEffect != None )
	{
		ImpactRoll += ImpactRollRate * DeltaTime;
		ImpactRot = rotator(vect(0,1,0));
		ImpactRot.Roll = ImpactRoll;
		ImpactEffect.SetLocation( TargetLocation );
		ImpactEffect.SetRotation( ImpactRot );
		ImpactEffect.ScaleGlow = ScaleGlow;
	}

	Super.Tick( DeltaTime );
	
}
//------------------------------------------------------------------------------
simulated function vector GetStartLoc()
{
	local vector Loc;
	Loc = Instigator.Location + vect(0.f,0.f,-30.f);
	Loc.Z += Instigator.BaseEyeHeight;
	return Loc;
}

//------------------------------------------------------------------------------
simulated function vector GetEndLoc()
{
	local vector Loc;
	Loc = GetStartLoc() + ((vect(1.f,0.f,0.f) * 128) >> Instigator.GetViewRotation());
	return Loc;
}
//------------------------------------------------------------------------------
simulated function Destroyed()
{
	local int i;

	// Game logic (server-side only).
	if( Role == ROLE_Authority )
	{
		if( DHL != None )
		{
			DHL.UnAttach();
			DHL.Destroy();
		}

/* -- OBE
		if( Role == ROLE_Authority && ProjLeechArtifact(SourceAngreal) != None )
		{
			ProjLeechArtifact(SourceAngreal).NotifyDestinationLost();
		}	
*/
	}
	
	if( ImpactEffect != None )
	{
		ImpactEffect.Destroy();
		ImpactEffect = None;
	}

	if( Sparks != None )
	{
		Sparks.LifeSpan = 2.000000;
		Sparks.bOn = false;
		Sparks = None;
	}

	if( chain[0] != none)
	{
		for(i=0;i<ArrayCount(chain);i++)
		{
			if (chain[i] != none)
			{
				chain[i].Destroy();
				chain[i] = none;
			}
		}
	}

	Super.Destroyed();
}

//------------------------------------------------------------------------------
function InstallNotifiers()
{
	// WaterNotifier.
	if( WaterNotifier == None )
	{
		WaterNotifier = Spawn( class'NotifyInWaterReflector' );
		WaterNotifier.InitializeWithLeech( Self );
	}

	if( WaterNotifier.Owner != None )
	{
		`warn( "WaterNotifier already installed in ("$WaterNotifier.Owner$")." );
		WaterNotifier.UnInstall();
	}

	WaterNotifier.Install( Pawn(Owner) );
}

//------------------------------------------------------------------------------
function UnInstallNotifiers()
{
	// WaterNotifier.
	if( WaterNotifier != None )
	{
		WaterNotifier.UnInstall();
	}
	else
	{
		`warn( "Missing WaterNotifier!" );
	}
}
defaultproperties
{

    StreakType=Class'LightningStreak02'

    ImpactRollRate=25000.00

    SparkType=Class'LightningSparks'

    AffectResolution=0.20

    bDeleterious=True

    RemoteRole=1

    bAlwaysRelevant=True

    SoundRadius=64

    SoundVolume=255

    AmbientSound=SoundCue'WOT.Sounds.LoopLS_Cue'

    NetPriority=6.00
	bNetTemporary=false

}