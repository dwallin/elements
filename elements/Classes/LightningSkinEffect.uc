//------------------------------------------------------------------------------
// LightningSkinEffect.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 2 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class LightningSkinEffect extends Effects;
struct LightningRingData
{
	var LSRing Ring;
	var float RiseRate;
	var vector Height;
};

var LightningRingData Rings[5];
var int RingIndex;
var DynamicLightEnvironmentComponent LightEnvironment;
var() float MinRingRiseRate, MaxRingRiseRate;	// Speed rings move upward.
var() float MinRingSpawnTime, MaxRingSpawnTime;	// Time between spawning rings.
var float NextRingSpawnTime;
var MaterialInstanceConstant MatInst;
var int TextureIndex;
var() Material LightningTextures[22];
var StaticMeshComponent LSEffectMesh;
simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	InitMaterialInstance();
}

function InitMaterialInstance()
{
   MatInst = new(None) Class'MaterialInstanceConstant';
   MatInst.SetParent(LSEffectMesh.GetMaterial(0));
}
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A01.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A02.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A03.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A04.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A05.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A06.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A07.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A08.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A09.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A10.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A11.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A12.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A13.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A14.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A15.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A16.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A17.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A18.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A19.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A20.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A21.pcx GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\LSE_A22.pcx GROUP=Effects

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	local int i;
	local vector BaseLocation;
	local float MaxHeight;
	local float FadeHeight;

	Super.Tick( DeltaTime );

	// Hide from First person perspective.
	if( ePawn(Owner) != None )
	{
		bOwnerNoSee = ePawn(Owner).ViewTarget == None;
	}

	// Why won't Unreal animate these for me?
	TextureIndex = (TextureIndex + 1) % ArrayCount(LightningTextures);
	Instigator.Mesh.SetMaterial( 0, LightningTextures[ TextureIndex ] );
	Instigator.Mesh.SetMaterial( 1, LightningTextures[ TextureIndex ] );

	// Create new rings.
	if( WorldInfo.TimeSeconds >= NextRingSpawnTime )
	{
		NextRingSpawnTime = WorldInfo.TimeSeconds + RandRange( MinRingSpawnTime, MaxRingSpawnTime );

		RingIndex = (RingIndex + 1) % ArrayCount(Rings);
		if( Rings[ RingIndex ].Ring != None )
		{
			Rings[ RingIndex ].Ring.Destroy();
		}
		Rings[ RingIndex ].Ring = Spawn( class'LSRing' );
		Rings[ RingIndex ].RiseRate = RandRange( MinRingRiseRate, MaxRingRiseRate );
		Rings[ RingIndex ].Height = vect(0,0,0);
	}

	// Update exiting rings.
	BaseLocation = Owner.Location - (vect(0,0,1) * ePawn(Owner).GetCollisionHeight());
	MaxHeight = ePawn(Owner).GetCollisionHeight() * 2;
	FadeHeight = MaxHeight * 0.9;
	for( i = 0; i < ArrayCount(Rings); i++ )
	{
		if( Rings[i].Ring != None )
		{
			Rings[i].Height.Z += Rings[i].RiseRate * DeltaTime;

			if( Rings[i].Height.Z > MaxHeight )
			{
				Rings[i].Ring.Destroy();
				Rings[i].Ring = None;
			}
			else
			{
				Rings[i].Ring.SetLocation( BaseLocation + Rings[i].Height );
				Rings[i].Ring.SetDrawScale( Rings[i].Ring.DrawScale + 0.2 * DeltaTime );
				
				// Fade rings out last 10%
				if( Rings[i].Height.Z > FadeHeight )
				{
					Rings[i].Ring.ScaleGlow = (1.0 - ((Rings[i].Height.Z - FadeHeight) / (MaxHeight - FadeHeight))) * Rings[i].Ring.default.ScaleGlow;
				}
			}
		}
	}
}

//------------------------------------------------------------------------------
simulated function Destroyed()
{
	local int i;

	for( i = 0; i < ArrayCount(Rings); i++ )
	{
		if( Rings[i].Ring != None )
		{
			Rings[i].Ring.Destroy();
			Rings[i].Ring = None;
		}
	}
	Instigator.Mesh.SetMaterial( 0, Instigator.default.Mesh.GetMaterial(0) );
	Instigator.Mesh.SetMaterial( 1, Instigator.default.Mesh.GetMaterial(1) );

	Super.Destroyed();
}

//------------------------------------------------------------------------------
simulated function SetTexture( Material T )
{
	local int i;

	for( i = 0; i < ArrayCount(MultiSkins); i++ )
	{
		MultiSkins[i] = T;
	}
}
defaultproperties
{
Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
	DrawType=2
    ScaleGlow=0.50
    AmbientGlow=200
    Fatness=157
	LightType=1
    LightEffect=13
    LightBrightness=255
    LightHue=204
    LightSaturation=204
    LightRadius=12
End Object
Components.Add(MyLightEnvironment)
LightEnvironment=MyLightEnvironment

    LightningTextures(0)=Material'WOT.Icons.LSE_A01_MAT'

    LightningTextures(1)=Material'WOT.Icons.LSE_A02_MAT'

    LightningTextures(2)=Material'WOT.Icons.LSE_A03_MAT'

    LightningTextures(3)=Material'WOT.Icons.LSE_A04_MAT'

    LightningTextures(4)=Material'WOT.Icons.LSE_A05_MAT'

    LightningTextures(5)=Material'WOT.Icons.LSE_A06_MAT'

    LightningTextures(6)=Material'WOT.Icons.LSE_A07_MAT'

    LightningTextures(7)=Material'WOT.Icons.LSE_A08_MAT'

    LightningTextures(8)=Material'WOT.Icons.LSE_A09_MAT'

    LightningTextures(9)=Material'WOT.Icons.LSE_A10_MAT'

    LightningTextures(10)=Material'WOT.Icons.LSE_A11_MAT'

    LightningTextures(11)=Material'WOT.Icons.LSE_A12_MAT'

    LightningTextures(12)=Material'WOT.Icons.LSE_A13_MAT'

    LightningTextures(13)=Material'WOT.Icons.LSE_A14_MAT'

    LightningTextures(14)=Material'WOT.Icons.LSE_A15_MAT'

    LightningTextures(15)=Material'WOT.Icons.LSE_A16_MAT'

    LightningTextures(16)=Material'WOT.Icons.LSE_A17_MAT'

    LightningTextures(17)=Material'WOT.Icons.LSE_A18_MAT'

    LightningTextures(18)=Material'WOT.Icons.LSE_A19_MAT'

    LightningTextures(19)=Material'WOT.Icons.LSE_A20_MAT'

    LightningTextures(20)=Material'WOT.Icons.LSE_A21_MAT'

    LightningTextures(21)=Material'WOT.Icons.LSE_A22_MAT'

    MinRingRiseRate=32.00

    MaxRingRiseRate=64.00

    MinRingSpawnTime=0.40

    MaxRingSpawnTime=1.00

    bAnimByOwner=True

    bNetTemporary=False

    bTrailerSameRotation=True

    Physics=11

    RemoteRole=1

    DrawType=2

    Style=3

    Texture=Texture2d'WOT.Icons.LSE_A01'

    Skin=Material'WOT.Icons.LSE_A01_Mat'

    ScaleGlow=0.50

    AmbientGlow=200

    Fatness=157

    bUnlit=True

    MultiSkins(0)=Material'WOT.Icons.LSE_A01_Mat'

    MultiSkins(1)=Material'WOT.Icons.LSE_A01_Mat'

    MultiSkins(2)=Material'WOT.Icons.LSE_A01_Mat'

    MultiSkins(3)=Material'WOT.Icons.LSE_A01_Mat'

    MultiSkins(4)=Material'WOT.Icons.LSE_A01_Mat'

    MultiSkins(5)=Material'WOT.Icons.LSE_A01_Mat'

    MultiSkins(6)=Material'WOT.Icons.LSE_A01_Mat'

    MultiSkins(7)=Material'WOT.Icons.LSE_A01_Mat'

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=204

    LightSaturation=204

    LightRadius=12
	DrawScale=1.0

}