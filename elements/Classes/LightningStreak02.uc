//================================================================================
// LightningStreak02.
//================================================================================

class LightningStreak02 extends UTReplicatedEmitter;
var class<UDKExplosionLight> ExplosionLightClass;
var() float StreakLength;
var Pawn BalePawn;
var float InitialLifeSpan;
var DynamicLightEnvironmentComponent BeamLightEnvironment;
var Vector Start;
var Vector End;

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
}

function SetBalePawn(Pawn BalePawns, Vector Starts, Vector Ends)
{
	self.BalePawn = BalePawns;
    self.BalePawn.Weapon = BalePawns.Weapon;
    self.Start = Starts;
    self.End = Ends;
}

simulated function Tick (float DeltaTime)
{
	//if( BalePawn == None || BalePawn.Health <= 0 || BalePawn.IsInPain() || BalePawn.Weapon == none || !(AngrealInvLightning(AngrealInventory(self.BalePawn.Weapon)).bCasting))
	//{
        SetLocation(Start);
        SetVectorParameter('LinkBeamEnd', End);
	//}
    //else
    //{
     //   return;
    //}
}
defaultproperties
{
bCanBeDamaged=true
bTicked=true
bUpdateSimulatedPosition=true
bCollideComplex=false
bDestroyInPainVolume=true
bDestroyOnSystemFinish=true
bForceNetUpdate=true
bLockLocation=true
bPostRenderIfNotVisible=true
bPostUpdateTickGroup=true
StreakLength=128.00
ExplosionLightClass=class'BaleImpactLight'
LifeSpan=100.00
DrawScale=0.50
TickGroup=TG_PreAsyncWork
bNetDirty=true
bGameRelevant=true
bAlwaysRelevant=true
bAlwaysTick=true
bReplicateInstigator=true
bReplicateRigidBodyLocation=true
LifeSpan=0.0
	//Begin Object class=StaticMeshComponent Name=LSBoltMesh
	//	//StaticMesh=StaticMesh'WOT.Meshes.LSBolt'
	//	//Materials[0]=Material'WOT.Icons.LB_A01_Mat'
		
	//	LightEnvironment=MyLightEnvironment
	//End Object
	//CollisionComponent=LSBoltMesh
	//Components.Add(LSBoltMesh)
	//StreakMesh=LSBoltMesh
    SegmentLength=64.00

    bRandomizeTextures=True
	EmitterTemplate=ParticleSystem'WOT.Particles.BlueBeam'
    Textures(0)=Material'WOT.Icons.LB_A01_Mat'

    Textures(1)=Material'WOT.Icons.LB_A02_Mat'

    Textures(2)=Material'WOT.Icons.LB_A03_Mat'

    Textures(3)=Material'WOT.Icons.LB_A04_Mat'

    Textures(4)=Material'WOT.Icons.LB_A05_Mat'

    Textures(5)=Material'WOT.Icons.LB_A06_Mat'

    Textures(6)=Material'WOT.Icons.LB_A07_Mat'

    Textures(7)=Material'WOT.Icons.LB_A08_Mat'

    Textures(8)=Material'WOT.Icons.LB_A09_Mat'

    Textures(9)=Material'WOT.Icons.LB_A10_Mat'

    NumTextures=10
    DrawType=2
    Skin=Material'WOT.Icons.LB_A01_Mat'

    AmbientGlow=250

    bUnlit=True

}