//================================================================================
// LineStreakSegment.
//================================================================================

class LineStreakSegment extends TracerSeg;

defaultproperties
{
    SegmentLength=510.00

    DrawType=2

    Style=3

    Texture=None

    Mesh=SkeletalMesh'WOT.Mesh.LSS010'

    DrawScale=16.00

    bUnlit=True

}