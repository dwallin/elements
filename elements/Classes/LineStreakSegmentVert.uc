//================================================================================
// LineStreakSegmentVert.
//================================================================================

class LineStreakSegmentVert extends TracerSeg;

defaultproperties
{
    SegmentLength=510.00

    DrawType=2

    Style=3

    Texture=None

    Skin=Material'WOT.Icons.JLSSGreen_Mat'

    Mesh=SkeletalMesh'WOT.Mesh.LSS010V'

    DrawScale=16.00

    bUnlit=True

}