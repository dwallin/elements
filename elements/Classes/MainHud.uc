class MainHUD extends BaseHUD;

const NumCrossHairs	= 21;

var TEXTURE2d TimersIcon[ 19 ];

var Texture2d HealthIcons;

//simulated function ChangeCrosshair( int d )
//{
//	Crosshair += d;
//	if( Crosshair >= NumCrossHairs ) 
//	{
//		Crosshair = 0;
//	}
//	else if( Crosshair < 0 ) 
//	{
//		Crosshair = NumCrossHairs - 1;
//	}
//}

simulated function PostBeginPlay() {
	super.PostBeginPlay();
}

simulated function DrawCrossHair( Canvas Canvas, int StartX, int StartY )
{
	//super.DrawCrossHair(Canvas, StartX, StartY);
	//C.Style = ERenderStyle.STY_Masked;
	Canvas.DrawColor.R = 255;
	Canvas.DrawColor.G = 255;
	Canvas.DrawColor.B = 255;
	Canvas.SetPos( StartX, StartY );
	//switch( Crosshair )
	//{
		//case 0:	 Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair1_0',  StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 1:  Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair2_0',  StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 2:  Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair3_0',  StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 3:  Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair4_0',  StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 4:  Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair5_0',  StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 5:  Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair6_0',  StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 6:  Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair7_0',  StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 7:  Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair8_0',  StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 8:  Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair9_0',  StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 9:  Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair10_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 10: Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair11_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 11: Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair12_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 12: Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair13_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		//Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair14_0', StartX, StartY, 16, 16, StartX, StartY );
		//case 14: Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair15_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 15: Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair16_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 16: Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair17_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 17: Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair18_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 18: Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair19_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		//case 19: Canvas.DrawTile( Texture2d'WOT.crosshair.Crosshair20_0', StartX, StartY, 16, 16, StartX, StartY ); break;
		// Crosshair21 == no crosshair
	//}
	//C.Style = ERenderStyle.STY_Normal;	
}


simulated function DrawCursor( Canvas Canvas)
{
	//local int StartX, StartY;

	//StartX = 0.5 * Canvas.ClipX - 8;
	//StartY = 0.5 * Canvas.ClipY - 8;
	//DrawCrossHair( StartX, StartY );
}


simulated function DrawStatusIcons( )
{
    local IconInfo I;
    local int GoodX;
    local int BadX;
	local int X;
	local int Y;
	local int Index;

    GoodX = IconSpacing + IconWidth + IconSpacing;
    BadX = ScaledSizeX - IconWidth - IconSpacing;
	Y = IconSpacing;
	if(ePawn(PlayerOwner.Pawn) != none) {
    for( I = ePawn(PlayerOwner.Pawn).FirstIcon; I != None; I = I.Next )
	{
		if( I.bGoodIcon ) 
		{
			X = GoodX;
			GoodX += IconWidth + IconSpacing;
		} 
		else 
		{
			X = BadX;
			BadX -= IconWidth + IconSpacing;
		}
		if( I.InitialDuration == 0 ) 
		{
			Index = 0;
		} 
		else 
		{
			Index = max( min( ( ArrayCount( TimersIcon ) - 1 ) * ( 1 - I.RemainingDuration / I.InitialDuration ), ArrayCount( TimersIcon ) -1 ), 0 );
		}

		Canvas.SetPos(X,Y);
		Canvas.DrawColor.R = 255;
		Canvas.DrawColor.G = 255;
		Canvas.DrawColor.B = 255;
		Canvas.DrawTile(I.Icon, IconWidth, IconWidth, 0, 0, 128, 128,, false, BLEND_Opaque); 
		Canvas.SetPos(X,Y);
		Canvas.DrawColor.R = 255;
		Canvas.DrawColor.G = 255;
		Canvas.DrawColor.B = 255;
		Canvas.DrawTile(TimersIcon[ Index ], IconWidth, IconWidth, 0, 0, 32, 32,,false,BLEND_Additive); 
		}
    }
}

simulated function DrawHealth( )
{
	Canvas.SetDrawColor (255,255,255);
	Canvas.SetPos(IconSpacing, IconSpacing);
	if (ePawn(PlayerOwner.Pawn) != none) {
		
		if ( ePawn(PlayerOwner.Pawn).Health > 80)
		{
			HealthIcons = Texture2D'WOT.Icons.H_Forsaken0';
		}
		else if ( ePawn(PlayerOwner.Pawn).Health > 60)
		{
			HealthIcons = Texture2D'WOT.Icons.H_Forsaken1';
		}
		else if ( ePawn(PlayerOwner.Pawn).Health > 40)
		{
			HealthIcons = Texture2D'WOT.Icons.H_Forsaken2';
		}
		else
		{
			HealthIcons = Texture2D'WOT.Icons.H_Forsaken3';
		}
		Canvas.SetDrawColor (255,255,255);
		Canvas.SetPos(8, 8);
		Canvas.DrawTile( HealthIcons, IconWidth, IconHeight, 0, 0,  IconWidth*2, IconHeight*2,, false, BLEND_Opaque);
		Canvas.SetPos(14, HealthOffsetY);
		Canvas.Font = Font'WOT.Fonts.UI_Fonts_Positec8';
		Canvas.DrawColor = GoldColor;
		Canvas.DrawText(ePawn(PlayerOwner.Pawn).Health, true);
		if( ePawn(PlayerOwner.Pawn).SeekerCount != 0 )
		{
			Canvas.SetDrawColor (255,255,255);
			Canvas.SetPos(8, 8);
			Canvas.DrawTile(Texture2d'WOT.Icons.SeekerFrame', IconWidth*2, IconHeight*2, 0, 0,  IconWidth*2, IconHeight*2,,false,BLEND_Additive);
		}
	}
}

simulated function DrawNormalHands() {
	super.DrawNormalHands();
}

simulated function PostRender(  )
{
	Super.PostRender();
	DrawNormalHands();
}

defaultproperties
{
    TimersIcon(0)=Texture2d'Wot.Icons.T32_01_0'

    TimersIcon(1)=Texture2d'Wot.Icons.T32_02_0'

    TimersIcon(2)=Texture2d'Wot.Icons.T32_03_0'

    TimersIcon(3)=Texture2d'WOT.Icons.T32_04_0'

    TimersIcon(4)=Texture2d'Wot.Icons.T32_05_0'

    TimersIcon(5)=Texture2d'Wot.Icons.T32_06_0'

    TimersIcon(6)=Texture2d'Wot.Icons.T32_07_0'

    TimersIcon(7)=Texture2d'Wot.Icons.T32_08_0'

    TimersIcon(8)=Texture2d'Wot.Icons.T32_09_0'

    TimersIcon(9)=Texture2d'Wot.Icons.T32_10_0'

    TimersIcon(10)=Texture2d'Wot.Icons.T32_11_0'

    TimersIcon(11)=Texture2d'Wot.Icons.T32_12_0'

    TimersIcon(12)=Texture2d'Wot.Icons.T32_13_0'

    TimersIcon(13)=Texture2d'Wot.Icons.T32_14_0'

    TimersIcon(14)=Texture2d'Wot.Icons.T32_15_0'

    TimersIcon(15)=Texture2d'Wot.Icons.T32_16_0'

    TimersIcon(16)=Texture2d'Wot.Icons.T32_17_0'

    TimersIcon(17)=Texture2d'Wot.Icons.T32_18_0'

    TimersIcon(18)=Texture2d'Wot.Icons.T32_19_0'

    AllowMinimizedInterface=false
}