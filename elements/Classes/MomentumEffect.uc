//================================================================================
// MomentumEffect.
//================================================================================

class MomentumEffect extends SingleVictimEffect;

var Vector Momentum;

function Initialize (Vector Momentum)
{
  self.Momentum = Momentum;
}

function Reset ()
{
  Super.Reset();
  Momentum = vect(0.00,0.00,0.00);
}

reliable server function Invoke ()
{
  Victim.AddVelocity(Momentum, Location, class'none');
}

function Invokable Duplicate ()
{
  local MomentumEffect NewInvokable;

  NewInvokable = MomentumEffect(Super.Duplicate());
  NewInvokable.Momentum = Momentum;
  return NewInvokable;
}

defaultproperties
{
    bDeleterious=True

}