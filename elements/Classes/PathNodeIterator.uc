//================================================================================
// PathNodeIterator.
//================================================================================

class PathNodeIterator extends LegendActorComponent
  Transient;

var NavigationPoint NodePath[256];
var int NodeCount;
var int NodeIndex;
var int NodeCost;
var Vector NodeStart;
var Vector NodeEnd;

final function BuildPath (Vector Start, Vector End);

final function NavigationPoint GetFirst ();

final function NavigationPoint GetPrevious ();

final function NavigationPoint GetCurrent ();

final function NavigationPoint GetNext ();

final function NavigationPoint GetLast ();

final function NavigationPoint GetLastVisible ();

defaultproperties
{
    bMovable=False

}