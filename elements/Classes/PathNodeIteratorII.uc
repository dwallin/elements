//================================================================================
// PathNodeIteratorII.
//================================================================================

class PathNodeIteratorII extends LegendActorComponent
  Transient;

var NavigationHandle NavHand;
var NavigationPoint NodePath[512];
var int NodeCount;
var int NodeIndex;
var int NodeCost;
var Vector NodeStart;
var Vector NodeEnd;
const MAX_PATH_NODES= 512;


function bool BuildPath (Vector Start, Vector End, optional bool bDesperate)
{

}

function NavigationPoint GetCurrent ()
{
}

final function NavigationPoint GetFirst ()
{
}

final function NavigationPoint GetLast ();

final function NavigationPoint GetNext ();

final function NavigationPoint GetPrevious ();

defaultproperties
{
    bMovable=False

}