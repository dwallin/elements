class ReflectEffect extends UTTimedPowerup;

var SoundCue UDamageFadingSound;
var vector OwnerLocation;
var bool bHandled;
var Projectile NewProjectile;
/** overlay material applied to owner */
var MaterialInterface OverlayMaterialInstance;

simulated static function AddWeaponOverlay(UTGameReplicationInfo GRI)
{
	GRI.WeaponOverlays[0] = default.OverlayMaterialInstance;
}

function GivenTo(Pawn NewOwner, optional bool bDoNotActivate)
{
	local UTPawn P;

	Super.GivenTo(NewOwner);
	
	P = UTPawn(NewOwner);

	if (P != None)
	{
		// apply UDamage overlay
		P.SetWeaponOverlayFlag(0);
	}
	// set timer for ending sounds
	SetTimer(TimeRemaining - 0.5f, false, 'PlayUDamageFadingSound');
}

/** called on a timer to play UDamage ending sound */
function PlayUDamageFadingSound()
{
	UDamageFadingSound.VolumeMultiplier=0.25f;
	Instigator.PlaySound(UDamageFadingSound);
	SetTimer(0.5f, false, 'PlayUDamageFadingSound');
}

simulated event Tick(float DeltaTime)
{
	local Projectile proj;
	local UTProjectile utproj;
	local float ColRadius, ColHeight;
	local Vector Extent;

	super.Tick(DeltaTime);

	if(Instigator != None)
	{
		OwnerLocation = Instigator.Location;
		Instigator.GetBoundingCylinder(ColRadius, ColHeight);
		Extent.X = ColRadius;
		Extent.Y = ColRadius;
		Extent.Z = ColHeight;
	}

	foreach AllActors (class'UTProjectile', utproj)
	{
		if (!(utproj.IsA('UTProj_Shockball')))
		{
			utproj.SetCollision(true, true, true);
			utproj.SetCollisionType(COLLIDE_BlockAllButWeapons);
			utproj.SetCollisionSize(ColRadius, ColHeight);
			utproj.bSuppressExplosionFX = true;
		}
	}

	foreach Instigator.VisibleCollidingActors (class'Projectile', proj, ColRadius * 2.f, OwnerLocation, FALSE, Extent, TRUE)
	{
		if(proj.Instigator != self.Instigator)
		{		
			if (proj.IsA('UTProj_Shockball'))
			{
				proj.Velocity = -(proj.Velocity);
				ePawn(Owner).PlaySound(SoundCue'WOT.Sounds.Reflected_Cue',,,true);
				utproj.bSuppressExplosionFX = false;
			}
			else if (proj.Velocity == vect(0,0,0))
			{
				proj.Velocity = -(OwnerLocation);
				ePawn(Owner).PlaySound(SoundCue'WOT.Sounds.Reflected_Cue',,,true);
				utproj.bSuppressExplosionFX = false;
			}
			else
			{
				StopsProjectile(proj);
				ePawn(Owner).PlaySound(SoundCue'WOT.Sounds.Reflected_Cue',,,true);
				utproj.bSuppressExplosionFX = false;
			}
		}
	}
}

//------------------------------------------------------------------------------
simulated function OnReflectedTouch(Projectile HitProjectile)
{
	bHandled = true;

	AdjustVelocity(HitProjectile);

	if(HitProjectile != None  && HitProjectile.Instigator != Owner)
	{
		HitProjectile.SetLocation(HitProjectile.Instigator.Location);

		if(Role == ROLE_Authority)
		{	
			ServerOnReflectedTouch(HitProjectile);

			if((HitProjectile) != None 
			&&	UDKProjectile(HitProjectile).SeekTarget	== HitProjectile.InstigatorController
			&&	(HitProjectile).GetDestination(HitProjectile.InstigatorController) == Owner.Location)
			{
				HitProjectile.GetDestination(HitProjectile.InstigatorController);
			}
		}
	}
}

function ServerOnReflectedTouch(Projectile HitProjectile)
{
	if( Owner != None )
	{
		ePawn(Owner).PlaySound(SoundCue'WOT.Sounds.Reflected_Cue',,,true);
	}
}

function DoAdjustVelocity(Projectile InProjectile, Projectile OutProjectile)
{
	OutProjectile.Velocity = VSize(InProjectile.Velocity) * vector(CalculateTrajectory( Owner, InProjectile.Instigator));
}

simulated function AdjustVelocity(out Projectile HitProjectile)
{
	if((HitProjectile != None && HitProjectile.Instigator == None))
	{
		HitProjectile.Velocity = -(HitProjectile.Velocity);
	}
	else
	{
		if(Role == ROLE_Authority)
		{
			NewProjectile = Spawn(HitProjectile.Class, HitProjectile.Owner, HitProjectile.Tag, HitProjectile.Location, HitProjectile.Rotation);
			DoAdjustVelocity(HitProjectile, NewProjectile);
		}

		HitProjectile.Destroy();
		HitProjectile = NewProjectile;
	}
}

//------------------------------------------------------------------------------
simulated function bool StopsProjectile(Projectile P)
{
	if(P == NewProjectile)
	{
		return true;		// We're not done with it yet.
	}

	bHandled = false;

	if(P != None)
	{
		OnReflectedTouch(P);
	}

	if( !bHandled )
	{
		super.StopsProjectile(P);
	}
	
	if(NewProjectile != None)
	{
		NewProjectile.SetCollision(false, false);
		NewProjectile = None;
	}
	return bHandled;
}

static function Rotator CalculateTrajectory (Actor Source, Actor Destination)
{
  local Vector Trajectory;
  local Vector PredictedLoc;
  local float diff;
  local float Time;

  if ((Source == None) || (Destination == None))
  {
    return rot(0,0,0);
  }
  diff = VSize(Destination.Location - Source.Location);
  Time = diff / 2000.f;
  PredictedLoc = Destination.Location + Destination.Velocity * Time;
  Trajectory = PredictedLoc - Source.Location;
  return rotator(Trajectory);
}

DefaultProperties
{
OverlayMaterialInstance=Material'Pickups.UDamage.M_UDamage_Overlay'
UDamageFadingSound=SoundCue'A_Pickups_Powerups.PowerUps.A_Powerup_UDamage_WarningCue'
TimeRemaining=5.00
TransitionDuration=0.5
WarningTime=1.0
}
