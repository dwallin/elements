//------------------------------------------------------------------------------
// ReflectSkinEffect.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 3 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class ReflectSkinEffect extends Effects;

var Material SkinMaterial;
var StaticMeshComponent SkinComp;

//#exec TEXTURE IMPORT FILE=MODELS\RefPurple.pcx	GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\RefBlue.pcx	GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\RefRed.pcx		GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\RefYellow.pcx	GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\RefGreen.pcx	GROUP=Effects
//#exec TEXTURE IMPORT FILE=MODELS\RefWhite.pcx	GROUP=Effects

//#exec TEXTURE IMPORT FILE=MODELS\RefMPurple.pcx	GROUP=MottledEffects
//#exec TEXTURE IMPORT FILE=MODELS\RefMBlue.pcx	GROUP=MottledEffects
//#exec TEXTURE IMPORT FILE=MODELS\RefMRed.pcx	GROUP=MottledEffects
//#exec TEXTURE IMPORT FILE=MODELS\RefMYellow.pcx	GROUP=MottledEffects
//#exec TEXTURE IMPORT FILE=MODELS\RefMGreen.pcx	GROUP=MottledEffects
//#exec TEXTURE IMPORT FILE=MODELS\RefMWhite.pcx	GROUP=MottledEffects

var ParticleSystemComponent ReflectSkinComp;

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	local float Scalar;
	
	Super.Tick( DeltaTime );

	// Hide from First person perspective.
	//if( Pawn(Owner) != None )
	//{
	//	bOwnerNoSee = ePawn(Owner).ViewTarget == None;
		
	//}

	// Scale values over lifespan.
	Scalar = LifeSpan / default.LifeSpan;
	//ScaleGlow = default.ScaleGlow * Scalar;
	//AmbientGlow = default.AmbientGlow * Scalar;
	//LightBrightness = default.LightBrightness * Scalar;

}

//------------------------------------------------------------------------------
simulated function SetColor( /*Color Colors,*/ optional bool bMottled )
{
	//switch( Colors )
	//{
	////case 'PC_Green':
	//case 'Green': 
	//	LightBrightness = 255; LightHue =  64; LightSaturation =   0;
	//	if( !bMottled )
	//		SetTexture( Texture2d'WOT.Icons.RefGreen_Mat' );
	//	else
	//		SetTexture( Texture2d'WOT.Icons.RefMGreen_Mat' );
	//	break;
	
	////case 'PC_Purple':
	//case 'Purple': 
	//	LightBrightness = 255; LightHue = 212; LightSaturation =   0;
	//	if( !bMottled )
	//		SetTexture( Texture2d'WOT.Icons.RefPurple_Mat' );
	//	else
	//		SetTexture( Texture2d'WOT.Icons.RefMPurple_Mat' );
	//	break;
	
	////case 'PC_Red':
	//case 'Red': 
	//	LightBrightness = 255; LightHue =   0; LightSaturation =   0;
	//	if( !bMottled )
	//		SetTexture( Texture2d'WOT.Icons.RefRed_Mat' );
	//	else
	//		SetTexture( Texture2d'WOT.Icons.RefMRed_Mat' );
	//	break;
	
	////case 'PC_Blue':
	//case 'Blue': 
	//	LightBrightness = 255; LightHue = 160; LightSaturation =   0;
	//	if( !bMottled )
	//		SetTexture( Texture'WOT.Icons.RefBlue_Mat' );
	//	else
	//		SetTexture( Texture'WOT.Icons.RefMBlue_Mat' );
	//	break;
	
	////case 'PC_Yellow':
	//case 'Yellow': 
	////case 'PC_Gold':
	//case 'Gold': 
	//	LightBrightness = 255; LightHue =  32; LightSaturation =   0;
	//	if( !bMottled )
	//		SetTexture( Texture'WOT.Icons.RefYellow_Mat' );
	//	else
	//		SetTexture( Texture'WOT.Icons.RefMYellow_Mat' );
	//	break;
	
	//case 'PC_White':
	//case 'White':
	//default:
		//LightBrightness = 255; LightHue =   0; LightSaturation = 255;
		//if( !bMottled )
		//	SetTexture( Texture2d'WOT.Icons.RefWhite_Mat' );
		//else
		//	SetTexture( Texture2d'WOT.Icons.RefMWhite_Mat' );
		//break;
	//}
}

//------------------------------------------------------------------------------
simulated function SetTexture( Material T )
{
	local int i;

	//Texture = T;
	Skin = T;

	for( i = 0; i < ArrayCount(MultiSkins); i++ )
	{
		SkinComp.CreateAndSetMaterialInstanceTimeVarying(i);
		MultiSkins[i] = T;
	}

}
defaultproperties
{
Begin Object Class=CylinderComponent Name=ReflectCol
	StaticMeshComponent=none
	Materials(0)=none
	CollisionRadius=+0021.000000
	CollisionHeight=+0042.000000
	CollideActors=false
End Object
	CollisionComponent=ReflectCol
	Components.Add(ReflectCol)
Begin Object Class=ParticleSystemComponent Name=ReflectSkin
    Template=ParticleSystem'GDC_Materials.Effects.P_SwordTrail_01'
    bAutoActivate=true
End Object
	ReflectSkinComp=ReflectSkin
	Components.Add(ReflectSkin)

	AmbientGlow=(R=1.0,G=0.0,B=1.0,A=1.0)
	LightDistance=5.0
	bIsCharacterLightEnvironment=true
	bEnabled=true
	MultiSkins(1)=Material'WOT.Icons.RefWhite_Mat'
    MultiSkins(2)=Material'WOT.Icons.RefWhite_Mat'
    MultiSkins(3)=Material'WOT.Icons.RefWhite_Mat'
    MultiSkins(4)=Material'WOT.Icons.RefWhite_Mat'
    MultiSkins(5)=Material'WOT.Icons.RefWhite_Mat'
    MultiSkins(6)=Material'WOT.Icons.RefWhite_Mat'
    MultiSkins(7)=Material'WOT.Icons.RefWhite_Mat'
    LightType=1
    LightEffect=13
    LightBrightness=255
    LightHue=204
    LightSaturation=204
    LightRadius=12
	Physics=11
    DrawType=2
    Style=3
    Texture=Texture2d'WOT.Icons.RefWhite'
    Skin=Material'WOT.Icons.RefWhite_Mat'
    ScaleGlow=0.50
	//bAnimByOwner=True
	RemoteRole=1
    LifeSpan=1.00
    bNetTemporary=False

//    bTrailerSameRotation=True

    DrawType=2

    Style=3

    Texture=Texture2d'WOT.Icons.RefWhite'

    Skin=Material'WOT.Icons.RefWhite_Mat'

    ScaleGlow=0.50

    AmbientGlow=64

    Fatness=157

    bUnlit=True

    MultiSkins(0)=Material'WOT.Icons.RefWhite_Mat'

    MultiSkins(1)=Material'WOT.Icons.RefWhite_Mat'

    MultiSkins(2)=Material'WOT.Icons.RefWhite_Mat'

    MultiSkins(3)=Material'WOT.Icons.RefWhite_Mat'

    MultiSkins(4)=Material'WOT.Icons.RefWhite_Mat'

    MultiSkins(5)=Material'WOT.Icons.RefWhite_Mat'

    MultiSkins(6)=Material'WOT.Icons.RefWhite_Mat'

    MultiSkins(7)=Material'WOT.Icons.RefWhite_Mat'

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightHue=204

    LightSaturation=204

    LightRadius=12

}