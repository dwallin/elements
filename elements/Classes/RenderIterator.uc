//================================================================================
// RenderIterator.
//================================================================================

class RenderIterator extends Actor;

struct ActorBuffer
{
  var byte Padding[540];
};

struct ActorNode
{
  var ActorBuffer ActorProxy;
  var Actor NextNode;
};

var() int MaxItems;
var int Index;
var transient ePawn Observer;
var transient Actor Frame;

function Delete()
{
	Destroy();
}

defaultproperties
{
    MaxItems=1
}