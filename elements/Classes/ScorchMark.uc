//================================================================================
// ScorchMark.
//================================================================================

class ScorchMark extends BloodDecal;

defaultproperties
{
    BloodTextures(0)=Texture2d'WOT.scorch5'

    BloodTextures(1)=Texture2d'WOT.scorch5'

    BloodTextures(2)=Texture2d'WOT.scorch5'

    BloodTextures(3)=Texture2d'WOT.scorch6'

    BloodTextures(4)=Texture2d'WOT.scorch6'

    BloodTextures(5)=Texture2d'WOT.scorch6'

    BloodTextures(6)=Texture2d'WOT.scorch7'

    BloodTextures(7)=Texture2d'WOT.scorch7'

    BloodTextures(8)=Texture2d'WOT.scorch7'

    BloodTextures(9)=Texture2d'WOT.scorch10'

    BloodTextures(10)=Texture2d'WOT.scorch10'

    BloodTextures(11)=Texture2d'WOT.scorch11'

    BloodTextures(12)=Texture2d'WOT.scorch11'

    BloodTextures(13)=Texture2d'WOT.scorch12'

    BloodTextures(14)=Texture2d'WOT.scorch12'

    GoreLevel=0

}