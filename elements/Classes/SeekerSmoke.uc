//================================================================================
// SeekerSmoke.
//================================================================================

class SeekerSmoke extends ParticleSprayer;

simulated function PreBeginPlay ()
{
  LifeSpan = 0.0;
  Super.PreBeginPlay();
}

defaultproperties
{
    NumTemplates=1

    Templates=(LifeSpan=2.00,Weight=1.00,MaxInitialVelocity=20.00,MinInitialVelocity=10.00,MaxDrawScale=1.50,MinDrawScale=1.00,MaxScaleGlow=0.00,MinScaleGlow=0.00,GrowPhase=1,MaxGrowRate=-0.50,MinGrowRate=-1.00,FadePhase=2,MaxFadeRate=0.10,MinFadeRate=0.05)

    Particles=Texture2d'WOT.Icons.FBExp503'

    bOn=True

    VolumeScalePct=1.00

    bStatic=False

    VisibilityRadius=2000.00

    VisibilityHeight=2000.00

}