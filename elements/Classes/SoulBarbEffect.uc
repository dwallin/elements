//================================================================================
// SoulBarbEffect.
//================================================================================

class SoulBarbEffect extends Effects;

simulated function Tick (float DeltaTime)
{
  Super.Tick(DeltaTime);
  if ( (Owner != None) && (Base == None) )
  {
    SetLocation(Owner.Location);
    SetBase(Owner);
  }
}

defaultproperties
{
    bAnimLoop=True

    AnimRate=0.05

    DrawType=1

    Style=3

    Texture=Texture2d'WOT.Icons.SB_A00'

    DrawScale=0.75

}