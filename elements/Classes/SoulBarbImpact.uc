//================================================================================
// SoulBarbImpact.
//================================================================================

class SoulBarbImpact extends Effects;

var() float FrameInterval;
var() Material SkinAnim[18];
var int SkinAnimIndex;
var Vector OwnerLocation;

replication
{
  if ( (Role == ROLE_Authority) && (Owner != None) /*&&  !Owner.bNetRelevant*/ )
    OwnerLocation;
}

simulated function name GetSequenceName (int Index)
{
  switch (Index)
  {
    case 0:
    return 'Frame0';
    break;
    case 1:
    return 'Frame1';
    break;
    case 2:
    return 'Frame2';
    break;
    case 3:
    return 'Frame3';
    break;
    case 4:
    return 'Frame4';
    break;
    case 5:
    return 'Frame5';
    break;
    case 6:
    return 'Frame6';
    break;
    case 7:
    return 'Frame7';
    break;
    case 8:
    return 'Frame8';
    break;
    case 9:
    return 'Frame9';
    break;
    case 10:
    return 'Frame10';
    break;
    case 11:
    return 'Frame11';
    break;
    case 12:
    return 'Frame12';
    break;
    case 13:
    return 'Frame13';
    break;
    case 14:
    return 'Frame14';
    break;
    case 15:
    return 'Frame15';
    break;
    case 16:
    return 'Frame16';
    break;
    case 17:
    return 'Frame17';
    break;
    default:
    `Log("*** SoulBarbImpact: Asked for invalid frame index " $ string(Index));
    break;
  }
}

auto simulated state Glowing
{
  simulated function BeginState (name Glowing)
  {
    SkinAnimIndex = 0;
  }
  
  simulated function Tick (float DeltaTime)
  {
    if ( Owner != None )
    {
      OwnerLocation = Owner.Location;
    }
    SetLocation(OwnerLocation);
    //FrameInterval -= DeltaTime;
    //if ( FrameInterval <= 0.0 )
    //{
    //  FrameInterval = Default.FrameInterval;
    //  Skin = SkinAnim[SkinAnimIndex];
    //  PlayAnim(GetSequenceName(SkinAnimIndex),0.5);
    //  SkinAnimIndex++;
    //  if ( SkinAnimIndex >= 18 )
    //  {
    //    SkinAnimIndex = 0;
    //    Destroy();
    //  }
    //}
  }
  
  //PlayAnim('Frame0',0.5);
  //SkinAnimIndex++;
}

defaultproperties
{
    FrameInterval=0.05

    SkinAnim(0)=Material'WOT.Icons.DSoulBarbImpact01_Mat'

    SkinAnim(1)=Material'WOT.Icons.DSoulBarbImpact02_Mat'

    SkinAnim(2)=Material'WOT.Icons.DSoulBarbImpact03_Mat'

    SkinAnim(3)=Material'WOT.Icons.DSoulBarbImpact04_Mat'

    SkinAnim(4)=Material'WOT.Icons.DSoulBarbImpact05_Mat'

    SkinAnim(5)=Material'WOT.Icons.DSoulBarbImpact06_Mat'

    SkinAnim(6)=Material'WOT.Icons.DSoulBarbImpact07_Mat'

    SkinAnim(7)=Material'WOT.Icons.DSoulBarbImpact08_Mat'

    SkinAnim(8)=Material'WOT.Icons.DSoulBarbImpact09_Mat'

    SkinAnim(9)=Material'WOT.Icons.DSoulBarbImpact10_Mat'

    SkinAnim(10)=Material'WOT.Icons.DSoulBarbImpact11_Mat'

    SkinAnim(11)=Material'WOT.Icons.DSoulBarbImpact12_Mat'

    SkinAnim(12)=Material'WOT.Icons.DSoulBarbImpact13_Mat'

    SkinAnim(13)=Material'WOT.Icons.DSoulBarbImpact14_Mat'

    SkinAnim(14)=Material'WOT.Icons.DSoulBarbImpact15_Mat'

    SkinAnim(15)=Material'WOT.Icons.DSoulBarbImpact16_Mat'

    SkinAnim(16)=Material'WOT.Icons.DSoulBarbImpact17_Mat'

    SkinAnim(17)=Material'WOT.Icons.DSoulBarbImpact18_Mat'

    RemoteRole=1

    DrawType=2

    Skin=Material'WOT.Icons.DSoulBarbImpact01_Mat'

    //Mesh=Mesh'SoulBarbImpact'

    LightType=1

    LightEffect=13

    LightBrightness=64

    LightHue=96

    LightSaturation=128

    LightRadius=8

}