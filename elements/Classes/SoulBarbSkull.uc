//================================================================================
// SoulBarbSkull.
//================================================================================

class SoulBarbSkull extends Effects;

var() float MinSpinRate;
var() float MaxSpinRate;
var float spinRate;
var float AccumYaw;
var() float MinRiseRate;
var() float MaxRiseRate;
var float RiseRate;
var() float MinGrowRate;
var() float MaxGrowRate;
var float GrowRate;
var() Material AnimTextures[7];
var() int NumTextures;

simulated function PreBeginPlay ()
{
  Super.PreBeginPlay();
  spinRate = RandRange(MinSpinRate,MaxSpinRate);
  RiseRate = RandRange(MinRiseRate,MaxRiseRate);
  GrowRate = RandRange(MinGrowRate,MaxGrowRate);
}

function Tick (float DeltaTime);

defaultproperties
{
    MinSpinRate=-32000.00

    MaxSpinRate=32000.00

    MinRiseRate=20.00

    MaxRiseRate=40.00

    MinGrowRate=0.80

    MaxGrowRate=1.00

    AnimTextures(0)=Material'WOT.Icons.64SkMOD01_Mat'

    AnimTextures(1)=Material'WOT.Icons.64SkMOD02_Mat'

    AnimTextures(2)=Material'WOT.Icons.64SkMOD03_Mat'

    AnimTextures(3)=Material'WOT.Icons.64SkMOD04_Mat'

    AnimTextures(4)=Material'WOT.Icons.64SkMOD05_Mat'

    AnimTextures(5)=Material'WOT.Icons.64SkMOD06_Mat'

    AnimTextures(6)=Material'WOT.Icons.64SkMOD07_Mat'

    NumTextures=7

    LifeSpan=1.50

    DrawType=2

    Style=4

    Texture=None

    Skin=Material'WOT.Icons.64SkMOD01_Mat'

    Mesh=SkeletalMesh'WOT.Mesh.soulskull'

    DrawScale=0.10

}