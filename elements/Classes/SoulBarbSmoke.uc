//================================================================================
// SoulBarbSmoke.
//================================================================================

class SoulBarbSmoke extends ParticleSprayer;

simulated function PreBeginPlay ()
{
  LifeSpan = 0.0;
  Super.PreBeginPlay();
  AlignGravity();
}

simulated function Tick (float DeltaTime)
{
  Super.Tick(DeltaTime);
  AlignGravity();
}

simulated function AlignGravity ()
{
  Gravity = Default.Gravity >> Rotation;
}

defaultproperties
{
    Spread=125.00

    Volume=15.00

    Gravity=(X=5.00,Y=0.00,Z=3.00)

    NumTemplates=2

    Templates(0)=(LifeSpan=2.00,Weight=2.00,MaxInitialVelocity=30.00,MinInitialVelocity=-5.00,MaxDrawScale=0.50,MinDrawScale=1.00,MaxScaleGlow=0.00,MinScaleGlow=-0.10,GrowPhase=1,MaxGrowRate=0.50,MinGrowRate=0.30,FadePhase=2,MaxFadeRate=1.00,MinFadeRate=0.00)

    Templates(1)=(LifeSpan=2.00,Weight=1.00,MaxInitialVelocity=30.00,MinInitialVelocity=-5.00,MaxDrawScale=0.25,MinDrawScale=0.50,MaxScaleGlow=0.00,MinScaleGlow=-0.10,GrowPhase=1,MaxGrowRate=0.50,MinGrowRate=0.30,FadePhase=2,MaxFadeRate=1.00,MinFadeRate=0.00)

    Particles(0)=Texture2d'WOT.Icons.Spirits03'

    Particles(1)=Texture2d'WOT.Icons.Spirits04'

    bOn=True

    bStatic=False

    VisibilityRadius=1000.00

    VisibilityHeight=1000.00

}