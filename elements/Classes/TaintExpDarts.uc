//================================================================================
// TaintExpDarts.
//================================================================================

class TaintExpDarts extends TaintExpAssets;

defaultproperties
{
    NumAnimFrames=27

    DrawType=2

    Texture=Texture2d'WOT.Icons.TaintDarts'

    Mesh=SkeletalMesh'WOT.Mesh.TaintExpDarts'

    AmbientGlow=20

    bUnlit=True

}