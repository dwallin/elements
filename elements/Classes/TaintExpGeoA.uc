//================================================================================
// TaintExpGeoA.
//================================================================================

class TaintExpGeoA extends TaintExpAssets;

defaultproperties
{
    NumAnimFrames=13

    DrawType=2

    Style=3

    Texture=Texture2d'WOT.Icons.RedIce'

    Mesh=SkeletalMesh'WOT.Mesh.TaintExpGeoA'

}