//================================================================================
// TaintExpGeoB.
//================================================================================

class TaintExpGeoB extends TaintExpAssets;

defaultproperties
{
    NumAnimFrames=13

    DrawType=2

    Style=3

    Texture=Texture2d'WOT.Icons.YeloIce'

    Mesh=SkeletalMesh'WOT.Mesh.TaintExpGeoB'

}