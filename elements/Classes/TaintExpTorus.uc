//================================================================================
// TaintExpTorus.
//================================================================================

class TaintExpTorus extends TaintExpAssets;

defaultproperties
{
    NumAnimFrames=15

    DrawType=2

    Style=3

    Texture=Texture2d'WOT.Icons.RedIce'

    Mesh=SkeletalMesh'WOT.Mesh.TaintExpTorus'

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightRadius=10

}