//------------------------------------------------------------------------------
// TaintVisual.uc
// $Author: Mfox $
// $Date: 1/05/00 2:27p $
// $Revision: 2 $
//
// Description:	
//------------------------------------------------------------------------------
// How to use this class:
//
//------------------------------------------------------------------------------
class TaintVisual extends Effects;

//#exec OBJ LOAD FILE=Textures\TaintPartsT.utx PACKAGE=Angreal.TaintVisual

var GenericSprite Assets[5];

//------------------------------------------------------------------------------
simulated function PostBeginPlay()
{
	Super.PostBeginPlay();
	//SetRotation( rot(65536,65536,65536) * FRand() );
	CreateAssets();
}

//------------------------------------------------------------------------------
simulated function CreateAssets()
{
	// Red comet 1.
	Assets[0] = Spawn( class'GenericSprite',,,, rotator(vect(1,0,0)) );
    Assets[0].SetPhysics( PHYS_Rotating );
    Assets[0].Style = BLEND_Translucent;
    Assets[0].Skin = Material'WOT.Icons.TaintRedcomet3';
    Assets[0].SetDrawScale(0.700000);
    Assets[0].bFixedRotationDir = true;
    Assets[0].RotationRate = rot(0,0,100000);
	Assets[0].DrawType = DT_Mesh;
	Assets[0].Mesh = SkeletalMesh'WOT.Mesh.ExpWard';
	Assets[0].bUnlit = true;
	Assets[0].ScaleGlow = 5.000000;
	Assets[0].SetDrawScale( 1.500000 );

	// Red comet 2.
	Assets[1] = Spawn( class'GenericSprite',,,, rotator(vect(0,1,0)) );
    Assets[1].SetPhysics( PHYS_Rotating );
    Assets[1].Style = BLEND_Translucent;
    Assets[1].Skin = Material'WOT.Icons.TaintRedcomet3';
    Assets[1].SetDrawScale( 0.700000 );
    Assets[1].bFixedRotationDir = true;
    Assets[1].RotationRate = rot(0,0,60000);
	Assets[1].DrawType = DT_Mesh;
	Assets[1].Mesh = SkeletalMesh'WOT.Mesh.ExpWard';
	Assets[1].bUnlit = true;
	Assets[1].ScaleGlow = 5.000000;
	Assets[1].SetDrawScale(1.500000);

	// Red comet 3.
	Assets[2] = Spawn( class'GenericSprite',,,, rotator(vect(0,0,1)) );
    Assets[2].SetPhysics( PHYS_Rotating );
    Assets[2].Style = BLEND_Translucent;
    Assets[2].Skin = Material'WOT.Icons.TaintRedcomet3';
    Assets[2].SetDrawScale(0.700000);
    Assets[2].bFixedRotationDir = true;
    Assets[2].RotationRate = rot(0,-80000,0);
	Assets[2].DrawType = DT_Mesh;
	Assets[2].Mesh = SkeletalMesh'WOT.Mesh.ExpWard';
	Assets[2].bUnlit = true;
	Assets[2].ScaleGlow = 5.000000;
	Assets[2].SetDrawScale( 1.500000 );

	// Center.
	Assets[3] = Spawn( class'GenericSprite' );
    Assets[3].Style = BLEND_Modulate;
    Assets[3].Texture = Texture2d'WOT.Icons.TntCntrA';
    Assets[3].SetDrawScale( 0.450000 );

	// Inner glow.
	Assets[4] = Spawn( class'GenericSprite' );
    Assets[4].Style = BLEND_Translucent;
    Assets[4].Texture = Texture2d'WOT.Icons.T2';
    Assets[4].SetDrawScale( 0.090000 );
	Assets[4].ScaleGlow = 5.000000;
}

//------------------------------------------------------------------------------
simulated function Tick( float DeltaTime )
{
	local int i;
	
	Super.Tick( DeltaTime );

	for( i = 0; i < ArrayCount(Assets); i++ )
	{
		if( Assets[i] != None )
		{
			Assets[i].SetLocation( Location );
		}
	}
}

//------------------------------------------------------------------------------
simulated function Destroyed()
{
	local int i;

	for( i = 0; i < ArrayCount(Assets); i++ )
	{
		if( Assets[i] != None )
		{
			Assets[i].Destroy();
			Assets[i] = None;
		}
	}

	Super.Destroyed();
}
defaultproperties
{
    bHidden=True

    RemoteRole=1

    LightType=1

    LightEffect=13

    LightBrightness=255

    LightRadius=7

    LightPeriod=32

}