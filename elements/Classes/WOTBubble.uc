//=============================================================================
// WOTBubble.
//=============================================================================
class WOTBubble extends Effects;
    
//#exec Texture Import File=models\bubble1.pcx Mips=Off Flags=2
//#exec Texture Import File=models\bubble2.pcx Mips=Off Flags=2
//#exec Texture Import File=models\bubble3.pcx Mips=Off Flags=2
//#exec Texture Import File=models\bubble4.pcx Mips=Off Flags=2

//=============================================================================

simulated function ZoneChange( ZoneInfo NewZone )
{
	if ( !NewZone.CanSplash() ) 
	{
		Destroy();

		PlaySound( EffectSound1 );
	}	
}

//=============================================================================

simulated function PostBeginPlay()
{
	//local float RandomFloat;

	Super.PostBeginPlay();

	if ( WorldInfo.NetMode != NM_DedicatedServer )
	{
		PlaySound( EffectSound2 ); //Spawned Sound

		LifeSpan = 3 + 4 * FRand();
		//Buoyancy = Mass + FRand()+0.1;

		//RandomFloat = FRand();

		//if( RandomFloat < 0.25 ) 
		//{
		//	Texture = texture2d'WOT.Icons.bubble2';
		//}
		//else if( RandomFloat < 0.50 ) 
		//{
		//	Texture = texture2d'WOT.Icons.bubble3';
		//}
		//else if( RandomFloat < 0.75 ) 
		//{
		//	Texture = texture2d'WOT.Icons.bubble4';
		//}

		SetDrawScale( FRand()*DrawScale/2);
	}
}

//=============================================================================
defaultproperties
{
    bNetOptional=True

    Physics=2

    RemoteRole=1

    LifeSpan=2.00

    DrawType=1

    Style=3

    //Texture=Texture2d'WOT.Icons.bubble1'

    Mass=3.00

    Buoyancy=3.75

}