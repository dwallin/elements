//------------------------------------------------------------------------------
// WOTFootZoneChangeReflector.uc
// $Author: Mfox $
// $Date: 1/05/00 2:38p $
// $Revision: 2 $
//
// Description:	Handles FootZoneChanges for HealZones for ePawns or WOTPawns.
//------------------------------------------------------------------------------
// How to use this class:
//
// + Install in a ePawn or WOTPawn at start-up using the Install() function.
//------------------------------------------------------------------------------
class WOTFootZoneChangeReflector extends Reflector;

/////////////////////////
// Overriden Functions //
/////////////////////////

//------------------------------------------------------------------------------
function FootZoneChange( ZoneInfo newFootZone )
{
	//if( ePawn(Owner) != None )
	//{
	//	ePawn(Owner).SuperFootZoneChange( newFootZone );
	//}
	//else if( WOTPawn(Owner) != None )
	//{
	//	WOTPawn(Owner).SuperFootZoneChange( newFootZone );
	//}
	
	//if( WOTZoneInfo( newFootZone ) != None && WOTZoneInfo( newFootZone ).bHealZone )
	//{
	//	if( ePawn(Owner) != None )
	//	{
	//		ePawn(Owner).PainTime = 0.01;
	//	}
	//}

	// Reflect function call to next reflector in line.
	Super.FootZoneChange( newFootZone );
}
