//------------------------------------------------------------------------------
// WOTInventory.uc
// $Author: Mpoesch $
// $Date: 10/16/99 3:59a $
// $Revision: 9 $
//
// Description:	Supports display of inventory in HUDs
//
//------------------------------------------------------------------------------
class WOTInventory extends UDKWeapon
	abstract;
var Canvas Canvas;
//#exec TEXTURE IMPORT FILE=Icons\I.pcx          GROUP=Icons MIPS=Off
//#exec TEXTURE IMPORT FILE=Icons\S.pcx          GROUP=Icons MIPS=Off FLAGS=2
//#exec TEXTURE IMPORT FILE=Icons\M.pcx          GROUP=Icons MIPS=Off FLAGS=2

var() localized string		Title;			// Inventory UI title
var() localized string		Description;	// Inventory UI description text
var() localized string		Quote;			// Inventory UI "experience" quote describing the Angreal effect

var() texture2d StatusIconSelected;			// Icon used for large inventory when selected
var() texture2d StatusIconFrame;				// Icon used for large (or small) inventory
var() texture2d StatusIcon;
var() bool bShowInfoHint;					// If set, when inventory item is picked up, info screen will pop up for that inventory item

var class<Actor> ResourceClass;		// the concrete class contained in this adapter
var name BaseResourceType;			// the base class name (Grunt, Captain, Champion) for all pawn resources -- not all Champions derive from Champion
var bool bActive;
var bool SpawnTempResource;
var int Count;

var bool bHeldItem;

var() SoundCue ActivateSound;
var() SoundCue DeactivateSound;
var() SoundCue RespawnSound;

var float PickupViewScale;

var bool bCarriedItem;
var WOTInventory WOTInventory;
var ePickupFactory ePickupFactory;
var Actor Target;

var LegendCanvas LegendCanvas;


replication
{
	// Data the server should send the client player -- this fixes the citadel editor hand replication (resource counts)
	if( Role==ROLE_Authority && bNetOwner )
		StatusIcon, StatusIconFrame, Count;
}

simulated function StartFire(byte FireModeNum) {
	super.StartFire(0);
	super.ServerStartFire(0);
}

simulated function StopFire(byte FireModeNum) {
	super.StopFire(0);
	super.ServerStopFire(0);
}

reliable server function SuperGiveTo (Pawn Other)
{
		Instigator = Other;
		//BecomeItem();
		//self.GiveTo(Instigator);
		
	  
	  //Other.InvManager.AddInventory(self);
	  //GotoState('Idle2');
}

simulated function TravelPreAccept()
{
	GiveTo(ePawn(Owner));
	GotoState('Active');
	if ( bActive )
	{
		Activate();
	}
}

static event float GetDistanceBetweenCylinders (Vector FirstOrigin, float FirstRadius, float FirstHalfHeight, Vector SecondOrigin, float SecondRadius, float SecondHalfHeight)
{
  local float DistanceBetween;
  //local float MinDistance;
  local Vector OriginDifference;
  local Vector OriginDifferenceNormal;
  local Vector FirstSurfaceLocation;
  local Vector SecondSurfaceLocation;

  OriginDifference = SecondOrigin - FirstOrigin;
  OriginDifference.Z = 0.0;
  OriginDifferenceNormal = Normal(OriginDifference);
  FirstSurfaceLocation = FirstOrigin + OriginDifferenceNormal * FirstRadius;
  SecondSurfaceLocation = SecondOrigin - OriginDifferenceNormal * SecondRadius;
  if ( SecondOrigin.Z - SecondHalfHeight > FirstOrigin.Z + FirstHalfHeight )
  {
    SecondSurfaceLocation.Z -= SecondHalfHeight;
    FirstSurfaceLocation.Z += FirstHalfHeight;
    DistanceBetween = VSize(FirstSurfaceLocation - SecondSurfaceLocation);
  } else {
    if ( FirstOrigin.Z - FirstHalfHeight > SecondOrigin.Z + SecondHalfHeight )
    {
      FirstSurfaceLocation.Z -= FirstHalfHeight;
      SecondSurfaceLocation.Z += SecondHalfHeight;
      DistanceBetween = VSize(FirstSurfaceLocation - SecondSurfaceLocation);
    } else {
      FirstSurfaceLocation.Z = 0.0;
      SecondSurfaceLocation.Z = 0.0;
      DistanceBetween = VSize(FirstSurfaceLocation - SecondSurfaceLocation);
      if ( VSize(OriginDifference) < FirstRadius + SecondRadius )
      {
        DistanceBetween =  -DistanceBetween;
      }
    }
  }
  return DistanceBetween;
}

static event float GetDistanceBetweenActors (Actor A1, Actor A2)
{
	local float CollisionRadius, CollisionHeight, CollisionRadius2, CollisionHeight2;
	A1.GetBoundingCylinder(CollisionRadius, CollisionHeight);
	A2.GetBoundingCylinder(CollisionRadius2, CollisionHeight2);
	return GetDistanceBetweenCylinders(A1.Location,CollisionRadius,CollisionHeight,A2.Location,CollisionRadius2,CollisionHeight2);
}

function bool HandlePickupQuery (WOTInventory Item)
{
  if ( Item.Class == Class )
  {
    return True;
  }
  if ( WOTInventory == None )
  {
    return False;
  }
  return WOTInventory.HandlePickupQuery(Item);
}

//function BecomePickup ()
//{
//  if ( Physics != 2 )
//  {
//    RemoteRole = 1;
//  }
//  //Mesh = PickupViewMesh;
//  SetDrawScale( PickupViewScale );
//  SetOnlyOwnerSee( False );
//  SetHidden ( False );
//  bCarriedItem = False;
//  NetPriority = 1.39999998;
//  SetCollision(True,False,False);
//}

//function BecomeItem ()
//{
//  RemoteRole = 1;
//  //Mesh = PlayerViewMesh;
//  SetDrawScale( PickupViewScale );
//  SetOnlyOwnerSee( True );
//  SetHidden( True );
//  bCarriedItem = True;
//  NetPriority = 1.39999998;
//  SetCollision(False,False,False);
//  SetPhysics(0);
//  SetTimer(0.0,False);
//  //AmbientGlow = 0;
//}

function SetRespawn ()
{
  if ( WorldInfo.Game.ShouldRespawn(ePickupFactory) )
  {
    GotoState('Sleeping');
  } else {
    Destroy();
  }
}

function InitializeCopy( WOTinventory Copy )
{
	//Super.InitializeCopy( Copy );	// for template purposes only.
	Copy.Tag           = Tag;
	//Copy.Event         = Event;
}

// This function call was taking up a lot of time, and since we never use it,
// there is no reason to have continue to slow us down.
function int ReduceDamage( int Damage, name DamageType, vector HitLocation )
{
	if( Damage<0 )
		return 0;
	else
		return Damage;
}

// keep items spawned by Angreal using NPCs from entering the Inventory.uc "auto Pickup" state
simulated event SetInitialState()
{
	if( !bHeldItem )
	{
		Super.SetInitialState();
	}
}

simulated function DrawStatusIconAt( Canvas Canvas, Texture2D Icon, optional float Scale )
{
	if (Scale != 0)
		SetDrawScale(Scale);
	Canvas.DrawTile( Icon, 64,64, 0, 0, 128, 128,,, BLEND_Masked);
}

defaultproperties
{
    Title="[Title Missing]"

    Description="[Description Missing]"

    Quote="[Quote Missing]"

    StatusIconSelected=Texture2d'WOT.Icons.S_0'

    StatusIconFrame=Texture2d'WOT.Icons.M_0'

    SpawnTempResource=True

    StatusIcon=Texture2d'WOT.Icons.I_0'

}