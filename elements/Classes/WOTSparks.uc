//================================================================================
// WOTSparks.
//================================================================================

class WOTSparks extends ParticleSprayer;

simulated function PreBeginPlay ()
{
  LifeSpan = 1.5;
  Super.PreBeginPlay();
}

simulated function SetInitialState ()
{
  Super.SetInitialState();
  //Trigger(self,None);
}

defaultproperties
{
    Spread=140.00

    Volume=400.00

    Gravity=(X=-100.00,Y=0.00,Z=5.00)

    NumTemplates=4

    //Templates(0)...
	Particles(0)=Texture2d'WOT.APurpleCorona'

    Particles(1)=Texture2d'WOT.Icons.PixieStar'

    Particles(2)=Texture2d'WOT.Icons.PixieStar'

    Particles(3)=Texture2d'WOT.Icons.PixieStar'

    TimerDuration=0.25

    bInitiallyOn=False

    bOn=True

    bInterpolate=True

    bStatic=False

    InitialState=TriggerTimed

    VisibilityRadius=1500.00

    VisibilityHeight=1500.00

}