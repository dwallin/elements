//=============================================================================
// WOTUtil
// $Author: Mfox $
// $Date: 1/05/00 2:38p $
// $Revision: 10 $
//=============================================================================

class WOTUtil extends LegendObjectComponent	abstract;

//util

//=============================================================================
// Helper function for FindBestTarget.
// Returns the cosine of the angle between v1 and v2.
//=============================================================================

static function float FindCosAngle( vector v1, vector v2 )
{
    local float CosAngle;
    CosAngle = ( v1 dot v2 ) / ( VSize( v1 ) * VSize( v2 ) );
    return CosAngle;
}

//=============================================================================
// Returns the sin of the given angle.
// Angle is in degrees.
//=============================================================================

static function float DSin( float Angle )
{
	return Sin( Angle * PI / 180.0 );
}

//=============================================================================
// Returns the cos of the given angle.
// Angle is in degrees.
//=============================================================================

static function float DCos( float Angle )
{
	return Cos( Angle * PI / 180.0 );
}

//=============================================================================
// Returns true if the given vectors are aproximately equal.
// Fix MWP: This should probably be added to the engine.
//=============================================================================

static function bool VectorAproxEqual( vector FirstVector, vector SecondVector )
{
	return ( ( FirstVector.x ~= SecondVector.x ) &&
			( FirstVector.y ~= SecondVector.y ) &&
			( FirstVector.z ~= SecondVector.z ) );
}



//=============================================================================

static function SwapInt( out int Num1, out int Num2 )
{
	local int temp;

	temp = Num1;
	Num1 = Num2;
	Num2 = temp;
}



//=============================================================================

static function SwapFloat( out float Num1, out float Num2 )
{
	local float temp;

	temp = Num1;
	Num1 = Num2;
	Num2 = temp;
}

//static simulated function Fade( Actor Other, optional float FadeTime, optional bool bNoDeleteActor, optional float InitialScaleGlow, optional float FinalScaleGlow, optional float DelayTime, optional int NumFlickers )
//{
//	local Fader F;

//	if( Other != None )
//	{
//		F = Other.Spawn( class'Fader' );
		
//		if( FadeTime > 0.0 )
//		{
//			F.FadeTime = FadeTime;
//		}

//		F.bDeleteActor = !bNoDeleteActor;

//		if( InitialScaleGlow > 0.0 )
//		{
//			F.InitialScaleGlow = InitialScaleGlow;
//		}

//		if( FinalScaleGlow > 0.0 )
//		{
//			F.FinalScaleGlow = FinalScaleGlow;
//		}

//		F.DelayTime = DelayTime;
		
//		F.NumFlickers = NumFlickers;

//		F.Fade( Other );
//	}
//}

//static function TriggerActor( Actor TriggeringActor,
//		Actor TriggeredActor,
//		Pawn EventInstigator,
//		Name TriggerEvent )
//{
//	local Name LastEvent;
//	local Pawn CurrentPawn;

//	//class'Debug'.static.DebugAssert( TriggeringActor, ( TriggeredActor != None ), "TriggerActor", default.DebugCategoryName );

//	LastEvent = TriggeringActor.Event;
//	TriggeringActor.Event = TriggerEvent;
		
//	TriggeredActor.Trigger( TriggeringActor, EventInstigator );

//	TriggeringActor.Event = LastEvent;
//}

static function Vector CalcClosestCollisionPoint (Actor Other, Vector Loc)
{
  local Vector HitNormal;
  local Vector Start;
  local Vector End;
  local Vector Extent;
  local float TopZ;
  local float BotZ;
  local float T0;
  local float T1;
  local float t;
  local float Kx;
  local float Ky;
  local float Vx;
  local float Vy;
  local float A;
  local float B;
  local float C;
  local float Discrim;
  local float Dir;
  local float R2A;
  local float ResultTime;
  local float CollisionRadius, CollisionHeight;
  if ( Other == None )
  {
    return vect(0.00,0.00,0.00);
  }
  HitNormal = vect(0.00,0.00,0.00);
  Start = Loc;
  End = Other.Location;
  Other.GetBoundingCylinder(CollisionRadius, CollisionHeight);
  Extent.X = CollisionRadius;
  Extent.Y = CollisionRadius;
  Extent.Z = CollisionHeight;
  TopZ = End.Z + Extent.Z;
  BotZ = End.Z - Extent.Z;
  T0 = 0.0;
  T1 = 1.0;
  if ( (Start.Z > TopZ) && (End.Z < TopZ) )
  {
    t = (TopZ - Start.Z) / (End.Z - Start.Z);
    if ( t > T0 )
    {
      T0 = FMax(T0,t);
      HitNormal = vect(0.00,0.00,1.00);
    }
  } else {
    if ( (Start.Z < TopZ) && (End.Z > TopZ) )
    {
      T1 = FMin(T1,(TopZ - Start.Z) / (End.Z - Start.Z));
    }
  }
  if ( (Start.Z < BotZ) && (End.Z > BotZ) )
  {
    t = (BotZ - Start.Z) / (End.Z - Start.Z);
    if ( t > T0 )
    {
      T0 = FMax(T0,t);
      HitNormal = vect(0.00,0.00,-1.00);
    }
  } else {
    if ( (Start.Z > BotZ) && (End.Z < BotZ) )
    {
      T1 = FMin(T1,(BotZ - Start.Z) / (End.Z - Start.Z));
    }
  }
  if ( T0 >= T1 )
  {
    return vect(0.00,0.00,0.00);
  }
  Kx = Start.X - End.X;
  Ky = Start.Y - End.Y;
  Vx = End.X - Start.X;
  Vy = End.Y - Start.Y;
  A = Vx * Vx + Vy * Vy;
  B = 2.0 * (Kx * Vx + Ky * Vy);
  C = Kx * Kx + Ky * Ky - Square(Extent.X);
  Discrim = B * B - 4.0 * A * C;
  if ( (C < Square(1.0)) && (Start.Z > BotZ) && (Start.Z < TopZ) )
  {
    Dir = (End - Start) * vect(1.00,1.00,0.00) Dot (Start - End);
    if ( Dir < -0.1 )
    {
      HitNormal = Normal((Start - End) * vect(1.00,1.00,0.00));
      return Start;
    } else {
      return vect(0.00,0.00,0.00);
    }
  }
  if ( Discrim < 0 )
  {
    return vect(0.00,0.00,0.00);
  }
  if ( A < Square(0.01) )
  {
    if ( C > 0 )
    {
      return vect(0.00,0.00,0.00);
    }
  } else {
    Discrim = Sqrt(Discrim);
    R2A = 0.5 / A;
    T1 = FMin(T1,(Discrim - B) * R2A);
    t =  -(Discrim + B) * R2A;
    if ( t > T0 )
    {
      T0 = t;
      HitNormal = Start + (End - Start) * T0 - End;
      HitNormal.Z = 0.0;
      HitNormal = Normal(HitNormal);
    }
    if ( T0 >= T1 )
    {
      return vect(0.00,0.00,0.00);
    }
  }
  ResultTime = FClamp(T0 - 0.01,0.0,1.0);
  return Start + (End - Start) * ResultTime;
}

//util
static function bool ActorFits (Actor MovingActor, Vector DesiredLocation, float ActorFitsRadius)
{
  local Actor IterA;
  local float RadiusDiff;
  local float HeightDiff;
  local Vector diff;
  local bool bFits;
  local float ColRad, ColHei;
  local float ColRad2, ColHei2;

  if ( MovingActor == None )
  {
    return False;
  }
  bFits = True;
  if ( MovingActor.bBlockActors /*|| MovingActor.bBlockPlayers*/ )
  {
    foreach MovingActor.CollidingActors(Class'Actor',IterA,ActorFitsRadius,DesiredLocation)
    {
      if ( (IterA != MovingActor) &&  !IterA.IsA('Mover') )
      {
        if ( IterA.bBlockActors /*|| IterA.bBlockPlayers*/ )
        {
          diff = IterA.Location - DesiredLocation;
          HeightDiff = diff.Z;
          diff.Z = 0.0;
          RadiusDiff = VSize(diff);
		  IterA.GetBoundingCylinder(ColRad, ColHei);
		  MovingActor.GetBoundingCylinder(ColRad2, ColHei2);
          if ( (ColRad + ColRad2 >= RadiusDiff) && (ColHei + ColHei2 >= HeightDiff) )
          {
            bFits = False;
          } else {
          }
        }
      }
    }
  }
  return bFits;
}

//------------------------------------------------------------------------------
// Use to remove ALL reflectors from the given Pawn.
// Note: This should only be used for cleaning out default reflectors on dead
// Pawns.  Manually removing !bRemovable Reflectors is generally dangerous.
//------------------------------------------------------------------------------
simulated static final function RemoveAllReflectorsFrom( Pawn Other )
{
	local ReflectorIterator IterR;
	local Reflector R;

	IterR = class'ReflectorIterator'.static.GetIteratorFor( Other );
	if(IterR != none)
	{
		for( IterR.First(); !IterR.IsDone(); IterR.Next() )
		{
			R = IterR.GetCurrent();

			R.UnInstall();
			R.Destroy();
		}
		IterR.Reset();
		IterR = None;
	}
}

//=============================================================================
// Traces from PlayerPawn's eyes 64K units along his ViewRotation and returns
// what was hit, and the HitLocation and HitNormal.
//=============================================================================

static function Actor GetHitActorInfo( ePawn P, out vector HitLocation, out vector HitNormal, bool bSafe )
{					 
	local vector StartTrace;
	local Actor HitActor;
	local Actor A;

	StartTrace		= P.Location; 
	StartTrace.Z   += P.BaseEyeHeight;

	// let us hit any visible actor which might not have the correct collision settings
	if( !bSafe )
	{
		foreach P.AllActors( Class'Actor', A )
		{
			if( !A.bHidden )
			{
				// this spams the old collision settings
  				A.SetCollision( true, true, true );
			}
		}		
	}

	HitActor = TraceRecursive( P, HitLocation, HitNormal, StartTrace, true, 0.0, vector(P.GetViewRotation()) );

	// let us hit any visible actor which might not have the correct collision settings
	if( !bSafe )
	{
		foreach P.AllActors( Class'Actor', A )
		{
			if( !A.bHidden )
			{
				A.SetCollision( A.default.bCollideActors, A.default.bBlockActors/*, A.default.bBlockPlayers*/ );
			}
		}		
	}

	return HitActor;		
}

//=============================================================================
// Recursively traces until we hit something (BSP or Actor).
//------------------------------------------------------------------------------
// Instance:		We need an instance Actor to call trace from.	(Pass in any Actor - Self is usually a good choice.)
// HitLocation:     Location of hit BSP/Actor.
// HitNormal:		Hit normal.
// StartLoc:		Location to start tracing from.
// bTraceActors:	Should we stop tracing if we hit an Actor?		(Default - false.)
// TraceInverval:	How far to trace per iteration.					(Default - 500 units.)
// TraceDirection:	Direction to trace in.							(Default - straight down.)
// TraceLimit:		How far to trace before giving up.				(Default - infinite.)
// Extent:			extents (optional)
// 
//------------------------------------------------------------------------------
// Returns:			Hit BSP/Actor or None if reached limit.
//------------------------------------------------------------------------------

static function Actor TraceRecursive
(	Actor			Instance,
	out vector		HitLocation,
	out vector		HitNormal,
	vector			StartLoc,
	optional bool	bTraceActors,
	optional float	TraceInterval,
	optional vector	TraceDirection,
	optional float	TraceLimit,
	optional vector Extent

)
{
	local Actor HitActor;
	local vector EndLoc;

	if( TraceInterval ~= 0.0 )
	{
		TraceInterval = 500.0;
	}

	if( TraceLimit != 0 && TraceInterval > TraceLimit )
	{
		TraceInterval = TraceLimit;
	}

	if( TraceDirection == vect(0,0,0) )
	{
		TraceDirection = vect(0,0,-1);
	}
	else
	{
		TraceDirection = Normal(TraceDirection);
	}

	EndLoc		= StartLoc + TraceDirection * TraceInterval;
	HitActor	= Instance.Trace( HitLocation, HitNormal, EndLoc, StartLoc, bTraceActors, Extent );

	if( TraceLimit > 0.0 )
	{
		TraceLimit -= TraceInterval;
		if( TraceLimit <= 0.0 )
		{
			return HitActor;	// Stop whether we've found anything or not.
		}
	}

	if( HitActor == None )
	{
		// we didn't hit anything -- continue tracing from where we stopped.
		HitActor = TraceRecursive( Instance, HitLocation, HitNormal, EndLoc, bTraceActors, TraceInterval, TraceDirection, TraceLimit, Extent );
	}

	return HitActor;
}


//=============================================================================

static function Actor GetHitActor( ePawn P, bool bSafe )
{					 
	local vector HitLocation, HitNormal;

	return GetHitActorInfo( P, HitLocation, HitNormal, bSafe );
}



//------------------------------------------------------------------------------
// Use to find out if a leech of the given type is attached to the given Pawn.
//------------------------------------------------------------------------------
static final function bool HasLeechOfType( Pawn Other, name LeechType, optional bool bIgnoreSubclasses )
{
	local bool bHasLeech;

	local LeechIterator IterL;
	local Leech L;

	IterL = class'LeechIterator'.static.GetIteratorFor( Other );
	for( IterL.First(); !IterL.IsDone(); IterL.Next() )
	{
		L = IterL.GetCurrent();

		if( bIgnoreSubclasses )
		{
			if( L.Class.Name == LeechType )
			{
				bHasLeech = true;
				break;
			}
		}
		else
		{
			if( L.IsA( LeechType ) )
			{
				bHasLeech = true;
				break;
			}
		}
	}
	IterL.Reset();
	IterL = None;

	return bHasLeech;
}



//------------------------------------------------------------------------------
// Use to find out if a reflector of the given type is installed in the given Pawn.
//------------------------------------------------------------------------------
static final function bool HasReflectorOfType( Pawn Other, name ReflectorType, optional bool bIgnoreSubclasses )
{
	local bool bHasReflector;

	local ReflectorIterator IterR;
	local Reflector R;

	IterR = class'ReflectorIterator'.static.GetIteratorFor( Other );
	for( IterR.First(); !IterR.IsDone(); IterR.Next() )
	{
		R = IterR.GetCurrent();

		if( bIgnoreSubclasses )
		{
			if( R.Class.Name == ReflectorType )
			{
				bHasReflector = true;
				break;
			}
		}
		else
		{
			if( R.IsA( ReflectorType ) )
			{
				bHasReflector = true;
				break;
			}
		}
	}
	IterR.Reset();
	IterR = None;

	return bHasReflector;
}



//------------------------------------------------------------------------------
// Shift out of all bad, removable, non-projectile leeches and reflectors.
//------------------------------------------------------------------------------
static final function ShiftOutOfLeechesAndReflectors( Pawn Other )
{
	ShiftOutOfLeeches( Other );
	ShiftOutOfReflector( Other );
}



//------------------------------------------------------------------------------
// Shift out of all bad, removable, non-projectile leeches.
//------------------------------------------------------------------------------
static final function ShiftOutOfLeeches( Pawn Other )
{
	local LeechIterator IterL;
	local Leech L;

	IterL = class'LeechIterator'.static.GetIteratorFor( Other );
	if (IterL != none)
	{
		for( IterL.First(); !IterL.IsDone(); IterL.Next() )
		{
			L = IterL.GetCurrent();

			if( L.bRemovable && L.bDeleterious && !L.bFromProjectile )
			{
				L.UnAttach();
				L.Destroy();
			}
		}
		IterL.Reset();
		IterL = None;
	}
}



//------------------------------------------------------------------------------
// Shift out of all bad, removable, non-projectile reflectors.
//------------------------------------------------------------------------------
static final function ShiftOutOfReflector( Pawn Other )
{
	local ReflectorIterator IterR;
	local Reflector R;

	IterR = class'ReflectorIterator'.static.GetIteratorFor( Other );
	
	if (IterR != none)
	{
		for( IterR.First(); !IterR.IsDone(); IterR.Next() )
		{
			R = IterR.GetCurrent();

			if( R.bRemovable && R.bDeleterious && !R.bFromProjectile )
			{
				R.UnInstall();
				R.Destroy();
			}
		}

		IterR.Reset();
		IterR = None;
	}
}

static function Inventory AddInventoryTypeToHolder( Pawn InventoryHolder, class<Inventory> InventoryType )
{
	local Inventory NewInventoryItem;
	//local bool bLastNeverSwitchOnPickup;
	
	//class'Debug'.static.DebugAssert( InventoryHolder, ( None != InventoryHolder ), "bogus inventory holder", default.DebugCategoryName );
	//class'Debug'.static.DebugAssert( InventoryHolder, ( None != InventoryType ), "bogus inventory type", default.DebugCategoryName );
	
	//class'Debug'.static.DebugLog( InventoryHolder, "AddInventoryTypeToHolder attempting to add a " $ InventoryType, default.DebugCategoryName );
	if( None == InventoryHolder.FindInventoryType( InventoryType ) )
	{
		//class'Debug'.static.DebugLog( InventoryHolder, "AddInventoryTypeToHolder " $ InventoryType $ " not currently in inventory", default.DebugCategoryName );
		//a valid inventory type was passed
		//an item of the same inventory type does not exist in the pawn's inventory
		
		NewInventoryItem = InventoryHolder.Spawn( InventoryType, InventoryHolder, , InventoryHolder.Location );
		//class'Debug'.static.DebugLog( InventoryHolder, "AddInventoryTypeToHolder spawned " $ NewInventoryItem, default.DebugCategoryName );
		//class'Debug'.static.DebugLog( InventoryHolder, "AddInventoryTypeToHolder bHeldItem " $ NewInventoryItem.bHeldItem, default.DebugCategoryName );

		if( NewInventoryItem != None )
		{
			//bLastNeverSwitchOnPickup = InventoryHolder.bNeverSwitchOnPickup;
			//InventoryHolder.bNeverSwitchOnPickup = true;
	
			NewInventoryItem = InventoryHolder.Spawn(NewInventoryItem.Class);
			//class'Debug'.static.DebugLog( InventoryHolder, "AddInventoryTypeToHolder spawned copy" $ NewInventoryItem, default.DebugCategoryName );
			
			//class'Debug'.static.DebugLog( InventoryHolder, "AddInventoryTypeToHolder " $ NewInventoryItem $ " added to inventory", default.DebugCategoryName );
			//class'Debug'.static.DebugLog( InventoryHolder, "AddInventoryTypeToHolder bHeldItem " $ NewInventoryItem.bHeldItem, default.DebugCategoryName );
			
			//InventoryHolder.bNeverSwitchOnPickup = bLastNeverSwitchOnPickup;
			//class'Debug'.static.DebugLog( InventoryHolder, "AddInventoryTypeToHolder " $ NewInventoryItem $ " added to inventory", default.DebugCategoryName );
		}
	}
	else
	{
		//class'Debug'.static.DebugLog( InventoryHolder, "AddInventoryTypeToHolder item exists", default.DebugCategoryName );
	}
	return NewInventoryItem;
}



//Return inventory item from holder's inventory matching class 'InventoryType'
static final function Inventory GetInventoryItem( Pawn InventoryHolder, class<Inventory> InventoryType )
{
    local Inventory CurrentInventoryItem;
    local Inventory FoundInventoryItem;
    
    for( CurrentInventoryItem = ePawn(InventoryHolder).Inventory;
    		( ( None != CurrentInventoryItem ) && ( None == FoundInventoryItem ) );
    		CurrentInventoryItem = CurrentInventoryItem.Inventory )
    {
        if( ClassIsChildOf( CurrentInventoryItem.Class, InventoryType ) )
        {
            FoundInventoryItem = CurrentInventoryItem;
        }
    }
    
    return FoundInventoryItem;
}



static function Inventory RemoveInventoryItem( Pawn InventoryHolder, class<Inventory> InventoryType )
{
	local Inventory RemovedInventoryItem;

	RemovedInventoryItem = class'WOTUtil'.static.GetInventoryItem( InventoryHolder, InventoryType );

	if( RemovedInventoryItem != None )
	{
		ePawn(InventoryHolder).DeleteInventory( RemovedInventoryItem );
	}

	return RemovedInventoryItem;
}



//Return weapon from holder's inventory matching class 'WeaponType'
static function Weapon GetInventoryWeapon( Pawn InventoryHolder, class<Weapon> WeaponType )
{
	return Weapon( class'WOTUtil'.static.GetInventoryItem( InventoryHolder, WeaponType ) );
}



//Delete weapon matching given 'WeaponType' from holder's inventory if found & return weapon 
static final function Weapon RemoveInventoryWeapon( Pawn InventoryHolder, class<Weapon> WeaponType )
{
	return Weapon( class'WOTUtil'.static.RemoveInventoryItem( InventoryHolder, WeaponType ) );
}


//static final function InvokeWotPawnFindLeader( Captain InvokingCaptain )
//{
//    local WOTPawn CurrentWotPawn;
//    //Could just look for Grunts, but you'd pick up the Captains
//    //anyway (since Captain derives from Grunt).
//    foreach InvokingCaptain.RadiusActors( class'WOTPawn', CurrentWotPawn, InvokingCaptain.LeadershipRadius )
//    {
//    	if( CurrentWotPawn != InvokingCaptain )
//    	{
//    		//class'Debug'.static.DebugLog( InvokingCaptain, "InvokeWotPawnFindLeader notifing " $ CurrentWotPawn, DebugCategoryName );
//	        CurrentWotPawn.FindLeader();
//        }
//    }
//}



//static function bool CollectAvailableWotPawns( Pawn SearchingPawn, ItemCollector Collector, float HelpRadius )
//{
//    local WOTPawn CurrentWotPawn;
//    local int i, ItemCount, RejectedCount;
    
//   	class'Debug'.static.DebugLog( SearchingPawn, "CollectAvailableWotPawns HelpRadius " $ HelpRadius, default.DebugCategoryName );
//	Collector.CollectRadiusItems( SearchingPawn, class'WotPawn', HelpRadius + 500 ); //xxxrlo
//	if( Collector.GetItemCount( ItemCount ) )
//	{
//		for( i = 0; i < ItemCount; i++ )
//		{
//			CurrentWotPawn = WotPawn( Collector.GetItem( i ) );
//	    	class'Debug'.static.DebugLog( SearchingPawn, "CollectAvailableWotPawns considering CurrentWotPawn: " $ CurrentWotPawn, default.DebugCategoryName );
//			if( ( CurrentWotPawn == None ) || !CurrentWotPawn.CanBeGivenNewOrders() )
//      		{
//	      		Collector.RejectItem( i );
//		      	RejectedCount++;
//    	    }
//		}
//	}
	
//   	class'Debug'.static.DebugLog( SearchingPawn, "CollectAvailableWotPawns ItemCount: " $ ItemCount, default.DebugCategoryName );
//   	class'Debug'.static.DebugLog( SearchingPawn, "CollectAvailableWotPawns RejectedCount: " $ RejectedCount, default.DebugCategoryName );
	
//	return ( ( ItemCount - RejectedCount ) > 0 );
//}



//static function bool GetNextAvailableWotPawn( Pawn SearchingPawn, GoalAbstracterInterf Goal, ItemSorter Sorter )
//{
//    local WOTPawn CurrentWotPawn;
//    local bool bFoundWotPawn;
//    local int i, ItemCount;
    
//	Sorter.InitSorter();
//	Sorter.SortReq.IR_Origin = SearchingPawn.Location;
//	Sorter.SortItems();
	
//	Sorter.DebugLog( SearchingPawn );
	
//	if( Sorter.GetItemCount( ItemCount ) )
//	{
//		for( i = 0; i < ItemCount; i++ )
//		{
//			if( Sorter.IsItemAccepted( i ) )
//			{
//    			class'Debug'.static.DebugLog( SearchingPawn, "CollectAvailableWotPawns considering CurrentWotPawn: " $ CurrentWotPawn, default.DebugCategoryName );
//				CurrentWotPawn = WotPawn( Sorter.GetItem( i ) );
//				if( CurrentWotPawn != None )
//		      	{
//		      		if( CurrentWotPawn.CanBeGivenNewOrders() )
//	    	  		{
//				    	Goal.AssignObject( SearchingPawn, CurrentWotPawn );
//    				    if( Goal.IsGoalNavigable( SearchingPawn ) )
//	        			{
//    	   	    	    	bFoundWotPawn = true;
//				    	  	Sorter.RejectItem( i );
//    	   		        	break;
//	        			}
//	        		}
//    	    		else
//        			{
//		   		      	Sorter.RejectItem( i );
//        			}
//		        }
//    	    }
//   		}
//	}
	
//	if( !bFoundWotPawn )
//	{
//		class'Debug'.static.DebugLog( SearchingPawn, "GetClosestAvailableWotPawn none available", default.DebugCategoryName );
//		Goal.Invalidate( SearchingPawn );
//	}
	
//	class'Debug'.static.DebugLog( SearchingPawn, "GetClosestAvailableWotPawn returning " $ bFoundWotPawn, default.DebugCategoryName );
//	return bFoundWotPawn;
//}



//static final function bool GetTeamSealAltar( GoalAbstracterInterf SealAltarGoal,
//		Pawn SearchingPawn )
//{
//    local SealAltar CurrentSealAltar, ReturnedSealAltar;
//    local bool bFoundSealAltar;
    
//    foreach SearchingPawn.AllActors( class'SealAltar', CurrentSealAltar )
//    {
//    	class'Debug'.static.DebugLog( SearchingPawn, "GetTeamSealAltar considering CurrentSealAltar: " $ CurrentSealAltar, default.DebugCategoryName );
//    	class'Debug'.static.DebugLog( SearchingPawn, "GetTeamSealAltar considering CurrentSealAltar.Team: " $ CurrentSealAltar.Team, default.DebugCategoryName );
//    	if( CurrentSealAltar.Team == SearchingPawn.PlayerReplicationInfo.Team )
//    	{
//	    	SealAltarGoal.AssignObject( SearchingPawn, CurrentSealAltar );
//        	if( SealAltarGoal.IsGoalNavigable( SearchingPawn ) )
//	        {
//	        	//the current seal altar is directly reachable or pathable
//	        	ReturnedSealAltar = CurrentSealAltar;
//				bFoundSealAltar = true;
//				break;
//    	    }
//        }
//    }
	
//	if( !bFoundSealAltar )
//	{
//		class'Debug'.static.DebugLog( SearchingPawn, "GetTeamSealAltar none available ", default.DebugCategoryName );
//		SealAltarGoal.Invalidate( SearchingPawn );
//	}


//	class'Debug'.static.DebugLog( SearchingPawn, "GetTeamSealAltar returning " $ bFoundSealAltar, default.DebugCategoryName );
//    return bFoundSealAltar;
//}


//static final function bool GetClosestUnownedSeal( GoalAbstracterInterf SealGoal, Pawn SearchingPawn )
//{
//	return GetClosestGoal( SealGoal, class'Seal', SearchingPawn, true );
//}



//static final function bool GetClosestTeamSeal( GoalAbstracterInterf SealGoal, Pawn SearchingPawn )
//{
//	return GetClosestGoal( SealGoal, class'Seal', SearchingPawn, false, true );
//}



//static function bool GetClosestAlarm( GoalAbstracterInterf AlarmGoal,
//		Pawn SearchingPawn, float AlarmRadius )
//{
//	//the alarm needs to be on the same team as the captain?
//	return GetClosestGoal( AlarmGoal, class'Alarm', SearchingPawn, true );
//}



//// <sarcasm>Let's hear it for polymorphism!</sarcasm> -- where's a good Interface when you need one?
//static final function byte GetActorTeam( Actor Other )
//{
//	// NOTE[aleiby]: Make sure we aren't pulling in too many assets (memory-wise) by doing this.

//	local byte Team;

//	Team = 255;	// Use something else for failure? (-1)

//	if( Other == None )
//	{
//		warn( Other $ " doesn't have a Team variable." );
//		assert( false );
//	}
//	if( Other.IsA( 'Pawn' ) && Pawn(Other).PlayerReplicationInfo != None )
//	{
//		Team = Pawn(Other).PlayerReplicationInfo.Team;
//	}
//	else if( Other.IsA( 'Seal' ) )
//	{
//		Team = Seal(Other).Team;
//	}
//	else if( Other.IsA( 'SealAltar' ) )
//	{
//		Team = SealAltar(Other).Team;
//	}
//	else if( Other.IsA( 'AngrealIllusionProjectile' ) )
//	{
//		Team = AngrealIllusionProjectile(Other).Team;
//	}
///*
//	else if( Other.IsA( 'MashadarGuide' ) )
//	{
//		Team = MashadarGuide(Other).Team;
//	}
//	else if( Other.IsA( 'MashadarManager' ) )
//	{
//		Team = MashadarManager(Other).Team;
//	}
//*/
//	else if( Other.IsA( 'WOTZoneInfo' ) )
//	{
//		Team = WOTZoneInfo(Other).Team;
//	}
//	else if( Other.IsA( 'BudgetInfo' ) )
//	{
//		Team = BudgetInfo(Other).Team;
//	}
//	else if( Other.IsA( 'Inventory' ) && Inventory(Other).Owner != None )
//	{
//		//!! hmmm... what if there is a cyclical chain of Inventory items each with the next as its Owner?
//		Team = GetActorTeam( Inventory(Other).Owner );
//	}
//	else
//	{
//		warn( Other $ " doesn't have a Team variable." );
//		assert( false );
//	}

//	return Team;
//}




//static final function bool GetClosestGoal( GoalAbstracterInterf Goal, class<actor> GoalClass, Pawn SearchingPawn, 
//		optional bool bOnlyUnownedGoals, optional bool bOnlySameTeam )
//{
//    local Actor CurrentActor;
//    local bool bFoundNavigableGoal;
//    local int i, ItemCount;
//    local ItemSorter GoalSorter;
	
//	GoalSorter = ItemSorter( class'Singleton'.static.GetInstance( SearchingPawn.XLevel, class'ItemSorter' ) );
//	GoalSorter.CollectAllItems( SearchingPawn, GoalClass );
//	GoalSorter.InitSorter();
//	GoalSorter.SortReq.IR_Origin = SearchingPawn.Location;
//	GoalSorter.SortItems();
//	if( GoalSorter.GetItemCount( ItemCount ) )
//	{
//		class'Debug'.static.DebugLog( SearchingPawn, "GetClosestGoal GoalClass " $ GoalClass, default.DebugCategoryName );
//		class'Debug'.static.DebugLog( SearchingPawn, "GetClosestGoal ItemCount " $ ItemCount, default.DebugCategoryName );
//		for( i = 0; i < ItemCount; i++ )
//		{
//			CurrentActor = GoalSorter.GetItem( i );
//			class'Debug'.static.DebugLog( SearchingPawn, "GetClosestGoal CurrentActor " $ CurrentActor, default.DebugCategoryName );
//			if(	!bOnlyUnownedGoals || ( CurrentActor.Owner == None ) && !bOnlySameTeam ||
//					( GetActorTeam( CurrentActor ) == GetActorTeam( SearchingPawn ) ) )
//    		{
//    			//goal ownership is irrelevent or the goal is unowned
//	    		Goal.AssignObject( SearchingPawn, CurrentActor );
//	    	    if( Goal.IsGoalNavigable( SearchingPawn ) )
//    	    	{
//					//the goal is navigable
//					bFoundNavigableGoal = true;
//					break;
//		        }
//			}
//		}
//	}
	
// 	if( !bFoundNavigableGoal )
//	{
//		Goal.Invalidate( SearchingPawn );
//		class'Debug'.static.DebugLog( SearchingPawn, "GetClosestGoal no goal", default.DebugCategoryName );
//	}
//	else
//	{
//		class'Debug'.static.DebugLog( SearchingPawn, "GetClosestGoal CurrentActor " $ CurrentActor, default.DebugCategoryName );
//	}
	
//    return bFoundNavigableGoal;
//}



//static function bool GetLeader( Pawn SearchingPawn, out Captain Leader )
//{
//    local Captain CurrentCaptain;
//    local ItemSorter GoalSorter;
//    local int i, ItemCount;
//    local bool bFoundLeader;
    
//	GoalSorter = ItemSorter( class'Singleton'.static.GetInstance( SearchingPawn.XLevel, class'ItemSorter' ) );
//	GoalSorter.CollectAllItems( SearchingPawn, class'Captain' );
//	GoalSorter.InitSorter();
//	GoalSorter.SortReq.IR_Origin = SearchingPawn.Location;
//	GoalSorter.SortItems();

//	if( GoalSorter.GetItemCount( ItemCount ) )
//	{
//		for( i = 0; i < ItemCount; i++ )
//		{
//			if( GoalSorter.IsItemAccepted( i ) )
//			{
//				CurrentCaptain = Captain( GoalSorter.GetItem( i ) );
//   				class'Debug'.static.DebugLog( SearchingPawn, "CollectAvailableWotPawns considering CurrentCaptain: " $ CurrentCaptain, default.DebugCategoryName );
//   				bFoundLeader = ( ( CurrentCaptain != None ) && CurrentCaptain.IsLeader() && ( GoalSorter.GetSortScore( i ) < CurrentCaptain.LeadershipRadius ) );
//				if( bFoundLeader )
//    		  	{
//					Leader = CurrentCaptain;
//					break;
//		        }
//		        else
//	    	    {
//		    		GoalSorter.RejectItem( i );
//		        }
//		 	}
//		}
//	}
//    return bFoundLeader;
//}



//===========================================================================
// Control when WOTPawns/WOTPlayers will gib.
//===========================================================================

static function bool WOTGibbed( Pawn DeadPawn, Name DamageType, float GibForSureFinalHealth, float GibSometimesFinalHealth, float GibSometimesOdds )
{
	if( DeadPawn.Tag == 'balefired' )
	{
		return false;
	}

	if ( (DeadPawn.Health < GibForSureFinalHealth) || 
		 ((DeadPawn.Health < GibSometimesFinalHealth) && (FRand() < GibSometimesOdds)) )
	{
		return true;
	}

	return false;
}
defaultproperties
{
    //DebugCategoryName=WOTUtil

}