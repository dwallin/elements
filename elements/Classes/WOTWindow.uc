//=============================================================================
// WOTWindow.uc
// $Author: Mfox $
// $Date: 1/05/00 2:38p $
// $Revision: 10 $
//=============================================================================
class WOTWindow extends uiWindow abstract;

//#exec TEXTURE IMPORT FILE=Textures\Hud\InfoBoxC1.pcx GROUP=UI MIPS=FALSE FLAGS=2050 //PF_NoSmooth | PF_Masked
//#exec TEXTURE IMPORT FILE=Textures\Hud\InfoBoxC2.pcx GROUP=UI MIPS=FALSE FLAGS=2050 //PF_NoSmooth | PF_Masked

var() bool bPauseGame;
var() bool bHideUI;
var() bool bBlank;
var() string MinimumResolution;

var string OldResolution;
var ePawn PlayerOwner;

var string CornerTextureName;
var string OtherTextureName;
var Texture2d CornerTexture;
var Texture2d OtherTexture;

// window component coordinates
var Region TL,   Top,        TR;
var Region Left, Background, Right;
var Region BL,   Bottom,     BR;
var float LOffset, TOffset, ROffset, BOffset;

//=============================================================================

simulated function PreBeginPlay()
{
	//local Pawn P;

	Super.PreBeginPlay();
	
	//foreach AllActors( class'Pawn', P )
	//{
		//PlayerOwner = P;
		
		//P.bShowMenu = false;
 		//	if( bPauseGame && Level != None && giWOT(Level.Game) != None )
		//{
		//	 pauses game but also stops sounds (e.g. MissionObjectives voiceover) from playing
		//	giWOT(Level.Game).InternalPause( true, false, P );
		//}
		//if( bBlank )
		//{
		//	 blank the background
		//	P.Player.Console.bNoDrawWorld = true;
		//}
		//if( bHideUI )
		//{
		//	ePawn(P).HideUI( true );
		//}
		//else
		//{
		//	ePawn(P).HideUIExceptForHands( true );
		//}
		//if( MinimumResolution != "" )
		//{
			//OldResolution = P.ConsoleCommand( "GetCurrentRes" );
			
			//if( OldResolution != "" )
			//{
			//	ePawn(P).SetResolution( MinimumResolution, true );
			//}
		//}
	//}
}

//=============================================================================

function Destroyed()
{
	//local Pawn P;

	//foreach AllActors( class'Pawn', P )
	//{
 	//	if( WorldInfo != None && giWOT(WorldInfo.Game) != None )
 	//	{
  // 			giWOT(WorldInfo.Game).InternalPause( false, false, P );
		//}

		//ePawn(P).HideUI( false );
		//ePawn(P).HideUIExceptForHands( false );

		//if( bBlank && P.Player != None && P.Player.Console != None )
		//{
		//	P.Player.Console.bNoDrawWorld = false;
		//}

		//if( MinimumResolution != "" && OldResolution != "" )
		//{
		//	ePawn(P).SetResolution( OldResolution, false );
		//	OldResolution = "";
		//}
	//}

	Super.Destroyed();
}

//=============================================================================

function DrawSeparator( Canvas C, int PosX, int PosY, int Indent, region TexRegL, region TexRegC, region TexRegR, Texture2d Tex )
{
	C.SetPos( PosX, PosY );
	DrawRegion( C, Indent, C.CurY, TexRegL, Tex );
	TileRegion( C, C.CurX, C.CurY, WindowClipSizeX - 2*(Indent+TexRegL.W), TexRegC.H, TexRegC, Tex );
	DrawRegion( C, C.CurX, C.CurY, TexRegR, Tex );
}

//=============================================================================

function DrawWindow( canvas C )
{
	C.bNoSmooth = true;
	
	//if( C.Style != ERenderStyle.STY_None )
	//{
	//	C.Style = ERenderStyle.STY_Normal;
	//}
	
	if( CornerTexture == None )
	{
		CornerTexture = Texture2d( DynamicLoadObject( CornerTextureName, class'Texture2d' ) );
	}

	FillRegion( C, WindowPosX+LOffset+8, WindowPosY+TOffset+8, WindowSizeX-LOffset-ROffset, WindowSizeY-TOffset-BOffset, Background, CornerTexture );

	//if( C.Style != ERenderStyle.STY_None )
	//{
	//	C.Style = ERenderStyle.STY_Masked;
	//}
	
	if( OtherTexture == None )
	{
		OtherTexture = Texture2d( DynamicLoadObject( OtherTextureName, class'Texture2d' ) );
	}

	TileRegion( C, WindowPosX+LOffset,	WindowPosY+TL.H,					Left.W,				WindowSizeY-TL.H*2, Left,		OtherTexture );
	TileRegion( C, WindowPosX+WindowSizeX-ROffset, WindowPosY+TL.H,					Right.W,			WindowSizeY-TL.H*2, Right,	OtherTexture );
	TileRegion( C, WindowPosX+TL.W,      WindowPosY+TOffset,				WindowSizeX-TL.W*2, Top.H,    Top,		OtherTexture );
	TileRegion( C, WindowPosX+TL.W,      WindowPosY+WindowSizeY-BOffset,	WindowSizeX-TL.W*2, Bottom.H, Bottom,	OtherTexture );

	DrawRegion( C, WindowPosX,					WindowPosY,					TL, CornerTexture );
	DrawRegion( C, WindowPosX+WindowSizeX-TR.W,	WindowPosY,					TR, CornerTexture );
	DrawRegion( C, WindowPosX,					WindowPosY+WindowSizeY-BL.H, BL, CornerTexture );
	DrawRegion( C, WindowPosX+WindowSizeX-BR.W,	WindowPosY+WindowSizeY-BR.H, BR, CornerTexture );
}

//=============================================================================

simulated function Draw( Canvas C)
{
	super.Draw(C);
	WindowSizeX = WindowBaseSizeX;
	WindowSizeY = WindowBaseSizeY;
	
	WindowPosX = (Max(640, C.SizeX) - WindowSizeX) / 2 + WindowOffsetX;
	WindowPosY = (Max(480, C.SizeY) - WindowSizeY) / 2 + WindowOffsetY;

	C.DrawColor.R = 255;
	C.DrawColor.G = 255;
	C.DrawColor.B = 255;

	DrawWindow( C );
	
	WindowClipPosX	= LegendCanvas(C).ScaleValX( WindowPosX + WindowBorderX );
	WindowClipPosY	= LegendCanvas(C).ScaleValY( WindowPosY + WindowBorderY );
	WindowClipSizeX	= LegendCanvas(C).ScaleValX( WindowSizeX - 2*WindowBorderX );
	WindowClipSizeY	= LegendCanvas(C).ScaleValY( WindowSizeY - 2*WindowBorderY );

	C.SetOrigin( WindowClipPosX, WindowClipPosY );
	C.SetClip( WindowClipSizeX, WindowClipSizeY );
	C.SetPos( 0, 0 );
}

// end of WOTWindow.uc
defaultproperties
{
    CornerTextureName="WOT.Icons.InfoBoxC1"

    OtherTextureName="WOT.Icons.InfoBoxC2"

    TL=(X=0,Y=0,W=64,H=64)

    Top=(X=96,Y=240,W=16,H=16)

    TR=(X=64,Y=0,W=64,H=64)

    Left=(X=64,Y=240,W=16,H=16)

    Background=(X=64,Y=64,W=16,H=16)

    Right=(X=80,Y=240,W=16,H=16)

    BL=(X=0,Y=64,W=64,H=64)

    Bottom=(X=112,Y=240,W=16,H=16)

    BR=(X=64,Y=64,W=64,H=64)

    LOffset=16.00

    TOffset=4.00

    ROffset=31.00

    BOffset=20.00

}