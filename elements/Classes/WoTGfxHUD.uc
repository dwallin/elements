class WoTGfxHUD extends GFxMoviePlayer;

//Create a Health Cache variable
var float LastHealthpc;

//Create variables to hold references to the Flash MovieClips and Text Fields that will be modified
var GFxObject HealthMC, HealthBarMC;
var GFxObject HealthTF;

//  Function to round a float value to an int
function int roundNum(float NumIn) {
 local int iNum;
 local float fNum;

 fNum = NumIn;
 iNum = int(fNum);
 fNum -= iNum;
 if (fNum >= 0.5f) {
 return (iNum + 1);
 }
 else {
 return iNum;
 }
}

//  Function to return a percentage from a value and a maximum
//function int getpc(int val, int max) {
// return roundNum((float(val) / float(max)) * 100.0f);
//}

//Called from STHUD'd PostBeginPlay()
//function Init(PlayerController PC) {
// //Start and load the SWF Movie
// Start();
// Advance(0.f);

 //Set the cahce value so that it will get updated on the first Tick
// LastHealthpc = -1337;

// //Load the references with pointers to the movieClips and text fields in the .swf
// HealthMC = GetVariableObject("_root.healthbar_mc");
// HealthBarMC = GetVariableObject("_root.healthbar_mc.bar_mc");
// HealthTF = GetVariableObject("_root.healthbar_mc.health_txt");
//}

//Called every update Tick
//function TickHUD() {
// local UTPawn UTP;

// //We need to talk to the Pawn, so create a reference and check the Pawn exists
// UTP = UTPawn(PlayerOwner.Pawn);
// if (UTP == None) {
// return;
// }

// //If the cached value for Health percentage isn't equal to the current...
// //if (LastHealthpc != getpc(UTP.Health, UTP.HealthMax)) {
// ////...Make it so...
// //LastHealthpc = getpc(UTP.Health, UTP.HealthMax);
// ////...Update the bar's xscale (but don't let it go over 100)...
// //HealthBarMC.SetFloat("_xscale", (LastHealthpc > 100) ? 100.0f : LastHealthpc);
// ////...and update the text field
// //HealthTF.SetString("text", round(LastHealthpc)$"%");
// }
//}

DefaultProperties
{
 //this is the HUD. If the HUD is off, then this should be off
 //bDisplayWithHudOff=false
 //The path to the swf asset we will create later
// MovieInfo=SwfMovie'STAssets.HUD.HudSwf'
 //Just put it in...
 //bGammaCorrection = false
}