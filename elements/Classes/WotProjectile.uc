//================================================================================
// WotProjectile.
//================================================================================

class WotProjectile extends UTProjectile
  Abstract;

var() const editconst name DestroyedEvent;

defaultproperties
{
    DestroyedEvent=WotProjectileDestroyed

    bCanTeleport=True

}