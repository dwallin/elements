//=============================================================================
// HUD: Superclass of the heads-up display.
//=============================================================================
class eHUD extends UDKHUD
	abstract
	config(user);

//=============================================================================
// Variables.

var globalconfig int HudMode;	
var globalconfig int crosshair;
//var() class<menu> MainMenuType;
var() string HUDConfigWindowType;

//var	Menu MainMenu;

//=============================================================================
// Status drawing.

simulated event PreRender(  );
simulated event PostRender(  );
simulated function InputNumber(byte F);
simulated function ChangeHud(int d);
simulated function ChangeCrosshair(int d);
simulated function DrawCrossHair( Canvas Canvas, int StartX, int StartY);

//=============================================================================
// Messaging.
simulated function PlayReceivedMessage( string S, string PName, ZoneInfo PZone )
{
	PlayerOwner.Pawn.ClientMessage(S);
	//if (PlayerOwner.Pawn.bMessageBeep)
	//	PlayerOwner.Pawn.PlayBeepSound();
}

// DisplayMessages is called by the Console in PostRender.
// It offers the HUD a chance to deal with messages instead of the
// Console.  Returns true if messages were dealt with.
simulated function bool DisplayMessages()
{
	return false;
}
defaultproperties
{
    Crosshair=13

    //HUDConfigWindowType="UMenu.UMenuHUDConfigCW"

    bHidden=True

    RemoteRole=1

}