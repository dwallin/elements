class ePawn extends UTPawn;

enum ELoadSave {
  LS_Load,
  LS_Save
};

struct PlayerTroopMap
{
  var name Player;
  var name Troop;
  var string TroopClassName;
};

enum ETransitionType {
  TRT_None,
  TRT_EndOfLevel,
  TRT_RestartLevel,
  TRT_LoadLevel,
  TRT_NewGame
};

enum EMotionDirection {
  MD_None,
  MD_Forward,
  MD_Backward,
  MD_Left,
  MD_Right
};

var ePlayerReplicationInfo ePlayerReplicationInfo;

var bool bForceRotation;
var rotator ForcedRotationRate;

var float DefaultFOV;
var float DesiredFOV;
var float FOVAngle;

var bool bIsFemale;
var bool bAdmin;
//Add package names to default properties as neeed.
var() string LoadClassFromNamePackages[4];
var bool bEditing;
var bool bTeleportingDisabled;
var float LastTeleportFailMessage;
var Vector EditStartLoc;
var Rotator EditStartRot;
var int SelectedZone;
var Rotator ViewOffset;
var bool bShowOverview;
var bool bIsPlayer;
var(WOTSounds) class<GenericTextureHelper>		TextureHelperClass;
var()		   class<GenericDamageHelper>		DamageHelperClass;
var GenericAssetsHelper	AssetsHelper;

var name ReducedDamageType;
var float ReducedDamagePct;

var Inventory Inventory;

var HandSet AngrealHandSet;
var HandSet CitadelEditorHandSet;
var HandSet CurrentHandSet;

var repnotify bool bWeaving;
var bool bHealing;
var bool bNeoing;
var() float StreakLength;
var BalefireDecal BDecal;
var class<BalefireDecal> BaleDecal;
var Vector HitLoc, HitNorm;
var EMotionDirection MotionDirection;
var int NumDeaths;
var() Color PlayerColor;
var Pawn SuicideInstigator;
var float SuicideInstigationTime;
var bool bAcceptedInventory;
var Class<ePawn> PlayerClass;
var HUD myHUD;
var HUD PrevHUD;
var IconInfo FirstIcon;				// the linked list of all angreal icons currently affecting the player
var float NextPainSoundTime;
var() float HitHardHealthRatio;
var() float ExceptionalDeathHealthRatio;
var() float WalkBackwardSpeedMultiplier;
var bool bSetSpeedCheatOn;
var() float SpeedPlayAnimAfterLanding;
var() float PlayLandHardMinVelocity;
var() float PlayLandSoftMinVelocity;
var() float LadderLandingNoiseVelocity;
var() float LadderLandedHitNormalZ;
var() float GibForSureFinalHealth;
var() float GibSometimesFinalHealth;
var() float GibSometimesOdds;
var() float BaseGibDamage;
var() bool bNeverGib;
var bool bCarcassSpawned;
var int SeekerCount;				// How many seekers are currently tracking the player

var transient UTWeapon       UTWeapon;        // The pawn's current weapon.
var UTWeapon				PendingWeapon;	// Will become weapon once current weapon is put down

var Inventory SelectedItem;

var AngrealInventory AngrealInventory;

var LegendActorComponent ViewTarget;

// Offset for SA function 
var() vector SAOffset;
var() localized string SelectedStr;
var() localized string TerAngrealStr;
var() localized string InventoryInfoHitStr;
var() name SubTitlesPackageName;
var() globalconfig bool bSubtitles;
var() string SubTitleLanguages[5];
var() float MessageDurationSecsPerChar;
var() float MinMessageDuration;
var() globalconfig bool bShowSounds;
var() globalconfig bool bShowOtherSounds;
var() globalconfig bool bShowLocalizeSoundErrors;
var() float InventoryInfoHintDuration;
//var WOTHelpInfo WOTHelpItem;
var()/* globalconfig*/ class<Inventory> AngrealHandOrder[100];
//var() name IllusionAnimSequence;
//var() float IllusionAnimRate;
var() globalconfig float PlayerRestartGameDelay;
var() Class<Reflector> DefaultReflectorClasses[20];
var Reflector CurrentReflector;
var Leech FirstLeech;
var Class<Pawn> ApparentClass;
var() transient int ApparentTeam;
var Texture2d ApparentIcon;
var Texture2d HealthIcons[4];
var Texture2d DisguiseIcon;
var int ShieldHitPoints;
var PlayerTroopMap PlayerTroops[12];
var localized string TeamDescription;
var transient int JoinTeamTimeout;
var bool bProhibitEditorExit;
var localized string CantChangeTeamsStr;
var localized string TeleportProhibitedEditingStr;
var localized string TeleportProhibitedJoinStr;
var localized string WaitingForSealsStr;
var localized string WaitingForEditStr;
var localized string CantEditLevelStr;
var localized string CantEditZoneStr;
var localized string CantEditAreaStr;
var localized string CantEditNowStr;
var localized string CantEditBudgetStr;
var localized string CantEditLimitStr;
var localized string CantPlaceResourceStr;
var localized string CantLeaveEditorStr;
var() Class<AngrealInventory> DefaultAngrealInventory;
var string ArtifactNames[55];
var class<hud> HUDType;
replication
{
	if ( Role == ROLE_Authority )
		bWeaving, StreakLength, bHealing, BDecal, HitLoc, HitNorm, CurrentHandset,		bEditing,
		bTeleportingDisabled,
		JoinTeamTimeout,
		bShowOverview,
        //OverviewType,
		FirstIcon,
        ShieldHitPoints,
		bForceRotation, 
		ForcedRotationRate,
		ApparentIcon,
		HealthIcons,
		DisguiseIcon,
		//AnimationTableClass,
		ApparentClass,
		SeekerCount;
}
//===========================================================================
simulated function  PreBeginPlay()
{
	local int i;
	//local AngrealInventory DefaultAngreal;
	local Reflector DefaultReflector;
	//local string LevelName;
	//local string SkinName;

    Super.PreBeginPlay();

	TravelPreAccept();
	TravelPostAccept();
	// initialize the player class with the concrete class (modified when the player changes classes)
	PlayerClass = Class;

	//if( Role == ROLE_Authority )
	//{
		CreateAngrealHands(); // build hands before accepting inventory during level load and SP level transition

		////////////////////////////////////
		// Default Reflector Installation //
		////////////////////////////////////
		for( i = 0; i < ArrayCount(DefaultReflectorClasses); i++ )
		{
			if( DefaultReflectorClasses[i] != None )
			{
				DefaultReflector = Spawn( DefaultReflectorClasses[i],self );
				DefaultReflector.Install( self );
			}
		}
		self.Controller.bIsPlayer = true;

		// single-player specific code to change main
		// Player Character's skin from mission_xx on
		//if( Level.Netmode == NM_Standalone )
		//{
		//	// force base skin
		//	SetSkin( PlayerClass.default.MultiSkins[1] );
	
		//	if( AltSkinStr != "" )
		//	{
		//		LevelName = Level.GetLocalURL();

		//		LevelName = Caps(LevelName);
		//		i = InStr( LevelName, "MISSION_" );

		//		if( i != -1 )
		//		{
		//			LevelName = Mid( LevelName, i+8, 2 );
		//			i = int( LevelName );

		//			if( i >= SkinSwitchLevel )
		//			{
		//				// switch to alternate skin
		//				SetSkin( Texture( DynamicLoadObject( AltSkinStr, Class'Texture' ) ) );
		//			}
		//		}
		//	}
		//}
		// Update PlayerType (PlayerReplicationInfo).
		//if		( IsA('Forsaken') )		PlayerReplicationInfo.PlayerType = 1;
		//else if	( IsA('Hound') )		PlayerReplicationInfo.PlayerType = 2;
		//else if	( IsA('Whitecloak') )	PlayerReplicationInfo.PlayerType = 3;
		//else							PlayerReplicationInfo.PlayerType = 0;

	//	Update Color (PlayerReplicationInfo).
	//	SkinName = string(Skin.Name);
	//	if		( InStr( SkinName, "Black"	)	!= -1 )	PlayerReplicationInfo.Color = 1;
	//	else if	( InStr( SkinName, "Blue"	)	!= -1 )	PlayerReplicationInfo.Color = 2;
	//	else if	( InStr( SkinName, "Red"	)	!= -1 )	PlayerReplicationInfo.Color = 3;
	//	else if	( InStr( SkinName, "Green"	)	!= -1 )	PlayerReplicationInfo.Color = 4;
	//	else											PlayerReplicationInfo.Color = 0;
	//}

	//if( AnimationTableClass == None )
	//{
	//	BroadcastMessage( Self $ "::AnimationTableClass not set!" );
	//	`warn( Self $ "::AnimationTableClass not set!" );
	//	Destroy();
	//}

	SetupAssetClasses();

	// enable subtitles (can also be enabled in user.ini with bSubtitles=true)?
	//for( i=0; i<ArrayCount(SubTitleLanguages); i++ )
	//{
	//	if( GetLanguage() == SubTitleLanguages[i] )
	//	{
	//		bSubtitles = true;
	//		break;
	//	}
	//}
}

//---------------------------------------------------------------------------
event Possess()
{

	if( WorldInfo.NetMode == NM_Standalone || WorldInfo.Netmode == NM_Client )
	{
		if( BaseHUD(myHUD) == None )
		{
			myHUD = Spawn( HUDType, Self );
		}
	}

	//if( WorldInfo.Netmode != NM_Client && Level.Game.IsA( 'giMPBattle' ) )
	//{
	//	ClientPreLoadBudgets();
	//	ClientSetNumTeams( giMPBattle(Level.Game).Battle.NumTeams );
	//}

	//if( Level.NetMode == NM_Standalone )
	//{
	//	NextState = GetStateName();
	//	GotoState( 'WaitToShowMissionObjectives' );
	//}
}

//===========================================================================
// Helper function  for creating and intializing the hands.
//-----------------------------------------------------------------------------
simulated function CreateAngrealHands()
{
	local int i;
	local int j;
	local HandInfo Hand;
	local class<Inventory> ClassName;

	if( AngrealHandSet != None )
	{
		CurrentHandSet = AngrealHandSet;
		return;
	}
	
	AngrealHandSet = Spawn( class'HandSet', self );
	CurrentHandSet = AngrealHandSet;
	
	for( i = 0; i < AngrealHandSet.GetArrayCount(); i++ ) 
	{
		Hand = Spawn( class'HandInfo', Self );
		AngrealHandSet.SetHand( i, Hand );
	}

	// Default initialization
	for( i = 0; i < AngrealHandSet.GetArrayCount(); i++ ) 
	{
		Hand = AngrealHandSet.GetHand( i );
		for( j = 0; j < Hand.GetArrayCount(); j++ ) 
		{
			ClassName = AngrealHandOrder[ i * Hand.GetArrayCount() + j ];
			if( ClassName != none ) 
			{
				Hand.AddClassName( ClassName, j );
				Hand.AddItem(ClassName, true);
			}
		}
	}
}

function SetDesiredFOV (float F)
{
  if ( (F >= 80.0) || /*WorldInfo.bAllowFOV ||*/ bAdmin || (WorldInfo.NetMode == 0) )
  {
    DefaultFOV = FClamp(F,1.0,170.0);
    DesiredFOV = DefaultFOV;
  }
}
//---------------------------------------------------------------------------
function SetupAssetClasses()
{
	if( CurrentReflector != None )
	{
		CurrentReflector.SetupAssetClasses();
	}
}

//-----------------------------------------------------------------------------
// Add to our normal inventory and distribute item to our hands.
//-----------------------------------------------------------------------------
simulated function bool AddInventory( class<Inventory> NewItem )
{
	local bool bSuccess;
	local Inventory Inv;
	local Inventory NewInv;
	local int InvCount;
	
		//check for already existing ta and add rand charges
		foreach self.AllActors(class'Inventory', Inv)
		{
 			if(Inv.Class == NewItem)
			{				
				AngrealInventory(Inv).AddCharges(rand(AngrealInventory(Inv).MaxCharges + 1));
				NewInv = spawn(NewItem, self);
				AngrealInventory(NewInv).CurCharges = AngrealInventory(Inv).CurCharges;
				self.InvManager.RemoveFromInventory(Inv);
				bSuccess = self.InvManager.AddInventory(NewInv);
				if( bSuccess )
				{
					ClientAddItem( NewItem );
				
					if( SelectedItem == None && ( class<AngrealInventory>(NewItem) != None /*|| Seal(NewItem) != None || SealInventory(NewItem) != None*/ ) ) // avoid keys, and other non-selectable items
					{
						Select( NewItem );
					}
				}
				return bSuccess;
			}
		}

		// Add this sucker to our normal inventory.
		NewInv = spawn(NewItem, self);
		bSuccess = self.InvManager.AddInventory(NewInv);
		
	// If pawn added it to our inventory, we better distribute it to the hands.
	if( bSuccess )
	{
		//Inv = spawn(NewItem, self);
		
		ClientAddItem( NewItem );

		if( SelectedItem == None && ( class<AngrealInventory>(NewItem) != None /*|| Seal(NewItem) != None || SealInventory(NewItem) != None*/ ) ) // avoid keys, and other non-selectable items
		{
			Select( NewItem );
		}

//DEBUG
		for( Inv = Inventory; Inv != None; Inv = Inv.Inventory )
		{
			if( Inv.Class == NewItem.Class )
			{
				InvCount++;
			}
		}
		if( InvCount != 1 )
		{
			`warn( Self$".AddInventory( "$ NewItem $" ) InvCount="$ InvCount );
		}

	}
	return bSuccess;
}

//---------------------------------------------------------------------------
simulated function Destroyed()
{
	//if( WorldInfo.NetMode != NM_Client ) // drop seals from server only
	//{
	//	DropSeals();
	//}

	CancelAngrealEffects();
	class'WOTUtil'.static.RemoveAllReflectorsFrom( Self );

	Super.Destroyed();

	CurrentHandSet = None;
	if( AngrealHandSet != None )
	{
		AngrealHandSet.Destroy();
		AngrealHandSet = None;
	}
	//if( CitadelEditorHandSet != None )
	//{
	//	CitadelEditorHandSet.Destroy();
	//	CitadelEditorHandSet = None;
	//}
	//if( MySoundSlotTimerList != None )
	//{
	//	MySoundSlotTimerList.Destroy();
	//}
	if( AssetsHelper != None )
	{
		AssetsHelper.Destroy();
	}
}

function FixInventory() //MWP ~*?
{
	local Inventory Inv;
	local int AbortCount;

	if( WorldInfo.NetMode == NM_Client && CurrentHandSet != None )
	{
		CenterMessage( "FixInventory" );

		CurrentHandSet.Empty();
		AbortCount = 999;
		foreach AllActors( class'Inventory', Inv )
		{
			if( Inv.Owner == Self )
			{
				CurrentHandSet.AddItem( Inv.Class );
			}
			if( --AbortCount == 0 ) //hack to avoid crash due to circularly linked inventory
			{
				break;
			}
		}
	}
}

function DumpInventory()
{
	local Inventory Inv;
	local int Count, AbortCount;

	CenterMessage( "DumpInventory" );
	// show player inventory (unsorted)
	`log( Self$".Inventory = "$ Inventory );
	foreach AllActors( class'Inventory', Inv )
	{
		if( Inv.Owner == Self )
		{
			`log( "    "$ Inv$".Inventory = "$ Inv.Inventory );
			Count++;
		}
	}
	`log( "Inventory Count:" $ Count );
	`log( " " );

	// show player's inventory list
	AbortCount = 999;
	foreach AllActors( class'Inventory', Inv )
	{
		`log( "  "$ Inv$".Inventory = "$ Inv.Inventory );
		if( --AbortCount == 0 ) //hack to avoid crash due to circularly linked inventory
		{
			break;
		}
	}
	`log( "STOP" );
}

//===========================================================================
// Called before getting any inventory items.
//---------------------------------------------------------------------------
simulated function TravelPreAccept()
{
	//Super.TravelPreAccept();
	// build hands before accepting inventory during level transitions
	CreateAngrealHands();
}

//===========================================================================
// Called after getting any inventory items.
//---------------------------------------------------------------------------
simulated function TravelPostAccept()
{
	//Super.TravelPostAccept();
	if ( Health <= 0 )
	{
		Health = Default.Health;
	}
	// update selected hand and selected item to match SelectedItem
	if( SelectedItem != None )
	{
		CurrentHandSet.SelectItem( SelectedItem.Class );
	}
}

simulated function  PostBeginPlay()
{
	super.PostBeginPlay();
}

//-----------------------------------------------------------------------------
// Removes all non default reflectors and leeches.
//-----------------------------------------------------------------------------
simulated function CancelAngrealEffects()
{
	local Leech L;
	local Reflector R;
	local LeechIterator IterL;
	local ReflectorIterator IterR;
	local eInventoryManager Manager;
	local Inventory Inv;
	
	//AngrealInventory(SelectedItem).StopFire(0);
	//for(Inv = InvManager.InventoryChain; Inv != none; Inv = InvManager.InventoryChain)
	//{
	//	if( AngrealInventory(Inv) != None )
	//	{
	//		AngrealInventory(Inv).Reset();
	//	}
	//}

	IterL = class'LeechIterator'.static.GetIteratorFor( Self );
	for( IterL.First(); !IterL.IsDone(); IterL.Next() )
	{
		L = IterL.GetCurrent();

		if( L.bRemovable )
		{
			L.UnAttach();
			L.Destroy();
		}
	}
	IterL.Reset();
	IterL = None;

	IterR = class'ReflectorIterator'.static.GetIteratorFor( Self );
	for( IterR.First(); !IterR.IsDone(); IterR.Next() )
	{
		R = IterR.GetCurrent();

		if( R.bRemovable )
		{
			R.UnInstall();
			R.Destroy();
		}
	}
	IterR.Reset();
	IterR = None;

	ShieldHitPoints = 0;	// Just to be safe.
}


//-----------------------------------------------------------------------------
// override Pawn.Died() to handle destruction of HandInfo (and to ditch ALL
// inventory, not just the current Weapon).
simulated function bool Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
	local Inventory Inv;
    local vector X,Y,Z;
	local int i;
	local BagHolding Bag;
	local float CollisionRad, CollisionHei;
	
	// cancel any angreal effects (like decay, soulbarb, etc.) so they aren't still active after
	// the player comes alive again.
	CancelAngrealEffects();

    GetAxes( Rotation, X, Y, Z );

    // ditch any seals
	//DropSeals();

	// Reset all the seeker count - this should be zero anyway...
	SeekerCount = 0;

	//if( WorldInfo.NetMode != NM_Standalone )	// Don't drop in singleplayer.
	//{
	if (Owner != none)
	{
		// Put artifacts into a bag.
		for(Inv = InvManager.InventoryChain; Inv != none; Inv = Inv.InvManager.InventoryChain)
		{
			// take the item away from the player before putting it in the bag (Inventory.DeleteInventory() sets Owner=None)
			DeleteInventory( Inv );

			if( Inv.Class != DefaultAngrealInventory && !Inv.IsA('AngrealInvSpecial') )
			{
				// Restrict charges.
				if( AngrealInventory(Inv) != None && AngrealInventory(Inv).CurCharges > AngrealInventory(Inv).MaxInitialCharges )
				{
					AngrealInventory(Inv).CurCharges = AngrealInventory(Inv).MaxInitialCharges;
				}
		
				// Make the bag only if we need it.
				if( Bag == None )
				{
					Bag = Spawn( class'BagHolding' );
				}
				Bag.AddItem( Inv );
			}
		}
		if( Bag != None )
		{
			Bag.GetBoundingCylinder(CollisionRad, CollisionHei);
			Bag.DropFrom( Location + ( CollisionRad + CollisionRad ) * X, Bag.Owner.Velocity );
			//Bag.DropFrom( Location + ( Bag.CollisionRadius + CollisionRadius ) * X );
		}
	}
	//}
	
	// don't let GameInfo.DiscardInventory() drop the weapon twice, and deselect the active item
    Weapon = None;
	SelectedItem = None;

	// Record death (includes suicides -- PlayerReplicationInfo.Deaths does not.)
	NumDeaths++;
	
    Super.Died( Killer, damageType, HitLocation );

	// inventory is destroyed in GameInfo.DiscardInventory() called by Pawn.Died()
	// clear hands to clear potential replication errors
	if( AngrealHandSet != None )
	{
		AngrealHandSet.Empty();
	}
	//if( CitadelEditorHandSet != None )
	//{
	//	CitadelEditorHandSet.Empty();
	//}
	return true;
}

function ProcessEffect (Invokable i)
{
  if ( CurrentReflector != None )
  {
    CurrentReflector.ProcessEffect(i);
  }
}

//-----------------------------------------------------------------------------
// Returns the best target that it can find for a seeking projectile.
//-----------------------------------------------------------------------------
reliable server function Actor FindBestTarget( vector Loc, rotator ViewRot, float MaxAngleInDegrees, optional AngrealInventory UsingArtifact )
{
	if( CurrentReflector != None )
	{
		return CurrentReflector.FindBestTarget( Loc, ViewRot, MaxAngleInDegrees, UsingArtifact );
	}
	else
	{
		`warn( "This function  shouldn't be called on the client." );
		return None;
	}
}

//simulated function IgnoreEffect(Invokable I)
//{
//	if( I.SourceProjectile != None )
//	{
//		// Don't let GenericProjectiles explode.
//		if( GenericProjectile(I.SourceProjectile) != None )
//		{
//			GenericProjectile(I.SourceProjectile).bExplode = false;
//			I.SourceProjectile.bSuppressExplosionFX = true;
//			// We have to destroy it manually since it won't explode now.
//			I.SourceProjectile.bProjTarget = false;
//			I.SourceProjectile.bBlockActors = false;
//			I.SourceProjectile.Damage = 0;
//			I.SourceProjectile.Destroy();
//		}
//	}
//}

//-----------------------------------------------------------------------------
// Increases the health of the pawn by the given amount.
//-----------------------------------------------------------------------------
simulated function IncreaseHealth( int Amount )
{
	if( CurrentReflector != None )
	{
		CurrentReflector.IncreaseHealth( Amount );	// Reflect function  call.
	}
}

//------------------------------------------------------------------------------
// Called by angreal projectiles to notify the victim what just hit them.
//------------------------------------------------------------------------------
simulated function NotifyHitByAngrealProjectile( AngrealProjectile HitProjectile )
{
	if( CurrentReflector != None )
	{
		CurrentReflector.NotifyHitByAngrealProjectile( HitProjectile );
	}
}

//------------------------------------------------------------------------------
// Called by seeker angreal to notify the victim that it has just targeted it.
//------------------------------------------------------------------------------
function NotifyTargettedByAngrealProjectile( AngrealProjectile Proj )
{

	if( CurrentReflector != None )
	{
		CurrentReflector.NotifyTargettedByAngrealProjectile( Proj );
	}
}

//===========================================================================
// Overridden so we can control when ePawns gib.

//function  bool Gibbed( Name DamageType )
//{
//	return !bNeverGib && class'WOTUtil'.static.WOTGibbed( Self, DamageType, GibForSureFinalHealth, GibSometimesFinalHealth, GibSometimesOdds );
//}

//===========================================================================
// Disable telefragging
//---------------------------------------------------------------------------
event EncroachedBy( actor Other );

//===========================================================================
// Reflect this function  call.
// MAY override; if so, must call parent function  
//===========================================================================
function TakeDamage(int Damage, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	//TBI -- call ReduceDamage first -- if Damage==0, then don't respond to attacks!

	//if( DamageType == class'SkewRip' )
	//{
	//	//Warder's thrust/lift attack
	//	GotoState( 'classImpaled' );
	//}
	//else if( DamageType == class'Grabbed' )
	//{
	//	//Minion's tentacle grab
	//	GotoState( 'Grabbed' );
	//}
	//if( Damage > MinDamageToShake )
	//{
	//	DamageShake( Damage );
	//}
	if( CurrentReflector != None )
	{
		CurrentReflector.TakeDamage( Damage, EventInstigator, HitLocation, Momentum, DamageType );
	}
}

//=============================================================================

function KilledBy( pawn EventInstigator )
{
	TakeDamage( Health-GibSometimesFinalHealth, EventInstigator.Controller, Location, vect(0,0,0), class'Suicided' );
}

//------------------------------------------------------------------------------
// This is a hack:  It is used by DefaultTakeDamageReflector.
//------------------------------------------------------------------------------
function SuperTakeDamage( int Damage, Controller EventInstigator, Vector HitLocation, vector Momentum, class<DamageType> DamageType)
{
	super.TakeDamage( Damage, EventInstigator, HitLocation, Momentum, DamageType );
	//if( CurrentReflector != None )
	//{
	//	CurrentReflector.TakeDamage( Damage, EventInstigator, HitLocation, Momentum, DamageType );
	//}
	
}

//-----------------------------------------------------------------------------
function  FootZoneChange( ZoneInfo newFootZone )
{
	if( CurrentReflector != None )
	{
		CurrentReflector.FootZoneChange( newFootZone );
	}
	//else
	//{
	//	Super.FootZoneChange( newFootZone );
	//}
}

//------------------------------------------------------------------------------
// This is a hack:  It is used by WOTFootZoneChangeReflector.
//------------------------------------------------------------------------------
function  SuperFootZoneChange( ZoneInfo newFootZone )
{
	//Super.FootZoneChange( newFootZone );
}

//------------------------------------------------------------------------------
// View shaking caused by player receiving damage. 
//------------------------------------------------------------------------------
// Get rid of damage if we don't scale shaking by damage amount..
function  DamageShake( int Damage )
{
	//ShakeView( 0.2, 2500, -50 );
}


simulated function  bool AdjustHitLocation (out Vector HitLocation, Vector TraceDir)
{
  local float adjZ;
  local float maxZ;
  local float CollisionRadius, CollisionHeight;
  TraceDir = Normal(TraceDir);
  GetBoundingCylinder(CollisionRadius, CollisionHeight);
  HitLocation = HitLocation + 0.5 * CollisionRadius * TraceDir;
  if ( BaseEyeHeight == Default.BaseEyeHeight )
  {
    return True;
  }
  maxZ = Location.Z + EyeHeight + 0.25 * CollisionHeight;
  if ( HitLocation.Z > maxZ )
  {
    if ( TraceDir.Z >= 0 )
    {
      return False;
    }
    adjZ = (maxZ - HitLocation.Z) / TraceDir.Z;
    HitLocation.Z = maxZ;
    HitLocation.X = HitLocation.X + TraceDir.X * adjZ;
    HitLocation.Y = HitLocation.Y + TraceDir.Y * adjZ;
    if ( VSize(HitLocation - Location) > CollisionRadius )
    {
      return False;
    }
  }
  return True;
}

simulated function IncrementSeekerCount ()
{
  //if ( Role < 4 )
  //{
  //  return;
  //}
  SeekerCount++;
  if ( SeekerCount == 1 )
  {
    PlaySound(SoundCue(DynamicLoadObject("WOT.Sounds.SeekerNotification_Cue",Class'SoundCue')));
  }
}

simulated function DecrementSeekerCount ()
{
  //if ( Role < 4 )
  //{
    //return;
  //}
  SeekerCount--;
  if ( SeekerCount < 0 )
  {
    SeekerCount = 0;
  }
}

//-----------------------------------------------------------------------------
// override of Pawn.DeleteInventory() to handle WOT-specific "Hands", and current selection
function bool DeleteInventory( inventory Item )
{
	local int i;
	local int j;
	local HandInfo Hand;
		
    if( Item == None )
        return false;

	if( AngrealInventory(Item) != None )
	{
		if( Item == SelectedItem )
		{
			CeaseUsingAngreal();
		}

		AngrealInventory(Item).Reset();
	}
	
	if (self != none && InvManager != none)
	{
		InvManager.RemoveFromInventory( Item );
	}
	ClientRemoveItem( Item.Class );

	return true;
}

//-----------------------------------------------------------------------------
simulated function ClientAddItem( class<Inventory> ItemName )
{
	if( CurrentHandSet != None )
	{
		CurrentHandSet.AddItem( ItemName );
	}
}

//-----------------------------------------------------------------------------
simulated function ClientRemoveItem( class<Inventory> ItemName )
{
	if( CurrentHandSet != None )
	{
		CurrentHandSet.RemoveItem( ItemName );
	}
}

//-----------------------------------------------------------------------------
function bool ServerDeleteInventory( inventory Item )
{
	return DeleteInventory( Item );
}

//=============================================================================

function Meteor()
{
	local Projectile Meteor;
	local vector HitLocation, SpawnLocation, HitNormal;
	
	if( !OKToCheat() )
	{
		return;
	}

	class'LegendActorComponent'.static.TraceRecursive( Self, SpawnLocation , HitNormal, Location, false,, vect(0,0,1) );
	class'LegendActorComponent'.static.TraceRecursive( Self, HitLocation, HitNormal, Location + vect(0,0,1) * BaseEyeHeight, false,, vector(GetViewRotation()) );
	Meteor = Spawn( class<Projectile>(DynamicLoadObject( "FireballSprayer", class'Class' )),,, SpawnLocation, rotator(HitLocation - SpawnLocation) );
	Meteor.Instigator = Self;
}

//=============================================================================
// Section/Zone render testing
//=============================================================================

function SelectPlayerStart( int Team )
{
	local PlayerStart S;

	if( !OKToCheat() )
	{
		return;
	}

	foreach AllActors( class'PlayerStart', S )
	{
		if( S.TeamIndex == Team )
		{
			SetLocation( S.Location );
			SetPhysics( PHYS_Falling );
			break;
		}
	}
}

////=============================================================================
//function  SetZone( int Zone )
//{
//	if( !OKToCheat() )
//	{
//		return;
//	}

//	super.SetZoneNumber( SelectedZone );
//}

//=============================================================================
// Sanity check to make sure my reflectors are installed in me.
//=============================================================================

function CheckReflectors()
{
	local Reflector ref;

	if( !OKToCheat() )
	{
		return;
	}

	ClientMessage( "Checking Reflectors:" );
	for( ref = CurrentReflector; ref != None; ref = ref.NextReflector )
	{
		if( ref.Owner == self )
		{
			ClientMessage( ref $" installed in self." );
		}
		else
		{
			ClientMessage( ref $ " not installed in self" );
		}
	}
}

//=============================================================================

function CheckLeeches()
{
	ClientCheckLeeches();
}

//=============================================================================

function ClientCheckLeeches()
{
	local Leech L;

	if( !OKToCheat() )
	{
		return;
	}

	ClientMessage( "Checking Leeches:" );
	for( L = FirstLeech; L != None; L = L.NextLeech )
	{
		if( L.Owner == Self )
		{
			ClientMessage( L $" attached to self." );
		}
		else
		{
			ClientMessage( L $ " not attached to self." );
		}
	}
}

//=============================================================================

function CheckInventory()
{
	local Inventory Inv;

	if( !OKToCheat() )
	{
		return;
	}

	ClientMessage( "Checking Inventory:" );
	for( Inv=Inventory; Inv!=None; Inv=Inv.Inventory )
	{
		if( Inv.Owner == self )
		{
			ClientMessage( Inv.class $ " installed in self." );
		}
		else
		{
			ClientMessage( Inv.class $ " not installed in self." );
		}
	}
}

//=============================================================================
// Debug function :
// Used to spawn your favorite angreal.
//=============================================================================

function SA( int i )
{
	local class<AngrealInventory> AngrealClass;
	
	if( !OKToCheat() )
	{
		return;
	}

	if( ArtifactNames[i] != "" )
	{
		AngrealClass = class<AngrealInventory>( DynamicLoadObject( ArtifactNames[i], class'Class' ) );
		if( AngrealClass != None )
		{
			Spawn( AngrealClass,,, Location + (SAOffset >> Rotation) );
		}
	}
}

//=============================================================================

function GiveItem( string ItemName )
{
	local class<Inventory> ItemClass;
	local Inventory Item;

	if( !OKToCheat() )
	{
		return;
	}
	
	ItemClass = class<Inventory>( DynamicLoadObject( ItemName, class'Class' ) );
	if( ItemClass != None )
	{
		Item = Spawn( ItemClass,,, Location + (SAOffset >> Rotation) );

		if( Item != None )
		{
			Item.Touch( Self, none, Item.Location, Location );
		}
		else
		{
			ClientMessage( "Unable to Spawn "$ItemName );
		}
	}
	else
	{
		ClientMessage( ItemName$" is not an Inventory class." );
	}
}

//=============================================================================
function PrevInventory()
{
	local int Selected;

	Selected = CurrentHandSet.GetSelectedHand().Selected;
	CurrentHandSet.GetSelectedHand().SelectPrevious();
	if( CurrentHandSet.GetSelectedHand().Selected >= Selected )
	{
		PrevHand();
	}
	else
	{
		PerformSelection( false );
	}
}

//-----------------------------------------------------------------------------
function NextInventory()
{
	local int Selected;

	Selected = CurrentHandSet.GetSelectedHand().Selected;
	CurrentHandSet.GetSelectedHand().SelectNext();
	if( CurrentHandSet.GetSelectedHand().Selected <= Selected )
	{
		NextHand();
	}
	else
	{
		PerformSelection( false );
	}
}

//-----------------------------------------------------------------------------
function PrevHand()
{
	SelectHand( CurrentHandSet.GetPrevious() );
}

//-----------------------------------------------------------------------------
function NextHand()
{
	SelectHand( CurrentHandSet.GetNext() );
}

//-----------------------------------------------------------------------------
function SelectHand( int Index )
{
	local bool bNewHand;

	bNewHand = (Index != CurrentHandSet.Selected);

	CurrentHandSet.Select( Index );
	PerformSelection( bNewHand );
}


simulated function PerformSelection (bool bNewHand)
{
  ServerSelectItem(CurrentHandSet.GetSelectedHand().GetSelectedClassName());
  if ( SelectedItem != None )
  {
    if ( bNewHand )
    {
      PlaySound(SoundCue(DynamicLoadObject("WOT.Sounds.SelectHand_Cue",Class'SoundCue')));
    } else {
      PlaySound(SoundCue(DynamicLoadObject("WOT.Sounds.SelectArtifact_Cue",Class'SoundCue')));
    }
  }
  //ClientSelectItem();
 // ClientSelectItemMessage(CurrentHandSet.GetSelectedHand().GetSelectedClassName());
}

//=============================================================================
// Select the specified object if found
//=============================================================================
simulated function Select( class<Inventory> InvName )
{
	local inventory Inv;
	//local inventory Item;
	local HandInfo Hand;

	Inv = FindInventoryType( InvName, true );
	if( Inv != None )
	{
		CeaseUsingAngreal();
		
		ClientSelect( InvName );
		SelectedItem = Inv;
		hand.SelectItem(InvName);
		super.SetActiveWeapon(AngrealInventory(Inv));
	}	
}

//=============================================================================

simulated function SelectItem( class<Inventory> ItemName )
{
	local Inventory ItemClass;

	if( !OKToCheat() )
	{
		return;
	}

	ItemClass = FindInventoryType(ItemName, true);
	//ItemClass = class<Inventory>( DynamicLoadObject( ItemName.Class.Name, class'Class' ) );
	//ItemClass = class<Inventory>(class'Singleton'.static.GetInstance(self.WorldInfo, ItemName));

	if( ItemClass != None )
	{
		CeaseUsingAngreal();
		SelectedItem = FindInventoryType( ItemName, true );
	}
	else
	{
		ClientMessage( ItemName$" is not an Inventory class." );
	}
}

//=============================================================================

function Summon( string ClassName )
{
	local string GivenClassName;
	local class<actor> NewClass;

	if( !OKToCheat() )
	{
		return;
	}

	GivenClassName = ClassName;
	
	if( instr( ClassName, "." ) >= 0 )
	{
		NewClass = class<actor>( DynamicLoadObject( ClassName, class'Class' ) );
	}
	else
	{
		// no package given -- try various WOT packages
		ClassName = "WOT." $ GivenClassName;
		
		NewClass = class<actor>( DynamicLoadObject( ClassName, class'Class', true ) );
		if( NewClass==None )
		{
			ClassName = "WOTPawns." $ GivenClassName;
		
			NewClass = class<actor>( DynamicLoadObject( ClassName, class'Class', true ) );
			if( NewClass==None )
			{
				ClassName = "Angreal." $ GivenClassName;
		
				NewClass = class<actor>( DynamicLoadObject( ClassName, class'Class', true ) );
				if( NewClass==None )
				{
					ClassName = "WOTDecorations." $ GivenClassName;
		
					NewClass = class<actor>( DynamicLoadObject( ClassName, class'Class', true ) );
				}
					if( NewClass==None )
					{
						ClassName = "elements." $ GivenClassName;
		
						NewClass = class<actor>( DynamicLoadObject( ClassName, class'Class', true ) );
					}
			}
		}
	}

	if( NewClass!=None )
	{
		`log( "Summon: fabricated a " $ ClassName );

		Spawn( NewClass,,,Location + 72 * Vector(Rotation) + vect(0,0,1) * 15 );
	}
	else
	{
		`log( "Summon: class " $ GivenClassName $ " not found!" );

		ClientMessage( "  Class " $ GivenClassName $ " not found!" );
	}
}

//=============================================================================

function SetSpeed( float F )
{
	if( !OKToCheat() )
	{
		return;
	}

	// restore default values before applying SetSpeed 
	ScaleSpeedSettings( 1.0 );

	GroundSpeed = default.GroundSpeed * F;
	WaterSpeed = default.WaterSpeed * F;

	bSetSpeedCheatOn = ( F != 1.0 );
}


//=============================================================================
// testing

function DumpPawnsInventory( Pawn P, string Indent )
{
	local Inventory Inv;
	local int Count;
	
	// make sure we catch existing inventory items
	for( Inv=ePawn(P).Inventory; Inv!=None; Inv=Inv.Inventory ) 
	{
		if( AngrealInventory(Inv) == None )
			ClientMessage( Indent $ Inv.Name $ ": " $ Inv.GetStateName() );
		else
			ClientMessage( Indent $ Inv.Name $ ": " $ Inv.GetStateName() $ " Charges:" $ AngrealInventory(Inv).CurCharges );
			
		Count++;
	}
	
	ClientMessage( Indent $ "    Inventory Count:" $ Count );
}
function  bool ServerSetLocation(vector Destination)
{
	return SetLocation(Destination);

}
//=============================================================================
// Returns IconInfo corresponding to `IconTag' from FirstIcon list. 
// Might return None.
//-----------------------------------------------------------------------------

simulated function IconInfo FindIconInfo( name IconTag )
{
    local IconInfo curII;

    for( curII = FirstIcon; curII != None; curII = curII.Next )
        if( curII.Tag == IconTag )
            return curII;

    return None;
}

simulated function AddIconInfo( Actor DurationActor, bool bGoodIcon, name IconID )
{
    local IconInfo curII;
	local AngrealInventory Inv;
	local float Duration;
	local Texture2d Icon;

	if( Leech(DurationActor) != None )
	{
		Inv = Leech(DurationActor).SourceAngreal;
		if( Leech(DurationActor).StatusIconFrame != None )
		{
			Icon = Leech(DurationActor).StatusIconFrame;
		}
		else if( Inv != None )
		{
			Icon = Inv.StatusIconFrame;
		}
		else
		{
			`warn( "Leech has neither a SourceAngreal nor a StatusIconFrame, therefore we cannot display an icon for it." );
			return;
		}
		Duration = Leech(DurationActor).GetInitialDuration();
	}
	else if( Reflector(DurationActor) != None )
	{
		Inv = Reflector(DurationActor).SourceAngreal;
		if( Reflector(DurationActor).StatusIconFrame != None )
		{
			Icon = Reflector(DurationActor).StatusIconFrame;
		}
		else if( Inv != None )
		{
			Icon = Inv.StatusIconFrame;
		}
		else
		{
			`warn( "Reflector has neither a SourceAngreal nor a StatusIconFrame, therefore we cannot display an icon for it." );
			return;
		}
		Duration = Reflector(DurationActor).GetInitialDuration();
	} 
	else if( AngrealInventory(DurationActor) != None )
	{
		Inv = AngrealInventory(DurationActor);
		if( Inv.StatusIconFrame != None )
		{
			Icon = Inv.StatusIconFrame;
		}
		else
		{
			`warn( "AddIconInfo called for AngrealInventory with no StatusIconFrame." );
		}
		Duration = AngrealInventory(DurationActor).GetDuration();
	}
	else 
	{
		`warn( "Illegal IconInfo" );
		return;
	}

    if( IconID == '' )
	{
		`warn( "IconID not set." );
		return;
	}
	
	curII = FindIconInfo( IconID );

    // Update texture if it's already in the list.  This handles cases where
    // player is disguised as one guy, and then changes his disguise to
    // something else.
    if( curII == None ) 
	{
        curII = Spawn( class 'IconInfo', self, IconID );
	    curII.Next = FirstIcon;
	    FirstIcon = curII;
	}
	curII.Inv = DurationActor;
    curII.Icon = Icon;
    curII.bGoodIcon = bGoodIcon;
	curII.InitialDuration = Duration;
	curII.RemainingDuration = curII.InitialDuration;
}

simulated function NotifyAutoSwitch()
{
	PlaySound( SoundCue( DynamicLoadObject( "WOT.Sounds.SelectNewItem_Cue", class'SoundCue' ) ) );
}

simulated function RemoveIconInfo( name IconTag )
{
    local IconInfo  curII;
    local IconInfo  prevII;

	// Error check
	if( FirstIcon == None )
	{
		// No icons to remove
		return;
	}

    // removal of head is a special case...
    if( FirstIcon.Tag == IconTag ) 
	{
        curII = FirstIcon;
        FirstIcon = FirstIcon.Next;
        curII.Destroy();
    } 
	else 
	{
        prevII = FirstIcon;
        curII  = FirstIcon.Next;    // if() clause took care of curII == FirstIcon
        while( curII != None )
		{
            if( curII.Tag == IconTag )
			{
                prevII.Next = curII.Next;
                curII.Destroy();
                break;  // assume only one matching texture
            }

            prevII = curII;
            curII = curII.Next;
        }
    }
}

function ChargeUsed (AngrealInventory Ang)
{
  if ( CurrentReflector != None )
  {
    CurrentReflector.ChargeUsed(Ang);
  }
}

function bool OKToCheat (optional bool bQuiet)
{
  local bool bRetVal;

  bRetVal = True;
  if (  !bAdmin && (WorldInfo.NetMode != 0) )
  {
    bRetVal = False;
  }
  if (  !bRetVal &&  !bQuiet )
  {
	CenterMessage("function  is disabled on the server.");
  }
  return bRetVal;
}


function ServerAssessBattlefield ()
{
  //local BudgetInfo B;
  //local ePawn P;
  //local int NumSeals;
  //local int MaxNumSeals;

  //foreach AllActors(Class'BudgetInfo',B)
  //{
  //  NumSeals = B.GetItemCount(B.GetIndex('SealInventory'));
  //  MaxNumSeals = Max(MaxNumSeals,NumSeals);
  //}
  //if ( MaxNumSeals > 0 )
  //{
  //  CenterMessage(WaitingForSealsStr);
  //  return;
  //}
  //foreach AllActors(Class'ePawn',P)
  //{
  //  if ( P.bEditing )
  //  {
  //    CenterMessage(WaitingForEditStr);
  //    return;
  //  }
  //}
  //if ( giMPBattle(Level.Game) != None )
  //{
  //  giMPBattle(Level.Game).EnableTeleporters();
  //}
}

//=============================================================================
// Try to use the specified angreal.
//=============================================================================
function  UseSpecifiedAngreal( class<Inventory> InvName )
{
	local Inventory OldSelectedItem;

	// while the Citadel Game has not started, inhibit weapon use
	if( bTeleportingDisabled )
	{
		// if the player is on a team, assess the battlefield to start the Citadel Game
		//if( PlayerReplicationInfo.GetTeamNum() != 255 )
		//{
		//	ServerAssessBattlefield();
		//}
		return;
	}
	
	//if( WorldInfo.Pauser == none )
	//{
		OldSelectedItem = SelectedItem;

		CeaseUsingAngreal();
		//SelectedItem = FindInventoryName( InvName );
		SelectedItem = self.FindInventoryType( InvName, true );
		ServerUseAngreal();

	 	SelectedItem = OldSelectedItem;
		`log("UseSpecifiedAngreal SelectedITem = "@SelectedItem);
	 //}
}

//=============================================================================
// Turn the currently selected angreal on.
//=============================================================================
function UseAngreal()
{
	if( AngrealHandSet == None || CurrentHandSet != AngrealHandSet )
	{
		`warn( Self$".UseAngreal() AngrealHandSet="$ AngrealHandSet $" CurrentHandSet="$ CurrentHandSet );
		return;
	}

	// while the Citadel Game has not started, inhibit weapon use
	//if( bTeleportingDisabled )
	//{
	//	// if the player is on a team, assess the battlefield to start the Citadel Game
	//	if( PlayerReplicationInfo.GetTeamNum() != 255 )
	//	{
	//		ServerAssessBattlefield();
	//	}
	//	return;
	//}

	//if( WorldInfo.Pauser == none )
	//{
		if( SelectedItem != None )
		if( CurrentHandSet.GetSelectedHand().GetSelectedClassName() != none )
		{
			ServerUseAngreal();
		}
		else
		{
			// Select next best artifact if nothing is currently selected.
			if( CurrentHandSet.GetSelectedHand().IsEmpty() )
			{
				SelectHand( CurrentHandSet.GetNext() );
			}
			else
			{
				CurrentHandSet.GetSelectedHand().SelectFirst();
			}
			ServerSelectItem( CurrentHandSet.GetSelectedHand().GetSelectedClassName() );
			ClientSelectItem();
			ClientSelectItemMessage( CurrentHandSet.GetSelectedHand().GetSelectedClassName() );

			if( CurrentHandSet.GetSelectedHand().GetSelectedClassName() != none )
			{
				NotifyAutoSwitch();
			}
		//}
	}
	//super.StartFire(0);
}

simulated function SalvageInventoryLinks()
{
	local Inventory Inv, PrevInv;

	CenterMessage( "BAD PING" );
	`warn( "Aborting infinite recursion" );

	if( WorldInfo.NetMode == NM_Client )
	{
		foreach AllActors( class'Inventory', Inv )
		{
			Inv.Inventory = None;
		}
	}
}

simulated function Inventory FindInventoryName( class<Inventory> ClassName )
{
	local Inventory Inv;
	
	local int AbortCount;

	if( ClassName == none )
		return None;
	
	Inv = self.FindInventoryType( ClassName, true );

	AbortCount = 999;
	foreach AllActors(class'Inventory', Inv)
	{
		if( Inv.Class == ClassName )
		{
			return Inv;
		}
		if( --AbortCount == 0 ) //hack to avoid crash due to circularly linked inventory
		{
			SalvageInventoryLinks();
			break;
		}
	}
	if (Inv != none)
		return Inv;
	return None;
}

//-----------------------------------------------------------------------------
reliable server function ServerUseAngreal()
{
	if( CurrentReflector != None )
	{
		CurrentReflector.UseAngreal();
	}
}

//-----------------------------------------------------------------------------
simulated function NotifyCastFailed( AngrealInventory FailedArtifact )
{
	if( CurrentReflector != None )
	{
		CurrentReflector.NotifyCastFailed( FailedArtifact );
	}
}

//-----------------------------------------------------------------------------
function SetSuicideInstigator( Pawn Other )
{
	SuicideInstigator = Other;
	SuicideInstigationTime = WorldInfo.TimeSeconds;
}


//-----------------------------------------------------------------------------
function HandMessage( string Message, optional float Duration )
{
	if( BaseHUD( myHUD ) != None )
	{
		BaseHUD( myHUD ).AddHandMessage( Message, Duration );
	}
}

//-----------------------------------------------------------------------------
function LeftMessage( string Message, optional float Duration )
{
	if( BaseHUD( myHUD ) != None )
	{
		BaseHUD( myHUD ).AddLeftMessage( Message, Duration );
	}
}

//-----------------------------------------------------------------------------
function RightMessage( string Message, optional float Duration )
{
	if( BaseHUD( myHUD ) != None )
	{
		BaseHUD( myHUD ).AddRightMessage( Message, Duration );
	}
}

//-----------------------------------------------------------------------------
function CenterMessage( string Message, optional float Duration, optional bool bEcho )
{
	if( BaseHUD( myHUD ) != None )
	{
		BaseHUD( myHUD ).AddCenterMessage( Message, Duration, bEcho );
	}
}

//-----------------------------------------------------------------------------
function SubtitleMessage( string Message, optional float Duration, optional bool bEcho )
{
	if( BaseHUD( myHUD ) != None )
	{
		BaseHUD( myHUD ).AddSubtitleMessage( Message, Duration, bEcho );
	}
}

//-----------------------------------------------------------------------------
function GenericMessage( string Message, int X, int Y, bool bCenter, byte Intensity, Font F, optional float Duration )
{
	if( BaseHUD( myHUD ) != None )
	{
		BaseHUD( myHUD ).AddGenericMessage( Message, X, Y, bCenter, Intensity, F, Duration );
	}
}

//-----------------------------------------------------------------------------
function BroadcastLeftMessage( string Message, optional float Duration )
{
	local Pawn P;
	//if(  WorldInfoGame.AllowsBroadcast( Self, Len(Message) ) )
		for( P = WorldInfo.PawnList; P != None; P = P.NextPawn )
			if( ePawn(P) != None )
				ePawn(P).LeftMessage( Message, Duration );
}

//-----------------------------------------------------------------------------
function BroadcastRightMessage( string Message, optional float Duration )
{
	local Pawn P;
	//if(  WorldInfoGame.AllowsBroadcast( Self, Len(Message) ) )
		for( P = WorldInfo.PawnList; P != None; P = P.NextPawn )
			if( ePawn(P) != None )
				ePawn(P).RightMessage( Message, Duration );
}

//-----------------------------------------------------------------------------
function BroadcastCenterMessage( string Message, optional float Duration, optional bool bEcho )
{
	local Pawn P;
	//if( WorldInfo.Game.AllowsBroadcast( Self, Len(Message) ) )
		for( P = WorldInfo.PawnList; P != None; P = P.NextPawn )
			if( ePawn(P) != None )
				ePawn(P).CenterMessage( Message, Duration, bEcho );
}

//-----------------------------------------------------------------------------
event ClientMessage( coerce string S, optional Name Type )
{
	if( S != "" )
	{
		if( Type == 'Pickup' )
		{
			HandMessage( S );
		}
		else if( Type == 'WOTDeathMessage' )
		{
			if( WorldInfo.NetMode != NM_Standalone )
			{
				RightMessage( S );
			}
		}
		else if( Type == 'DeathMessage' )
		{
			// Depricated.
		}
		else if( Type == 'FailedMessage' )
		{
			HandMessage( S );
		}
		else
		{
			Super.ClientMessage( S, Type/*, bBeep*/ );
		}
	}
}


//=============================================================================

function byte GetApparentTeam() 
{
	if( ApparentTeam == -1 )
	{
		return PlayerReplicationInfo.GetTeamNum();
	}
	else
	{
	    return ApparentTeam;
	}
}

//=============================================================================

function ScaleSpeedSettings( float Multiplier )
{
	GroundSpeed = Multiplier * ApparentClass.default.GroundSpeed;
	WaterSpeed	= Multiplier * ApparentClass.default.WaterSpeed;
	AirControl	= Multiplier * ApparentClass.default.AirControl;
	AccelRate	= Multiplier * ApparentClass.default.AccelRate;
}

//=============================================================================

function ScaleSpeedForDirection()
{
	// if SetSpeed cheat was used, leave current values alone
	if( !bSetSpeedCheatOn )
	{
		if( MotionDirection == MD_Backward )
		{
			// player is backing up -- reduce his GroundSpeed etc.
			ScaleSpeedSettings( WalkBackwardSpeedMultiplier );
		}
		else
		{
			// player isn't backing up -- restore his GroundSpeed etc.
			ScaleSpeedSettings( 1.0 );
		}
	}
}

//=============================================================================
// Sets MotionDirection (general direction of motion of the given the motion 
// vector). Logical values for MotionVec and Velocity and/or Acceleration.
//=============================================================================

function SetMotionDirection( vector MotionVec ) //RLO Pitch isn't working, so never get Down/Up
{
    local rotator RotDiff;
    local rotator VelocityAsRot;
	local EMotionDirection NewMotionDirection;

    if( VSize(MotionVec) ~= 0.0 )
	{
        NewMotionDirection = MD_None;
    }
	else
	{    
	    VelocityAsRot = rotator(MotionVec);
	    RotDiff = Normalize( GetViewRotation() - VelocityAsRot );
	
	    if( -8192 <= RotDiff.Yaw && RotDiff.Yaw <= 8192 )
		{
			// moving forward
	        NewMotionDirection = MD_Forward;
	    }
	    else if( 8192 <= RotDiff.Yaw && RotDiff.Yaw <= 24576 )
		{
			// strafing left
	        NewMotionDirection = MD_Left;
	    }
	    else if( -24576 <= RotDiff.Yaw && RotDiff.Yaw <= -8192 )
		{
			// strafing right
	        NewMotionDirection = MD_Right;
		}
		else
		{
			// moving backwards
			NewMotionDirection = MD_Backward;
		}
	}

	// disables changing GroundSpeed etc. in MP for now -- replication issues
	if( WorldInfo.Netmode == NM_Standalone )
	{
		if( MotionDirection != NewMotionDirection )
		{
		    MotionDirection = NewMotionDirection;
			ScaleSpeedForDirection();
		}
	}

	MotionDirection = NewMotionDirection;
}


//-----------------------------------------------------------------------------
// Client request to select given item.
//-----------------------------------------------------------------------------
reliable client function ServerSelectItem( class<Inventory> ItemName )
{
	CeaseUsingAngreal();
	//SelectedItem = FindInventoryName( ItemName );
	SelectedItem = FindInventoryType( ItemName, true );
}

//-----------------------------------------------------------------------------
simulated function ClientSelect( class<Inventory> InvName )
{
	if( CurrentHandSet != None )
	{
		CurrentHandSet.SelectItem( InvName );
	}
	ClientSelectItem();
	ClientSelectItemMessage( CurrentHandSet.GetSelectedHand().GetSelectedClassName());
}

//-----------------------------------------------------------------------------
simulated function ClientSelectItem()
{
	// prior to the PostRender() -- during game startup -- myHUD will be none, but DefaultInventory will cause Select() to cascade here
	if( myHUD != None )
	{
		if( BaseHUD(myHUD) != None )
		{
			BaseHUD(myHUD).SelectItem();
		}

		if( BaseHud(myHUD) == None )
		{
			`warn( Self$".ClientSelectItem() myHUD="$myHUD );
			return;
		}

		if( uiHUD(myHUD).IsWindowActive( class'InventoryInfoWindow' ) )
		{
			// "notify" the InventoryInfoWindow that the current object has been changed
			UpdateInventoryInfo();
		}
	}
}

function UpdateInventoryInfo ()
{
  uiHUD(myHUD).UpdateWindows(Class'InventoryInfoWindow',CurrentHandSet.GetSelectedHand().GetSelectedItem());
}
//===========================================================================
// UI / Health Code
//---------------------------------------------------------------------------
function Texture2d GetHealthIcon()
{
	local int Index;

	if( ApparentIcon != None ) 
	{
		return ApparentIcon;
	}

	// This ensures the value is between 0 and 3, inclusive.
	Index = ArrayCount( HealthIcons ) - 1 - min( max( ArrayCount( HealthIcons ) * Health / default.Health, 0 ), ArrayCount( HealthIcons ) - 1 );
	return HealthIcons[ Index ];
}

//---------------------------------------------------------------------------
function Texture2d GetDisguiseIcon()
{
	if( ApparentIcon != None ) 
	{
		return ApparentIcon;
	}
	return DisguiseIcon;
}


//------------------------------------------------------------------------------
static function class<Object> LoadClassFromName( name ClassName )
{
	local int i;
	local class<Object> LoadedClass;

	//Catch bad input.
	if( ClassName != '' )
	{
		for( i = 0; i < ArrayCount( default.LoadClassFromNamePackages ); i++ )
		{
			LoadedClass = class<Object>( DynamicLoadObject( "elements." $ string(ClassName), class'Class', true ) );
			if( LoadedClass != None )
			{
				break;
			}
		}
	}

	return LoadedClass;
}

//-----------------------------------------------------------------------------
simulated function ClientSelectItemMessage( class<Inventory> ItemName )
{
	local class<Inventory> InvClass;

	if( ItemName != none )
	{
		InvClass = class<Inventory>( class'Singleton'.static.GetInstance(self.WorldInfo, ItemName));
		
		if( ClassIsChildOf( InvClass, class'AngrealInventory' ) ) 
		{
			//ClientMessage( "Selected the "$class<AngrealInventory>(InvClass).default.Title$" Ter'Angreal" );
			HandMessage( SelectedStr $ class<AngrealInventory>(InvClass).default.Title $ TerAngrealStr );
		} 
		else if( ClassIsChildOf( InvClass, class'WOTInventory' ) ) 
		{
			//ClientMessage( "Selected the "$class<AngrealInventory>(InvClass).default.Title$" Ter'Angreal" );
			HandMessage( SelectedStr $ class<WOTInventory>(InvClass).default.Title );
		} 
		else if( ClassIsChildOf( InvClass, class'WOTInventory' ) ) 
		{
			//ClientMessage( "Selected the "$class<AngrealInventory>(InvClass).default.Title$" Ter'Angreal" );
			HandMessage( SelectedStr $ class<WOTInventory>(InvClass).default.Title );
		} 
		else if( InvClass != None ) 
		{
			//ClientMessage( "Selected the "$InvClass );
			HandMessage( SelectedStr $ GetItemName(string(InvClass)) );
		}
		else
		{
			//ClientMessage( "Selected the "$ItemName );
			HandMessage( SelectedStr $ GetItemName(string(ItemName)) );
		}
	}
}

//=============================================================================
// Turn the currently selected angreal off.
//=============================================================================
function CeaseUsingAngreal()
{
	super.StopFire(0);
	
	if( CurrentReflector != None )
	{
		CurrentReflector.CeaseUsingAngreal();
	}
}

//=============================================================================
// Gets rid of all the decals in the WorldInfo.  (Local system only.)
//=============================================================================
function  FlushDecals()
{
	local Decal D;

	foreach AllActors( class'Decal', D )
	{
		D.Destroy();
	}
}

//=============================================================================
function Drop()
{
//    local int i;
    local Inventory Item;
    local vector Loc;
	local BagHolding Bag;
	local float Distance;
	local float CRad, CHei;
    
	// don't allow Citadel Editor inventory to be dropped
	if( CurrentHandSet != AngrealHandSet )
	{
		return;
	}

	// check for SelectedItems that can't be dropped
	if( SelectedItem == None || SelectedItem.Class == DefaultAngrealInventory || SelectedItem.IsA( 'AngrealInvSpecial' ) )
	{
        return;
    }

	Item = SelectedItem;

	// Special handling for angreal.
	if( AngrealInventory(Item) != None )
	{
		// Look for local bags.
		foreach VisibleCollidingActors( class'BagHolding', Bag, 160.0 ) break;

		// Make the bag only if we need it.
		if( Bag == None )
		{
			Bag = Spawn( class'BagHolding' );
		}

		DeleteInventory( Item );

		// Restrict charges.
		if( AngrealInventory(Item) != None && AngrealInventory(Item).CurCharges > AngrealInventory(Item).MaxInitialCharges )
		{
			AngrealInventory(Item).CurCharges = AngrealInventory(Item).MaxInitialCharges;
		}
		Bag.AddItem( Item );

		Item = Bag.Inventory;
	}

	//if( Item.IsA( 'Seal' ) && WOTZoneInfo(Region.Zone) != None && WOTZoneInfo(Region.Zone).bPitZone )
	//{
	//	// if the player has died/dropped seals in a pit zone, move the seals to the top of the pit
	//	Loc = GetPitLocation() + vect(0,0,16);
	//}
	//else
	//{
	//	// finish moving the item from the player to the world
	//	if( Item.IsA( 'Seal' ) )
	//	{
	//		Distance = CollisionRadius;
	//	}
	//	else
	//	{
			Item.GetBoundingCylinder(CRad, CHei);
			Distance = CRad + CRad + 1;
	//	}
			Loc = Location + ( ( Distance * vect(1,0,0) ) >> Rotation );
	//}

	Item.DropFrom( Loc, Item.Velocity );
}

//-----------------------------------------------------------------------------
function DropTainted()
{
	local Inventory Inv;
	
		foreach AllActors(class'Inventory', Inv)
		{
		if( AngrealInventory(Inv) != None && AngrealInventory(Inv).bTainted )
		{
			SelectedItem = Inv;
			Drop();
		}
	}
}

//-----------------------------------------------------------------------------
//function  DropSeals()
//{
//	local Inventory Inv;
	
//	for(Inv = Instigator.InvManager.InventoryChain; Inv != None; Inv = Inv.Inventory)	
//	{
//		if( Inv.IsA( 'Seal' ) )
//		{
//			SelectedItem = Inv;
//			Drop();
//		}
//	}
//}

//=============================================================================
// Do not allow changes to FOV if player is disguised.

//function  SetDesiredFOV(float F)
//{
//	if( LooksLikeAePawn() )
//	{
//		Super.SetDesiredFOV( F );
//	}
//}

//=============================================================================
// END OF NON-DEBUG/CHEAT function S.
//=============================================================================

function  PlayFiring ()
{
  //if ( AnimSequence == LookupAnimSequence('Run') )
  //{
  //  AnimSequence = LookupAnimSequence('AttackRun');
  //} else {
  //  if ( AnimSequence == 'RunL' )
  //  {
  //    AnimSequence = LookupAnimSequence('AttackRunL');
  //  } else {
  //    if ( AnimSequence == 'RunR' )
  //    {
  //      AnimSequence = LookupAnimSequence('AttackRunR');
  //    } else {
  //      if ( AnimSequence == 'Walk' )
  //      {
  //        AnimSequence = LookupAnimSequence('AttackWalk');
  //      } else {
  //        AnimationTableClass.TweenSlotAnim(self,LookupAnimSequence('Attack'));
  //      }
  //    }
  //  }
  //}
}
//=============================================================================

function PlayHit(float Damage, Controller InstigatedBy, vector HitLocation, class<DamageType> damageType, vector Momentum, TraceHitInfo HitInfo)
{
    local float rnd;

    if( damage <= 0 && ReducedDamageType != 'All' )
	{
		return;
	}

    // pain flashes
    rnd = FClamp(Damage, 20, 40);
	//if( InStr( string(DamageType), "NoFlash" ) != -1 ) { /*do nothing*/ }
    /*else if( class'AngrealInventory'.static.DamageTypeContains( damageType, 'Fire' ) )	ClientFlash( -0.009375 * rnd, rnd * vect(16.41, 11.719, 4.6875) );	//yellow
    else if( class'AngrealInventory'.static.DamageTypeContains( damageType, 'Air' ) )	ClientFlash( -0.390, vect(312.5,468.75,468.75));					//light blue
    else if( class'AngrealInventory'.static.DamageTypeContains( damageType, 'Earth' ) )	ClientFlash( -0.01171875 * rnd, rnd * vect(9.375, 14.0625, 4.6875));//green
    else if( class'AngrealInventory'.static.DamageTypeContains( damageType, 'Water' ) )	ClientFlash( -0.390, vect(312.5,468.75,468.75));					//light blue
    else if( class'AngrealInventory'.static.DamageTypeContains( damageType, 'Spirit' ) )ClientFlash( -0.390, vect(312.5,468.75,468.75));					//light blue
    else */																				
    //ClientFlash( -0.019 * rnd, rnd * vect(26.5, 4.5, 4.5));				//red

    //ShakeView( 0.15 + 0.005 * Damage, Damage * 30, 0.3 * Damage );

    //PlayTakeHitSound( Damage, DamageType, 1.0 );

 //   if( Damage > 0.25 * Default.Health || Momentum.Z > 300 )
	//{
 //       PlayTakeHit( 0.2, HitLocation, Damage );
 //   }
}
//function ClientFlash (float Scale, Vector fog)
//{
//  DesiredFlashScale = Scale;
//  DesiredFlashFog = 0.01 * fog;
//}

//=============================================================================
// Test the path node network, and tells you where it's broke.
//=============================================================================

//=============================================================================
// Traverses all possible paths.
//=============================================================================

DefaultProperties
{
	Begin Object Name=MyLightEnvironment
		bSynthesizeSHLight=TRUE
		bIsCharacterLightEnvironment=TRUE
		bUseBooleanEnvironmentShadowing=FALSE
		InvisibleUpdateTime=1
		MinTimeBetweenFullUpdates=.2
	End Object
	Components.Add(MyLightEnvironment)
	LightEnvironment=MyLightEnvironment

	Begin Object Name=WPawnSkeletalMeshComponent
		bCacheAnimSequenceNodes=TRUE
		AlwaysLoadOnClient=true
		AlwaysLoadOnServer=true
		bOwnerNoSee=true
		CastShadow=true
		BlockRigidBody=TRUE
		bUpdateSkelWhenNotRendered=false
		bIgnoreControllersWhenNotRendered=TRUE
		bUpdateKinematicBonesFromAnimation=true
		bCastDynamicShadow=true
		Translation=(Z=8.0)
		RBChannel=RBCC_Untitled3
		RBCollideWithChannels=(Untitled3=true)
		LightEnvironment=MyLightEnvironment
		bOverrideAttachmentOwnerVisibility=false
		bAcceptsDynamicDecals=True
		AnimTreeTemplate=AnimTree'CH_AnimHuman_Tree.AT_CH_Human'
		bHasPhysicsAssetInstance=true
		TickGroup=TG_PreAsyncWork
		MinDistFactorForKinematicUpdate=0.2
		bChartDistanceFactor=true
		RBDominanceGroup=20
		Scale=1.075
		bUseOnePassLightingOnTranslucency=TRUE
		bPerBoneMotionBlur=true
	End Object
	Mesh=WPawnSkeletalMeshComponent
	Components.Add(WPawnSkeletalMeshComponent)

	DefaultMeshScale=1.075
	BaseTranslationOffset=6.0

	Begin Object Name=OverlayMeshComponent0 
		Scale=1.015
		bAcceptsDynamicDecals=TRUE
		CastShadow=true
		bOwnerNoSee=true
		bUpdateSkelWhenNotRendered=true
		bOverrideAttachmentOwnerVisibility=false
		TickGroup=TG_PostAsyncWork
		bPerBoneMotionBlur=true
	End Object
	OverlayMesh=OverlayMeshComponent0

	Begin Object Name=MeshSequenceA
	End Object

	Begin Object Name=MeshSequenceB
	End Object

	Begin Object Name=FirstPersonArms
		PhysicsAsset=None
		FOV=55
		Animations=MeshSequenceA
		DepthPriorityGroup=SDPG_Foreground
		bUpdateSkelWhenNotRendered=false
		bIgnoreControllersWhenNotRendered=true
		bOnlyOwnerSee=True
		bOverrideAttachmentOwnerVisibility=true
		bAcceptsDynamicDecals=FALSE
		AbsoluteTranslation=false
		AbsoluteRotation=true
		AbsoluteScale=true
		HiddenGame=true
		bSyncActorLocationToRootRigidBody=false
		CastShadow=false
		TickGroup=TG_DuringASyncWork
	End Object
	ArmsMesh[0]=FirstPersonArms

	Begin Object Name=FirstPersonArms2
		PhysicsAsset=none
		FOV=55
		Scale3D=(Y=-1.0)
		Animations=MeshSequenceB
		DepthPriorityGroup=SDPG_Foreground
		bUpdateSkelWhenNotRendered=false
		bIgnoreControllersWhenNotRendered=true
		bOnlyOwnerSee=True
		bOverrideAttachmentOwnerVisibility=true
		HiddenGame=true
		bAcceptsDynamicDecals=FALSE
		AbsoluteTranslation=false
		AbsoluteRotation=true
		AbsoluteScale=true
		bSyncActorLocationToRootRigidBody=false
		CastShadow=false
		TickGroup=TG_DuringASyncWork
	End Object
	ArmsMesh[1]=FirstPersonArms2

	Begin Object Name=CollisionCylinder
		CollisionRadius=+0017.000000
		CollisionHeight=+0046.000000
	End Object
	CylinderComponent=CollisionCylinder

	Begin Object name=AmbientSoundComponent
	End Object
	PawnAmbientSound=AmbientSoundComponent
	Components.Add(AmbientSoundComponent)

	Begin Object name=AmbientSoundComponent2
	End Object
	WeaponAmbientSound=AmbientSoundComponent2
	Components.Add(AmbientSoundComponent2)
Health=999
bCachedRelevant=true
bForceNetUpdate=true
bNetDirty=true
bNetInitial=true
bNetInitialRotation=true
bOnlyOwnerSee=false
bWeaponAttachmentVisible=true
bReplicateMovement=true
bReplicateHealthToAll=true
bReplicateInstigator=true
bReplicateRigidBodyLocation=true
bAlwaysRelevant=true
bGameRelevant=true
ViewPitchMin=-19000.00
ViewPitchMax=19000.00
StreakLength=8000.00
//InventoryManagerClass=class'eInventoryManager'
name='Default__.ePawn'
	AirSpeed=400.00

    AccelRate=2048.00

    AirControl=1.00

    BaseEyeHeight=42.00

    UnderWaterTime=30.00

    Physics=1

//    bRotateToDesired=False

    Mass=175.00

    Buoyancy=150.00

    RotationRate=(Pitch=15000,Yaw=0,Roll=0)
	PlayerRestartGameDelay=15.00

    SAOffset=(X=64.00,Y=0.00,Z=0.00)

    DefaultReflectorClasses(0)=Class'elements.DefaultProcessEffectReflector'

    DefaultReflectorClasses(1)=Class'elements.DefaultTargetingReflector'

    DefaultReflectorClasses(2)=Class'elements.DefaultHealthReflector'

    DefaultReflectorClasses(3)=Class'elements.DefaultCastReflector'

    DefaultReflectorClasses(4)=Class'elements.DefaultTakeDamageReflector'

    DefaultReflectorClasses(5)=Class'elements.DamageTakeDamageReflector'

    DefaultReflectorClasses(6)=Class'elements.SetupAssetClassesReflector'

    DefaultReflectorClasses(7)=Class'elements.WOTFootZoneChangeReflector'

	ArtifactNames(0)="elements.AngrealInvLightGlobe"

    ArtifactNames(1)="elements.AngrealInvDart"

    ArtifactNames(2)="elements.AngrealInvFireball"

    ArtifactNames(3)="Angreal.AngrealInvSeeker"

    ArtifactNames(4)="Angreal.AngrealInvTarget"

    ArtifactNames(5)="elements.AngrealInvHeal"

    ArtifactNames(6)="elements.AngrealInvFork"

    ArtifactNames(7)="elements.AngrealInvReflect"

    ArtifactNames(8)="elements.AngrealInvAbsorb"

    ArtifactNames(9)="elements.AngrealInvDecay"

    ArtifactNames(11)="elements.AngrealInvMinion"

    ArtifactNames(12)="elements.AngrealInvGuardian"

    ArtifactNames(13)="elements.AngrealInvChampion"

    ArtifactNames(14)="elements.AngrealInvEarthTremor"

    ArtifactNames(15)="elements.AngrealInvAirShield"

    ArtifactNames(16)="elements.AngrealInvEarthShield"

    ArtifactNames(17)="elements.AngrealInvFireShield"

    ArtifactNames(18)="elements.AngrealInvSpiritShield"

    ArtifactNames(19)="elements.AngrealInvWaterShield"

    ArtifactNames(20)="elements.AngrealInvShield"

    ArtifactNames(21)="elements.AngrealInvShift"

    ArtifactNames(22)="elements.AngrealInvTaint"

    ArtifactNames(23)="elements.AngrealInvSoulBarb"

    ArtifactNames(24)="elements.AngrealInvAMA"

    ArtifactNames(25)="elements.AngrealInvRemoveCurse"

    ArtifactNames(26)="elements.AngrealInvLightning"

    ArtifactNames(27)="elements.AngrealInvBalefire"

    ArtifactNames(28)="elements.AngrealInvWhirlwind"

    ArtifactNames(30)="elements.AngrealInvExpWard"

    ArtifactNames(31)="elements.AngrealInvTracer"

    ArtifactNames(32)="elements.AngrealInvWallOfAir"

    ArtifactNames(33)="elements.AngrealInvDistantEye"

    ArtifactNames(34)="elements.AngrealInvLevitate"

    ArtifactNames(35)="elements.AngrealInvSwapPlaces"

    ArtifactNames(36)="elements.AngrealInvTrapDetect"

    ArtifactNames(37)="elements.AngrealInvIllusion"

    ArtifactNames(38)="elements.AngrealInvDisguise"

    ArtifactNames(39)="elements.AngrealInvSpecial"

    ArtifactNames(40)="elements.AngrealInvIce"

    ArtifactNames(41)="elements.AngrealInvAirBurst"

    SubTitlesPackageName=WOTSubtitles

    SubTitleLanguages(0)="est"

    SubTitleLanguages(1)="itt"

    MessageDurationSecsPerChar=0.07

    MinMessageDuration=3.00

    InventoryInfoHintDuration=5.00

    AngrealHandOrder(0)=AngrealInvBalefire

    AngrealHandOrder(1)=AngrealInvFireball

    AngrealHandOrder(2)=AngrealInvFireworks

    AngrealHandOrder(3)=AngrealInvEarthTremor

    AngrealHandOrder(4)=AngrealInvDart

    AngrealHandOrder(5)=AngrealInvAirBurst

    AngrealHandOrder(10)=AngrealInvSeeker

    AngrealHandOrder(11)=AngrealInvLightning

    AngrealHandOrder(12)=AngrealInvSoulBarb

    AngrealHandOrder(13)=AngrealInvDecay

    AngrealHandOrder(14)=AngrealInvTaint

    AngrealHandOrder(20)=AngrealInvShield

    AngrealHandOrder(21)=AngrealInvFireShield

    AngrealHandOrder(22)=AngrealInvAirShield

    AngrealHandOrder(23)=AngrealInvEarthShield

    AngrealHandOrder(24)=AngrealInvWaterShield

    AngrealHandOrder(25)=AngrealInvSpiritShield

    AngrealHandOrder(30)=AngrealInvAbsorb

    AngrealHandOrder(31)=AngrealInvReflect

    AngrealHandOrder(32)=AngrealInvSwapPlaces

    AngrealHandOrder(33)=AngrealInvShift

    AngrealHandOrder(34)=AngrealInvFork

    AngrealHandOrder(40)=AngrealInvAMA

    AngrealHandOrder(41)=AngrealInvRemoveCurse

    AngrealHandOrder(42)=AngrealInvWallOfAir

    AngrealHandOrder(50)=AngrealInvIce

    AngrealHandOrder(51)=AngrealInvExpWard

    AngrealHandOrder(52)=AngrealInvTarget

    AngrealHandOrder(53)=AngrealInvWhirlwind

    AngrealHandOrder(60)=AngrealInvChampion

    AngrealHandOrder(61)=AngrealInvGuardian

    AngrealHandOrder(62)=AngrealInvMinion

    AngrealHandOrder(63)=AngrealInvIllusion

    AngrealHandOrder(70)=AngrealInvHeal

    AngrealHandOrder(71)=AngrealInvLevitate

    AngrealHandOrder(72)=AngrealInvDisguise

    AngrealHandOrder(80)=AngrealInvTracer

    AngrealHandOrder(81)=AngrealInvTrapDetect

    AngrealHandOrder(82)=AngrealInvDistantEye

    AngrealHandOrder(83)=AngrealInvLightGlobe

    AngrealHandOrder(90)=Seal

    AngrealHandOrder(91)=AngrealInvSpecial
	
	//VolumeMultiplier=1.00

 //   RadiusMultiplier=1.00

 //   PitchMultiplier=1.00

 //   TimeBetweenHitSoundsMin=1.00

 //   TimeBetweenHitSoundsMax=3.00

    HitHardHealthRatio=0.25

    ExceptionalDeathHealthRatio=0.25

    WalkBackwardSpeedMultiplier=0.70

    SpeedPlayAnimAfterLanding=1100.00

    PlayLandHardMinVelocity=1200.00

    PlayLandSoftMinVelocity=600.00

    LadderLandingNoiseVelocity=200.00

    LadderLandedHitNormalZ=0.00

    GibForSureFinalHealth=-40.00

    GibSometimesFinalHealth=-30.00

    GibSometimesOdds=0.50

    BaseGibDamage=40.00
	WalkBob=(0.00,0.00,0.00)
}
