class ePlayerInput extends UDKPlayerInput
	DLLBind(DI3dCnnxn);

//these are not necessary, i added them for my project
var input float aPitch;
var input float aYaw;
var input float aRoll;

var	float	PitchRotationSpeed;
var	float	YawRotationSpeed;
var	float	RollRotationSpeed;

//-----------------3D Mouse definitions--------------------
var int SelectedDevice;
var bool b3DMouseConnected;

//---------------------------------------------------------
//------------------DLLBind definitions--------------------
struct SPACEPILOTSTATE {
    var float       TX;                     /* x-axis position              */
    var float       TY;                     /* y-axis position              */
    var float       TZ;                     /* z-axis position              */
    var float       RX;                     /* x-axis rotation              */
    var float       RY;                     /* y-axis rotation              */
    var float	      RZ;                     /* z-axis rotation              */
    var int         rglSlider[2];           /* unnecessary, does nothing */
    var int         rgdwPOV[4];          /* unnecessary,	does nothing  */
    var byte        rgbButtons[32];    /* 32 buttons  not supported yet */
};

const R_OK = 0;
const R_FAILED = -1;

final function int Init3dCnnxn();
final function int Exit3dCnnxn();
final function int GetData(out SPACEPILOTSTATE state);
final function int GetSelectedDevice();
final function int SetDeadzone(float translationDZ, float rotationDZ);
final function int GetNumberOfDevices();
final function int GetTestNumber();

//----------------DLLBind definitions end------------------
//---------------------------------------------------------


function InitInputSystem()
{
	super.InitInputSystem();
	b3DMouseConnected = false;	
	if(Init3dCnnxn() == R_OK){
		b3DMouseConnected = true;	
	}
}

function CloseSystem()
{
	
}

function Destroy()
{
	CloseSystem();
}

event PlayerInput( float DeltaTime )
{
	local float TimeScale, DeltaT;
	local SPACEPILOTSTATE sps;
	local int num;
	
	DeltaT = DeltaTime;

	DeltaTime /= WorldInfo.TimeDilation;
	if (Outer.bDemoOwner && WorldInfo.NetMode == NM_Client)
	{
		DeltaTime /= WorldInfo.DemoPlayTimeDilation;
	}
	
	num = GetData(sps);
        //Add to axis you want. I added 3 for rotation.
	if(num == R_OK)
	{
		aBaseY += sps.TX/3200;
		aStrafe += sps.TY/3200;
		aUp += sps.TZ/3200;
		aRoll += sps.RX/5000;
		aPitch += sps.RY/5000;
		aYaw += sps.RZ/5000;
	}
	//GetALocalPlayerController().ClientMessage("TX"@sps.TX@"TY"@sps.TY@"TZ"@sps.TZ@"RX"@sps.RX@"RY"@sps.RY@"RZ"@sps.RZ);
	// Scale to game speed
	TimeScale = 100.f*DeltaTime;
	aPitch		*= TimeScale * PitchRotationSpeed;
	aYaw		*= TimeScale * YawRotationSpeed;
	aRoll		*= TimeScale * RollRotationSpeed;

	Super.PlayerInput(DeltaT);
}

DefaultProperties
{
	PitchRotationSpeed = 1200
	YawRotationSpeed = 1200
	RollRotationSpeed = 1200
	
        SelectedDevice = 0
}