class ePlayerReplicationInfo extends UTPlayerReplicationInfo;

var string				DisguiseName;   // Occupied if the player is disguised.
var string				TeamName;		// Team name, or blank if none.
var int					TeamID;			// Player position in team.
var	bool				bIsABot;
var bool				bFeigningDeath;
var ZoneInfo			PlayerZone;
var name				SuicideType;

var int					PacketLoss;
var int					Suicides;
var float				TimeOnServer;
var byte				PlayerType;	// 0=AesSedai, 1=Forsaken, 2=Hound, 3=Whitecloak
var bool				bIsNPC;

replication
{
	// Things the server should send to the client.
	if ( Role == ROLE_Authority )
		DisguiseName, TeamName, TeamID,
		bIsABot, bFeigningDeath, PlayerZone, SuicideType;

	if( Role==ROLE_Authority )
		PacketLoss, Suicides, PlayerType, bIsNPC;

	 if( Role==ROLE_Authority && bNetInitial )
		TimeOnServer;
}

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	//Timer();
	//SetTimer(2.0, true);
	//bIsFemale = ePawn(Owner).bIsFemale;
}
 					
//function Timer()
//{
//	local float MinDist, Dist;
//	local LocationID L;

//	MinDist = 1000000;
//	PlayerLocation = None;
//	if ( PlayerZone != None )
//		for ( L=PlayerZone.LocationID; L!=None; L=L.NextLocation )
//		{
//			Dist = VSize(Owner.Location - L.Location);
//			if ( (Dist < L.Radius) && (Dist < MinDist) )
//			{
//				PlayerLocation = L;
//				MinDist = Dist;
//			}
//		}

	//if( PlayerPawn(Owner) != None )
	//{
	//	Ping = int(PlayerPawn(Owner).ConsoleCommand("GETPING"));
	//	PacketLoss = int(PlayerPawn(Owner).ConsoleCommand("GETLOSS"));
	//}
//}

//#if 1 //NEW
simulated function Tick( float DeltaTime )
{
	// No need to call -- Super.Tick( DeltaTime );
	TimeOnServer += DeltaTime;
}
//#endif
defaultproperties
{
    //Team=255

    RemoteRole=1

    NetUpdateFrequency=4.00

}
