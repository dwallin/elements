//=============================================================================
// Projectile.
//
// A delayed-hit projectile moves around for some time after it is created.
// An instant-hit projectile acts immediately. 
//=============================================================================
class eProjectile extends UTProjectile;

//#exec Texture Import File=Textures\S_Camera.pcx Name=S_Camera Mips=Off Flags=2

//-----------------------------------------------------------------------------
// Projectile variables.
var       const vector    OldLocation;   // Actor's old location one tick ago.
// Motion information.
var() float    Speed;               // Initial speed of projectile.
var() float    MaxSpeed;            // Limit on speed of projectile (0 means no limit)

// Damage attributes.
var() float    Damage;         
var() int	   MomentumTransfer; // Momentum imparted by impacting projectile.
var() name	   MyDamageType;

// Projectile sound effects
var() soundCue    SpawnSound;		// Sound made when projectile is spawned.
var() soundCue	   ImpactSound;		// Sound made when projectile hits something.
var() soundCue    MiscSound;		// Miscellaneous Sound.

var() float		ExploWallOut;	// distance to move explosions out from wall

//#if 1 //NEW
var(SeePlayer) bool bNotifySeePlayer;		// Set to true if you want SeePlayer notifications.
var(SeePlayer) float SeePlayerRadius;
var(SeePlayer) float SeePlayerHeight;
//#endif

//#if 1 //NEW
var const Projectile nextProjectile;

replication
{
	if( Role==ROLE_Authority && bNetInitial )
		Speed, MaxSpeed;

	if( Role==ROLE_Authority )
		bNotifySeePlayer,
		SeePlayerRadius,
		SeePlayerHeight;
}

//==============
// Initialization
simulated function PreBeginPlay()
{
	Super.PreBeginPlay();
}

simulated function Destroyed()
{
	Super.Destroyed();
}
//#endif

//==============
// Encroachment
//function bool EncroachingOn( actor Other )
//{
//	if ( (Other.Brush != None) || (Brush(Other) != None) )
//		return true;
		
//	return false;
//}

//simulated event HitWall(vector HitNormal, actor Wall, PrimitiveComponent WallComp)
//{
//	if ( Role == ROLE_Authority )
//	{
//		if ( (Wall != none) /*&& Mover(Wall).bDamageTriggered*/ )
//			Wall.TakeDamage( Damage, instigator.Controller, Location, MomentumTransfer * Normal(Velocity), class'none');

//		MakeNoise(1.0);
//	}
//	Explode(Location + ExploWallOut * HitNormal, HitNormal);
//}

simulated function Explode(vector HitLocation, vector HitNormal)
{
	Destroy();
}

simulated event Touch( Actor Other, PrimitiveComponent OtherComp, vector HitLocation, vector HitNormal )
{
	local actor HitActor;
	local float ColRad, ColHei;

	if ( Other.IsA('BlockAll') )
	{
		HitWall( Normal(Location - Other.Location), Other, OtherComp);
		return;
	}
	if ( Other.bProjTarget || (Other.bBlockActors /*&& Other.bBlockPlayers*/) )
	{
		//get exact hitlocation
	 	HitActor = Trace(HitLocation, HitNormal, Location, OldLocation, true);
		if (HitActor == Other)
		{
			if ( Other.IsA('Pawn')
				&& !ePawn(Other).AdjustHitLocation(HitLocation, Velocity) )
					return;
			ProcessTouch(Other, HitLocation, HitNormal); 
		}
		else 
		{
			Other.GetBoundingCylinder(ColRad, ColHei);
			ProcessTouch(Other, Other.Location + ColRad * Normal(Location - Other.Location), HitNormal);
		}
	}
}

simulated function ProcessTouch(Actor Other, Vector HitLocation, Vector HitNormal)
{
	//should be implemented in subclass
}

defaultproperties
{
    MaxSpeed=2000.00

    bNetTemporary=True

    bReplicateInstigator=True

    Physics=6

    LifeSpan=140.00

    bDirectional=True

    DrawType=2

    bGameRelevant=True

    SoundVolume=0

    CollisionRadius=0.00

    CollisionHeight=0.00

    bCollideActors=True

    bCollideWorld=True

    NetPriority=2.50

}