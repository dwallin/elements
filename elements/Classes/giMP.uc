class giMP extends giCombatBase;

function GivePawnAngreal( Pawn P, String AngrealName, optional int Charges )
{
	local class<AngrealInventory> AngrealType;
	local AngrealInventory A;

	if( P.IsPlayerOwned() )
	{
		AngrealType = class<AngrealInventory>(DynamicLoadObject( AngrealName, class'Class' ));
		if( AngrealType != None && P.FindInventoryType( AngrealType, true ) == None )
		{
			A = Spawn( AngrealType );
			if( A != None )
			{
				if( Charges == 0 )
				{
					A.CurCharges = RandRange( A.MinInitialCharges, A.MaxInitialCharges );
				}
				else
				{
					A.CurCharges = Charges;
				}
				if( WorldInfo.Game.PickupQuery( P, AngrealType, A ) )
				{
					A.GiveTo( P );
				}
				else
				{
					A.Destroy();
				}
			}
		}
	}
}

defaultproperties
{
    bPauseable=False

}