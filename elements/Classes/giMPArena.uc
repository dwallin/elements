class giMPArena extends giMP;

function AddDefaultInventory( pawn PlayerPawn )
{

	// Start the player off with 25 charges of dart along with their normal default weapon.
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvDart", 99);
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvFireball", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvFork", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvHeal", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvBalefire", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvShift", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvReflect", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvDisguise", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvIllusion", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvLevitate", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvWhirlwind", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvTarget", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvWallOfAir", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvShield", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvAMA", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvLightning", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvDecay", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvAbsorb", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvAirBurst", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvAirShield", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvDistantEye", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvAirBurst", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvEarthShield", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvFireShield", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvSpiritShield", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvWaterShield", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvEarthTremor", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvExpWard", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvFireworks", 999 );
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvIce",999);
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvRemoveCurse",999);
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvSwapPlaces",999);
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvSoulBarb",999);
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvTaint",999);
	GivePawnAngreal( PlayerPawn, "elements.AngrealInvSeeker",999);
}

defaultproperties
{
FragLimit=20
//bChangeLevels=True
//Map="Arena_02.wot"
//ScoreBoardType=Class'ArenaScoreBoard'
//MapPrefix="Arena_"
bGivePhysicsGun=false
Acronym="Arena"
bUseClassicHUD=true
DefaultPawnClass=class'ePawn'									 
bDelayedStart=false
PlayerControllerClass=class'ePlayerController'
//DefaultInventory(0)=class'UTGame.UTWeap_LinkGun'
//DefaultInventory(1)=class'UTGameContent.UTWeap_RocketLauncher_Content'
//DefaultInventory(2)=class'UTGameContent.UTWeap_ShockRifle'
MaxPlayersAllowed=64
bAutoNumBots=true
HUDType=class'MainHUD'
name='Default__Arena'
TickGroup=TG_PreAsyncWork
GameSpeed=1.1
bWaitingToStartMatch=false
bRestartLevel=false
bPauseable=True

}
