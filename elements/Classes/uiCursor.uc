//=============================================================================
// uiCursor.uc
// $Author: Mfox $
// $Date: 1/05/00 2:38p $
// $Revision: 2 $
//=============================================================================
class uiCursor extends uiObject
	abstract;

var() texture2d CursorIcon;
var() int HotSpotX;
var() int HotSpotY;
var() int SizeX;
var() int SizeY;
var Canvas Canvas;
simulated event PreBeginPlay()
{
	Super.PreBeginPlay();
	assert( uiMouse(Owner) != None );
	assert( CursorIcon != None );
}

simulated function Draw( )
{
//	Canvas.Style = Style;
	Canvas.SetPos( uiMouse(Owner).CurrentX - HotSpotX, uiMouse(Owner).CurrentY - HotSpotY );
	Canvas.DrawTile( CursorIcon,
		CursorIcon.SizeX, 
		CursorIcon.SizeY, 
		0,0, 
		CursorIcon.SizeX,
		CursorIcon.SizeY		
	);
}
defaultproperties
{
    Style=2

}
//end of uiCursor.uc
