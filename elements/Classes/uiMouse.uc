//================================================================================
// uiMouse.
//================================================================================

class uiMouse extends uiComponent;

var uiCursor CurrentCursor;
var bool HasDrawn;
var int DeltaX;
var int DeltaY;
var Canvas Canvas;
simulated function Destroyed ()
{
  if ( CurrentCursor != None )
  {
    CurrentCursor.Destroy();
  }
  CurrentCursor = None;
  Super.Destroyed();
}

simulated function SetCursor (Class<uiCursor> CursorClass)
{
  if ( CurrentCursor != None )
  {
    CurrentCursor.Destroy();
  }
  CurrentCursor = Spawn(CursorClass,self);
}

simulated function Draw (Canvas C)
{
  assert (uiHUD(Owner) != None);
  if (  !HasDrawn )
  {
    CurrentX = C.SizeX / 2;
    CurrentY = C.SizeY / 2;
    HasDrawn = True;
  }
  if ( CurrentCursor != None )
  {
    CurrentCursor.Draw();
  }
}

simulated function Reset ()
{
  assert (uiHUD(Owner) != None);
  CurrentX = uiHUD(Owner).SizeX / 2;
  CurrentY = uiHUD(Owner).SizeY / 2;
}

//function MoveX (float Delta)
//{
//  assert (uiHUD(Owner) != None);
//  DeltaX = None;
//  bool(vect(0.00,0.00,0.00))
//  )
//}

//function MoveY (float Delta)
//{
//  assert (uiHUD(Owner) != None);
//  DeltaY = None;
//  bool(vect(0.00,0.00,0.00))
//  )
//}

simulated function RestrainToX ()
{
  assert (uiHUD(Owner) != None);
  CurrentX = Min(Max(0,CurrentX),uiHUD(Owner).SizeX);
}

simulated function RestrainToY ()
{
  assert (uiHUD(Owner) != None);
  CurrentY = Min(Max(0,CurrentY),uiHUD(Owner).SizeY);
}

simulated function LeftMouseDown ()
{
  uiHUD(Owner).LeftMouseDown();
}

simulated function LeftMouseUp ()
{
  uiHUD(Owner).LeftMouseUp();
}

simulated function RightMouseDown ()
{
  uiHUD(Owner).RightMouseDown();
}

simulated function RightMouseUp ()
{
  uiHUD(Owner).RightMouseUp();
}
